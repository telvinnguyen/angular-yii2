<?php
namespace phpjwt\Exceptions;

use yii\db\Exception;

class UnexpectedValueException extends Exception {
    public function __construct($message = "", $code = 0, Exception $previous = null) { }
}