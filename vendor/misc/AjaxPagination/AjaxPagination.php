<?php
namespace misc\AjaxPagination;
use Yii;
use yii\data\Pagination;
use yii\web\Request;

class AjaxPagination extends Pagination{

    private $_page;
    private $_pageSize;

    protected function getPostParam($name, $defaultValue = null)
    {
        $request = Yii::$app->getRequest();
        /*
        if (($params = $this->params) === null) {
            $params = $request instanceof Request ? $request->post($this->pageParam) : [];
        }
        return isset($params[$name]) && is_scalar($params[$name]) ? $params[$name] : $defaultValue;
        */
        return $request->post($this->pageParam);

    }

    public function getPage($recalculate = false)
    {
        if ($this->_page === null || $recalculate) {
            $page = (int) $this->getPostParam($this->pageParam, 1) - 1;
            $this->setPage($page, true);
        }

        return $this->_page;
    }

    public function setPage($value, $validatePage = false)
    {
        if ($value === null) {
            $this->_page = null;
        } else {
            $value = (int) $value;
            if ($validatePage && $this->validatePage) {
                $pageCount = $this->getPageCount();
                if ($value >= $pageCount) {
                    $value = $pageCount - 1;
                }
            }
            if ($value < 0) {
                $value = 0;
            }
            $this->_page = $value;
        }
    }

    public function getOffset()
    {
        $pageSize = $this->getPageSize();

        return $pageSize < 1 ? 0 : $this->getPage() * $pageSize;
    }
}