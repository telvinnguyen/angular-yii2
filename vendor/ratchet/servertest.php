<?php
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

// Make sure composer dependencies have been installed

$yii=__DIR__.'/../../../framework/yii.php';
$config=__DIR__.'/../../../protected/config/main.php';


require_once($yii);

$app = Yii::createWebApplication($config);

require __DIR__ . '/vendor/autoload.php';


/**
 * chat.php
 * Send any incoming messages to all connected clients (except sender)
 */
class MyChat implements MessageComponentInterface {
    protected $clients;

    public function __construct() {
        //$this->clients = new \SplObjectStorage;
        $this->clients = array();
    }

    public function onOpen(ConnectionInterface $conn) {

        //var_dump($conn->WebSocket->request->getQuery()->get('data'));

        // Store the new connection to send messages to later

        $query_mid = $conn->WebSocket->request->getQuery()->get('mid');
        //var_dump("New connection has been created! ({$conn->resourceId})\n");

        $conn->mid = $query_mid;

        //$this->clients->attach($conn);
        $this->clients[$conn->resourceId] = $conn;

        echo "New connection has been created! ({$conn->resourceId})\n";

    }

    public function onMessage(ConnectionInterface $from, $msg) {
        $numRecv = count($this->clients) - 1;
        echo sprintf('Connection %d sending message "%s" to %d other connection%s' . "\n"
            , $from->resourceId, $msg, $numRecv, $numRecv == 1 ? '' : 's');

        foreach ($this->clients as $client) {
            if ($from != $client) {
                //$client->send($msg);
            }
        }
//        $client->send("Message successfully sent to $numRecv users.");


//
//        $types = SubscriptionType::model()->findAll();
//        Commons::varDump(count($types));

        // Send a message to a known resourceId (in this example the sender)
        //$client = $this->clients[$from->resourceId];


//        $decode_msg = json_decode($msg);
//        var_dump($decode_msg->page_id);
//        var_dump($decode_msg->msg);
//        $from->send($msg);
    }

    public function onClose(ConnectionInterface $conn) {
        //$this->clients->detach($conn);
        unset($this->clients[$conn->resourceId]);

        echo "Connection {$conn->resourceId} has disconnected\n";

    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";
        $conn->close();
    }
}

// Run the server application through the WebSocket protocol on port 8082
$rApp = new Ratchet\App('localhost', 8082);
$rApp->route('/chat', new MyChat);
$rApp->route('/echo', new Ratchet\Server\EchoServer, array('*'));
$rApp->run();