<?php
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

use Ratchet\WebSocket\WsServer;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;

// Make sure composer dependencies have been installed

$yii = __DIR__ . '/../../../framework/yii.php';
$config = __DIR__ . '/../../../protected/config/main.php';


require_once($yii);

$app = Yii::createWebApplication($config);

require __DIR__ . '/vendor/autoload.php';


/**
 * chat.php
 * Send any incoming messages to all connected clients (except sender)
 */
class SignServer1 implements MessageComponentInterface
{
    protected $clients;
    protected $online_signs;

    public function __construct()
    {
        //$this->clients = new \SplObjectStorage;
        $this->clients = array();
        $this->online_signs = array();
    }

    public function onOpen(ConnectionInterface $conn)
    {

        // Store the new connection to send messages to later

        $conn->peer_id = $conn->WebSocket->request->getQuery()->get('peer_id');
        $conn->skin_id = $conn->WebSocket->request->getQuery()->get('skin_id');
        $conn->sign_id = $conn->WebSocket->request->getQuery()->get('sign_id');

        //Commons::varDump("New connection has been created! ({$conn->resourceId})\n");

        //$this->clients->attach($conn);
        $this->clients[$conn->resourceId] = (object)array(
            'peer_id' => $conn->peer_id,
            'conn' => $conn
        );


        $this->online_signs[$conn->peer_id] = $conn->sign_id;
        //Commons::varDump("New connection has been created! ({$conn->resourceId})\n");

    }

    public function onMessage(ConnectionInterface $from, $msg)
    {
        $numRecv = count($this->clients) - 1;
        //echo sprintf('Connection %d sending message "%s" to %d other connection%s' . "\n" , $from->resourceId, $msg, $numRecv, $numRecv == 1 ? '' : 's');


        $decode_msg = json_decode($msg);
        $to_boxes = $decode_msg->to_boxes;
        $htmlContent = $decode_msg->html;

        $from_box_id = $decode_msg->from_box;
        $from_sign_id = $decode_msg->sign_id;

        $target_sign = array();
        $packages = array();

        foreach ($to_boxes as $to_box) {
            $signBoxKey = $to_box->to_sign_id;
            if ($to_box != null) {
                // array( peer_id => array( array("target_box_id"=>$to_box_id, "message"=> $htmlContent) ))
                // this will help the server from overload when it directly access to target box instead of searching all of concurrent connections

                $target_sign[] = $to_box->to_sign_id;
                $packages[$to_box->to_sign_id][] = (object)array("target_box_id" => $to_box->to_box_id, "message" => $htmlContent);
            }
        }


        foreach ($this->clients as $client) {
            if ($from != $client && in_array($this->online_signs[$client->peer_id], $target_sign)) {
                //Commons::varDump('Sent to' . $client->conn->peer_id);
                $returnMsg = Commons::convertAnythingToArray($packages[$this->online_signs[$client->peer_id]]); // convert anything to array
                $client->conn->send(json_encode($returnMsg));

                /* send data for each box
                foreach($packages[$client->peer_id] as $box){
                    $returnMsg = Commons::convertAnythingToArray($box); // convert anything to array
                    $client->conn->send(json_encode($returnMsg));
                }
                */
            }

        }

        //$from->send("Message successfully sent to $numRecv users.");

    }

    public function onClose(ConnectionInterface $conn)
    {
        //$this->clients->detach($conn);
        unset($this->clients[$conn->resourceId]);

        echo "Connection {$conn->resourceId} has disconnected\n";

    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "An error has occurred: {$e->getMessage()}\n";
        $conn->close();
    }
}
/*
//Run the server application through the WebSocket protocol on port 8082

//$rApp = new Ratchet\App('zwenglobal.com', 8082);
//$rApp->route('/chat', new KioskServer);
//$rApp->run();
*/

$server = IoServer::factory(
    new HttpServer(
        new WsServer(
            new SignServer()
        )
    ),
    8083
);
$server->run();