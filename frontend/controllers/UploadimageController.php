<?php

// exp: http://localhost:8081/signsmart_yii2_angularjs/frontend/web/collections/2
namespace frontend\controllers;
use yii\rest\ActiveController;

use Yii;

class UploadimageController extends ActiveController
{
    public $modelClass = '';
    public function actionUpload()
    {
        if (!empty($_FILES)) {
            $tempPath = $_FILES['file']['tmp_name'];
            $uploadPath = dirname(__FILE__) . DIRECTORY_SEPARATOR . '../web/uploads/temp' . DIRECTORY_SEPARATOR . $_FILES['file']['name'];

            move_uploaded_file($tempPath, $uploadPath);

            $answer = array('answer' => 'File transfer completed');
            $json = json_encode($uploadPath);

            echo $json;

        } else {

            echo 'No files';

        }


    }
//

}
