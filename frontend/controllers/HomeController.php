<?php

namespace frontend\controllers;

use common\component\helpers\CommonHelper;
use common\component\helpers\FileHelper;
use common\models\Member;

use common\models\MemberLoginForm;
use yii\console\Controller;
use yii\rest\ActiveController;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Response;

use yii\filters\auth\QueryParamAuth;

class HomeController extends ActiveController
{
    public $modelClass = '';

    /*
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
    }
    */

    public function actionResetpassword(){

        $result = [
            'OK' => 0
        ];
        $request = Yii::$app->request;
        $email = $request->post('email');

        $member = Member::findOne([
            'disable' => 0,
            'email' => $email,
        ]);

        if($member != null){
            $newPassword = Yii::$app->security->generateRandomString();
            $member->setPassword($newPassword); //return the hashed password
            $done = \Yii::$app->mailer->compose('forgotPasswordTemplate', ['member' => $member, 'newPassword' => $newPassword])
                ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' robot'])
                ->setTo($member->email)
                ->setSubject('Forgot password from ' . \Yii::$app->name)
                ->send();

            $member->save(false);

            if($done){
                $result = [
                    'OK' => 1,
                    'msg' => Yii::t('frontend', 'reset.password.done')
                ];
            }else{

                $result['msg'] = Yii::t('frontend.errors', 'reset.password.email.not.sent');
            }

        }else{

            $result['msg'] = Yii::t('frontend.errors', 'reset.password.email.not.found');
        }

        return $result;
    }

    public function actionLogin(){
        $result = [
            'OK' => 0
        ];

        $request = Yii::$app->request;
        //var_dump($_COOKIE['ss-identity']);exit;
        //var_dump(Member::getLoginId());exit;

        $memberForm = new MemberLoginForm();
        $memberForm->login_name = $request->post('login_name');
        $memberForm->password = $request->post('password');
        $memberForm->rememberMe = filter_var($request->post('rememberMe'), FILTER_VALIDATE_BOOLEAN);
        $getMember = $memberForm->getMember();
        $isBeta = $getMember->beta;
        if($getMember && $getMember->validatePassword($memberForm->password, $getMember->password)){
            $session = Yii::$app->session;
            if($session['verifyCode'] != null && $session['verifyCode'] == true ){
                $isBeta = $getMember->beta = '0';
                $getMember->save(false);
                unset($session['verifyCode']);
            }

            $token = Member::createLoginToken($getMember->member_id);
            //if($memberForm->rememberMe)
            setcookie('ss-identity', $token, time()+24*3600*30*365, '/'); // 1 year
//            else{
//                //remote cookies
//                if (isset($_COOKIE['ss-identity'])) {
//                    unset($_COOKIE['ss-identity']);
//                    setcookie('ss-identity', '', time() - 3600, '/'); // empty value and old timestamp
//                }
//            }

            $result = [
                'OK' => 1,
                'id_token' => $token,
                'member_id' => $getMember->member_id,
                'isBeta' => $isBeta
            ];
        }else{
            $result['errors'] = Yii::t('frontend.errors', 'incorrect.login');
        }

        return $result;

    }

    public function actionLogout(){
        if (isset($_COOKIE['ss-identity'])) {
            unset($_COOKIE['ss-identity']);
            setcookie('ss-identity', '', time() - 3600, '/'); // empty value and old timestamp
        }

        return ['OK' => 1];
    }

    public function actionGetuser(){
        $request = Yii::$app->request;
        $id_token = $request->post('id_token');
        $member_id = $request->post('member_id');
        if($id_token != $_COOKIE['ss-identity']){
            return [ 'OK' => 0, 'errors' => 'The authentication does not match'];
        }

        $member = Member::findOne($member_id);
        if($member == null){
            return [ 'OK' => 0, 'errors' => 'Member information is not found'];
        }

        return [
            'OK' => 1,
            'name' => $member->getFullName(),
            'avatarUrl' => FileHelper::getMemberAvatar($member->member_id, 63, 63, 'img_user_account_picture.png'),
            'member_type' => $member->member_type
        ];
    }

    public function actionAuthenticate(){
        $result = [
            'OK' => 0
        ];

        $request = Yii::$app->request;
        $member_id = Member::getLoginId();

        if($member_id == null || $member_id == '') return [ 'OK' => 0];

        $member = Member::findOne($member_id);
        if($member == null) return [ 'OK' => 0];

        if(Member::validateLoginToken('ss-identity')){
            $result = [
                'member_id' => $member_id,
                'username' => $member->login_name,
                'serverCookie' => $_COOKIE['ss-identity'],
                'OK' => 1
            ];
        }

        return $result;

    }

}
