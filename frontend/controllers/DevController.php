<?php

namespace frontend\controllers;

use common\component\helpers\CommonHelper;
use common\component\helpers\FileHelper;
use common\component\helpers\MembershipHelper;
use common\models\Member;
use common\models\AccountUser;
use yii\rest\ActiveController;

use Yii;

class DevController extends ActiveController
{
    public $modelClass = '';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'index'  => ['get'],
                    '*'   => ['post'],
                ],
            ],
        ];
    }

    public function actionGetdata()
    {
        $request = Yii::$app->request;
        $acc_id = $request->post('acc_id');
        $_member_id = $request->post('member_id');
        $kiosk_id = $request->post('kiosk_id');
        $sign_id = $request->post('sign_id');

        $member_id = Member::getLoginId();
        $_member = Member::findOne($member_id);
        $member = [
            'id' => $member_id,
            'upload_folder' => $_member->upload_folder
        ];

        $_acc = AccountUser::findOne($acc_id);
        $acc = [
            'id' => $acc_id,
            'upload_folder' => $_member->upload_folder . '/accounts/' . $_acc->upload_folder
        ];

        $accounts = [];
        $_member = Member::findOne($_member_id);
        if($_member){
            $mAccounts = $_member->getOwnAccounts()->all();
            foreach($mAccounts as $acc){
                $accounts[] = $acc->attributes;
            }
        }


        $_kiosk = CollectionVariant::findOne($kiosk_id);
        $skin = KioskHelper::getSkinOfKiosk($kiosk_id);

        $skin_id = '';
        $skin_name = '';
        if ($skin != null) {
            $skin_id = $skin->cv_id;
            $skin_name = $skin->name;

        }

        $publishedKioskData = KioskHelper::getKioskPublishedData($kiosk_id);
        $kiosk = [
            'id' => $kiosk_id, 'name' => $_kiosk->name,
            'skin' => [
                'id' => $skin_id,
                'name' => $skin_name,
            ],
            'publishedData' => $publishedKioskData
        ];

        $_sign = Sign::findOne($sign_id);
        $publishSignData = SignHelper::getSignPublishedData($sign_id);
        $sign = [
            'id' => $sign_id,
            'name' => $_sign->name,
            'publishedData' => $publishSignData
        ];

        $skinFormat = [];
        $skinCols = GroupVariant::find()->innerJoinWith(['cvGvs' => function ($query) use ($skin_id) {
            $query->andWhere(['cv_id' => $skin_id]);
        }])->andWhere(['group_id' => 30])->asArray()->all();
        foreach ($skinCols as $col_index =>$col) {
            $map_col = $col['cvGvs'];
            $d = ['gv_id' => $col['gv_id'], 'name' => $col['name'], 'sort' => $map_col[0]['sort']];
            $skinFormat['columns'][] = $d;


            $rows = ItemVariant::find()->innerJoinWith(['gvIvs' => function ($query) use ($col) {
                $query->andWhere(['gv_id' => $col['gv_id']]);
            }])->andWhere(['item_id' => 23])->asArray()->all();

            $boxes = [];

            foreach ($rows as $box_index=> $box) {
                $map_box = $box['gvIvs'];
                $b = ['gv_id' => $box['iv_id'], 'name' => $box['name'], 'sort' => $map_box[0]['sort']];
                $boxes[] = $b;
            }
            $skinFormat['boxes'][] = [$col['gv_id'] => $boxes];
        }


        $kioskFormat = [];
        $kioskGroups = GroupVariant::find()->innerJoinWith(['cvGvs' => function ($query) use ($kiosk_id) {
            $query->andWhere(['cv_id' => $kiosk_id]);
        }])->andWhere(['group_id' => 15])->asArray()->all();
        foreach ($kioskGroups as $col) {
            $map_col = $col['cvGvs'];
            $d = ['gv_id' => $col['gv_id'], 'name' => $col['name'], 'sort' => $map_col[0]['sort']];

            $kioskFormat['columns'][] = $d;


            $rows = ItemVariant::find()->innerJoinWith(['gvIvs' => function ($query) use ($col) {
                $query->andWhere(['gv_id' => $col['gv_id']]);
            }])->andWhere(['in', 'item_id', [1,2,6,7,9,21,22,48]])->asArray()->all();

            $items = [];
            foreach ($rows as $box) {
                $map_box = $box['gvIvs'];
                $b = ['iv_id' => $box['iv_id'], 'name' => $box['name'], 'sort' => $map_box[0]['sort']];
                $items[] = $b;
            }
            $kioskFormat['boxes'][] = [$col['gv_id'] => $items];
        }

        $membership = [];
        $subscriptions = Subscription::find()->all();
        foreach($subscriptions as $s){
            $m = new \stdClass();
            $m->id = $s->id;
            $m->name = $s->subscriptionType->feature_name;
            $m->storage = $s->storage;
            $m->account = $s->desktops;
            $m->isign = $s->isign;
            $m->dsign = $s->dsign;
            $membership[] = $m;
        }

        return [
            'OK' => 1,
            'member' => $member,
            'membership' => $membership,
            'acc' => $acc,
            'kiosk' => $kiosk,
            'skin' => $skin,
            'sign' => $sign,

            'skinFormat' => $skinFormat,
            'kioskFormat' => $kioskFormat,
            'accounts' => $accounts
        ];
    }

    public function actionGetlog()
    {
        $path = Yii::getAlias('@frontend') . DIRECTORY_SEPARATOR . 'runtime' . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'local.log';
        $content = file_get_contents($path);
        return [
            'OK' => 1, 'content' => $content
        ];
    }

    public function actionCleanlog()
    {
        $path = Yii::getAlias('@frontend') . DIRECTORY_SEPARATOR . 'runtime' . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR;
        unlink($path . 'local.log');
        FileHelper::createFile($path, 'local.log');
        chmod($path . 'local.log', 0777);
        return [
            'OK' => 1
        ];
    }


}
