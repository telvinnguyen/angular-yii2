<?php

namespace frontend\controllers;

use common\component\helpers\CommonHelper;
use common\component\helpers\FileHelper;
use yii\rest\ActiveController;

use misc\AjaxPagination\AjaxPagination;

use Yii;

class FileController extends ActiveController
{
    public $modelClass = '';

    /**
     * Upload image to the temp directory
     * @return array
     */
    public function actionUploadtempimage(){
        $fileName = "";
        $genFileName = "";
        $OK = 0; $errors ="";
        $imageTempUrl = "";
        $scaledImageForCrop = null;

        $request = Yii::$app->request;
        $checkStorage = $request->post('checkStorage', 0);
        $isAvatar = $request->post('isAvatar', 0);
        //Yii::info($_FILES);

        if (!empty($_FILES)) {
            $fileName = $_FILES['file']['name'];
            $fileSize = $_FILES['file']['size']; //size in bytes
            $ext = pathinfo($fileName, PATHINFO_EXTENSION);
            $genFileName = CommonHelper::getRandomStringByUnixTimetemp(10) . ".$ext";

            $tempPath = $_FILES['file']['tmp_name'];
            $saveToPath = FileHelper::getUploadFolderRealPath(). 'temp/' . $genFileName;

            $done = move_uploaded_file($tempPath, $saveToPath);

            if($done){
                $OK = 1;

                //get scaled image for crop size 500
                $scaledImageForCrop = FileHelper::getImageScaledInfoFromRealPath($saveToPath);
                $imageTempUrl = FileHelper::getUploadPath(true) . 'temp/' . $genFileName;
            }
            else $errors = Yii::t('frontend.errors', 'upload.file.path.not.found');
			if($isAvatar == '1'){
				$havingSpace = StorageHelper::havingSpace();
				if($fileSize > $havingSpace){
					$OK = 0;
					$errors = Yii::t('frontend.errors', 'upload.file.space.not.enough');
				}
			}
            elseif($checkStorage == '1'){
                $userRemainSpace = StorageHelper::checkAvailableSpace();
                if($fileSize > $userRemainSpace){
                    $OK = 0;
                    $errors = Yii::t('frontend.errors', 'upload.file.space.not.enough');
                }
            }

        } else {
            $errors = Yii::t('frontend.errors', 'upload.file.not.found');
        }

        return [
            'OK' => $OK, 'errors' => $errors,
            'fileName' => $fileName, 'genFileName' => $genFileName,
            'imageTempUrl' => $imageTempUrl,
            'scaledImageForCrop' => $scaledImageForCrop];
    }

    /**
     * crop image in the temp folder & save overwrite the gen_file_name
     */
    public function actionCroptempimage(){
        $OK = 0; $errors ="";

        $request = Yii::$app->request;
        $x1 = $request->post("x1");
        $y1 = $request->post("y1");
        $w = $request->post("width");
        $h = $request->post("height");

        $scale_ratio = $request->post("scale_ratio");
        $fileName = $request->post("fileName");

        $w = ceil($w * $scale_ratio);
        $h = ceil($h * $scale_ratio);
        $x1 = ceil($x1 * $scale_ratio);
        $y1 = ceil($y1 * $scale_ratio);

        $fileRealPath = FileHelper::getUploadFolderRealPath() . 'temp/' . $fileName;

        $done = FileHelper::resizeImage($fileRealPath, $w, $h, $x1, $y1);
        $errors = $done['errors'];
        if($errors == ''){
            $OK = 1;
        }

        //blob
        $fileData = FileHelper::getImageDataUri($fileRealPath);
        return [
            'OK' => $OK, 'errors' => $errors,
            'fileData' => $fileData
        ];

    }


    public function actionUploadtempfile(){
        $fileName = "";
        $genFileName = "";
        $OK = 0; $errors ="";

        $request = Yii::$app->request;
        $checkStorage = $request->post('checkStorage', 0);

        if (!empty($_FILES)) {
            $fileName = $_FILES['file']['name'];
            $fileSize = $_FILES['file']['size']; //size in bytes
            $ext = pathinfo($fileName, PATHINFO_EXTENSION);
            $genFileName = CommonHelper::getRandomStringByUnixTimetemp(10) . ".$ext";

            $tempPath = $_FILES['file']['tmp_name'];
            $saveToPath = FileHelper::getUploadFolderRealPath(). 'temp/' . $genFileName;

            $done = move_uploaded_file($tempPath, $saveToPath);



            if($done){
                $OK = 1;
                $fileTempUrl = FileHelper::getUploadPath(true) . 'temp/' . $genFileName;
            }
            else $errors = Yii::t('frontend.errors', 'upload.file.path.not.found');

            if($checkStorage == '1'){
                $userRemainSpace = StorageHelper::checkAvailableSpace();
                if($fileSize > $userRemainSpace){
                    $OK = 0;
                    $errors = Yii::t('frontend.errors', 'upload.file.space.not.enough');
                }
            }

        } else {
            $errors = Yii::t('frontend.errors', 'upload.file.not.found');
        }

        return [
            'OK' => $OK, 'errors' => $errors,
            'fileName' => $fileName, 'genFileName' => $genFileName,
            'fileTempUrl' => $fileTempUrl
        ];
    }

    /**
     * after the file is uploaded to temp, it will be automatically moved to the logged in user folder
     */
    public function actionUploadandsaveimagevariant(){
        $fileName = "";
        $genFileName = "";
        $OK = 0; $errors ="";

        $request = Yii::$app->request;
        $checkStorage = $request->post('checkStorage', 0);
        $wovid = $request->post('wovid');
        $wgvid = $request->post('wgvid');

        if (!empty($_FILES)) {
            $fileName = $_FILES['file']['name'];
            $fileSize = $_FILES['file']['size']; //size in bytes
            $ext = pathinfo($fileName, PATHINFO_EXTENSION);
            $genFileName = CommonHelper::getRandomStringByUnixTimetemp(10) . ".$ext";

            $tempPath = $_FILES['file']['tmp_name'];
            $saveToPath = FileHelper::getUploadFolderRealPath(). 'temp/' . $genFileName;

            $done = move_uploaded_file($tempPath, $saveToPath);

            if($done){
                $OK = 1;
                $fileTempUrl = FileHelper::getUploadPath(true) . 'temp/' . $genFileName;
            }
            else $errors = Yii::t('frontend.errors', 'upload.file.path.not.found');

            if($checkStorage == '1'){
                $userRemainSpace = StorageHelper::checkAvailableSpace();
                if($fileSize > $userRemainSpace){
                    $OK = 0;
                    $errors = Yii::t('frontend.errors', 'upload.file.space.not.enough');
                }
            }

            if($OK == 1){
                $currentUser = PermissionHelper::getUserAccessing();
                $model = FileHelper::saveImageFileAsAVariant($genFileName, $fileName, $currentUser['val'], $currentUser['id']);

                if ($model->is_temp == 0 && $wgvid != '') {
                    GvIv::createRelationship($wgvid, $model->iv_id);
                }
                elseif ($model->is_temp == 0 && $wovid != '') {
                    CvIv::createRelationship($wovid, $model->iv_id);
                }
            }

        } else {
            $errors = Yii::t('frontend.errors', 'upload.file.not.found');
        }

        return [
            'OK' => $OK, 'errors' => $errors,
            'fileName' => $fileName, 'genFileName' => $genFileName,
            'fileTempUrl' => $fileTempUrl
        ];
    }

    public function actionMultipleuploadingforzip(){
        $file = "";
        $OK = 0; $errors ="";
        $request = Yii::$app->request;
        $checkStorage = $request->post('checkStorage', 0);
        $currentUser = PermissionHelper::getUserAccessing();
        if (count($_FILES) > 0) {
            $fileName = preg_replace('/[^\w\._]+/', '_', $_FILES['file']['name']);
            $fileSize = $_FILES['file']['size']; //size in bytes
            $ext = pathinfo($fileName, PATHINFO_EXTENSION);
            //$genFileName = CommonHelper::getRandomStringByUnixTimetemp(10) . ".$ext";
            $tempPath = $_FILES['file']['tmp_name'];
          
            $saveToPath = FileHelper::getUploadFolderRealPath() . FileHelper::TEMP_ZIP_FOLDER;
            if (!file_exists($saveToPath)) {
                $saveToPath = str_replace('//', '/', $saveToPath);
                mkdir($saveToPath, 0755);
            }
            $saveToPath = $saveToPath . $fileName;
           
            //IF file is video => convert to mp4
            
           $fileType = FileHelper::get_type_file_upload($fileName);
           $ext = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));
           if ($fileType == ObjectToolbar::VIDEO_EXT && $ext != 'mp4' && CommonHelper::isLocalHostRequest() == false) {
           		$outputPath = FileHelper::getUploadFolderRealPath() . FileHelper::TEMP_ZIP_FOLDER.pathinfo($fileName, PATHINFO_FILENAME).".mp4";
           		$done = move_uploaded_file($tempPath, $saveToPath);
                FileHelper::convertVideoToMp4($saveToPath, $outputPath);
                if(file_exists($saveToPath)){
                	unlink($saveToPath);
                }
                $saveToPath = $outputPath;
           }else{
           		$done = move_uploaded_file($tempPath, $saveToPath);
           }
            
            if ($done) {
                $OK = 1;
                $file = $saveToPath;
            } else $errors = Yii::t('frontend.errors', 'upload.file.path.not.found');

            if ($checkStorage == '1') {
                $userRemainSpace = StorageHelper::checkAvailableSpace();
                if ($fileSize > $userRemainSpace) {
                    $OK = 0;
                    $errors = Yii::t('frontend.errors', 'upload.file.space.not.enough');
                }
            }
        }
        return [
            'OK' => $OK,
            'result' =>  $file,
        	'fileName' => $_FILES['file']['name'],
            'error' => $errors
        ];
    }

    public function actionHandlemultipleuploadingthenzip(){
        $mes = '';
        $request = Yii::$app->request;
        $totalItem = $request->post('total_item',0);
        $name = $request->post('name','');
        $oid = $request->post('oid','');
        $files = $request->post('files','');
        $wovid = $request->post('wovid', '');
        $wgvid = $request->post('wgvid','');
        $mtype = $request->post('mtype','');
        $wtype = $request->post('wtype', '');
        $currentUser = PermissionHelper::getUserAccessing();
        $folderUpload = FileHelper::get_folder_upload_file($currentUser['val'], $currentUser['id']);
        $folderUpload = FileHelper::getUploadFolderRealPath().$folderUpload;
        $folderUpload = str_replace('//', '/', $folderUpload);
        $useHOS = Yii::$app->params['enableHOS'];
        if($totalItem!=0 && !empty($files)){
            // if total file in session equals total uploading file, zip them into 1 and unset the session, create item variant with the upload_file is zip file
            if (count($files) == $totalItem) {
                $files_array = $files;
                $file_name_upload_file = CommonHelper::uniquename(FileHelper::FILE_ZIP_PREFIX, "zip");
                FileHelper::createZipFile("", $files_array, $folderUpload.$file_name_upload_file);
                $zip = new \ZipArchive();
                if ($zip->open($folderUpload.$file_name_upload_file)) {
                	//delete temp file
                	foreach($files as $file){
                		if (file_exists($file['result'])){
                			unlink($file['result']);
                		}
                	}
                	$model = new ItemVariant(); //init default value
                	$model->item_id = $oid;
                	$model->$currentUser['id'] = $currentUser['val'];
                	$model->created_date = date("Y-m-d H:i:s");
                	$model->name = $name;
                	$model->real_file_name = $name . '.zip';
                	$model->upload_file = $file_name_upload_file;
                	$model->save();
                	
                	if ($model->is_temp == 0 && $mtype == ObjectToolbar::TYPE_ITEM && $wgvid != '') {
                		GvIv::createRelationship($wgvid, $model->iv_id);
                	}
                	elseif ($model->is_temp == 0 && $wtype == ObjectToolbar::TYPE_COLLECTION && $wovid != '') {
                		CvIv::createRelationship($wovid, $model->iv_id);
                	}
                	
                	StorageHelper::saveFileSizeToViewingUserAccount($model->iv_id);
                	
                	return [
                			'OK' => 1,
                			'ovid' => $model->iv_id,
                	];
                	
                }else{
                	if (file_exists($folderUpload.$file_name_upload_file)){
                		unlink($folderUpload.$file_name_upload_file);
                	}
                }
           
            }
        }

        return[
            'OK' => 0,
        	'mes' => "Upload failed. Please try again !"
        ];
    }

    public function actionUploadtempfilebychunking(){

        /**
         * upload.php
         *
         * Copyright 2009, Moxiecode Systems AB
         * Released under GPL License.
         *
         * License: http://www.plupload.com/license
         * Contributing: http://www.plupload.com/contributing
         */

        // HTTP headers for no cache etc
        /*
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        */

        // Settings
        //$targetDir = ini_get("upload_tmp_dir") . DIRECTORY_SEPARATOR . "plupload";
        if(isset($_REQUEST['path'])){
        	echo $_REQUEST['path'];
        };

        $targetDir = FileHelper::getUploadFolderRealPath() . 'temp/';

        $cleanupTargetDir = true; // Remove old files
        $maxFileAge = 5 * 3600; // Temp file age in seconds

        // 5 minutes execution time
        @set_time_limit(5 * 60);

        // Uncomment this one to fake upload time
        // usleep(5000);

        // Get parameters
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
        $fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';

        // Clean the fileName for security reasons
        $fileName = preg_replace('/[^\w\._]+/', '_', $fileName);

        // Make sure the fileName is unique but only if chunking is disabled
        if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
        	$ext = strrpos($fileName, '.');
        	$fileName_a = substr($fileName, 0, $ext);
        	$fileName_b = substr($fileName, $ext);

        	$count = 1;
        	while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
        		$count++;

        	$fileName = $fileName_a . '_' . $count . $fileName_b;
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

        // Create target dir
        if (!file_exists($targetDir))
        	@mkdir($targetDir);

        // Remove old temp files
        if ($cleanupTargetDir) {
        	if (is_dir($targetDir) && ($dir = opendir($targetDir))) {
        		while (($file = readdir($dir)) !== false) {
        			$tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

        			// Remove temp file if it is older than the max age and is not the current file
        			if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part")) {
        				@unlink($tmpfilePath);
        			}
        		}
        		closedir($dir);
        	} else {
        		die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
        	}
        }

        // Look for the content type header
        if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
        	$contentType = $_SERVER["HTTP_CONTENT_TYPE"];

        if (isset($_SERVER["CONTENT_TYPE"]))
        	$contentType = $_SERVER["CONTENT_TYPE"];

        // Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
        if (strpos($contentType, "multipart") !== false) {
        	if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
        		// Open temp file
        		$out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
        		if ($out) {
        			// Read binary input stream and append it to temp file
        			$in = @fopen($_FILES['file']['tmp_name'], "rb");

        			if ($in) {
        				while ($buff = fread($in, 4096))
        					fwrite($out, $buff);
        			} else
        				die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
        			@fclose($in);
        			@fclose($out);
        			@unlink($_FILES['file']['tmp_name']);
        		} else
        			die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
        	} else
        		die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
        } else {
        	// Open temp file
        	$out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
        	if ($out) {
        		// Read binary input stream and append it to temp file
        		$in = @fopen("php://input", "rb");

        		if ($in) {
        			while ($buff = fread($in, 4096))
        				fwrite($out, $buff);
        		} else
        			die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');

        		@fclose($in);
        		@fclose($out);
        	} else
        		die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
        }

        $newPath = '';
        $fileTempName = '';
        // Check if file has been uploaded
        if (!$chunks || $chunk == $chunks - 1) {
        	// Strip the temp .part suffix off
        	rename("{$filePath}.part", $filePath);

            //extend the file name with an unique id

            $fileDir = pathinfo($filePath, PATHINFO_DIRNAME);
            $fileNameWoExt = pathinfo($filePath, PATHINFO_FILENAME);
            $ext = pathinfo($filePath, PATHINFO_EXTENSION);
            $newPath = $fileDir . DIRECTORY_SEPARATOR . $fileNameWoExt . '_' . CommonHelper::getRandomStringByUnixTimetemp(5) . ".$ext";

        	rename("{$filePath}", $newPath);
            $fileTempName = pathinfo($newPath, PATHINFO_BASENAME);
        }

        die(json_encode([
            'fileTempName' => $fileTempName
        ]));

    }
    
    public function actionGetfilestozip(){
    	$request = Yii::$app->request;
    	$sorter = $request->post('sorter', '');
    	$currentUser = PermissionHelper::getUserAccessing();
    
    	$availableExtension = [];
    	$postPageParam = intval($request->post('pageIndex', 0));
    	$report_iv_id = $request->post('ovid', '');
    	$pageCount = 0;
    
    	//Get supported extensions
    	$supportAttachObject = array(ObjectToolbar::DOCUMENT_EXT,ObjectToolbar::IMAGE_EXT,ObjectToolbar::VIDEO_EXT, ObjectToolbar::MUSIC_EXT);
    	foreach($supportAttachObject as $object){
    		$objectExt = Item::find()->where(['item_extension'=>$object])->one();
    		$availableExtension[] = $objectExt->item_id;
    	}
    	//Get all item variants of member with supported extension
    	if(isset($availableExtension) && $availableExtension != null ){
    		$strsql = sprintf('%s = %s ',$currentUser["id"], $currentUser["val"]);
    		$buildQuery = ItemVariant::find()->where($strsql)->andWhere(["item_id" => $availableExtension]);
    	}
    	$buildQuery->orderBy("tbl_item_variant.iv_id DESC");
    
    	$itemsEachPage = 11;
    	if ($postPageParam == 0) {
    		$countBuildQuery = clone $buildQuery;
    		$pages = new AjaxPagination([
    				'totalCount' => $countBuildQuery->count(),
    				'pageParam' => 'pageIndex',
    				'pageSize' => $itemsEachPage
    		]);
    		$pageCount = $pages->getPageCount();
    		$buildQuery->offset($pages->offset)
    		->limit($pages->limit*2);
    	}
    
    	if($sorter != ''){ $buildQuery->orderBy("$sorter $sort_order"); }
    	
    	$entries = $buildQuery->all();
    
    	//Process to highlight current selected objects
    	$itemVariants = [];
    	foreach($entries as $entry){
    		$t = new ObjectVariant($entry);
    		if($t->isYoutubeLink == false){
	    		$t->getShortName(13);
	    		$t->getMasterObjectShortName(16);
	    		$itemVariants[] = $t;
    		}
    	}
    	
    	return [
    			'OK' => 1,
    			'selected_list' => [],
    			'pageCount' => $pageCount,
    			'itemVariants' => $itemVariants,
    	];
    }
    
    public function actionAddfilestozip(){
    	$request = Yii::$app->request;
    	$selected_list = $request->post('selected_list', '');
    	$formInfo = $request->post('formInfo', '');
    	$oid = $request->post('oid', '');
    	$mtype = $request->post('mtype', '');
    	$wtype = $request->post('wtype', '');
    	$wovid = $request->post('wovid', '');
    	$wgvid = $request->post('wgvid', '');
    	$mf_gv_id = $request->post('mf_gv', ''); // if there's required to add item into the group which has machform
    	
    	$mes = "Files error. Please try again !";
    	$currentUser = PermissionHelper::getUserAccessing();
    	$totalStorage = 0;
    	if(!empty($selected_list)){
    		
    		$folderUpload = FileHelper::getUploaderFolderOfMemberOrAccount();
    		$files = [];
    		foreach($selected_list as $file){
    			$item = Itemvariant::findOne($file);
    			$fileSize = FileHelper::getSizeOfFile($file);
    			$totalStorage += $fileSize;
    			$files[] = $folderUpload . $item->upload_file;
    		}
    		
    		if(FileHelper::checkStorageForCreateFile($totalStorage)){
    			$file_name_upload_file = CommonHelper::uniquename(FileHelper::FILE_ZIP_PREFIX, "zip");
    			FileHelper::createZipFile("", $files, $folderUpload.$file_name_upload_file);
    			$zip = new \ZipArchive();
    			if ($zip->open($folderUpload.$file_name_upload_file)) {
    				$model = new ItemVariant(); //init default value
    				$model->item_id = $oid;
    				$model->$currentUser['id'] = $currentUser['val'];
    				$model->created_date = date("Y-m-d H:i:s");
    				$model->name = $formInfo['name'];
    				$model->real_file_name = $formInfo['name'] . '.zip';
    				$model->upload_file = $file_name_upload_file;
    				$model->save();
    				
    				$model->save(false);
    				if ($model->is_temp == 0 && $mtype == ObjectToolbar::TYPE_ITEM && $wgvid != '') {
    					GvIv::createRelationship($wgvid, $model->iv_id);
    				} elseif ($model->is_temp == 0 && $wtype == ObjectToolbar::TYPE_COLLECTION && $wovid != '') {
    					CvIv::createRelationship($wovid, $model->iv_id);
    				}
    				
    				if ($mf_gv_id != '') {
    					MachformHelper::addItemVariantIntoMachformGv($model->iv_id, $mf_gv_id);
    				}
    				
    				StorageHelper::saveFileSizeToViewingUserAccount($model->iv_id);
    				 
    				return [
    						'OK' => '1',
    						'ovid' => $model->iv_id
    				];
    			} else{
    				if (file_exists($folderUpload.$file_name_upload_file)){
    					unlink($folderUpload.$file_name_upload_file);
    				}
    			}
    		
    		}else{
    			$mes = "Your storage is not enough available space.";
    		}
    	}
    	return [
    			'OK' => '0',
    			'mes' => $mes
    	];
    	
    }
    
    public function actionExtractzipfile(){
    	$request = Yii::$app->request;
    	$formInfo = $request->post('formInfo', '');
    	$wtype = $request->post('wtype', '');
    	$wovid = $request->post('wovid', '');
    	$wgvid = $request->post('wgvid', '');
    	$mtype = $request->post('mtype', '');
    	$ovid = $request->post('ovid', '');
    	$name = $request->post('name', '');
    	$acc_id = $request->post('acc_id', '');
    	$mes = "Extract failed. Please try again !";
    	$currentUser = PermissionHelper::getUserAccessing();
    	$zipFile = FileHelper::getFilePathOfIv($ovid);
    	$originalSize = FileHelper::get_zip_originalsize($zipFile);
    	if(FileHelper::checkStorageForCreateFile($originalSize)){
    		$zip = new \ZipArchive();
    		if ($zip->open($zipFile)) {
    			//group is workspace : no create folder or group containt file extracted
    			if($wtype != ObjectToolbar::TYPE_GROUP && $wgvid == ''){
    				$group = Group::find()->where(['group_extension' => ObjectToolbar::FOLDER_EXT])->one();
    				$zipFolder = new GroupVariant();
    				$zipFolder->name = $name;
    				$zipFolder->group_id = $group->group_id;
    				$zipFolder->$currentUser['id'] = $currentUser['val'];
    				$zipFolder->save(false);
    			}
    				
    			$folderUpload = FileHelper::getUploaderFolderOfMemberOrAccount();
    				
    			//read zip file and create item
    			for ($i = 0; $i < $zip->numFiles; $i++) {
    				$model = new ItemVariant();
    				$icon = '';
    				$preview_kiosk_icon = '';
    				$fileName = $zip->getNameIndex($i);
    				$ext = pathinfo($fileName, PATHINFO_EXTENSION);
    				$genFileName = CommonHelper::getRandomStringByUnixTimetemp(10) . ".$ext";
    				$real_file_name = $fileName;
    				$itemVariant = Itemvariant::find()->where(['upload_file' => $fileName])->one();
    				if($itemVariant != null){
    					$real_file_name = $itemVariant->name;
    					$icon = $itemVariant->icon_image;
    					$preview_kiosk_icon = $itemVariant->preview_kiosk_icon;
    				}
    				
    				// rename file for not duplicate file
    				$zip->renameIndex($i,$genFileName);
    		
    				// get type of item video || music || image || document
    				$fileType = FileHelper::get_type_file_upload($fileName);
    				if ($fileType == ObjectToolbar::VIDEO_EXT) {
    					$item = Item::find()->where(['item_extension' => ObjectToolbar::VIDEO_EXT])->one();
    					$model->item_id = $item->item_id;
    				}
    				elseif ($fileType == ObjectToolbar::IMAGE_EXT) {
    					$item = Item::find()->where(['item_extension' => ObjectToolbar::IMAGE_EXT])->one();
    					$model->item_id = $item->item_id;
    				}
    				elseif ($fileType == ObjectToolbar::DOCUMENT_EXT) {
    					$item = Item::find()->where(['item_extension' => ObjectToolbar::DOCUMENT_EXT])->one();
    					$model->item_id = $item->item_id;
    				}
    				elseif ($fileType == ObjectToolbar::MUSIC_EXT) {
    					$item = Item::find()->where(['item_extension' => ObjectToolbar::MUSIC_EXT])->one();
    					$model->item_id = $item->item_id;
    				}
    				
    				//extract item
    				$zip->extractTo($folderUpload, $genFileName);
    				//undo rename to keep orginal file name.
    				$zip->renameIndex($i,$fileName);
    				
    				$model->upload_file = $genFileName;
    				$model->icon_image = $icon;
    				$model->preview_kiosk_icon = $preview_kiosk_icon;
    				$model->name = $real_file_name;
    				$model->real_file_name = $real_file_name;
    				$model->$currentUser['id'] = $currentUser['val'];
    				$model->created_date = date("Y-sm-d H:i:s");
    				
    				//generate a screenshot from the video
    				if($fileType == ObjectToolbar::VIDEO_EXT && $icon == '' && $preview_kiosk_icon == ''){
    					$preview_kiosk_icon = FileHelper::getThumbnailImageFromVideo($acc_id, $model);
    					$model->preview_kiosk_icon = $preview_kiosk_icon;
    					if ($useHOS) {
    						FileHelper::doUploadViewingUserFileToHOS($model->preview_kiosk_icon, $model->upload_file);
    					}
    				}
    				$model->save(false);
    		
    				//update storage
    				StorageHelper::saveFileSizeToViewingUserAccount($model->iv_id);
    		
    				//create relationship between zipfolder and file
    				if ($wovid != '' && $wtype == ObjectToolbar::TYPE_GROUP) {
    					 // unzip when workspace is group, no create group
    						GvIv::createRelationship($wovid, $model->iv_id);
    						$gv_id = $wovid;
    				} else{
    					if($wgvid != ''){// unzip when workspace is collection, no create group
    						GvIv::createRelationship($wgvid, $model->iv_id);
    						$gv_id = $wgvid;
    					}
    					else{
    						GvIv::createRelationship($zipFolder->gv_id, $model->iv_id);
    						$gv_id = $zipFolder->gv_id;
    					}
    				}
    			}
    			// when unzip in workspace
    			if ($wovid != '' && $wtype == ObjectToolbar::TYPE_COLLECTION) {
    				CvGv::createRelationship($wovid, $gv_id);
    			}
    		
    			$zip->close();
    			return [
    					'OK' => '1',
    					'gv_id' => $gv_id
    			];
    		}
    		
		}else{
    		$mes = "Your storage is not enough available space.";
    	}
    	
		return [
				'OK' => '0',
				'mes' => $mes
		];
    }
}
