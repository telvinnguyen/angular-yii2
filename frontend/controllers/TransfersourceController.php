<?php

namespace frontend\controllers;
use Yii;
use common\component\helpers\FileHelper;
use yii\console\Controller;

class TransfersourceController extends Controller
{
    /**
     * Move code from signsmart4 folder > signsmart4_dev4 folder
     */
    public function actionMovecodetodev4folder(){

        #region generate angularjs layout
        $scanFile = Yii::$app->params['viewLayoutDir']  . DIRECTORY_SEPARATOR . 'angularjs.php';
        $saveTo = Yii::$app->params['viewLayoutDir']  . DIRECTORY_SEPARATOR . 'angularjs_minify.php';

        $content = file_get_contents($scanFile);
        $re = "/src\\s*=\\s*\"(.+)(\\/(.+)\\.js)\"/";
        $replace = "src=\"<?= \Yii::\$app->request->BaseUrl ?>/angularjs/build/live/$3.min.js\"";
        $newContent = preg_replace($re, $replace, $content);
        //overwrite the new content
        file_put_contents($saveTo, $newContent);
        #endregion

        $fromFolder = 'E:\wamp\www\signsmart4' .DIRECTORY_SEPARATOR;
        $toFolder = 'E:\wamp\www\signsmart4_dev4' .DIRECTORY_SEPARATOR;
        //$svnExecPath = 'C:\Program Files\TortoiseSVN\bin\TortoiseProc.exe';

        $request = Yii::$app->request;
        $runTo = $request->post('runto'); //example value: 1 or 1-2 or 1-2-3
        $commit = $request->post('commit'); // 1 or 0

        //NOTE: never set the root of source in this list because the .svn folder is there
        $transferList1 = [
            'common\components\helpers' => 'common\components\helpers',
            'vendor\composer\autoload_psr4.php' => 'vendor\composer\autoload_psr4.php',
            'common\config\bootstrap.php' => 'common\config\bootstrap.php',
            'common\messages\en-GB\frontend.errors.php' => 'common\messages\en-GB\frontend.errors.php',
            'frontend\views\layouts' => 'frontend\views\layouts',

            'frontend\controllers' => 'frontend\controllers',
            'frontend\models' => 'frontend\models',
            'frontend\assets' => 'frontend\assets',

            'frontend\web\angularjs\app\apps' => 'frontend\web\angularjs\app\apps',
            'frontend\web\angularjs\app\controllers' => 'frontend\web\angularjs\app\controllers',
            'frontend\web\angularjs\app\directives' => 'frontend\web\angularjs\app\directives',
            'frontend\web\angularjs\app\partials' => 'frontend\web\angularjs\app\partials',
            'frontend\web\angularjs\app\services' => 'frontend\web\angularjs\app\services',
            //'frontend\web\angularjs\build\live' => 'frontend\web\angularjs\build\live',
            'frontend\web\angularjs\app\css\custom.css' => 'frontend\web\angularjs\app\css\custom.css', //frequently updated
            'frontend\web\angularjs\app\css\ss\css\custom.css' => 'frontend\web\angularjs\app\css\ss\css\custom.css', //frequently updated
            'frontend\web\angularjs\app\css\ss\css\new_content_database.css' => 'frontend\web\angularjs\app\css\ss\css\new_content_database.css' //frequently updated
        ];

        $transferList2 = [
            'common\models' => 'common\models'
        ];

        $transferList3 = [
            'frontend\web\angularjs\lib' => 'frontend\web\angularjs\lib',
            'frontend\web\angularjs\app\css' => 'frontend\web\angularjs\app\css',
        ];

        $transferList4 = [

            'frontend\config\bootstrap.php' => 'frontend\config\bootstrap.php',
            'frontend\config\main.php' => 'frontend\config\main.php',
            'frontend\config\params.php' => 'frontend\config\params.php',
            'frontend\views\generator' => 'frontend\views\generator',

            'common\mail' => 'common\mail',
            'common\messages' => 'common\messages',
            'common\config\bootstrap.php' => 'common\config\bootstrap.php',
            'common\config\main.php' => 'common\config\main.php',
            'common\config\params.php' => 'common\config\params.php',
        ];

        $removeList = [
            'frontend/web/angularjs/build/original/*',
            'frontend/web/angularjs/build/live/*'
        ];

        if($runTo == '1' || $runTo == '2' || $runTo == '3' || $runTo == '4' || $runTo == '5'){
            foreach($transferList1 as $from => $to){
                FileHelper::xcopy($fromFolder . $from, $toFolder . $to);
                var_dump('step1-done: ' .$toFolder . $to);
            }
        }
        if($runTo == '2' || $runTo == '3' || $runTo == '4' || $runTo == '5'){
            foreach($transferList2 as $from => $to){
                FileHelper::xcopy($fromFolder . $from, $toFolder . $to);
                var_dump('step2-done: ' .$toFolder . $to);
            }
        }

        if($runTo == '3' || $runTo == '4' || $runTo == '5'){
            foreach($transferList3 as $from => $to){
                FileHelper::xcopy($fromFolder . $from, $toFolder . $to);
                var_dump('step3-done: ' .$toFolder . $to);
            }
        }

        if($runTo == '4' || $runTo == '5'){
            foreach($transferList4 as $from => $to){
                FileHelper::xcopy($fromFolder . $from, $toFolder . $to);
                var_dump('step3-done: ' .$toFolder . $to);
            }
        }
        foreach($removeList as $dir){
            $files = glob($fromFolder . $dir); // get all file names
            foreach($files as $file){ // iterate files
                if(is_file($file))
                    unlink($file); // delete file
            }
        }



        var_dump($commit == 1 ? 'true' : 'false');
        exit;
        if($commit == '1'){ //start to commit
            $now = gmdate("Y-m-1\TH:i:s\Z");
            $message="'Transferring file on $now'";
            $command = "svn ci -q -m $message " . $toFolder; // notice the commit does not include the added file automatically
            //$command = " /command:commit /path:'E:\\wamp\\www\\signsmart4_dev4' /logmsg:'$message' /closeonend:0";
            exec($command);
        }
    }

}
