<?php use yii\helpers\Url; ?>

<script>
    var vendorScripts = [
        '<?= Url::base(true) ?>/angularjs/lib/vendor/ocLazyLoad/dist/ocLazyLoad.min.js',

        '<?= Url::base(true) ?>/angularjs/lib/vendor/ng-idle/angular-idle.min.js',
        '<?= Url::base(true) ?>/angularjs/lib/ui-bootstrap-tpls-0.11.2.min.js',
        '<?= Url::base(true) ?>/angularjs/lib/angular-1.3.14/angular-route.min.js',
        '<?= Url::base(true) ?>/angularjs/lib/angular-1.3.14/angular-animate.min.js',
        '<?= Url::base(true) ?>/angularjs/lib/angular-1.3.14/angular-cookies.min.js',
        '<?= Url::base(true) ?>/angularjs/lib/angular-1.3.14/angular-loader.min.js',
        //'<?= Url::base(true) ?>/angularjs/lib/angular-1.3.14/angular-mocks.js', // for testing
        '<?= Url::base(true) ?>/angularjs/lib/angular-1.3.14/angular-resource.min.js',
        '<?= Url::base(true) ?>/angularjs/lib/angular-1.3.14/angular-sanitize.min.js',
        //'<?= Url::base(true) ?>/angularjs/lib/angular-1.3.14/angular-scenario.js',
        '<?= Url::base(true) ?>/angularjs/lib/angular-1.3.14/angular-touch.min.js',
        '<?= Url::base(true) ?>/angularjs/lib/ui-router-extras/ct-ui-router-extras.min.js',
        '<?= Url::base(true) ?>/angularjs/lib/bootstrap.min.js',
        '<?= Url::base(true) ?>/angularjs/lib/underscore-min.js',
        '<?= Url::base(true) ?>/angularjs/lib/underscore.string.min.js',
        '<?= Url::base(true) ?>/angularjs/lib/vendor/angular-websocket/angular-websocket.min.js',

        '<?= Url::base(true) ?>/angularjs/lib/angular-validation.min.js',
        '<?= Url::base(true) ?>/angularjs/lib/angular-validation-rule.min.js',

        '<?= Url::base(true) ?>/angularjs/lib/vendor/ngLoader/ngLoader.js',
        '<?= Url::base(true) ?>/angularjs/lib/vendor/angular-ui-router/release/angular-ui-router.js',
        '<?= Url::base(true) ?>/angularjs/lib/vendor/angular-css-injector.min.js',


        '<?= Url::base(true) ?>/angularjs/lib/vendor/a0-angular-storage/dist/angular-storage.js',
        '<?= Url::base(true) ?>/angularjs/lib/vendor/angular-jwt/dist/angular-jwt.js',
        '<?= Url::base(true) ?>/angularjs/lib/vendor/ui-router-styles.js',
        '<?= Url::base(true) ?>/angularjs/lib/vendor/angular-file-upload.js',

        '<?= Url::base(true) ?>/angularjs/lib/vendor/jcarousel/jquery.jcarousel.min.js',
        '<?= Url::base(true) ?>/angularjs/lib/vendor/angular-post-message/angular-post-message.min.js',
        //'<?= Url::base(true) ?>/angularjs/lib/vendor/jwplayer/jwplayer.js',

        '<?= Url::base(true) ?>/angularjs/lib/vendor/ng-clip-0.2.6/dest/ng-clip.min.js',
        '<?= Url::base(true) ?>/angularjs/lib/vendor/ng-clip-0.2.6/dest/ZeroClipboard.min.js',

        '<?= Url::base(true) ?>/angularjs/lib/vendor/angular-ui-utils/ui-utils.min.js',
        '<?= Url::base(true) ?>/angularjs/lib/vendor/dialog/dialogs.min.js',
        '<?= Url::base(true) ?>/angularjs/lib/vendor/chosen/chosen.jquery.js',

        '<?= Url::base(true) ?>/angularjs/lib/vendor/xtForm-validation/xtForm.tpl.js',
        '<?= Url::base(true) ?>/angularjs/lib/vendor/angular-iscroll/dist/lib/iscroll.min.js',
        '<?= Url::base(true) ?>/angularjs/lib/vendor/angular-iscroll/dist/lib/angular-iscroll.js',
        '<?= Url::base(true) ?>/angularjs/lib/vendor/angular-local-storage/dist/angular-local-storage.min.js'
    ]

    var angularInternalScripts = [
        '<?= Url::base(true) ?>/angularjs/app/apps/app-states.js',
        '<?= Url::base(true) ?>/angularjs/app/apps/routes-states.js',

        '<?= Url::base(true) ?>/angularjs/app/services/q-allsettled-decorator.js',
        '<?= Url::base(true) ?>/angularjs/app/services/ssHelper.js',
        '<?= Url::base(true) ?>/angularjs/app/services/signal-handler.js',
        '<?= Url::base(true) ?>/angularjs/app/services/modal-utils.js',
        '<?= Url::base(true) ?>/angularjs/app/services/jsonOrder.js',
        '<?= Url::base(true) ?>/angularjs/app/services/trust-url.js',

        '<?= Url::base(true) ?>/angularjs/app/controllers/error/errorService.js',

        '<?= Url::base(true) ?>/angularjs/app/controllers/content/contentService.js',

        '<?= Url::base(true) ?>/angularjs/app/directives/common/trigger-element.js',
        '<?= Url::base(true) ?>/angularjs/app/directives/common/window-exit.js',

        '<?= Url::base(true) ?>/angularjs/app/directives/common/chosen.js',
        '<?= Url::base(true) ?>/angularjs/app/directives/common/nav-item.js',
        '<?= Url::base(true) ?>/angularjs/app/directives/common/draggable.js',
        '<?= Url::base(true) ?>/angularjs/app/directives/common/img-preload.js',
        '<?= Url::base(true) ?>/angularjs/app/directives/common/load-dym-template.js',
        '<?= Url::base(true) ?>/angularjs/app/directives/common/jcarousel.js',
        '<?= Url::base(true) ?>/angularjs/app/directives/common/hint-text.js',
        '<?= Url::base(true) ?>/angularjs/app/directives/common/ng-thumb.js',
        '<?= Url::base(true) ?>/angularjs/app/directives/common/checklistModel/checklist-model.js',
        '<?= Url::base(true) ?>/angularjs/app/directives/common/color-progress-bar.js',
        '<?= Url::base(true) ?>/angularjs/app/directives/common/imgAreaSelect/ng-imgAreaSelect.js',
        '<?= Url::base(true) ?>/angularjs/app/directives/common/tooltip/qtip-directive.js',
        '<?= Url::base(true) ?>/angularjs/app/directives/partials/kiosk-layout-preview.js',
        '<?= Url::base(true) ?>/angularjs/app/directives/common/colorPicker/color-picker-directive.js',
        '<?= Url::base(true) ?>/angularjs/app/directives/common/partial-css.js',

        '<?= Url::base(true) ?>/angularjs/app/directives/common/resize-iframe-content.js',
        '<?= Url::base(true) ?>/angularjs/app/directives/common/fixed-iframe.js',
        '<?= Url::base(true) ?>/angularjs/app/directives/common/imagemap/imagemap.js',
        '<?= Url::base(true) ?>/angularjs/app/directives/common/ng-click-select.js',

        '<?= Url::base(true) ?>/angularjs/app/directives/partials/has-dot.js',


        '<?= Url::base(true) ?>/angularjs/app/directives/common/current-time.js',
        '<?= Url::base(true) ?>/angularjs/app/directives/common/fancy-box/fancy-box.js',


        '<?= Url::base(true) ?>/angularjs/app/controllers/main/masterPageController.js',
        '<?= Url::base(true) ?>/angularjs/app/controllers/global-modal/globalModalController.js',
    ]

    var vendorLength = vendorScripts.length;
    var angularLength = angularInternalScripts.length;
    var totalScripts = vendorLength + angularLength;


    var vendorScriptsDone = 0;
    var angularScriptsDone = 0;

    var percent = 0;
    for (var i = 0; i < vendorLength; i++) {
        insertScript(i, vendorScripts[i], function(index){
            vendorScriptsDone = index;

            percent = Math.ceil(index*100/totalScripts).toString() + '%';
            //$('#appLoadAmt').html(percent);
        })

    }

    var appIsDefined = false;
    for (var j = 0; j < angularLength; j++) {
        insertScript(j , angularInternalScripts[j], (function(index){
            if(index == 1){ // if it is app-states
                appIsDefined = true;
            }
            angularScriptsDone = index;

//            percent = Math.ceil(allScriptLoaded*100/totalScripts).toString() + '%';
//            $('#appLoadAmt').html(percent);
        }))
    }


    function insertScript(index, url, callback){

        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.onload = (function () {
            callback(index);
        })(index);
        script.src = url;
        document.getElementsByTagName('head')[0].appendChild(script);
    }

    /*
    setTimeout(function(){
        if(appIsDefined && (vendorScriptsDone + angularScriptsDone == (totalScripts-2))){
            angular.bootstrap( document, [ "zwenGlobal" ] );
            $('#preload-cover').fadeOut();
            $('#preload-cover').remove();
        }
    }, 500)
    */

    window.onload = function(){
        var delayBootstrap = setTimeout(function(){
            if(appIsDefined && (vendorScriptsDone + angularScriptsDone == (totalScripts-2))){
                angular.bootstrap( document, [ "zwenGlobal" ] );
                $('#preload-cover').fadeOut();
                $('#preload-cover').remove();
                clearTimeout(delayBootstrap)
            }
        }, 2000)
    }
</script>

