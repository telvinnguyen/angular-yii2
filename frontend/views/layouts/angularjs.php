<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;
use common\component\helpers\CommonHelper;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>

<html  lang="<?= Yii::$app->language ?>" <?php if(!Yii::$app->view->params['enablePreBootstrap']){ ?> ng-app="<?= Yii::$app->view->params['ngApp'] ?>" <?php } ?>>
<head>
    <!--<base href="/signsmart_angularjs/index.html" />-->
    <script>
        //document.write('<base href="' + document.location + '" />');
        document.write('<base href="<?= Url::toRoute('site/page', true) ?>/" />');
    </script>

    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- pinned logo windows 8 -->
    <meta name="application-name" content="ZwenGlobal"/>
    <meta name="msapplication-TileColor" content="#000000"/>
    <meta name="msapplication-TileImage" content="<?= Url::base(true) ?>/angularjs/app/css/ss/images/favicon.png"/>
    <link rel="shortcut icon" href="<?= Url::base(true) ?>/angularjs/app/css/ss/images/favicon.png" type="image/x-png" />

    <?= Html::csrfMetaTags() ?>

    <!-- Global settings: required -->
    <?php echo $this->render('global-settings', ['app' => \Yii::$app]) ?>

    <title><?= Html::encode($this->title) ?></title>
    <!-- Registered JS (POS_HEAD) will be loaded here -->
    <?php $this->head() ?>

    <!-- *** Initialize AngularJS app *** -->
    <script src="<?= Url::base(true) ?>/angularjs/app/apps/app-states.js"></script>
    <script src="<?= Url::base(true) ?>/angularjs/app/apps/routes-states.js"></script>

    <!-- *** Services *** -->
    <script src="<?= Url::base(true) ?>/angularjs/app/services/httpRequestInterceptor.js"></script>
    <script src="<?= Url::base(true) ?>/angularjs/app/services/q-allsettled-decorator.js"></script>
    <script src="<?= Url::base(true) ?>/angularjs/app/services/ssHelper.js"></script>
    <script src="<?= Url::base(true) ?>/angularjs/app/services/signal-handler.js"></script>
    <script src="<?= Url::base(true) ?>/angularjs/app/services/modal-utils.js"></script>
    <script src="<?= Url::base(true) ?>/angularjs/app/services/jsonOrder.js"></script>
    <script src="<?= Url::base(true) ?>/angularjs/app/services/trust-url.js"></script>

    <script src="<?= Url::base(true) ?>/angularjs/app/controllers/error/errorService.js"></script>

    <!-- *** Directives *** -->
    <script src="<?= Url::base(true) ?>/angularjs/app/directives/common/trigger-element.js"></script>
    <script src="<?= Url::base(true) ?>/angularjs/app/directives/common/window-exit.js"></script>
    <script src="<?= Url::base(true) ?>/angularjs/app/directives/common/content-resolver.js"></script>

    <script src="<?= Url::base(true) ?>/angularjs/app/directives/common/chosen.js"></script>
    <script src="<?= Url::base(true) ?>/angularjs/app/directives/common/nav-item.js"></script>
    <script src="<?= Url::base(true) ?>/angularjs/app/directives/common/draggable.js"></script>
    <script src="<?= Url::base(true) ?>/angularjs/app/directives/common/img-preload.js"></script>
    <script src="<?= Url::base(true) ?>/angularjs/app/directives/common/load-dym-template.js"></script>
    <script src="<?= Url::base(true) ?>/angularjs/app/directives/common/jcarousel.js"></script>
    <script src="<?= Url::base(true) ?>/angularjs/app/directives/common/hint-text.js"></script>
    <script src="<?= Url::base(true) ?>/angularjs/app/directives/common/ng-thumb.js"></script>
    <script src="<?= Url::base(true) ?>/angularjs/app/directives/common/checklistModel/checklist-model.js"></script>
    <script src="<?= Url::base(true) ?>/angularjs/app/directives/common/color-progress-bar.js"></script>
    <script src="<?= Url::base(true) ?>/angularjs/app/directives/common/imgAreaSelect/ng-imgAreaSelect.js"></script>
    <script src="<?= Url::base(true) ?>/angularjs/app/directives/common/tooltip/qtip-directive.js"></script>
    <script src="<?= Url::base(true) ?>/angularjs/app/directives/common/colorPicker/color-picker-directive.js"></script>
    <script src="<?= Url::base(true) ?>/angularjs/app/directives/common/partial-css.js"></script>

    <script src="<?= Url::base(true) ?>/angularjs/app/directives/common/resize-iframe-content.js"></script>
    <script src="<?= Url::base(true) ?>/angularjs/app/directives/common/fixed-iframe.js"></script>
    <script src="<?= Url::base(true) ?>/angularjs/app/directives/common/imagemap/imagemap.js"></script>
    <script src="<?= Url::base(true) ?>/angularjs/app/directives/common/ng-click-select.js"></script>
    <script src="<?= Url::base(true) ?>/angularjs/app/directives/partials/has-dot.js"></script>
    <script src="<?= Url::base(true) ?>/angularjs/app/directives/common/current-time.js"></script>
    <script src="<?= Url::base(true) ?>/angularjs/app/directives/common/fancy-box/fancy-box.js"></script>
    <!-- *** Controllers *** -->
    <script src="<?= Url::base(true) ?>/angularjs/app/controllers/main/masterPageController.js"></script>
    <script src="<?= Url::base(true) ?>/angularjs/app/controllers/global-modal/globalModalController.js"></script>

    <!-- Some Bootstrap Helper Libraries -->
    <link rel="stylesheet" href="<?= Url::base(true) ?>/angularjs/app/css/bootstrap.min.css" type="text/css" />

    <!-- *** Third party *** -->
    <link rel="stylesheet" href="<?= Url::base(true) ?>/angularjs/app/css/ss/css/font-awesome-4.4.0/css/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="<?= Url::base(true) ?>/angularjs/app/css/color-progress-bar.css" type="text/css" />

    <!-- CSS of detail page would be loaded here later -->
</head>
<body window-exit ng-class="bodyClass">

<?php $this->beginBody() ?>
<?= $content ?>


<?php if(Yii::$app->view->params['enablePreBootstrap']){
    echo $this->render('//layouts/prebootstrap-standalone');
    ?>

    <script src="<?= Url::base(true) ?>/angularjs/app/directives/common/prebootstrap.js"></script>
<?php } ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
