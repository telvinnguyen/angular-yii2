<form name="Form<?php echo $index ?>" xt-form novalidate>
    <div class="titleBar bgGreen">
        <div class="page-title">Signsmart Content View</div>
        <storage-info si-refresh="{{refreshStorage}}"></storage-info>
        <div class="line lineGold" style="width:1021px; height:6px;"></div>
    </div>

    <div class="content_wrap textShadow">
        <div class="w1021_noscroll">
            <div class="inner_title Gold">{{pageTitle}}</div>

            <div class="inner_content">
                <div class="inner_left">
                    <table cellpadding="0" cellspacing="0" class="form fixed">
                        <tbody>
                        <tr>
                            <td align="right" nowrap="">
                                <label class="content-label" for="monthly">Monthly Rate</label>:
                                <span class="popup-field-required">*</span> &nbsp;
                            </td>
                            <td align="right" style="vertical-align:middle">
                                <input ng-model="formDetail.monthly" class="detail-input" currency="true" name="monthly" id="monthly" type="text" maxlength="20" required>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" nowrap="">
                                <label class="content-label" for="desktops_unit_price">Account Rate</label>:
                                <span class="popup-field-required">*</span> &nbsp;
                            </td>
                            <td align="right" style="vertical-align:middle">
                                <input ng-model="formDetail.desktops_unit_price" class="detail-input" currency="true" name="desktops_unit_price" id="desktops_unit_price" type="text" maxlength="10" required>
                            </td>
                        </tr>

                        <tr>
                            <td align="right" nowrap="">
                                <label class="content-label" for="storage_unit_price">Storage Rate</label>:
                                <span class="popup-field-required">*</span> &nbsp;
                            </td>
                            <td align="right" style="vertical-align:middle">
                                <input ng-model="formDetail.storage_unit_price" class="detail-input" currency="true" name="storage_unit_price" id="storage_unit_price" type="text" maxlength="10" required>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" nowrap="">
                                <label class="content-label" for="isign_price" for="isign_price">i-Sign Rate</label>:
                                <span class="popup-field-required">*</span> &nbsp;
                            </td>
                            <td align="right" style="vertical-align:middle">
                                <input ng-model="formDetail.isign_price" class="detail-input" currency="true" name="isign_price" id="isign_price" type="text" maxlength="10" required>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>

                <div class="inner_right">
                    <table cellpadding="0" cellspacing="0" class="form">

                        <tbody>
                        <tr>
                            <td align="right" nowrap="">
                                <label class="content-label" for="desktops">Accounts cap</label>:
                                <span class="popup-field-required">*</span> &nbsp;
                            </td>
                            <td>
                                <input ng-model="formDetail.desktops" class="detail-input" name="desktops" id="desktops" type="text" maxlength="20" required>
                            </td>
                        </tr>

                        <tr>
                            <td align="right" nowrap="">
                                <label class="content-label" for="software">Software level</label>:
                                <span class="popup-field-required">*</span> &nbsp;
                            </td>
                            <td>
                                <input ng-model="formDetail.software" class="detail-input" name="software" id="software" type="text" maxlength="20" required>
                            </td>
                        </tr>

                        <tr>
                            <td align="right" nowrap="">
                                <label class="content-label" for="storage">Storage Cap</label>:
                                <span class="popup-field-required">*</span> &nbsp;
                            </td>
                            <td align="right" style="vertical-align:middle">
                                <input ng-model="formDetail.storage" class="detail-input" name="storage" id="storage" type="text" maxlength="30" required>
                            </td>
                        </tr>


                        <tr>
                            <td align="right" nowrap="">
                                <label class="content-label" for="dsign_price">d-Sign Rate</label>:
                                <span class="popup-field-required">*</span> &nbsp;
                            </td>
                            <td align="right" style="vertical-align:middle">
                                <input ng-model="formDetail.dsign_price" class="detail-input" currency="true" name="dsign_price" id="dsign_price" type="text" maxlength="10" required>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                    <xt-validation-summary></xt-validation-summary>

                </div>

            </div>
        </div>
    </div>
    <div class="line Gold" style="height:5px;"></div>
    <div class="clr"></div>
    <div class="docbar">
        <ul class="topIcon">
            <li>
                <label>Actions:</label>
                <?php foreach ($stepButtons as $eventName => $btnName) {
                    if (strtolower($eventName) == 'submit') {
                        ?>
                        <button type="submit" class="jqitbutton" ng-click="Form<?php echo $index ?>.$valid && <?php echo $eventName ?>()"><?php echo $btnName ?></button>
                    <?php }
                    elseif (strtolower($eventName) == 'uploadimage') {
                        ?>
                        <button upload-more-image remote="remote" umi-form-info="formInfo" umi-loader="formLoading" type="button" class="jqitbutton">Upload Image</button>
                    <?php }
                    else { ?>
                        <button type="button" class="jqitbutton" ng-click="<?php echo $eventName ?>()"><?php echo $btnName ?></button>
                    <?php }
                } ?>
                <button type="button" class="jqitbutton" ng-click="switch()">Switch</button>

                <div loader working="bodyLoader" template="3"></div>
                <div class="clr"></div>
            </li>
        </ul>
        <div class="clr"></div>
    </div>
</form>