//===========================================1:BEGIN: <?php echo $camelCaseName ?> ========================================================
.state('main.<?php echo $name ?>', {
    url: '/<?php echo $name ?>/:acc_id/mtype={mtype}/oid={oid}/ovid={ovid}',
    views: {
        '<?php echo $name ?>@main': {
            controller: '<?php echo $camelCaseName ?>Controller',
            templateUrl: _yii_app.controllerPath + '/<?php echo $name ?>/<?php echo $name ?>.html'
        }
    },
    data: {
        css: [
            _yii_app.angularPath + '/app/controllers/<?php echo $name ?>/<?php echo $name ?>.css',
            _yii_app.angularPath + '/app/css/custom.css',
            _yii_app.angularPath + '/app/css/ss/css/screen.css',
            _yii_app.angularPath + '/app/css/ss/css/new_content_database.css',
            _yii_app.angularPath + '/app/css/ss/css/addcontent121212.css'
        ], requiresLogin: true
    },
    resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            // you can lazy load files for an existing module
            return $ocLazyLoad.load({
                name: 'zwenGlobal',
                files: [
                    _yii_app.controllerPath + '/<?php echo $name ?>/<?php echo $camelCaseName ?>Controller.js',
                    //_yii_app.controllerPath + '/livePath/<?php echo $camelCaseName ?>Controller.min.js'<?php foreach($routeStateDeps as $deps){ echo "
                    $deps"; }?>

                ]
            });
        }]
    }

})
//===========================================1:END: <?php echo $camelCaseName ?> ========================================================