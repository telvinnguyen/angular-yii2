<?php use yii\helpers\Url; ?>

<?php echo $this->render('@frontend/views/layouts/global-settings', ['app' => \Yii::$app]) ?>

<script src="<?= Url::to('@web/angularjs/lib/angular-1.3.14/angular.min.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/lib/vendor/ui-codemirror/src/ui-codemirror.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/lib/ui-bootstrap-tpls-0.11.2.min.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/lib/vendor/angular-ui-router/release/angular-ui-router.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/lib/angular-1.3.14/angular-route.min.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/lib/vendor/a0-angular-storage/dist/angular-storage.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/lib/ui-router-extras/ct-ui-router-extras.min.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/lib/vendor/angular-ui-utils/ui-utils.min.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/lib/vendor/dialog/dialogs.min.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/lib/vendor/ngLoader/ngLoader.js') ?>"></script>

<script src="<?= Url::to('@web/angularjs/lib/angular-1.3.14/angular-resource.min.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/lib/angular-1.3.14/angular-sanitize.min.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/app/services/ssHelper.js') ?>"></script>

<script src="<?= Url::to('@web/angularjs/lib/underscore-min.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/lib/underscore.string.min.js') ?>"></script>

<link rel="stylesheet" href="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/lib/codemirror.css') ?>">
<link rel="stylesheet" href="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/addon/fold/foldgutter.css') ?>">
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/lib/codemirror.js') ?>"></script>


<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/addon/fold/foldcode.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/addon/fold/foldgutter.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/addon/fold/brace-fold.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/addon/fold/xml-fold.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/addon/fold/markdown-fold.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/addon/fold/comment-fold.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/mode/javascript/javascript.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/mode/xml/xml.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/mode/markdown/markdown.js') ?>"></script>

<link rel="stylesheet" href="<?= Url::to('@web/angularjs/lib/vendor/ng-tags-input/ng-tags-input.min.css') ?>">
<link rel="stylesheet" href="<?= Url::to('@web/angularjs/lib/vendor/ng-tags-input/ng-tags-input.bootstrap.min.css') ?>">
<script src="<?= Url::to('@web/angularjs/lib/vendor/ng-tags-input/ng-tags-input.min.js') ?>"></script>

<style>
    .row {
        margin-top: 20px;
    }

    tags-input{
        width: 100%;
        display: block;
    }
    .left-panel {
        float: left;
    }

    .left-panel img {
        width: 48px;
        height: 48px;
        vertical-align: middle;
    }

    .right-panel {
        float: left;
        margin-left: 5px;
        margin-top: 7px;
    }

    .right-panel span:first-child {
        font-size: 16px;
    }

    .right-panel span:nth-child(2) {
        font-size: 14px;
        color: gray;
    }

    .right-panel span:last-child {
        display: block;
        font-size: 14px;
        font-style: italic;
    }



</style>

<div ng-controller="MyController">

    <form action="<?php yii\helpers\Url::toRoute('generator/quicksavetoolbar') ?>" method="post" class="form-inline">
        <div loader working="mainLoader" template="3"></div>
        <br/>

        <!-- COLLECTION -->
        <div class="row">
            <label for="stepInput">Collection ids:</label>


            <tags-input ng-model="cids" display-property="name" placeholder="Add an id" replace-spaces-with-dashes="false">
                <auto-complete source="suggestCollection($query)"
                               min-length="0"
                               load-on-focus="true"
                               load-on-empty="true"
                               max-results-to-show="32"
                               template="my-custom-template"></auto-complete>
            </tags-input>

            <script type="text/ng-template" id="my-custom-template">
                <div class="left-panel">
                    <img ng-src="{{data.icon}}" />
                </div>
                <div class="right-panel">
                    <span ng-bind-html="$highlight($getDisplayText())"></span>
                    <span>({{data.name}})</span>
                    <span>{{data.extension}}</span>
                </div>
            </script>
        </div>
        <!-- :COLLECTION -->

        <!-- GROUP -->
        <div class="row">
            <label for="stepInput">Group ids:</label>


            <tags-input ng-model="gids" display-property="name" placeholder="Add an id" replace-spaces-with-dashes="false">
                <auto-complete source="suggestGroup($query)"
                               min-length="0"
                               load-on-focus="true"
                               load-on-empty="true"
                               max-results-to-show="32"
                               template="my-custom-template"></auto-complete>
            </tags-input>

            <script type="text/ng-template" id="my-custom-template">
                <div class="left-panel">
                    <img ng-src="{{data.icon}}" />
                </div>
                <div class="right-panel">
                    <span ng-bind-html="$highlight($getDisplayText())"></span>
                    <span>({{data.name}})</span>
                    <span>{{data.extension}}</span>
                </div>
            </script>
        </div>
        <!-- :GROUP -->


        <!-- ITEM -->
        <div class="row">
            <label for="stepInput">Item ids:</label>


            <tags-input ng-model="iids" display-property="name" placeholder="Add an id" replace-spaces-with-dashes="false">
                <auto-complete source="suggestItem($query)"
                               min-length="0"
                               load-on-focus="true"
                               load-on-empty="true"
                               max-results-to-show="32"
                               template="my-custom-template"></auto-complete>
            </tags-input>

            <script type="text/ng-template" id="my-custom-template">
                <div class="left-panel">
                    <img ng-src="{{data.icon}}" />
                </div>
                <div class="right-panel">
                    <span ng-bind-html="$highlight($getDisplayText())"></span>
                    <span>({{data.name}})</span>
                    <span>{{data.extension}}</span>
                </div>
            </script>
        </div>
        <!-- :ITEM -->


        <div class="navbar"></div>

    </form>
    <div class="navbar"></div>
    <div class="form-group">
        <button ng-click="submit()" class="btn btn-success">Submit</button>
    </div>

</div>


<script>
    var genApp = angular.module('generatorApp', ['ui.codemirror', 'ngTagsInput', 'dialogs.main', 'ssFactories', 'ngLoader']);

    /*
    genApp.config(function(tagsInputConfigProvider) {
        tagsInputConfigProvider
            .setDefaults('tagsInput', {
                placeholder: 'New tag',
                minLength: 5,
                addOnEnter: false
            })
            .setDefaults('autoComplete', {
                debounceDelay: 200,
                loadOnDownArrow: true,
                loadOnEmpty: true
            })
    });
    */

    angular.module('generatorApp').controller('MyController', function($scope, ssHelper) {

        $scope.mainLoader = false;
        $scope.cids = [];
        $scope.suggestCollection = function (query) {
            console.log(query)
            var _list = [], _item;
            return ssHelper.post('/toolbar/getToolbar', {
                        name: query,
                        mtype: 'Collection'

                    }).then(function (response) {

                        //$scope.$apply(function(){})
                        if (response.data.OK == 1) {
                            _.each(response.data.list, function(item){
                                _item = {
                                    id: item.collection_id,
                                    name: item.collection_name,
                                    extension: item.collection_extension,
                                    icon: s.sprintf('%s/uploads/systemresources/%s', _yii_app.baseUrl, item.icon_image)
                                }
                                _list.push(_item)
                            })
                        }
                        return _list;
                    });
        };

        $scope.gids = [];
        $scope.suggestGroup = function (query) {
            console.log(query)
            var _list = [], _item;
            return ssHelper.post('/toolbar/getToolbar', {
                name: query,
                mtype: 'Group'

            }).then(function (response) {

                //$scope.$apply(function(){})
                if (response.data.OK == 1) {
                    _.each(response.data.list, function(item){
                        _item = {
                            id: item.group_id,
                            name: item.group_name,
                            extension: item.group_extension,
                            icon: s.sprintf('%s/uploads/systemresources/%s', _yii_app.baseUrl, item.icon_image)
                        }
                        _list.push(_item)
                    })
                }
                return _list;
            });
        };

        $scope.iids = [];
        $scope.suggestItem = function (query) {
            console.log(query)
            var _list = [], _item;
            return ssHelper.post('/toolbar/getToolbar', {
                name: query,
                mtype: 'Item'

            }).then(function (response) {

                //$scope.$apply(function(){})
                if (response.data.OK == 1) {
                    _.each(response.data.list, function(item){
                        _item = {
                            id: item.item_id,
                            name: item.item_name,
                            extension: item.item_extension,
                            icon: s.sprintf('%s/uploads/systemresources/%s', _yii_app.baseUrl, item.icon_image)
                        }
                        _list.push(_item)
                    })
                }
                return _list;
            });
        };

        $scope.submit = function () {
            $scope.mainLoader = true;
            ssHelper.post('/dev/QuickSaveToolbar', {
                        cids: $scope.cids,
                        gids: $scope.gids,
                        iids: $scope.iids

                    }).then(function (response) {
                        //$scope.$apply(function(){})
                        if (response.data.OK == 1) {
                            $scope.mainLoader = false;
                        } else {
                            //var errors = response.data.errors
                        }
                    });
        };
    });

    angular.element(document).ready(function () {
        angular.bootstrap(document, ['generatorApp']);

    });
</script>
