<form name="Form<?php echo $index ?>" xt-form novalidate>
    <div class="titleBar bgGreen">
        <div class="page-title">Signsmart Content View</div>
        <storage-info si-refresh="{{refreshStorage}}" remote="remote"></storage-info>
        <div class="line lineGold" style="width:1021px; height:6px;"></div>
    </div>

    <div class="content_wrap textShadow">
        <div class="w1021_noscroll">
            <div class="inner_title Gold">{{pageTitle}}</div>

            <div class="inner_content">
                <div class="SearchPanel formSearch">
                    <table style="width: 100%">
                        <tbody>
                        <tr>
                            <td style="vertical-align:middle; width: 8%">

                                <label class="content-label" for="name" field_name="Name">Name</label>:
                                <span class="popup-field-required">*</span> &nbsp;
                            </td>
                            <td style="vertical-align:middle">
                                <input class="popup-detail-input" name="name" id="name" type="text" ng-model="formInfo.name" required>
                            </td>

                        </tr>
                        </tbody>
                    </table>
                    <table>
                        <tbody>
                        <tr>
                            <td colspan="4">
                                <table class="select_item_wrapper">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <b>Select an image in the account if you would like a
                                               custom
                                               Icon for this object</b>

                                            <div class="clr"></div>
                                        </td>

                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="clr"></div>
                </div>
                <div class="clr"></div>
                <div class="rp_content">
                    <item-variant-selection remote="remote" selected-ovid="formInfo.selected_ovid" list-oid="[2]" trigger-id="{{updateStatusId}}" multiple='true'></item-variant-selection>

                    <!-- current object variant info -->
                    <!-- this always return to tell which type ov would be selected-->
                </div>

            </div>
        </div>
    </div>
    <div class="clr"></div>
    <div class="docbar">
        <ul class="topIcon">
            <li>
                <label>Actions:</label>
                <?php foreach ($stepButtons as $eventName => $btnName) {
                    if (strtolower($eventName) == 'submit') {
                        ?>
                        <button type="submit" class="jqitbutton" ng-click="Form<?php echo $index ?>.$valid && <?php echo $eventName ?>()"><?php echo $btnName ?></button>
                    <?php }
                    elseif (strtolower($eventName) == 'uploadimage') {
                        ?>
                        <button upload-more-image remote="remote" umi-form-info="formInfo" umi-loader="formLoading" type="button" class="jqitbutton">Upload Image</button>
                    <?php }
                    else { ?>
                        <button type="button" class="jqitbutton" ng-click="<?php echo $eventName ?>()"><?php echo $btnName ?></button>
                    <?php }
                } ?>
                <button type="button" class="jqitbutton" ng-click="switch()">Switch</button>

                <div loader working="bodyLoader" template="3"></div>
                <div class="clr"></div>
            </li>
        </ul>
        <div class="clr"></div>
    </div>
    <xt-validation-summary></xt-validation-summary>
</form>