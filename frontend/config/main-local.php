<?php
$params = array_merge(
    //require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    //require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

/* do some little bit stuff to hide the frontend/web from url

add the .htaccess file into the yii app root folder
modify the .htaccess file in the frontend/web/ folder

*/

use \yii\web\Request;
$baseUrl = str_replace('/frontend/web', '', (new Request)->getBaseUrl());
$config = [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,

            /* use login by token instead of session & cookies*/
//            'enableSession' => false,
//            'loginUrl' => null
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'categories' => ['application', 'yii\web\HttpException:*']
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'request' => [
            'baseUrl' => $baseUrl,
            'cookieValidationKey' => '0ArUHfVS-bwYwlBq6rkSVWmXrWQq1SgL',
            'enableCsrfValidation'=>false
        ],

        'assetManager' => [
            'linkAssets' => true
        ],

        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'baseUrl' => $baseUrl,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,

            'rules' => [
                '<controller>/<action>' => '<controller>/<action>',
                '<controller>/<action>/<angular1:.+>' => '<controller>/<action>',
                '<controller>/<action>/<angular1:.+>/' => '<controller>/<action>',
                '<controller>/<action>/<angular1:.+>/<angular2:.+>' => '<controller>/<action>',
                '<controller>/<action>/<angular1:.+>/<angular2:.+>/' => '<controller>/<action>',
                '<controller>/<action>/<angular1:.+>/<angular2:.+>/<angular3:.+>/' => '<controller>/<action>',
            ],
        ]
    ],
    'params' => $params,
];

if (!YII_ENV_TEST) {
    // configuration adjustments for 'dev' environment
//     $config['bootstrap'][] = 'debug';
//     $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;