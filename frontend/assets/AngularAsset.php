<?php

namespace frontend\assets;
use yii\web\AssetBundle;

class AngularAsset extends AssetBundle
{
    /**
     * @sourcePath specifies the root directory that contains the asset files in this bundle. This property should be set if the root directory is not Web accessible. Otherwise, you should set the basePath property
     */
    public $baseUrl = '@web/angularjs/';
    public $basePath = '@webroot/angularjs/';

    public $js = [
        /*** Main scripts */
        'lib/jquery/jquery-1.10.2.min.js',
        'lib/angular-1.3.14/angular.min.js',

        /*** Secondary third party scripts ***/
        'lib/vendor/ocLazyLoad/dist/ocLazyLoad.min.js',
        'lib/angular-css.min.js',

        'lib/vendor/ng-idle/angular-idle.min.js',
        'lib/ui-bootstrap-tpls-0.11.2.min.js',
        'lib/angular-1.3.14/angular-route.min.js',
        'lib/angular-1.3.14/angular-animate.min.js',
        'lib/angular-1.3.14/angular-cookies.min.js',
        'lib/angular-1.3.14/angular-loader.min.js',
        //'lib/angular-1.3.14/angular-mocks.js', // for testing
        'lib/angular-1.3.14/angular-resource.min.js',
        'lib/angular-1.3.14/angular-sanitize.min.js',
        //'lib/angular-1.3.14/angular-scenario.js',
        'lib/angular-1.3.14/angular-touch.min.js',
        'lib/ui-router-extras/ct-ui-router-extras.min.js',
        'lib/bootstrap.min.js',
        'lib/underscore-min.js',
        'lib/underscore.string.min.js',
        'lib/vendor/angular-websocket/angular-websocket.min.js',
        'lib/angular-validation.min.js',
        'lib/angular-validation-rule.min.js',
        'lib/vendor/ngLoader/ngLoader.js',
        'lib/vendor/angular-ui-router/release/angular-ui-router.js',
        'lib/vendor/a0-angular-storage/dist/angular-storage.js',
        'lib/vendor/angular-jwt/dist/angular-jwt.js',
        'lib/vendor/ui-router-styles.js',
        'lib/vendor/angular-file-upload.js',

        'lib/vendor/jcarousel/jquery.jcarousel.min.js',
        'lib/vendor/angular-post-message/angular-post-message.min.js',

        'lib/vendor/ng-clip-0.2.6/dest/ng-clip.min.js',
        'lib/vendor/ng-clip-0.2.6/dest/ZeroClipboard.min.js',

        'lib/vendor/angular-ui-utils/ui-utils.min.js',
        'lib/vendor/dialog/dialogs.min.js',
        'lib/vendor/chosen/chosen.jquery.js',

        'lib/vendor/angular-noty/jquery.noty.packaged.min.js',
        'lib/vendor/angular-noty/angular-noty.js',

        'lib/vendor/xtForm-validation/xtForm.tpl.js',
        'lib/vendor/angular-iscroll/dist/lib/iscroll.min.js',
        'lib/vendor/angular-iscroll/dist/lib/angular-iscroll.js',
        'lib/vendor/angular-local-storage/dist/angular-local-storage.min.js',
        'lib/vendor/heartcode-canvasloader-min-0.9.1.js',

    ];

    public $css = [
        'lib/vendor/angular-noty/animate.css',
        'lib/vendor/ngLoader/loading.css',
        //'lib/vendor/jquery-ui-1.11.4/jquery-ui.min.css',
        'lib/vendor/chosen/chosen.css',
        'lib/vendor/angular-iscroll/dist/lib/scss/_iscroll.css'
    ];

    public $jsOptions = [ 'position' => \yii\web\View::POS_HEAD ];

    public $depends = [
        //'frontend\assets\LoaderAsset',
    ];
}