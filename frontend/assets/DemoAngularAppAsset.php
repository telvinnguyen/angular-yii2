<?php

namespace frontend\assets;
use yii\web\AssetBundle;

class DemoAngularAppAsset extends AssetBundle
{
    /**
     * @sourcePath specifies the root directory that contains the asset files in this bundle. This property should be set if the root directory is not Web accessible. Otherwise, you should set the basePath property
     */
    public $baseUrl = '@web/angularjs/';
    public $basePath = '@webroot/angularjs/';

    public $js = [
        'app/apps/demo-app.js',
        'app/apps/demo-routes.js',

        //services
        'app/services/ssHelper.js',
        'app/services/yii-data.js',
        'app/services/signal-handler.js',
        'app/services/modal-utils.js',

        //directives - common
        'app/directives/common/demo-header-1.js',
        'app/directives/common/nav-item.js',
        'app/directives/common/draggable.js',
        'app/directives/common/img-preload.js',

        //directives - partials
        'app/directives/partials/demo-top-nav.js',

    ];

    public $jsOptions = [ 'position' => \yii\web\View::POS_HEAD ];

    public $css = [

    ];

    public $depends = [
        'frontend\assets\AngularAsset',
    ];
}