app.directive('uploadMultipleFiles', function ($timeout, ssHelper,$stateParams, FileUploader) {

    return {
        restrict: 'E',
        templateUrl: _yii_app.directivePath + '/partials/upload-multiple-files/upload-multiple-files.html',
        scope: {
            listFiles: '=?', // list id of file will return when uploaded. define: move file to temp folder | no define : save as item variant to DB
            totalItem: '=?',
            zipFile  : '=?'  // return response to ZIP file
        },
        //bindToConroller: true,
        controller: function($scope, dialogs ){
        	
        	$scope.baseUrl = _yii_app.baseUrl;
        	$scope.layoutPath = _yii_app.layoutPath;
		    $scope.acc_id = $stateParams.acc_id;
		    $scope.oid = $stateParams.oid;
		    $scope.wtype = $stateParams.wtype;
		    $scope.wovid = $stateParams.wovid;
		    $scope.wgvid = $stateParams.wgvid;
		    $scope.mtype = $stateParams.mtype;
	
		    $scope.totalItem = 0;
		    $scope.totalSize = 0;
		    $scope.totalSize_toshow = 0;
		    $scope.queueSuccess = 0;
		    $scope.totalProgress = 0;
		    $scope.showStartbtn = false;
		    $scope.showstatus = false;
		    $scope.height = {height: '100%'};
		    $scope.fileUpload = [];
		    $scope.viewMode = 'thumbs';
		    
		    
		    $scope.remainStorage = 0;
		    ssHelper.post('/subscribe/GetRemainStorage', {acc_id: $scope.acc_id}).then(function (response) {
		        if (response.data.OK == 1) {
		            $scope.remainStorage = response.data.remainStorage
		        }
		    });
		    	
		    if(!angular.isDefined($scope.listFiles)){
		    	if ($scope.oid != 2) {
			        var validFileExtensions = '|jpg|png|jpeg|gif|svg+xml|pdf|msword|vnd.openxmlformats-officedocument.wordprocessingml.document|plain|vnd.openxmlformats-officedocument.spreadsheetml.sheet|vnd.ms-excel|mp3|x-m4a|mp4|webm|x-ms-wmv|mpeg|quicktime|avi|';
			        var url = '/file/multipleuploadingforzip';
			        $scope.height = {height: '70%'};
		    	}else{
		    		 var validFileExtensions = '|jpg|png|jpeg|gif|svg+xml|';
				     var url = '/file/uploadandsaveimagevariant';
		    	}
		    }else{
			    	 var validFileExtensions = '|jpg|png|jpeg|gif|svg+xml|pdf|msword|vnd.openxmlformats-officedocument.wordprocessingml.document|plain|vnd.openxmlformats-officedocument.spreadsheetml.sheet|vnd.ms-excel|mp4|webm|x-ms-wmv|mpeg|quicktime|avi|';
				     var url = '/file/uploadtempfile'
		    }
	
		    var uploader = $scope.uploader = new FileUploader({
		        headers: {'Accept': 'application/json'},
		        url: $scope.baseUrl + url, // /File/UploadAndSaveImageVariant
		        formData: [{acc_id: $scope.acc_id, wovid: $scope.wovid, wgvid: $scope.wgvid, checkStorage: '1', total_item: $scope.totalItem}],
		        views: {
		            list: true,
		            thumbs: true, // Show thumbs
		            active: 'list'
		        }
		    });
	
		    uploader.filters.push({
		        name: 'imageFilter',
		        fn: function (item /*{File|FileLikeObject}*/, options) {
		            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
		            var _validFileExtensions = validFileExtensions.indexOf(type) !== -1;
		            var enoughStorage = $scope.remainStorage > item.size; //compare the file size with the remain storage of user
		            
		            var ext = item.name.slice(item.name.lastIndexOf('.') + 1);
		            var extension = ext != 'csv';
		            return _validFileExtensions && enoughStorage && extension;
	
		        }
		    });
		    uploader.onWhenAddingFileFailed = function (item, filter, options) {
		        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
		        var _validFileExtensions = validFileExtensions.indexOf(type) !== -1;
		        
		        var enoughStorage = $scope.remainStorage > item.size;
		        var ext = item.name.slice(item.name.lastIndexOf('.') + 1);
		        var extension = ext != 'csv';
		        if ($scope.turn == true) { // flag using set diaglog run one time
		            if(!_validFileExtensions || !extension)
			            dialogs.notify('Upload Error', 'File type is invalid.', {size: 'md'})
			        else if(!enoughStorage){
			        	if($scope.acc_id == '')
			        		dialogs.notify('Upload Error', 'Your storage is not enough to upload this file.', {size: 'md'})
			        	else
			        		dialogs.notify('Upload Error', 'This account has no available space for uploads.<br>Please select the “STORAGE” Action when in your personal account profile and allocate storage to this sub-account.', {size: 'md'})
			        }
		            $scope.turn = false;
		        }
		    }
	
		    uploader.onSuccessItem = function (fileItem, responseData, status, headers) {
		        if (responseData.OK == 1) {
		        	 if(angular.isDefined($scope.zipFile))
		        		$scope.zipFile.push(responseData)
		        	else if(angular.isDefined($scope.listFiles))
		        		$scope.listFiles.push({fileName:responseData.fileName, genFileName:responseData.genFileName});
		        	 
		        	$scope.fileUpload.push({fileName:responseData.fileName, genFileName:responseData.genFileName});
		            // number of image upload success
		            $scope.queueSuccess = $scope.fileUpload.length;
	
		        } else {
		            ssHelper.errorLog(responseData.errors);
		        }
		    };
	
		    $scope.triggerFileInput = function () {
		        angular.element('#file-input').trigger('click');
		        $scope.turn = true; // flag using set diaglog run one time
		    }
	
		    $scope.done = function () {
		        $scope.$broadcast('update-storage-info', {reload: true})
	
		        ssHelper.doneSaveIvAndReturn('', $scope.wovid, $scope.wgvid, '', $scope.remote);
		    }
	
	
		    uploader.onAfterAddingFile = function (fileItem) {
	
		        $scope.showStartbtn = true;
		        ssHelper.debugLog('onAfterAddingFile', fileItem);
		        $scope.totalItem = $scope.totalItem + 1;
		        //get total size
		        var _totalSize = $scope.totalSize + fileItem.file.size;
		        if(_totalSize > $scope.remainStorage){
		        	 fileItem.remove();
		        	 if($scope.turn != undefined){
			    		 dialogs.notify('Upload Error', 'This account has no available space for uploads.<br>Please select the “STORAGE” Action when in your personal account profile and allocate storage to this sub-account.', {size: 'md'})
			    		 $scope.turn = undefined;
		        	 }
		    	}else{
		    		 $scope.totalSize = $scope.totalSize + fileItem.file.size;
		    		 $scope.totalSize_toshow = ssHelper.convertToShow($scope.totalSize);
		    	}
		    };
		    uploader.onAfterAddingAll = function (addedFileItems) {
		    };
		    $scope.removeTotalItem = function (item) {
		        $scope.totalItem = $scope.totalItem - 1;
		        $scope.totalSize = $scope.totalSize - item.size;
		        $scope.totalSize_toshow = ssHelper.convertToShow($scope.totalSize);
		        if (uploader.queue.length == 0){
		        	uploader.progress = "0";
		        	$scope.showStartbtn = false;
		        }
		        
		    };
	
	
		    /**
		     * remove image of list upload
		     */
		    $scope.removeTempImg = function (item) {
		        if ($scope.listFiles != '' || $scope.zipFile != '') {
		            var stop = false;
		            
		            if(angular.isDefined($scope.zipFile)){
		            	angular.forEach($scope.zipFile, function (img, i) {
			                if (item.name == img.fileName && stop == false) {
			                    $scope.zipFile.splice(i, 1);
			                    $scope.queueSuccess = $scope.queueSuccess - 1;
			                    $scope.totalSize = $scope.totalSize - item.size;
			                    $scope.totalSize_toshow = ssHelper.convertToShow($scope.totalSize);
			                    if ($scope.totalSize == 0) uploader.progress = "0";
			                    stop = true;
		
			                }
			            });
		            }else{
		            	angular.forEach($scope.listFiles, function (img, i) {
			                if (item.name == img.fileName && stop == false) {
			                    $scope.listFiles.splice(i, 1);
			                    $scope.queueSuccess = $scope.queueSuccess - 1;
			                    $scope.totalSize = $scope.totalSize - item.size;
			                    $scope.totalSize_toshow = ssHelper.convertToShow($scope.totalSize);
			                    if ($scope.totalSize == 0) uploader.progress = "0";
			                    stop = true;
		
			                }
			            });
		            }
		           
		        }
		    }
	
		    uploader.onProgressAll = function (progress) {
		        $scope.showStartbtn = false; // flag show start upload button.
		        $scope.showstatus = true; //flag show status bar : capacity, process, number of file uploaded
		    };
		    uploader.onCompleteAll = function () {
		        $scope.showstatus = false;
		    };
		    uploader.onCancelItem = function (item, response, status, headers) {
		        $scope.showStartbtn = true;
		    };
        	
        	
        },
        link: function (scope, element, attrs) {
        }
    };
});

angular.module('zwenGlobal').filter('split', function () {
	   return function(input, splitChar, splitIndex) {
	       return input.split(splitChar)[splitIndex];
	   }
	});
