angular.module('zwenGlobal').directive('frameMatrix', function ($timeout, $sce, $compile, ssHelper) {
    function beautify(current_iframe, boxWidth, boxHeight) {
        //console.log('beautify iframe')
        var useZoomerPlugin = true;

        var t = $(current_iframe).get(0).contentWindow.document.body;
        var sizeLocalNode = $(t).find('ssize');

        var ratioAspect = 1, scaleRatio = 1;
        var innerContentWidth = 0, innerContentHeight = 0;

        if (sizeLocalNode.length > 0) {
            innerContentWidth = sizeLocalNode.data('width');
            innerContentHeight = sizeLocalNode.data('height');
        } else {
            var a = $(current_iframe).get(0).contentWindow.document.body;
            var b = $(a).children(':not(script, style)').first();
            var firstNodeName_AfterBody = '';
            if (angular.isDefined(b[0]))
                firstNodeName_AfterBody = b[0].nodeName.toLowerCase();
            var c = $(current_iframe).contents().find("key").text();

            if (firstNodeName_AfterBody == 'key') {
                if (c == '.video') {
                    innerContentWidth = $(current_iframe).contents().find("#video-container").width();
                    innerContentHeight = $(current_iframe).contents().find("#video-container").height();
                } else if (c == '.scrollmsg') {
                    innerContentWidth = $(current_iframe).contents().find("#marquee-container").width();
                    innerContentHeight = $(current_iframe).contents().find("#marquee-container").height();
                }
            }
            else if (firstNodeName_AfterBody == 'table' || firstNodeName_AfterBody == 'p') {
                innerContentWidth = $(current_iframe).contents().find("table").width();
                innerContentHeight = $(current_iframe).contents().find("table").height();
                ;
            } else if (firstNodeName_AfterBody == 'div') {
                innerContentWidth = $(current_iframe).contents().find("div").first().width();
                innerContentHeight = $(current_iframe).contents().find("div").first().height();

            } else if (firstNodeName_AfterBody == 'form') {
                innerContentWidth = $(current_iframe).contents().find("div").width();
                innerContentHeight = $(current_iframe).contents().find("div").height();
            }
            else {
                innerContentWidth = $(current_iframe).contents().find("body").width();
                innerContentHeight = $(current_iframe).contents().find("body").height();
                if (firstNodeName_AfterBody == 'img') { /*isImageUrl = true; */
                }
            }
        }


        var body = $(current_iframe).contents().find("body");
        var backupStyle = body.attr('style')

        body.css('display', 'inline-table');
        setTimeout(function () {

            innerContentWidth = body.width()
            innerContentHeight = body.height()

            body.removeAttr('style')
            if (angular.isDefined(backupStyle)) body.attr('style', backupStyle)


            var userScreenWidth = boxWidth,
                userScreenHeight = boxHeight, epw, eph;
            ratioAspect = ssHelper.roundNumber(innerContentWidth / innerContentHeight, 2)
            if (innerContentWidth > userScreenWidth) {
                epw = userScreenWidth
                eph = ssHelper.roundNumber(epw / ratioAspect, 2)
                if (eph > userScreenHeight) {
                    eph = userScreenHeight
                    //epw = eph*ratioAspect
                    scaleRatio = ssHelper.roundNumber(userScreenHeight / innerContentHeight, 2)
                } else {
                    scaleRatio = ssHelper.roundNumber(userScreenWidth / innerContentWidth, 2)
                }
            }
            else if (innerContentHeight > userScreenHeight) {
                eph = userScreenHeight
                epw = eph * ratioAspect
                if (epw > userScreenWidth) {
                    epw = userScreenWidth
                    //eph = ssHelper.roundNumber(epw/ratioAspect, 2)
                    scaleRatio = ssHelper.roundNumber(userScreenWidth / innerContentWidth, 2)
                } else {
                    scaleRatio = ssHelper.roundNumber(userScreenHeight / innerContentHeight, 2)
                }
            }

            //console.log(innerContentWidth, innerContentHeight, scaleRatio)
            if (useZoomerPlugin) {
                current_iframe.zoomer({
                    width: boxWidth,
                    height: boxHeight,
                    zoom: scaleRatio,
                    scrollWidth: 0
                });
                //console.log(boxWidth, boxHeight, scaleRatio, 'zoomer')
            } else {
                var partContent = $(current_iframe).contents().find(attr.scaleSelector)
                if (angular.isDefined(partContent)) {
                    partContent.css('zoom', '1.00');
                    partContent.css('-moz-transform-origin', '0.0');
                    partContent.css('-webkit-transform-origin', '0.0');
                    partContent.css('-moz-transform', s.sprintf('scale(%1$s)', scaleRatio));
                    partContent.css('-o-transform', s.sprintf('scale(%1$s)', scaleRatio));
                    partContent.css('-webkit-transform', s.sprintf('scale(%1$s)', scaleRatio));
                    partContent.css('-webkit-transform-origin-x', '0');
                    partContent.css('-webkit-transform-origin-y', '0');
                }
            }
            $(current_iframe).contents().find('body').css({overflow: 'hidden'});
        }, 1000)


        //$(current_iframe).css('width', innerContentWidth);
        //$(current_iframe).css('height', innerContentHeight);
    }

    return {
        restrict: 'E',
        //scope: {},
        scope: {box: '=fmBox'},
        link: function (scope, element, attrs) {
            scope.url = '';

            var url, boxElem, contentExt;
            var loadOnce = false;

            scope.$watch('box.data.iframe_uid', function (iframe_uid) {
                console.log('!!!!!!!!!!!!!!!! watch', scope.box)
                console.log('frame-matrix: box content(or iframe html) should be loaded', iframe_uid)

                //$timeout(function () {
                    //console.log('trigger frame-matrix change', $(element).html() )
                    //console.log(elementBody)
                    //get the content under of the frame-matrix tag
                    boxElem = $(element).find('preview-kiosk-item .box').eq(0);
                    contentExt = boxElem.data('toolbar-ext'); // take the toolbar extension (ObjectToolbar Item extension)

                    console.log('contentExt', contentExt)


                    scope.notScroll = s.toBoolean(attrs.notScroll);
                    scope.isSource = s.toBoolean(attrs.isSource);

                    if (contentExt == '.url') { // load iframe inline
                        //console.log('!!!!!!!!!!!!!!!!', scope.box)
                        var fakeIframe = $(element).find('iframe[fake-src]');
                        console.log('fakeIframe', fakeIframe, fakeIframe.attr('fake-src'), fakeIframe.length)

                        if (fakeIframe.length > 0) {
                            //console.log('hey good')
                            var boxWidth = $(element).data('box-w');
                            var boxHeight = $(element).data('box-h');

                            var _url;
                            //if(scope.url != fakeIframe.attr('fake-src')){
                            loadOnce = false; //prevent webkit load twice

                            scope.url = fakeIframe.attr('fake-src')
                            fakeIframe.removeAttr('fake-src')
                            _url = $sce.trustAsResourceUrl(scope.url);


                            fakeIframe.attr('src', _url)

                            fakeIframe.on('load', function () {
                                //console.log('iframe of frame matrix onload')
                                if (!loadOnce) {
                                    loadOnce = true;
                                    beautify(fakeIframe, boxWidth, boxHeight)
                                }

                            })
                            //}
                        }
                    }
                    else if (contentExt == '.bookmark' && scope.isSource) { // check if the box enable the redirection & is bookmark
                        //console.log("contentExt == '.bookmark' && scope.isSource")
                        boxElem.attr('href', 'javascript:void(0)');
                    }
                    else if (!scope.notScroll) { // check if the box enable the redirection & is bookmark
                        //console.log("the box enable the redirection & is bookmark")
                        // make the anchor is clickable
                        $(boxElem).attr('href', boxElem.attr('fake-href'))
                    }

                }, true)
            //});

            scope.click = function (box) {}
        }
    };
});