angular.module('zwenGlobal').directive('miniSearchBox', function ($timeout, $ocLazyLoad, ssHelper) {
    return {
        restrict: 'A',
        scope: {
            keyword: '=miniSearchBox'
        },
        templateUrl: _yii_app.directivePath + '/partials/mini-search-box/mini-search-box.html',
        controller: function ($scope) {
            $ocLazyLoad.load(_yii_app.directivePath + '/partials/mini-search-box/mini-search-box.css');
        },
        link: function(scope, element){
            $(".keyword_search_box").on('focus', function () {
                $(this).parent('label').addClass('active');
            });

            $(".keyword_search_box").on('blur', function () {
                if($(this).val().length == 0)
                    $(this).parent('label').removeClass('active');
            });
        }
    };
});

