angular.module('zwenGlobal').directive('gridItemHover01', function ($timeout, $ocLazyLoad, ssHelper) {
    return {
        restrict: 'A',
        scope: {
            src: '@ngSrc',
            text: '@ngText',
            size: '@ngSize',
        },
        transclude: true,
        templateUrl: _yii_app.directivePath + '/effect/grid-item-hover-01/grid-item-hover-01.html',
        controller: function ($scope) {
            $ocLazyLoad.load(_yii_app.directivePath + '/effect/grid-item-hover-01/grid-item-hover-01.css');
            //$scope.size = $scope.size || {width: '250px', height: '250px'}
            $scope.size = $scope.size || {width: 250, height: 250}
            console.log($scope.size)
        }
    };
});