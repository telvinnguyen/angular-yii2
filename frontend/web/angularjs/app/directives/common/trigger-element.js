app.directive('triggerElement', function ($timeout, ssHelper) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var selector = attrs.triggerElement;
            var eventType = angular.isDefined(attrs.eventType) ? attrs.eventType : 'click';

            attrs.$observe('trigger', function (trigger) {
                if (trigger != 0) {
                    element.bind("click", function (e) {
                        console.log(selector, eventType)
                        angular.element(selector).trigger(eventType);
                    });
                }
            })
        }
    };
});