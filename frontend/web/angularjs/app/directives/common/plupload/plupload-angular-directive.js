angular.module('plupload.directive', [])
    .provider('plUploadService', function () {

        var config = {
            flashPath: _yii_app.baseUrl + '/angularjs/app/directives/common/plupload/plupload.flash.swf',
            silverLightPath: _yii_app.baseUrl + '/angularjs/app/directives/common/plupload/plupload.flash.swf',
            uploadPath: 'upload.php'
        };

        this.setConfig = function (key, val) {
            config[key] = val;
        };

        this.getConfig = function (key) {
            return config[key];
        };

        var that = this;

        this.$get = [function () {

            return {
                getConfig: that.getConfig,
                setConfig: that.setConfig
            };

        }];

    })
    .directive('plUpload', function ($parse, plUploadService, dialogs) {
        return {
            restrict: 'A',
            scope: {
                'plProgressModel': '=',
                'plFilesModel': '=',
                'plFiltersModel': '=',
                'plMultiParamsModel': '=',
                'plInstance': '='
            },
            link: function (scope, element, attrs) {
                console.log('plupload-angular-directive has been bound')

                scope.randomString = function (len, charSet) {
                    charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
                    var randomString = '';
                    for (var i = 0; i < len; i++) {
                        var randomPoz = Math.floor(Math.random() * charSet.length);
                        randomString += charSet.substring(randomPoz, randomPoz + 1);
                    }
                    return randomString;
                }

                if (!attrs.id) {
                    var randomValue = scope.randomString(5);
                    attrs.$set('id', randomValue);
                }
                if (!attrs.plAutoUpload) {
                    attrs.$set('plAutoUpload', 'true');
                }
                if (!attrs.plMaxFileSize) {
                    attrs.$set('plMaxFileSize', '10mb');
                }
                if (!attrs.plUrl) {
                    attrs.$set('plUrl', plUploadService.getConfig('uploadPath'));
                }
                if (!attrs.plFlashSwfUrl) {
                    attrs.$set('plFlashSwfUrl', plUploadService.getConfig('flashPath'));
                }
                if (!attrs.plSilverlightXapUrl) {
                    attrs.$set('plSilverlightXapUrl', plUploadService.getConfig('silverLightPath'));
                }
                if (typeof scope.plFiltersModel == "undefined") {
                    scope.filters = [{title: "Image files", extensions: "jpg,jpeg,gif,png,tiff,pdf"}];
                    //alert('sf');
                } else {
                    scope.filters = scope.plFiltersModel;
                }

                var options = {
                    runtimes: 'html5,flash,silverlight',
                    browse_button: attrs.id,
                    multi_selection: false,
                    //		container : 'abc',
                    max_file_size: attrs.plMaxFileSize,
                    url: attrs.plUrl,
                    flash_swf_url: attrs.plFlashSwfUrl,
                    silverlight_xap_url: attrs.plSilverlightXapUrl,
                    filters: scope.filters
                }

                options.multi_selection = false;
                if (attrs.plMultiSelect) {
                    options.multi_selection = attrs.plMultiSelect == 'true' ? true : false;
                }


                if (scope.plMultiParamsModel) {
                    options.multipart_params = scope.plMultiParamsModel;
                }

                if (attrs.plChunk) {
                    options.chunk_size = attrs.plChunk;
                    options.max_retries = 3;
                }
                //console.log(options)
                var uploader = new plupload.Uploader(options);

                uploader.settings.headers = plUploadService.getConfig('headers');

                uploader.init();

                uploader.bind('Error', function (up, err) {
                    if (attrs.onFileError) {
                        scope.$parent.$apply(onFileError);
                    }

                   // alert("Cannot upload, error: " + err.message + (err.file ? ", File: " + err.file.name : "") + "");
                    
                    dialogs.notify('Upload Error', err.message, {size: 'md'});
                    up.refresh(); // Reposition Flash/Silverlight
                });

                uploader.bind('FilesAdded', function (up, files) {
                    //uploader.start();
                    scope.$apply(function () {
                        if (attrs.plFilesModel) {
                            angular.forEach(files, function (file, key) {
                                scope.plFilesModel.push(file);
                            });
                        }

                        if (attrs.onFileAdded) {
                            scope.$parent.$eval(attrs.onFileAdded);
                        }
                    });

                    if (attrs.plAutoUpload == "true") {
                        uploader.start();
                    }
                });

                uploader.bind('FileUploaded', function (up, file, res) {
                    if (attrs.onFileUploaded) {
                        if (attrs.plFilesModel) {
                            scope.$apply(function () {
                                angular.forEach(scope.plFilesModel, function (file, key) {
                                    scope.allUploaded = false;
                                    if (file.percent == 100)
                                        scope.allUploaded = true;
                                });

                                if (scope.allUploaded) {
                                    var fn = $parse(attrs.onFileUploaded);
                                    fn(scope.$parent, {$response: res, $file: file});
                                }

                            });
                        } else {
                            var fn = $parse(attrs.onFileUploaded);
                            scope.$apply(function () {
                                fn(scope.$parent, {$response: res});
                            });
                        }
                        //scope.$parent.$apply(attrs.onFileUploaded);
                    }
                });

                uploader.bind('UploadProgress', function (up, file) {
                    if (!attrs.plProgressModel) {
                        return;
                    }

                    if (attrs.plFilesModel) {
                        scope.$apply(function () {
                            scope.sum = 0;
                            angular.forEach(scope.plFilesModel, function (file, key) {
                                scope.sum = scope.sum + file.percent;
                            });

                            scope.plProgressModel = scope.sum / scope.plFilesModel.length;
                        });
                    } else {
                        scope.$apply(function () {
                            scope.plProgressModel = file.percent;
                        });
                    }


                    if (attrs.onFileProgress) {
                        scope.$parent.$eval(attrs.onFileProgress);
                    }
                });
                
                if (attrs.plInstance) {
                    scope.plInstance = uploader;
                }

                // scope.upload = function(){
                // 	uploader.start();
                // };
            }
        };
    })
