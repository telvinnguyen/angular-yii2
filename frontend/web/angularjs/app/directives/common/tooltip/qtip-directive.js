app.directive('qtip', function(ssHelper){
    return {
        restrict: 'A',
        scope: {'qtipOptions': '='},
        link: function(scope, element, attrs){

            var options = {
                content: {
                    text: attrs.qtip,
                    button: false
                },
                position: {
                    container: angular.element(element).parent(),
                    at: 'top center'//'top center'

                },
                style: {
                    classes: 'tip-overlay'
                },
                show: 'click mouseover'
//                hide: 'click'

            };
            if(angular.isDefined(scope.qtipOptions)){

                options = _.extendOwn(options, scope.qtipOptions);
            }

            options = ssHelper.removeUndefinedProperty(options)
            angular.element(element).qtip(options);
        }
    };
});