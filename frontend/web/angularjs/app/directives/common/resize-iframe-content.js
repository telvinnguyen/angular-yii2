/**
 * resize-iframe-content: options { ...}
 * expect-width: number - the fixed width that iframe should be scaled for
 * expect-height: number - the fixed height that iframe should be scaled for
 * scale-selector: the selector (id, class...) of the element that inside of the iframe would be scaled off
 * usage: <div resize-iframe ng-src="{{url}}" expect-width="width" expect-height="height" on-message-key="previewSkin" scale-selector="#wrapper" style="width:200px;height:200px" />
 * the default width height on the style of div creates the space for the loader icon
 */
app.directive('resizeIframeContent', function ($timeout, ssHelper, $sce) {
    return {
        restrict: 'A',
        scope: {
            expectWidth: '=?', expectHeight: '=?', 'iframeUrl': '='
        },
        template: '<div><div loader working="frameLoader" template="3"></div> <iframe sandbox="allow-same-origin allow-scripts" ng-src="{{url}}" ng-hide="frameLoader"/></div>',
        link: function (scope, element, attrs) {
            scope.frameLoader = true;
            var iframe = element.find('iframe');
            scope.url = $sce.trustAsResourceUrl(scope.iframeUrl)
            //iframe.attr('src', attrs.ngSrc)
            iframe.on('load', function () {
                var current_iframe = this;

                scope.expectWidth = angular.isDefined(scope.expectWidth) ? scope.expectWidth : 100;
                scope.expectHeight = angular.isDefined(scope.expectHeight) ? scope.expectHeight : 100;

                //for external site
                $(current_iframe).css('width', scope.expectWidth);
                $(current_iframe).css('height', scope.expectHeight);

                element.css('width', scope.expectWidth);
                element.css('height', scope.expectHeight);

                $(current_iframe).zoomer({ width: scope.expectWidth, height: scope.expectHeight});

                var ratioAspect = 1, scaleRatio = 1;
                var iframeWidth = 0, iframeHeight = 0;
                scope.$root.$on('$messageIncoming', function (event, data) { //for internal site which emit the $messageOutoging
                    console.log('on $messageIncoming')
                    var rp = angular.fromJson(data);
                    if(angular.isDefined(rp.type) && rp.type == 'kiosk-box'){
                        scope.expectWidth = $(parent).width()
                        scope.expectHeight = $(parent).height()
                    }

                    if (rp.key == attrs.onMessageKey && rp.onLoaded == 1) {
                        var t = $(current_iframe).get(0).contentWindow.document.body;
                        var sizeLocalNode = $(t).find('ssize');

                        if (angular.isDefined(sizeLocalNode)) {
                            iframeWidth = sizeLocalNode.data('width');
                            iframeHeight = sizeLocalNode.data('height');
                        } else {
                            var a = $(current_iframe).get(0).contentWindow.document.body;
                            var b = $(a).children(':not(script, style)').first();
                            var firstNodeName_AfterBody = '';
                            if (angular.isDefined(b[0]))
                                firstNodeName_AfterBody = b[0].nodeName.toLowerCase();
                            var c = $(current_iframe).contents().find("key").text();

                            if (firstNodeName_AfterBody == 'key') {
                                if (c == '.video') {
                                    iframeWidth = $(current_iframe).contents().find("#video-container").width();
                                    iframeHeight = $(current_iframe).contents().find("#video-container").height();
                                } else if (c == '.scrollmsg') {
                                    iframeWidth = $(current_iframe).contents().find("#marquee-container").width();
                                    iframeHeight = $(current_iframe).contents().find("#marquee-container").height();
                                }
                            }
                            else if (firstNodeName_AfterBody == 'table' || firstNodeName_AfterBody == 'p') {
                                iframeWidth = $(current_iframe).contents().find("table").width();
                                iframeHeight = $(current_iframe).contents().find("table").height();
                                ;
                            } else if (firstNodeName_AfterBody == 'div') {
                                iframeWidth = $(current_iframe).contents().find("div").first().width();
                                iframeHeight = $(current_iframe).contents().find("div").first().height();


                            } else if (firstNodeName_AfterBody == 'form') {
                                iframeWidth = $(current_iframe).contents().find("div").width();
                                iframeHeight = $(current_iframe).contents().find("div").height();
                            }
                            else {
                                iframeWidth = $(current_iframe).contents().find("body").width();
                                iframeHeight = $(current_iframe).contents().find("body").height();
                                if (firstNodeName_AfterBody == 'img') { /*isImageUrl = true; */
                                }
                            }
                        }

                        var userScreenWidth = $(window).width();
                        var userScreenHeight = $(window).height();

                        ratioAspect = ssHelper.roundNumber(iframeWidth/iframeHeight, 2)
                        if(iframeWidth > userScreenWidth){
                            scope.expectWidth = userScreenWidth
                            scope.expectHeight = ssHelper.roundNumber(scope.expectWidth/ratioAspect, 2)
                            if(scope.expectHeight > userScreenHeight){
                                scope.expectHeight = userScreenHeight
                                scope.expectWidth = scope.expectHeight*ratioAspect
                                scaleRatio = ssHelper.roundNumber(userScreenHeight/iframeHeight, 2)
                            }else{
                                scaleRatio = ssHelper.roundNumber(userScreenWidth/iframeWidth, 2)
                            }
                        }
                        else if(iframeHeight > userScreenHeight){
                            scope.expectHeight = userScreenHeight
                            scope.expectWidth = scope.expectHeight*ratioAspect
                            if(scope.expectWidth > userScreenWidth){
                                scope.expectWidth = userScreenWidth
                                scope.expectHeight = ssHelper.roundNumber(scope.expectWidth/ratioAspect, 2)
                                scaleRatio = ssHelper.roundNumber(userScreenWidth/iframeWidth, 2)
                            }else{
                                scaleRatio = ssHelper.roundNumber(userScreenHeight/iframeHeight, 2)
                            }
                        }
                        $timeout(function(){})

                        $(current_iframe).css('width', iframeWidth);
                        $(current_iframe).css('height', iframeHeight);

                        element.css('width', scope.expectWidth);
                        element.css('height', scope.expectHeight);

                        if(scope.useZoomerPlugin){
                            $(current_iframe).zoomer({
                                width: iframeWidth,
                                height: iframeHeight,
                                zoom: scaleRatio,
                                scrollWidth: scrollbarWidth
                            });
                        }else{
                            var partContent = $(current_iframe).contents().find(attrs.scaleSelector)
                            if(angular.isDefined(partContent)){
                                partContent.css('zoom', '1.00');
                                partContent.css('-moz-transform-origin', '0.0');
                                partContent.css('-webkit-transform-origin', '0.0');
                                partContent.css('-moz-transform', s.sprintf('scale(%1$s)', scaleRatio));
                                partContent.css('-o-transform', s.sprintf('scale(%1$s)', scaleRatio));
                                partContent.css('-webkit-transform', s.sprintf('scale(%1$s)', scaleRatio));
                                partContent.css('-webkit-transform-origin-x', '0');
                                partContent.css('-webkit-transform-origin-y', '0');
                            }
                        }
                        $(current_iframe).contents().find('body').css({ overflow: 'hidden' });
                    }
                })

                scope.frameLoader = false;
                $timeout(function(){});
            })
        }
    };
})