app.directive('globalAction', function ($timeout, ssHelper) {
    return {
        restrict: 'A',
        scope: { msg: '=globalAction' },
        bindToController: true,
        controller: function ($scope, $element) {
            $element.bind('click', function(){
                console.log('sending message');
            });
            
        },
        link: function (scope, element, attrs) {
        }
    };
});