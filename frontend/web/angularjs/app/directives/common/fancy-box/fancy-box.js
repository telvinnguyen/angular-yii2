app.directive('fancyBox', function ($timeout, ssHelper) {

    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var options = {
                helpers: {
                    title : {
                        type : 'outside'
                    },
                    overlay : {
                        speedOut : 0
                    }
                }
            };

            if(angular.isDefined(attrs.fancyOptions))
                options = scope.$eval(attrs.fancyOptions)

            $(element).fancybox(options);
        }
    };
});