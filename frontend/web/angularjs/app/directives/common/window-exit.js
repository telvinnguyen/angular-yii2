app.directive('windowExit', function($window, store, $ocLazyLoad, $injector) {
    return {
        restrict: 'AE',
        //performance will be improved in compile
        compile: function(element, attrs){
            var myEvent = $window.attachEvent || $window.addEventListener,
                chkevent = $window.attachEvent ? 'onbeforeunload' : 'beforeunload'; /// make IE7, IE8 compatable

            myEvent(chkevent, function (e) { // For >=IE7, Chrome, Firefox
                var confirmationMessage = ' ';  // a space

                //show confirm messeage before closing browser
                /*
                (e || $window.event).returnValue = "Are you sure that you'd like to close the browser?";
                return confirmationMessage;
                */
                if(!store.get('remember-me')) {
                    store.remove('ss-identity-jwt')


                    $ocLazyLoad.load('syncVideoComponent').then(function () {
                        var syncVideoService = $injector.get('syncVideoService');
                    })
                    //upgradememberService.reset();
                }
                return false;
            });
        },
        link: function(){
            //this belongs to another different part of the app. It is checking if the angular is done to bootstrap
            console.log('11111')
            $('#appLoadAmt').html('100%');
            $('#preload-cover').fadeOut();
            $('#preload-cover').remove();
        }
    };
});