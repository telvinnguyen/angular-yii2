

app.directive('hintText', function() {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, element, attr, ctrl) {

            var value;

            var setText = function () {
                element.val(attr.hintText);
                //element.attr('style','color:#5d6b81;');
                $(element).css('color', '#5d6b81')
            };
            var unsetText = function () {
                element.val('');
                //element.css('color','green');
                //element.attr('style','');
                $(element).css('color', 'auto')
            };
            scope.$watch(attr.ngModel, function (val) {
                value = val || '';
            });

            element.bind('focus', function () {
                if(value == '') unsetText();
            });

            element.bind('blur', function () {
                if (element.val() == '') setText();
            });

            ctrl.$formatters.unshift(function (val) {
                if (!val) {
                    setText();
                    value = '';
                    return attr.hintText;
                }
                return val;
            });
        }
    };
});