app.directive("regExInput", function(){
    return {
        restrict: "A",
        require: "?regEx",
        scope: {},
        replace: false,
        link: function(scope, element, attrs, ctrl){
          element.bind('keypress', function (event) {
            var regex = new RegExp(attrs.regEx);
            var key = String.fromCharCode(!event.keyCode ? event.which : event.keyCode);
            console.log(regex.test(key))
            if (!regex.test(key)) {
               event.preventDefault();
               return false;
            }
          });
        }
    };
});
