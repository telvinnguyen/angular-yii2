/**
 * make-resizable: options { ...}
 * on-resize: function (resize-item, ui, event, object) (object contains the element which is attached the directive)
 * on-resize-stop: function (resize-item, ui, event, object)  (object contains the element which is attached the directive)
 */

angular.module('zwenGlobal').directive('makeResizableIsolated', function ($timeout, ssHelper) {
    return {
        restrict: 'A',
        scope: {makeResizable: '=makeResizableIsolated'},
        link: function (scope, element, attrs) {
            /* attach function into the object is holding directive element,
             could use this on the controller of directive */


            var options = scope.makeResizable;
            //var options = JSON.stringify(eval("(" + scope.makeResizable + ")"));
            console.log(options)

            element.resizable(options);
        }
    };
});

angular.module('zwenGlobal').directive('makeResizable', function ($parse) {
    return {
        restrict: 'A',
        scope: true,
        link: function (scope, element, attrs) {

            console.log(attrs.makeResizable)
            var options = scope.$eval(attrs.makeResizable);
            //options = JSON.stringify(eval("(" + attrs.makeResizable + ")"));

            if(!angular.isDefined(options)){
                options = {}
            }
            element.resizable(options);
            var obj = {element: undefined}
            element.on('resize', function (evt, ui) {
                obj.element = element;
                $parse(attrs.onResize)(scope.$parent, {ui: ui, evt: evt, object: obj});
            });
            element.on('resizestop', function (evt, ui) {
                obj.element = element;
                $parse(attrs.onResizeStop)(scope.$parent, { ui: ui, evt: evt, object: obj});
            });
        }
    };
})