/**
 * resize-iframe: options { ...}
 * expect-width: number - the fixed width that iframe should be scaled for
 * expect-height: number - the fixed height that iframe should be scaled for
 * usage: <div resize-iframe ng-src="{{url}}" expect-width="width" expect-height="height" on-message-key="previewSkin" style="width:200px;height:200px" />
 * the default width height on the style of div creates the space for the loader icon
 */
app.directive('resizeIframe', function ($timeout, ssHelper, $sce) {
    return {
        restrict: 'A',
        bindToController: true,
        scope: {
            expectWidth: '=', expectHeight: '=', 'iframeUrl': '='
        },
        template: '<div loader working="frameLoader" template="3"></div> <iframe ng-src="{{url}}" ng-hide="frameLoader"/>',
        link: function (scope, element, attrs) {
            scope.frameLoader = true;
            var iframe = element.find('iframe');
            scope.url = $sce.trustAsResourceUrl(scope.iframeUrl)
            //document.getElementById("e123").setAttribute('src', attrs.iframeSrc);
           //iframe.attr('ng-src', url)
            //console.log(url)
            iframe.on('load', function () {
                //var set_iFrameWidth = '100%';
                //var set_iFrameHeight = element[0].contentWindow.document.body.scrollHeight + 'px';

                //get curent user window's size
                //var expectWidth = 0, expectHeight = 0;
                var ratioAspect = 1, scaleRatio = 1;

                var iframeWidth = 0, iframeHeight = 0;

                //var sizeLocalNode = $(this).contents().find("#wrapper");
                var current_iframe = this;

                scope.$root.$on('$messageIncoming', function (event, data) {
                    var rp = angular.fromJson(data);
                    if (rp.key == attrs.onMessageKey && rp.onLoaded == 1) {
                        //var t = $(current_iframe).get(0).contentWindow.document.body;
                        var sizeLocalNode = $(current_iframe).contents().find('ssize');

                        if (angular.isDefined(sizeLocalNode)) {
                            iframeWidth = sizeLocalNode.data('width');
                            iframeHeight = sizeLocalNode.data('height');
                        } else {
                            var a = $(current_iframe).get(0).contentWindow;
                            if(a != null){
                                var _a = a.document.body;
                                var b = $(_a).children(':not(script, style)').first();
                                var firstNodeName_AfterBody = '';
                                if (angular.isDefined(b[0]))
                                    firstNodeName_AfterBody = b[0].nodeName.toLowerCase();
                                var c = $(current_iframe).contents().find("key").text();
                                console.log(firstNodeName_AfterBody);
                                if (firstNodeName_AfterBody == 'key') {
                                    if (c == '.video') {
                                        iframeWidth = $(current_iframe).contents().find("#video-container").width();
                                        iframeHeight = $(current_iframe).contents().find("#video-container").height();
                                    } else if (c == '.scrollmsg') {
                                        iframeWidth = $(current_iframe).contents().find("#marquee-container").width();
                                        iframeHeight = $(current_iframe).contents().find("#marquee-container").height();
                                    }
                                }
                                else if (firstNodeName_AfterBody == 'table' || firstNodeName_AfterBody == 'p') {
                                    iframeWidth = $(current_iframe).contents().find("table").width();
                                    iframeHeight = $(current_iframe).contents().find("table").height();
                                    ;
                                } else if (firstNodeName_AfterBody == 'div') {
                                    iframeWidth = $(current_iframe).contents().find("div").first().width();
                                    iframeHeight = $(current_iframe).contents().find("div").first().height();

                                    console.log(iframeWidth, iframeHeight);
                                } else if (firstNodeName_AfterBody == 'form') {
                                    iframeWidth = $(current_iframe).contents().find("div").width();
                                    iframeHeight = $(current_iframe).contents().find("div").height();
                                }
                                else {
                                    iframeWidth = $(current_iframe).contents().find("body").width();
                                    iframeHeight = $(current_iframe).contents().find("body").height();
                                    if (firstNodeName_AfterBody == 'img') { /*isImageUrl = true; */
                                    }
                                }
                            }
                        }

                        var userScreenWidth = $(window).width();
                        var userScreenHeight = $(window).height();

                        ratioAspect = ssHelper.roundNumber(iframeWidth/iframeHeight, 2)
                        if(iframeWidth > userScreenWidth){
                            scope.expectWidth = userScreenWidth
                            scope.expectHeight = ssHelper.roundNumber(expectWidth/ratioAspect, 2)
                            if(scope.expectHeight > userScreenHeight){
                                scope.expectHeight = userScreenHeight
                                scope.expectWidth = scope.expectHeight*ratioAspect
                                scaleRatio = ssHelper.roundNumber(userScreenHeight/iframeHeight, 2)
                            }else{
                                scaleRatio = ssHelper.roundNumber(userScreenWidth/iframeWidth, 2)
                            }
                        }
                        else if(iframeHeight > userScreenHeight){
                            scope.expectHeight = userScreenHeight
                            scope.expectWidth = scope.expectHeight*ratioAspect
                            if(scope.expectWidth > userScreenWidth){
                                scope.expectWidth = userScreenWidth
                                scope.expectHeight = ssHelper.roundNumber(scope.expectWidth/ratioAspect, 2)
                                scaleRatio = ssHelper.roundNumber(userScreenWidth/iframeWidth, 2)
                            }else{
                                scaleRatio = ssHelper.roundNumber(userScreenHeight/iframeHeight, 2)
                            }
                        }

                        $timeout(function(){})

                        ssHelper.debugLog(scaleRatio);

                        $(current_iframe).css('width', iframeWidth);
                        $(current_iframe).css('height', iframeHeight);

                        element.css('width', scope.expectWidth);
                        element.css('height', scope.expectHeight);
                        element.css('zoom', '1.00');
                        element.css('-moz-transform-origin', '0.0');
                        element.css('-webkit-transform-origin', '0.0');
                        element.css('-moz-transform', s.sprintf('scale(%1$s)', scaleRatio));
                        element.css('-o-transform', s.sprintf('scale(%1$s)', scaleRatio));
                        element.css('-webkit-transform', s.sprintf('scale(%1$s)', scaleRatio));
                        element.css('-webkit-transform-origin-x', '0');
                        element.css('-webkit-transform-origin-y', '0');

                        $(current_iframe).contents().find('body').css({ overflow: 'hidden' });

                        scope.frameLoader = false;
                    }
                })


            })
        }
    };
})