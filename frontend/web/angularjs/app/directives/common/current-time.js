
app.directive('currentTime',
    function($interval, dateFilter) {
        // return the directive link function. (compile function not needed)
        return {
            restrict: "A",
            scope:{
                timeValue:"="
            },
            link:function(scope, element, attrs) {
                var format="MM/dd/yyyy HH:mm:ss",  // date format
                    stopTime; // so that we can cancel the time updates

                // used to update the UI
                function updateTime() {
                    var date = new Date();
                    var _utc = new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),  date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
                    if(scope.timeValue == "" || scope.timeValue == null)
                        element.text(dateFilter(_utc, format));
                    else
                        element.text(dateFilter(scope.timeValue, format));
                }

                //// watch the expression, and update the UI on change.
                //scope.$watch(attrs.myCurrentTime, function(value) {
                //    format = "MM/dd/yyyy h:mm:ss";
                //    updateTime();
                //});

                stopTime = $interval(updateTime, 1000);

                //// listen on DOM destroy (removal) event, and cancel the next UI update
                //// to prevent updating time after the DOM element was removed.
                //element.on('$destroy', function() {
                //    $interval.cancel(stopTime);
                //});
            }
        };
    });