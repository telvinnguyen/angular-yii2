app.directive('pgwSlideshow',function($timeout){

    return {
        restrict:"AE",
        bindToController: true,
        scope:{
            list : '=',
            setting : '=?'
        },
        controller: function($scope){
            console.log($scope.setting);
        },
        link:function(scope,element,attrs){

            scope.option = {
                autoSlide: true,
                displayList: false,
                displayControls: false,
                intervalDuration: scope.setting.df_duration,
                beforeSlide: function(index){
                        disableAllVideo(index);
                        addForVideo(index);
                }
            };

            $timeout(function(){
                element.pgwSlideshow(scope.option);
            }, 1000);
            function addForVideo(index){
                var html = $('[data-index="'+index+'"]').html();
                $('.ps-current').find('.elt_' + index).html(html);
                var displayIframe = $('.ps-current').find('.elt_' + index).find('iframe');
                displayIframe.attr('src', displayIframe.attr('data-src'));
                //displayIframe.removeAttr('data-src');
            }
            function disableAllVideo(index){
                $('.ps-current').find('[class^="elt_"] iframe').remove();
            }

        }

    }
});