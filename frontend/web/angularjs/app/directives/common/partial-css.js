/**
 * the format of the input of the directive is
 * {
 *     [selector]: { // example: .scroll, .wrapper, div.abc
 *          [property] => 'value' // example: 'color' =>  '#000';
 *     }
 * }
 *
 */
app.directive('partialCss', function ($timeout, ssHelper) {

    return {
        restrict: 'E',
        scope: {listCss: '='},
        link: function (scope, element, attrs) {
            scope.$watch('listCss', function(listCss){
                var output = '<style>';
                _.each(listCss, function(cssContent, selector){
                    output += s.sprintf('%1$s{', selector);
                    _.each(cssContent, function(value, propertyName){
                        output += s.sprintf('%1$s: %2$s;', propertyName, value);
                    })
                    output += '}\n';
                });


                output += '</style>';
                element.html(output);
            }, true)
        }
    };
});