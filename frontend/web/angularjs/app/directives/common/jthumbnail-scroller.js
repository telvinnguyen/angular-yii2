
angular.module('zwenGlobal').directive('jthumbnailScroller', function($timeout) {
    return {
        restrict: 'A',
        link: function(scope, element, attr, ctrl) {
        	
        	$timeout(function(){
        		
                var getW1 = 0;
                var getW2 = 0;
                var getW3 = 0;
                var getW4 = 0;
                var getW5 = 0;
                var getW6 = 0;
        		
        		$(element).thumbnailScroller({
                    scrollerType:"clickButtons",
                    scrollerOrientation:"vertical",
                    scrollSpeed:2,
                    scrollEasing:"easeOutCirc",
                    scrollEasingAmount:600,
                    acceleration:4,
                    scrollSpeed:800,
                    noScrollCenterSpace:10,
                    autoScrolling:0,
                    autoScrollingSpeed:2000,
                    autoScrollingEasing:"easeInOutQuad",
                    autoScrollingDelay:500,
                    nextButton:'prevButton',
                    prevButton:'nextButton'
                });
        	})
        	
        	
        }
    };
});