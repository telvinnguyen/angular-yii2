angular.module('zwenGlobal').controller('forgotController', function ($rootScope, $scope, $modal, $filter, modalUtils, $injector, ssHelper) {

    $rootScope.baseUrl = _yii_app.baseUrl;
    $rootScope.layoutPath = _yii_app.layoutPath;
    $rootScope.bodyClass = 'short_main';
    $scope.loading = false;

    $scope.email = '';
    $scope.msg = '';
    $scope.done = false;
    var $validationProvider = $injector.get('$validation');

    $scope.submitEmail = function () {
        $scope.loading = true;
        ssHelper.post('/home/resetpassword', {
            email: $scope.email

        }).then(function (response) {
            $scope.loading = false;
            $scope.msg = response.data.msg;

            var PopupInstance = $modal.open({
                templateUrl: _yii_app.absTemplatePath + '/modal-msg.html',
                controller: 'resetPasswordModalCtrl',
                scope: $scope,
                backdrop: 'static'
            });

            if (response.data.OK == 1) {
                $scope.done = true;

            }
        });
    }
});



angular.module('zwenGlobal').controller('resetPasswordModalCtrl', function ($scope, $state, $modalInstance, SignalService, $injector, $location) {

    $scope.ok = function () {
        if($scope.done){
            $modalInstance.dismiss('cancel');
            //$location.path('./login').replace();
            $state.go('login');
        }else
            $modalInstance.dismiss('cancel');

    };
});
