/** for the remote functionality, we have to make the remote that is able to control every screen, therefore
 * I keep modal controller as top parent scope to any children scope **/
angular.module('zwenGlobal').controller('MasterPageCtrl', function ($rootScope, $window, $scope, $state, $stateParams, $modal, dialogs, $location, $route, $timeout,$ocLazyLoad, $log, modalUtils, ssHelper, store, $previousState, $previousState, noty) {

    $scope.layoutPath = _yii_app.layoutPath;
    //$ocLazyLoad.load(_yii_app.baseUrl + '/angularjs/app/controllers/remote/remote.css');


    //this will operate and distribute function & variable in $scope.remote to corresponding state
    $scope.remoteHoler = {};
    $scope.backQueue = [];

    // this remote variable will share and pass to any children controller to access their functions
    $scope.remote = {
        set toolbars(value){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};$scope.remoteHoler[$state.current.name]['toolbars'] = value;},
        get toolbars(){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};return $scope.remoteHoler[$state.current.name]['toolbars'];},

        set toggleToolbar(value){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};$scope.remoteHoler[$state.current.name]['toggleToolbar'] = value;},
        get toggleToolbar(){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};return $scope.remoteHoler[$state.current.name]['toggleToolbar'];},

        set toggleObjectVariant(value){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};$scope.remoteHoler[$state.current.name]['toggleObjectVariant'] = value;},
        get toggleObjectVariant(){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};return $scope.remoteHoler[$state.current.name]['toggleObjectVariant'];},

        set reloadDataByTarget(value){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};$scope.remoteHoler[$state.current.name]['reloadDataByTarget'] = value;},
        get reloadDataByTarget(){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};return $scope.remoteHoler[$state.current.name]['reloadDataByTarget'];},

        set item_filters(value){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};$scope.remoteHoler[$state.current.name]['item_filters'] = value;},
        get item_filters(){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};return $scope.remoteHoler[$state.current.name]['item_filters'];},

        set loadActions(value){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};$scope.remoteHoler[$state.current.name]['loadActions'] = value;},
        get loadActions(){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};return $scope.remoteHoler[$state.current.name]['loadActions'];},

        set loadCollectionVariants(value){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};$scope.remoteHoler[$state.current.name]['loadCollectionVariants'] = value;},
        get loadCollectionVariants(){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};return $scope.remoteHoler[$state.current.name]['loadCollectionVariants'];},

        set loadGroupVariants(value){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};$scope.remoteHoler[$state.current.name]['loadGroupVariants'] = value;},
        get loadGroupVariants(){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};return $scope.remoteHoler[$state.current.name]['loadGroupVariants'];},

        set loadItemVariants(value){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};$scope.remoteHoler[$state.current.name]['loadItemVariants'] = value;},
        get loadItemVariants(){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};return $scope.remoteHoler[$state.current.name]['loadItemVariants'];},

        set loadAdminActions(value){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};$scope.remoteHoler[$state.current.name]['loadAdminActions'] = value;},
        get loadAdminActions(){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};return $scope.remoteHoler[$state.current.name]['loadAdminActions'];},

        set reloadData(value){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};$scope.remoteHoler[$state.current.name]['reloadData'] = value;},
        get reloadData(){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};return $scope.remoteHoler[$state.current.name]['reloadData'];},

        set get3FilterItemTypes(value){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};$scope.remoteHoler[$state.current.name]['get3FilterItemTypes'] = value;},
        get get3FilterItemTypes(){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};return $scope.remoteHoler[$state.current.name]['get3FilterItemTypes'];},

        set hidden_list_id(value){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};$scope.remoteHoler[$state.current.name]['hidden_list_id'] = value;},
        get hidden_list_id(){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};return $scope.remoteHoler[$state.current.name]['hidden_list_id'];},

        set getDirectoryLayout(value){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};$scope.remoteHoler[$state.current.name]['getDirectoryLayout'] = value;},
        get getDirectoryLayout(){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};return $scope.remoteHoler[$state.current.name]['getDirectoryLayout'];},

        set sharedToggleToolbar(value){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};$scope.remoteHoler[$state.current.name]['sharedToggleToolbar'] = value;},
        get sharedToggleToolbar(){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};return $scope.remoteHoler[$state.current.name]['sharedToggleToolbar'];},
        
        set loadAvatarNavigation(value){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};$scope.remoteHoler[$state.current.name]['loadAvatarNavigation'] = value;},
        get loadAvatarNavigation(){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};return $scope.remoteHoler[$state.current.name]['loadAvatarNavigation'];},
        
        set pagerManager(value){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};$scope.remoteHoler[$state.current.name]['pagerManager'] = value;},
        get pagerManager(){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};return $scope.remoteHoler[$state.current.name]['pagerManager'];},
        
        set returnScope(value){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};$scope.remoteHoler[$state.current.name]['returnScope'] = value;},
        get returnScope(){if(!angular.isDefined($scope.remoteHoler[$state.current.name])) $scope.remoteHoler[$state.current.name] = {};return $scope.remoteHoler[$state.current.name]['returnScope'];},

        recall: function(stateName, property){
            if(angular.isDefined($scope.remoteHoler[stateName]))
                return $scope.remoteHoler[stateName][property]
            return undefined;
        },
        //backPromise: { state: {name: '', params: {}}, callbacks: [array of functions]}
        set backPromise(value){ $scope.backQueue.push(value) },
        get backPromise(){
            var t=undefined; // format of t should be { state: {name: '', params: {}}, callbacks: [array of callback functions]}
            if($scope.backQueue.length > 0) {
                t = $scope.backQueue[$scope.backQueue.length - 1]
                $scope.backQueue.splice(-1,1)
            }
            return t;
        }
    };
    
    

    $rootScope.triggerRatchet = _.uniqueId('');
    $rootScope.triggerLoadRemote = 0;

    $scope.remote.type_target = 'cv';
    $scope.remote.sorter = '';
    $scope.remote.sort_order ='desc';
    $scope.remote.keyword = '';

    $scope.remote.item_filters = []; // list of Item object that would be show in the popup filters

    <!-- globalRemote -> remote for the filter/sorter   -->
    $scope.modalsExist = false;
    $scope.globalModalExist = false;
    $scope.templateUrl = _yii_app.controllerPath + '/remote/default-remote-template.html';
    $scope.remoteCtrlName = 'RemoteInstanceCtrl';
    
    $scope.backgroundTasksManager ={
    		tasks: [],
    		getTaskByName: function(name){
    			return _.findWhere(this.tasks, {name: name});
    		},
    		showTask: function(taskName){
    			var task = this.getTaskByName(taskName);
        		noty[task.style](task.options);
    			
    		},
    		hideTask: function(taskName){
    			noty.closeAll();
    		},
    		addTask: function(task){
    			this.tasks.push(task);
    		},
    		removeTask: function(taskName){
    			this.tasks.splice( this.tasks.indexOf(taskName), 1 );
    		},
    		updateStatusTask: function(taskName, status){
    			var task = this.getTaskByName(taskName);
    			task.status = status;
    		},
    		stopVideo: function(taskName){
    			var task = this.getTaskByName(taskName);
    			console.log(task.uploader)
    			task.uploader.stop();
    		}
    }

    $scope.openRemote = function (size) {

        if(!$scope.modalsExist){
            var modalInstance = $modal.open({
                templateUrl: $scope.templateUrl,
                windowTemplateUrl: _yii_app.controllerPath + '/remote/modal-remote-frame-home.html',
                controller: $scope.remoteCtrlName,
                windowClass: 'remote-modal-frame',
                size: size,
                scope: $scope,
                backdrop: false
            });

            modalInstance.opened.then(function(){
                $scope.modalsExist = true;
            })
            
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function (reason) {
                if(reason == 'cancel')
                    $scope.modalsExist = false;

                //$log.info('Modal dismissed at: ' + new Date());
            });
        }else{
            $scope.modalsExist = false;
            modalUtils.closeAllModals();
        }

    };
    
    
    $scope.openBackgroundTM = function () {

        if(!$scope.modalsExist){
            var modalInstance = $modal.open({
//                templateUrl:  _yii_app.controllerPath + '/remote/default-remote-template.html',
                templateUrl:  _yii_app.controllerPath + '/background-tasks-manager/background-tasks-manager-template.html',
                			
                windowTemplateUrl: _yii_app.controllerPath + '/remote/modal-remote-frame-home.html',
                controller: 'BackgroundTMCtr',
                windowClass: 'BackgroundTM-iframe',
                size: 'md',
                scope: $scope,
                backdrop: false,
            });

            modalInstance.opened.then(function(){
                $scope.modalsExist = true;
            })
//
//            modalInstance.result.then(function (selectedItem) {
//                $scope.selected = selectedItem;
//            }, function (reason) {
//                if(reason == 'cancel')
//                    $scope.modalsExist = false;
//
//                //$log.info('Modal dismissed at: ' + new Date());
//            });
        }else{
            $scope.modalsExist = false;
            modalUtils.closeAllModals();
        }

    };

    /* open the remote from the published Sign */
    $ocLazyLoad.load(_yii_app.controllerPath + '/global-modal/global-modal.css')
    $scope.openRemoteSign = function(sign_id, size){
        if (!$scope.globalModalExist) {
            $ocLazyLoad.load('kioskDesign').then(function () {
                var modalInstance = $modal.open({
                    templateUrl: _yii_app.controllerPath + '/global-modal/global-modal.html',
                    windowTemplateUrl: _yii_app.controllerPath + '/remote/modal-remote-frame-home.html',
                    controller: 'globalModalController',
                    windowClass: 'global-modal-frame',
                    scope: $scope,
                    backdrop: false,
                    resolve: {
                        userRemoteInfo: function () {
                            return {sign_id: sign_id, size: size};
                        }
                    }
                });

                modalInstance.opened.then(function () {
                    $scope.globalModalExist = true;
                    $scope.remoteLoader = 'opened';
                })

                modalInstance.result.then(function (selectedItem) {
                    $scope.selected = selectedItem;
                }, function (reason) {
                    if (reason == 'cancel'){
                        $scope.globalModalExist = false;
                        $scope.remoteLoader = 'closed'
                    }


                    //$log.info('Modal dismissed at: ' + new Date());
                });
            })

        } else {
            $scope.globalModalExist = false;
            modalUtils.closeAllModals();
        }
    }

    $scope.remoteLoader = 'closed';
    $scope.openGlobalModal = function () {
        var size, sign_id;

        if(!$scope.globalModalExist){
            $scope.remoteLoader = 'openning';
        }else{
            $scope.remoteLoader = 'closed';
        }


        ssHelper.post('/remote/getUserRemote', {
            acc_id: $stateParams.acc_id
        }).then(function (response) {
            if (response.data.OK == 1) {
                sign_id = response.data.sign_id
                size = {
                    width: response.data.remoteSize.skinWidth,
                    height: response.data.remoteSize.skinHeight,
                };
                $scope.openRemoteSign(sign_id, size);

            }
            else {
                dialogs.notify('', response.data.errors, {size: 'md'});
            }
        });
    }

    $scope.logOut = function (){
    	ssHelper.post('/home/logout', {});
        store.remove('ss-identity-jwt')
        store.remove('ss-member-id')
        $state.go('login');
        //upgradememberService.reset();
    }

    $scope.$on('$locationChangeStart',function(evt, absNewUrl, absOldUrl) {
        //console.log('start', evt, absNewUrl, absOldUrl);
    });
    $scope.$on('$locationChangeSuccess',function(evt, absNewUrl, absOldUrl) {
       //console.log('success', evt, absNewUrl, absOldUrl);
    });

    $scope.historyBack = function(){
            $window.history.back();
    }

    $scope.historyForward = function(){
            $window.history.forward();
    }
});

angular.module('zwenGlobal').controller('RemoteInstanceCtrl', function ($scope, $modalInstance, $state, $stateParams, $timeout, SignalService, ssHelper) {

    $scope.$watch('remote.item_filters', function(item_filters){
        if(angular.isDefined(item_filters) && item_filters.length > 3){
            $scope.filterBoxHeight = 65;
            $scope.filterIconSize = 40;
        }else{
            $scope.filterBoxHeight = 75;
            $scope.filterIconSize = 63;
        }
    })

    $scope.remoteInstanceClick = function(){
        //$scope.remote.sayHello();
    }

    $scope.ok = function () {
        $modalInstance.close($scope.selected.item);
    };

    $scope.closeRemote = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.content_setTarget = function(type){
        if(type == 'iv' && $stateParams.tb_mtype == 'Action' && $state.includes('main.toolbar-list'))
            type = 'action';
        $scope.remote.type_target = type;
        if(angular.isFunction($scope.remote.reloadDataByTarget))
            $scope.remote.reloadDataByTarget();
        if(angular.isFunction($scope.remote.get3FilterItemTypes))
            $scope.remote.get3FilterItemTypes();
    }

    $scope.filterByKeyword = function () {
        if ($scope.remote.type_target == 'cv') {
            $scope.remote.cvPageIndex = 0;

            if(angular.isFunction($scope.remote.loadCollectionVariants)){
                $scope.remote.loadCollectionVariants()
            }
        }
        if ($scope.remote.type_target == 'gv') {
            $scope.remote.gvPageIndex = 0;
            if(angular.isFunction($scope.remote.loadGroupVariants)){
                $scope.remote.loadGroupVariants()
            }
        }
        if ($scope.remote.type_target == 'iv') {
            $scope.remote.ivPageIndex = 0;
            if(angular.isFunction($scope.remote.loadItemVariants)){
                $scope.remote.loadItemVariants()
            }
        }

    }

    $scope.runSorter = function(sorter){

        if($scope.remote.sorter != sorter)
            $scope.remote.sort_order = 'desc';
        else{
            if($scope.remote.sort_order == 'asc') $scope.remote.sort_order = 'desc';
            else $scope.remote.sort_order = 'asc';
        }
        $scope.remote.sorter = sorter;

        if(angular.isFunction($scope.remote.reloadData))
            $scope.remote.reloadData();
    }
});

angular.module('zwenGlobal').controller('BackgroundTMCtr', function ($scope, $modalInstance, $state, $stateParams, $timeout, SignalService, ssHelper) {

	$scope.closeRemote = function () {
		$modalInstance.dismiss('cancel');
	};

});

