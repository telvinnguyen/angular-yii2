angular.module('zwenGlobal').controller('accountController', function ($rootScope, $scope, $modal, modalUtils, $injector, $http, store, $state, $cookieStore, $timeout, ssHelper, dialogs, iScrollService, FileUploader, registrationService,$stateParams,errorService) {

    $scope.layoutPath = _yii_app.layoutPath;
    $scope.acc_id = $stateParams.acc_id;
    $scope.ovid = $stateParams.ovid;

    $scope.account_info = {
        account_id: 0,
        company: '',
        email:'',
        address_type:'',
        address:'',
        address2:'',
        city:'',
        state:'',
        zip:'',
        phone_number:'',
        fax_number:'',
        avatar:'',
        registered_date:'',
        last_login_date:'',
        upload_folder:'',
        member_id:'',
        is_deleted:'',
        is_date:'',
        diskspace_cap:'',
        is_frozen:'',
        priv_administer:'',
        priv_new_themes:'',
        cookie_hash:'',
        is_public: 0,
        public_description:'',
        file_name:'',
        gen_file_name: ''
    };

    $scope.member_info ={
        address:'',
        address2:'',
        city:'',
        email:'',
        phone_number:'',
        state:'',
        zip:''
    }

    $scope.address_type_list = [];
    $scope.state_list = [];
    $scope.email_list = [];
    $scope.phone_number_list = [];
    $scope.account_type_list = [];
    $scope.title_bar = '';


    $scope.totalStorage = 0;
    
    
    $scope.gettotalStorage = function(){
		ssHelper.post('/subscribe/Gettotalstorage', {acc_id : $scope.acc_id}).then(function (response) {
		    if (response.data.OK == 1) {
		        $scope.totalStorage = response.data.totalStorage
		    }
		});
    }


    $scope.saveChange = function(){
        ssHelper.post('/account/checkexisteompanyname', {
                    account_id: $scope.account_info.account_id,
                    company_name: $scope.account_info.company

                }).then(function (response) {
                    if (response.data.OK == 1) {
                        $scope.save_account_info();
                    } else {
                        var dlg = dialogs.confirm('','Warning: There is already an account with the same Organization Name. You can use this name for now, but we reserve the right to change it later should a dispute arise.', {size: 'lg'});
                        dlg.result.then(function(){
                            $scope.save_account_info();
                        },function(){

                        });
                    }
                });

    }

    $scope.save_account_info = function(){
    	$scope.waiting_loader = true;
        ssHelper.post('/account/account', {
            formInfo: $scope.account_info,
            save_change: '1',
            email_list: $scope.email_list,
            phone_number_list: $scope.phone_number_list
        }).then(function (response) {
            //$scope.$apply(function(){})
        	 $scope.waiting_loader = false;
            if (response.data.OK == 1) {
                var acc_id = response.data.account_user.account_id;
                ssHelper.goBackToPreviousPage($scope).then(function(scope){
                	var page = (scope.remote.ivPageCount >1) ? scope.remote.ivPageCount : 0;
                    scope.remote.loadAvatarNavigation();
                    var done = scope.remote.loadItemVariants(page);
                    done.then(function (response) {
                        scope.remote.toggleObjectVariant({id: acc_id, modelName: 'AccountUser'}); // call function in the /main/content
                    });
                })

            } else {
                //var errors = response.data.errors
                errorService.throw(response.data.errors)
            }
            iScrollService.refresh();
        });
    }

    if($scope.ovid > 0){
        ssHelper.post('/account/account', {
        	group_account_id: $scope.ovid
        }).then(function (response) {
            //$scope.$apply(function(){})
            if (response.data.OK == 1) {
                $scope.account_info = _.extend($scope.account_info, response.data.account_user);
                $scope.member_info = _.extend($scope.member_info, response.data.member);

                $scope.account_info.is_public = response.data.account_user.is_public.toString();
                $scope.account_info.address_type = response.data.account_user.address_type.toString();

                $scope.address_type_list = response.data.address_type_list;
                $scope.state_list = response.data.state_list;
                $scope.email_list = response.data.email_list;
                $scope.phone_number_list = response.data.phone_number_list;
                $scope.account_type_list = response.data.account_type_list;
                $scope.title_bar = response.data.title_bar;

                //convert URL to File or Blob for FileReader.readAsDataURL
                if(response.data.fileData != null){
                	$scope.recentFile._file = ssHelper.dataURItoBlob(response.data.fileData);
                }
                $scope.recentFile.random = _.random(0, 9999);
                
                if($scope.acc_id == ''){// in case update account when $stateParams.acc_id is empty;
                	$scope.acc_id = response.data.account_user.account_id;
                }
                $scope.gettotalStorage();
                
                iScrollService.refresh();
            } else {
                //var errors = response.data.errors
                if(response.data.error_code == 1)
                {
                    $state.go("login");
                }
                else if(response.data.error_code == 2)
                {
                    errorService.throw(response.data.errors);
                }
                else{
                    errorService.throw(response.data.errors)
                }
            }
        });
    }
    else
    {
        ssHelper.post('/account/account', {
        }).then(function (response) {
            //$scope.$apply(function(){})
            if (response.data.OK == 1) {
                $scope.account_info = _.extend($scope.account_info, response.data.account_user);
                $scope.member_info = _.extend($scope.member_info, response.data.member);
                $scope.address_type_list = response.data.address_type_list;
                $scope.state_list = response.data.state_list;
                $scope.email_list = response.data.email_list;
                $scope.phone_number_list = response.data.phone_number_list;
                $scope.account_type_list = response.data.account_type_list;
                $scope.title_bar = response.data.title_bar;
                
                $scope.gettotalStorage();
                iScrollService.refresh();

            } else {
                //var errors = response.data.errors
                if(response.data.error_code == 1)
                {
                    $state.go("login");
                }
                if(response.data.error_code == 2)
                {
                    var dlg = $scope.showAccountNotify(response.data.errors);
                    dlg.result.then(function(){
                        //$state.go('main.member');
                        //ssHelper.doneSaveIvAndReturn($scope.ovid, '', '', '', $scope.remote);
                    	  ssHelper.backToPreviousScreen();
                    },function(){

                    });
                }
            }

        });
    }
    

    $scope.use_member_info = function(){
        $scope.account_info = _.extend($scope.account_info, $scope.member_info);
        $scope.account_info.address_type = '1';
//        $scope.account_info.email = $scope.member_info.email;
//        $scope.account_info.address = $scope.member_info.address;
//        $scope.account_info.address2 = $scope.member_info.address2;
//        $scope.account_info.city = $scope.member_info.city;
//        $scope.account_info.state = $scope.member_info.state;
//        $scope.account_info.zip = $scope.member_info.zip;
//        $scope.account_info.phone_number = $scope.member_info.phone_number;
    }

    $scope.back= function(){
        var transitionPromise = ssHelper.backToPreviousScreen();
    }


    $scope.addPhoneNumberField = function () {
        $scope.phone_number_list.push({phone_number:'', comment:''});
        iScrollService.refresh()
    }

    $scope.addEmailField = function () {
        $scope.email_list.push({email:''});
        iScrollService.refresh()
    }

    $scope.removeEmailField = function (index) {
        $scope.email_list.splice(index, 1);
        iScrollService.refresh()
    }

    $scope.removePhoneNumberField = function (index) {
        $scope.phone_number_list.splice(index, 1);
        iScrollService.refresh()
    }
    

    //=================================================== UPLOAD FILE =================================================================

    $scope.triggerIconInput = function () {
        angular.element('#upload_icon_input').trigger('click');
    }

    var uploader = $scope.uploader = new FileUploader({
        headers: { 'Accept': 'application/json' },
        url: $rootScope.baseUrl + '/file/uploadtempimage',
        formData: [{acc_id: $scope.acc_id, checkStorage: '1', isAvatar : '1'}],
        autoUpload: true
        //removeAfterUpload: true
    });

    $scope.type='';
    var validFileExtensions = '|jpg|png|jpeg|gif|svg+xml|';
    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            //var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            $scope.type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            var _validFileExtensions = validFileExtensions.indexOf($scope.type) !== -1;
            var enoughStorage = $scope.totalStorage >= item.size; //compare the file size with the remain storage of user
            return _validFileExtensions && enoughStorage;
        }
    });

    uploader.onWhenAddingFileFailed = function(item, filter, options){
        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
        var _validFileExtensions = validFileExtensions.indexOf(type) !== -1;
        //show message when condition invalid
        ssHelper.checkExtAndStorage(_validFileExtensions, item.size, $scope.totalStorage, $scope.acc_id);
    }


    $scope.recentFile = {_file: {}};
    /*** after user picked a file, upload file to the temp directory immediately***/
    uploader.onAfterAddingFile = function (fileItem) {
        $scope.recentFile = fileItem;
        $scope.recentFile.random = _.random(0, 9999)
    };

    $scope.scaledImageForCrop = {};
    $scope.imageTempUrl = ""
    //done to upload to temp folder
    uploader.onSuccessItem = function (fileItem, responseData, status, headers) {
        //console.info('onSuccessItem', fileItem, responseData, status, headers);
        $scope.formLoading = true;
        if (responseData.OK == 1) {
            $scope.account_info.file_name = responseData.fileName;
            $scope.account_info.gen_file_name = responseData.genFileName;

            //get info of scaled image
            $scope.scaledImageForCrop = responseData.scaledImageForCrop; // {'filename':string, width:number, height:number, width_scaled:number, height_scaled:number, scale_ratio:number}
            $scope.imageTempUrl = ssHelper.removeBackSlash(responseData.imageTempUrl);

            //leave a bit of time to inform user by effect of the progress bar
            $timeout(function () {
                $scope.formLoading = false;
                uploader.clearQueue();
                if($scope.type != '|svg+xml|'){
                    $scope.openCropModal()
                }

            }, 1200);
        } else {
            ssHelper.errorLog(responseData.errors);
        }
    };

    $scope.openCropModal = function () {
        $scope.selection = {x1: 0, y1: 0, x2: 0, y2: 0};
        $scope.ngImgAreaSelectInstance = {};
        $scope.options = {
            handles: true,
            movable: true,
            resizable: true,
            show: true
            , x1: 0,
            y1: 0,
            x2: $scope.scaledImageForCrop.width_scaled,
            y2: $scope.scaledImageForCrop.height_scaled
        };

        $scope.$on('img-area-select-change', function (event, args) { /* console.log(args.img);*/
        });


        var cropImagePopupInstance = $modal.open({
            templateUrl: _yii_app.absTemplatePath + '/modal-crop-image.html',
            windowTemplateUrl: _yii_app.absTemplatePath + '/modal-crop-image-frame.html',
            windowClass: '',
            resolve: {
                scopeData: function () {
                    return {
                        scaledImageForCrop: $scope.scaledImageForCrop,
                        previewImage: $scope.imageTempUrl,
                        ngImgAreaSelectInstance: $scope.ngImgAreaSelectInstance,
                        options: $scope.options,
                        selection: $scope.selection,
                        recentFile: $scope.recentFile
                    }
                }
            },
            backdrop: true,
            controller: function ($scope, $modalInstance, $q, scopeData) {
                $scope = _.extend($scope, scopeData)

                $scope.apiCropImage = function (coords) {
                    coords.fileName = $scope.scaledImageForCrop.filename; //required: temp file name after it's uploaded

                    var deferred = $q.defer();
                    ssHelper.post('/File/CropTempImage', coords).then(function (response) {
                        //$scope.$apply(function(){})
                        if (response.data.OK == 1) {
                            $scope.recentFile._file = ssHelper.dataURItoBlob(response.data.fileData);
                            $scope.recentFile.random = _.random(0, 9999)
                            $modalInstance.close({msg: 'done'});
                        } else {
                            $modalInstance.dismiss('error');
                            dialogs.notify('Crop Image Error', response.data.errors);
                        }

                        deferred.resolve({cropOK: response.data.OK});
                    });
                    return deferred.promise;
                }
                $scope.shrinkToFit = function () {
                    var coords = {
                        width: $scope.scaledImageForCrop.width_scaled,
                        height: $scope.scaledImageForCrop.height_scaled,
                        x1: 0,
                        y1: 0
                    }
                    coords.scale_ratio = $scope.scaledImageForCrop.scale_ratio;
                    var promise = $scope.apiCropImage(coords);
                    promise.then(function (cropOK) {
                        if (cropOK == 1) {
                            ssHelper.debugLog('shrink to fit done');
                            $modalInstance.close({msg: 'done'});
                        }
                    })
                };

                $scope.Crop = function () {
                    //$modalInstance.dismiss('cancel');
                    var coords = $scope.ngImgAreaSelectInstance.getSelection();
                    /* {height: number, width: number, x1: number, x2: number, y1: number, y2: number}*/
                    coords.scale_ratio = $scope.scaledImageForCrop.scale_ratio;
                    //send coordinates and crop the image on temp folder
                    var promise = $scope.apiCropImage(coords);
                    promise.then(function (cropOK) {
                        if (cropOK == 1) {
                            ssHelper.debugLog('crop done');
                            $modalInstance.close({msg: 'done'});
                        }
                    })
                };
            }
        });
    }

    $scope.showAccountNotify = function (message){
        var accountPopupInstance = $modal.open({
            templateUrl: _yii_app.absTemplatePath + '/modal-account.html',
            controller: 'AccountModalCtrl',
            backdrop: false,
            resolve: {
                message: function () {
                    return message;
                }
            }
        });
        return accountPopupInstance;
    }
    

});

angular.module('zwenGlobal').controller('AccountModalCtrl', function ($scope, $modalInstance,message)    {
    $scope.message = message;
    $scope.Ok = function(){
        $modalInstance.close();
    }
});