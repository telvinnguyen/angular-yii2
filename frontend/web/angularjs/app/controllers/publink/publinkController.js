angular.module('zwenGlobal').controller('publinkController', function ($rootScope, $window, $scope, $http, $state, $stateParams, $q, $timeout, dialogs, ssHelper, errorService, $sce) {
    $rootScope.layoutPath = _yii_app.layoutPath;
    $scope.acc_id = $stateParams.acc_id;
    $scope.kiosk_id = $stateParams.kiosk_id;
    $scope.mtype = $stateParams.mtype;
    $scope.ovid = $stateParams.ovid;
    $scope.sign_iv_id = $stateParams.sign_iv_id;
    $scope.homebtn = $stateParams.homebtn;
    

    $scope.content = {}

    $scope.width = $window.innerWidth; // user's browser screen width
    $scope.height = $window.innerHeight; // user's browser screen height

    ssHelper.post('/sign/getpublink', {
        acc_id: $scope.acc_id,
        kiosk_id: $scope.kiosk_id,
        mtype: $scope.mtype,
        ovid: $scope.ovid,
        sign_iv_id: $scope.sign_iv_id 
    }).then(function (response) {
        if (response.data.OK == 1) {

            $scope.homeUrl = response.data.homeUrl;
            $scope.menuButtons = response.data.menuButtons;
            $scope.handicapButton = response.data.handicapButton;
            $scope.handicapBtnSetting = response.data.handicapBtnSetting;
            $scope.content = response.data.content;
            if($scope.content.ext == '.document'){
           	 $scope.content.url =  $sce.trustAsResourceUrl($scope.content.url);
           }

        } else{
           // errorService.throw(response.data.errors);
        }
    });
});