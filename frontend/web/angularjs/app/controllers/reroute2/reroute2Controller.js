angular.module('zwenGlobal').controller('reroute2Controller', function ($rootScope, $scope, $http, $state, $stateParams, $q, iScrollService, $timeout, dialogs, ssHelper) {
    $rootScope.layoutPath = _yii_app.layoutPath;

    $scope.acc_id = $stateParams.acc_id;
    $scope.sign_id = $stateParams.sign_id;

    $scope.skinZoom = 40; //40%
    $scope.toSkinZoom = 40; //40%

    $scope.screens = [
        {name: 'manage-rules'},
        {name: 'set-rule'}
    ]

    $scope.getScreen = function (condition) {
        return _.findWhere($scope.screens, condition)
    };


    $scope.currentScreen = $scope.getScreen({name: 'set-rule'});

    $scope.switchScreen = function (name) {
        $scope.currentScreen = $scope.getScreen({name: name});
    };

    $scope.pageTitle = 'Redirection from Sign';


    $scope.steps = [
        {
            name: 'selectSourceBox',
            number: '1',
            title: 'Select Source Box'
        },
        {
            name: 'selectTargetSign',
            number: '2',
            title: 'Select Target Sign'
        },
        {
            name: 'selectTargetBox',
            number: '3',
            title: 'Select Target Box'
        },
        {
            name: 'done',
            number: '',
            title: 'Completed'
        }
    ]

    $scope.getStep = function (condition) {
        return _.findWhere($scope.steps, condition)
    };


    $scope.currentStep = $scope.getStep({name: 'selectSourceBox'});

    $scope.zoomLevelTip =  {
        content: {
            text: 'You can adjust the zoom level (%) of the layout below',
            button: false
        },
        position: {
            //container: angular.element(element).closest('.step-body'),
            my: 'top left',
            at: 'bottom right', //'top center',
            //adjust: {method: 'shift none'},
            //target: $(element).parent(),

        },
        style: {
            width: {min: 200},
        },
        show: 'mouseover'
    };
    $scope.searchTargetSign_keyword = '';

    /*
    $scope.$on('$includeContentLoaded', function(){
        console.log(arguments);
    });
    */

    $scope.sharedData = {
        sign: {},
        skin: {},
        toSign: {},
        toSkin: {},
        from_box_coord: '',
        to_sign_id: '',
        to_box_coords: [],
    }

    $scope.navWidth = Math.floor(100 / $scope.steps.length)

    $scope.mainLoader = true;

    $scope.buildRuleItems = function(rules){
        _.each(rules, function(entry, index){
            entry.titleHtml = s.sprintf('%s %s (%s,%s) %s  %s  %s (%s,%s)',
                '<i class="fa fa-television"></i>',
                entry.fromSign.name, entry.from_box.c+1,entry.from_box.r+1,
                '<i class="fa fa-long-arrow-right"></i>',
                '<i class="fa fa-television"></i>',
                entry.toSign.name, entry.to_box.c+1,entry.to_box.r+1);

            entry.name = s.sprintf('Redirection from %s (%s,%s) to %s (%s,%s)',
                entry.fromSign.name, entry.from_box.c+1,entry.from_box.r+1,
                entry.toSign.name, entry.to_box.c+1,entry.to_box.r+1);
        })
        return rules;
    }

    $scope.getRules = function () {
        $scope.mainLoader = true;
        ssHelper.post('/SignRdr/getRules', {from_sign_id: $scope.sign_id, acc_id: $scope.acc_id}).then(function (response) {

            if (response.data.OK == 1) {

                $scope.sharedData.signRules = $scope.buildRuleItems(response.data.signRules);
                console.log($scope.sharedData.signRules)
                $scope.mainLoader = false;
            }
        });
    };
    
    $scope.removeRule = function(rule){

        var dlg = dialogs.confirm('', 'Remove Rule: "' + rule.name + '". Are you sure?', {size: 'md'});

        //var deferred = $q.defer();
        dlg.result.then(function (btn) { // if user OK
            $scope.mainLoader = true;
            ssHelper.post('/Signrdr/removeRule', {
                from_sign_id: rule.fromSign.sign_id, to_sign_id: rule.toSign.sign_id,
                from_box_coord: rule.from_box_coord, to_box_coord: rule.to_box_coord
            }).then(function (response) {
                if(response.data.OK == 1){
                    $scope.sharedData.signRules = _.without($scope.sharedData.signRules, rule);
                }
                $scope.mainLoader = false;
            });
        }, function (btn) {});

    }

    $scope.getSourceSignData = function () {
        ssHelper.post('/SignRdr/getSourceSignData', {sign_id: $scope.sign_id, acc_id: $scope.acc_id}).then(function (response) {
            //$scope.$apply(function(){})
            if (response.data.OK == 1) {

                $scope.sharedData.skin = response.data.skin //_.extendOwn(response.data.skin, $scope.sharedData.skin);
                $scope.sharedData.sign = response.data.sign //_.extendOwn(response.data.sign, $scope.sharedData.sign);

                $scope.pageTitle = s.sprintf('Redirection from %s', $scope.sharedData.sign.name);
                $scope.mainLoader = false;
            }
        });
    }


    $scope.sourceBoxOnClick = function (box) {
        $scope.pageTitle = s.sprintf('Redirection from %s (%s,%s)', $scope.sharedData.sign.name, box.c+1,box.r+1);
        $scope.sharedData.from_box = box;
        $scope.sharedData.from_box_coord = s.sprintf('(%s,%s)', box.c, box.r);

        $timeout(function () {
            $scope.currentStep = $scope.getStep({name: 'selectTargetSign'})

            $scope.getTargetSignLayouts();

            iScrollService.refresh()
        })
    };

    $scope.targetSignLayouts = [];
    $scope.getTargetSignLayouts = function () {

        $scope.selectTargetSign_loader = true;
        ssHelper.post('/SignRdr/GetListSignItems', {
            from_sign_id: $scope.sign_id, from_box_coord: $scope.sharedData.from_box_coord,
            acc_id: $scope.acc_id
        }).then(function (response) {
            //$scope.$apply(function(){})
            if (response.data.OK == 1) {

                $scope.targetSignLayouts = response.data.list;
                var rules = response.data.active_to_rdr;
                console.log(rules)
                _.each($scope.targetSignLayouts, function(signIv, index){
                    signIv.hasRedirect = _.contains(rules.to_sign_id_list, signIv.sign_id)
                })
            }
            iScrollService.refresh()
            $scope.selectTargetSign_loader = false;
        });
    }

    $scope.getTargetSignLayouts();

    $scope.active_boxes_name = '';
    $scope.getTargetSignLayout = function (sign_id) {
        $scope.selectTargetBox_loader = true;
        ssHelper.post('/SignRdr/GetTargetSignLayout', {
            from_sign_id: $scope.sharedData.sign.sign_id,
            to_sign_id: sign_id,
            acc_id: $scope.acc_id,
            from_box_coord: $scope.sharedData.from_box_coord
        }).then(function (response) {
            //$scope.$apply(function(){})
            if (response.data.OK == 1) {
                $scope.sharedData.toSkin = response.data.skin;
                $scope.sharedData.toSign = response.data.sign, $scope.sharedData.toSign;
                var rules = response.data.active_to_rdr;


                _.each($scope.sharedData.toSkin.boxes, function(box){
                    box.active = _.contains(rules.to_box_coord_list, s.sprintf('(%s,%s)', box.c, box.r)) //true or false
                    //if(box.active){
                    //    $scope.active_boxes_name += s.sprintf('(%s,%s)', box.c+1, box.r+1)
                    //}

                })

                iScrollService.refresh()
                $scope.selectTargetBox_loader = false;
            }else{
                dialogs.notify('', 'There is problem to get the sign info from the server')
            }
        });
    };

    $scope.targetSignOnClick = function (sign) {

        $scope.pageTitle = s.sprintf('Redirection from %s(%s,%s) to %s', $scope.sharedData.sign.name, $scope.sharedData.from_box.c+1,$scope.sharedData.from_box.r+1, sign.sign_name);

        $scope.sharedData.toSign = sign;
        $scope.sharedData.to_sign_id = sign.sign_id;

        $timeout(function () {

            $scope.currentStep = $scope.getStep({name: 'selectTargetBox'})
            $scope.getTargetSignLayout(sign.sign_id);
        })
    };


    $scope.targetBoxOnClick = function (box) {
        var active_boxes_name = '';
        _.each($scope.sharedData.toSkin.boxes, function(box){
            if(box.active){
                active_boxes_name += s.sprintf('(%s,%s)', box.c+1, box.r+1)
            }
        })

        $scope.pageTitle = s.sprintf('Redirection from %s (%s,%s) to %s %s',
            $scope.sharedData.sign.name, $scope.sharedData.from_box.c+1,$scope.sharedData.from_box.r+1,
            $scope.sharedData.toSign.name, active_boxes_name);
        /*
         $scope.sharedData.from_box_id = box.id;
         var selectSourceBox_step = $scope.getStep({name: 'selectSourceBox'})
         var selectTargetSign_step = $scope.getStep({name: 'selectTargetSign'})

         $timeout(function () {

         selectSourceBox_step.active = false;
         selectTargetSign_step.active = true;
         })
         */
    };

    $scope.savingLoader = false;
    //save
    $scope.applyAllChange = function () {
        var deferred = $q.defer();

        var _to_box_coords = []
        _.each($scope.sharedData.toSkin.boxes, function(box){
            if(box.active){
                _to_box_coords.push(s.sprintf('(%s,%s)', box.c, box.r));
            }
        })

        $scope.savingLoader = true;
        ssHelper.post('/Signrdr/SaveRedirectionRules', {
            from_sign_id: $scope.sharedData.sign.sign_id,
            from_box_coord: $scope.sharedData.from_box_coord,
            to_sign_id: $scope.sharedData.toSign.sign_id,
            to_box_coords: _to_box_coords
        }).then(function (response) {
            //$scope.$apply(function(){})
            if (response.data.OK == 1) {
                $scope.savingLoader = false;
                $scope.switchScreen('manage-rules')
                deferred.resolve();
            }
        });

        return deferred.promise
    };

    $scope.startOver = function () {
        $state.go('main.reroute-setup', {acc_id: $scope.acc_id},{reload: true})
    };


    //when everything completed
    $scope.key = 'applyRdr';
    $scope.$root.$on($scope.key + '-save-done-animation', function(){
        var selectTargetBox_step = $scope.getStep({name: 'selectTargetBox'})
        var done_step = $scope.getStep({name: 'done'})
        $timeout(function () {
            selectTargetBox_step.active = false;

            done_step.active = true;
            done_step.enabled = true;
        })
    })
    /*** :FOR set-rule screen ***/



    $scope.$watch('currentScreen', function (currentScreen) {
        if(currentScreen.name == 'manage-rules'){
            $scope.getRules();
        }
        else if(currentScreen.name == 'set-rule'){
            $timeout(function () {
                $scope.getSourceSignData();
                $scope.currentStep = $scope.getStep({name: 'selectSourceBox'});
            })
        }
    });
});