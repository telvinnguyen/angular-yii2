angular.module('zwenGlobal').controller('loginController', function ($rootScope, $scope, $modal, modalUtils, httpRequestInterceptor, $http, store, $state, $cookieStore, ssHelper, jwtHelper) {

    $rootScope.baseUrl = _yii_app.baseUrl;
    $rootScope.layoutPath = _yii_app.layoutPath;
    $rootScope.bodyClass = '';

    //if (store.get('ss-identity-jwt') && !jwtHelper.isTokenExpired(store.get('ss-identity-jwt'))) {
    //    $state.go('main.member');
    //}



    $scope.login_form = {
        user_name: '',
        password: '',
        errors: '',
        rememberMe: true
    }

    $scope.loginClick = function () {
        ssHelper.post('/home/login', $scope.login_form).then(function (response) {
            //$scope.$apply(function(){})
            if (response.data.OK == 1) { // if member _id on the server not found, the OK would be 0
                //store.set('ss-identity-jwt', serverAuth)
                $rootScope.authToken = response.data.jwt;
            }else{
                console.log('Your session is not valid')
            }
        });
    }
    
    $scope.resetError = function () {
    	if($scope.login_form.errors != ''){
    		$scope.login_form.errors = '';
    	}
    }

});
