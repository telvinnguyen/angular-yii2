app.config(function ($controllerProvider, $compileProvider, $filterProvider, $locationProvider, $provide) {
    app.register = {
        controller: $controllerProvider.register,
        directive: $compileProvider.directive,
        filter: $filterProvider.register,
        factory: $provide.factory,
        service: $provide.service
    };
});

app.config(['$stateProvider', '$routeProvider', '$httpProvider',
    function ($stateProvider, $routeProvider, $httpProvider) {

        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: _yii_app.controllerPath + '/home/home.html',
                controller: 'homeController',
                data: {
                    css: [
                        _yii_app.angularPath + '/app/controllers/home/home.css',
                    ]
                },
                resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                                name: 'zwenGlobal',
                                files: [
                                    _yii_app.controllerPath + '/home/homeController.js',
                                ]
                            }
                        );
                    }]
                }
            })
//===========================================1:BEGIN: REGISTRATION ========================================================
            .state('registration', {
                url: '/registration',
                templateUrl: _yii_app.controllerPath + '/registration/registration.html',
                controller: 'registrationController',
                data: {
                    css: [
                        _yii_app.angularPath + '/app/controllers/registration/registration.css',
                        _yii_app.angularPath + '/app/css/custom.css',
                        _yii_app.angularPath + '/app/css/ss/js/qtip/css/jquery.qtip.css',
                        _yii_app.angularPath + '/app/directives/common/imgAreaSelect/css/img-area-select.css',
                        _yii_app.angularPath + '/app/css/color-progress-bar.css',
                        _yii_app.angularPath + '/app/css/ss/css/jquery-ui.css',
                        _yii_app.angularPath + '/app/css/ss/css/screen.css'
                    ]
                },
                resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load({
                                name: 'zwenGlobal',
                                files: [
                                    _yii_app.controllerPath + '/registration/registrationController.js',

                                    //_yii_app.livePath + '/registrationController.min.js',
                                    _yii_app.layoutPath + '/js/jquery.imgareaselect.min.js',
                                    _yii_app.layoutPath + '/js/qtip/js/jquery.qtip.old.js',
                                ]
                            }
                        );
                    }]
                }
            })

//===========================================1:END: REGISTRATION ==========================================================

//===========================================2:BEGIN: login ========================================================
            .state('login', {
                url: '/login',
                templateUrl: _yii_app.controllerPath + '/login/login.html',
                controller: 'loginController',
                data: {
                    css: [
                        _yii_app.angularPath + '/app/controllers/login/login.css',
                        _yii_app.angularPath + '/app/css/ss/css/screen.css'
                    ]
                },

                resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load files for an existing module

                        $ocLazyLoad.load('ngEnter');
                        return $ocLazyLoad.load({
                                name: 'zwenGlobal',
                                files: [
                                    _yii_app.controllerPath + '/login/loginController.js',
                                    //_yii_app.livePath + '/loginController.min.js'
                                ]
                            }
                        );
                    }]
                }
            })
//===========================================2:END: login ==========================================================

//===========================================3:BEGIN: forgot ========================================================
            .state('forgot-password', {
                url: '/forgot',
                templateUrl: _yii_app.controllerPath + '/forgot/forgot.html',
                controller: 'forgotController',
                data: {
                    css: [
                        _yii_app.angularPath + '/app/controllers/forgot/forgot.css',
                        _yii_app.angularPath + '/app/css/custom.css',
                        _yii_app.angularPath + '/app/css/ss/css/screen.css'
                    ]
                },
                resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load({
                                name: 'zwenGlobal',
                                files: [
                                    _yii_app.controllerPath + '/forgot/forgotController.js'
                                    //_yii_app.livePath + '/forgotController.min.js'
                                ]
                            }
                        );
                    }]
                }
            })
//===========================================3:END: forgot ==========================================================
//===========================================4:BEGIN: MAIN ========================================================
            .state('main', {
                url: '/main',
                templateUrl: _yii_app.controllerPath + '/main/main.html'
            })
//===========================================4:END: MAIN ==========================================================


//===========================================5:BEGIN: dev-info ========================================================
            .state('main.dev-info', {
                url: '/dev-info/:acc_id',
                views: {
                    'dev-info@main': {
                        controller: 'devInfoController',
                        templateUrl: _yii_app.controllerPath + '/dev-info/dev-info.html'
                    }
                },
                data: {
                    css: [
                        _yii_app.angularPath + '/app/controllers/dev-info/dev-info.css',
                        _yii_app.angularPath + '/app/css/custom.css',
                        _yii_app.angularPath + '/app/css/ss/css/new_content_database.css',
                        _yii_app.angularPath + '/app/css/ss/css/addcontent121212.css'
                    ], requiresLogin: true
                },
                sticky: true,
                resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load({
                                name: 'zwenGlobal',
                                files: [
                                    _yii_app.controllerPath + '/dev-info/devInfoController.js'
                                    //_yii_app.livePath + '/devInfoController.min.js'
                                ]
                            }
                        );
                    }]
                }
            })
//===========================================5:END: dev-info ==========================================================


//===========================================6:BEGIN: chunkUploadFile ========================================================
            .state('main.chunk-upload-file', {
                url: '/chunk-upload-file/:acc_id/mtype={mtype}/oid={oid}/ovid={ovid}/wovid={wovid}/wtype={wtype}/wgvid={wgvid}',
                views: {
                    'chunk-upload-file@main': {
                        controller: 'chunkUploadFileController',
                        templateUrl: _yii_app.controllerPath + '/chunk-upload-file/chunk-upload-file.html'
                    }
                },
                data: {
                    css: [
                        _yii_app.angularPath + '/app/controllers/chunk-upload-file/chunk-upload-file.css',
                        _yii_app.angularPath + '/app/css/color-progress-bar.css',
                        _yii_app.angularPath + '/app/css/custom.css',
                        _yii_app.angularPath + '/app/css/ss/css/screen.css'
                    ], requiresLogin: true
                },
                resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        //$ocLazyLoad.load('plupload.directive')
                        return $ocLazyLoad.load([
                                {
                                    name: 'zwenGlobal',
                                    files: [
                                        _yii_app.controllerPath + '/chunk-upload-file/chunkUploadFileController.js',
                                        //_yii_app.controllerPath + '/livePath/chunkUploadFileController.min.js'
                                    ]
                                },
                                'plupload.directive'
                            ]
                        );
                    }]
                }

            })
//===========================================6:END: chunkUploadFile ========================================================


            .state("otherwise", {
                url: "*path",
                templateUrl: _yii_app.controllerPath + '/404/404.html',
                data: {
                    css: [
                        _yii_app.angularPath + '/app/css/custom.css',
                        _yii_app.angularPath + '/app/css/ss/css/screen.css'
                    ]
                },
                controller: function ($scope) {
                    $scope.baseUrl = _yii_app.baseUrl;
                    $scope.layoutPath = _yii_app.layoutPath;
                    $scope.bodyClass = '';

                    $scope.msg = "Sorry, this page doesn't exist."

                }
            });
    }]);

/* last state #: 110*/



app.run(function ($rootScope, $state, $stateParams, store, dialogs, $ocLazyLoad, ssHelper) {

    $rootScope.$on('$stateChangeStart', function (e, toState, toParams, fromState, fromParams) {
        if (toState.data && toState.data.requiresLogin) {
            if (!store.get('ss-identity-jwt')) { // || jwtHelper.isTokenExpired(store.get('ss-identity-jwt'))
                e.preventDefault(); // add this to make sure the next line $state.go is going to work correctly
                $state.go('login');
            } else {

                //valdidate account id
                if (toState.data.requireSuperAdminRole && toParams.acc_id != '1') {
                    e.preventDefault();
                    dialogs.notify('', 'Access denined.', {size: 'sm'});
                    $state.go('home');
                }

                if (ssHelper.isNumber(toParams.acc_id)) {

                    ssHelper.post('/subscribe/Validateaccount', {
                        acc_id: toParams.acc_id
                    }).then(function (response) {
                        //$scope.$apply(function(){})
                        if (response.data.OK == 1) {
                            if (!response.data.allowToAccess) {
                                $state.go('home');
                            }
                        }
                    });
                }
            }
        }
        else if (toState.data.isPublished) {
            //startGlobalServer($rootScope, ratchetService, store, $timeout);
        }
    });

    $rootScope.$state = $state;
})



