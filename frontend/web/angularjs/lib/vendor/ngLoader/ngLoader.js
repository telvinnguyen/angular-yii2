/**!
 * AngularJS Loader animation
 * @author  James Feigel <james.feigel@gmail.com>
 * @version 0.1.0
 */
angular.module('ngLoaderTemplates', [
    'template/ngLoader/ngLoaderTemplate1.html', 'template/ngLoader/ngLoaderTemplate2.html',
    'template/ngLoader/ngLoaderTemplate3.html', 'template/ngLoader/ngLoaderTemplate4.html',
    'template/ngLoader/ngLoaderTemplate5.html', 'template/ngLoader/ngLoaderTemplate6.html',
    'template/ngLoader/ngLoaderTemplate7.html', 'template/ngLoader/ngLoaderTemplate8.html',
    'template/ngLoader/ngLoaderTemplate9.html', 'template/ngLoader/ngLoaderTemplate10.html',
    'template/ngLoader/ngLoaderTemplate11.html'
]);

angular.module('template/ngLoader/ngLoaderTemplate1.html', []).run(['$templateCache', '$sce',
  function($templateCache, $sce) {
    $templateCache.put($sce.getTrustedResourceUrl('template/ngLoader/ngLoaderTemplate1.html'),
      "<div class=\"loader\" data-ng-show=\"working\" style=\"position: absolute;top: 0px;bottom: 0px;left: 0px;right: 0px;height: 100% !important;width: 100% !important;\">\n" +
      "  <div class=\"loader-content\" style=\"position: absolute;top: 50%;left: 50%;line-height: 1;max-width: 50%;padding: 7px;-o-border-radius: 5px;border-radius: 5px;background-color: rgba(0, 50, 160, 0.5);color: #ffffff;text-transform: uppercase;text-align: center;word-break: break-word;z-index: 1;\">\n" +
      "    <svg version=\"1.1\" id=\"loader_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\n" +
      "   width=\"30px\" height=\"30px\" viewBox=\"0 0 40 40\" enable-background=\"new 0 0 40 40\" xml:space=\"preserve\">\n" +
      "      <path opacity=\"0.2\" fill=\"#FFFFFF\" d=\"M20.201,5.169c-8.254,0-14.946,6.692-14.946,14.946c0,8.255,6.692,14.946,14.946,14.946\n" +
      "        s14.946-6.691,14.946-14.946C35.146,11.861,28.455,5.169,20.201,5.169z M20.201,31.749c-6.425,0-11.634-5.208-11.634-11.634\n" +
      "        c0-6.425,5.209-11.634,11.634-11.634c6.425,0,11.633,5.209,11.633,11.634C31.834,26.541,26.626,31.749,20.201,31.749z\"/>\n" +
      "      <path fill=\"#FFFFFF\" d=\"M26.013,10.047l1.654-2.866c-2.198-1.272-4.743-2.012-7.466-2.012h0v3.312h0\n" +
      "        C22.32,8.481,24.301,9.057,26.013,10.047z\">\n" +
      "        <animateTransform attributeType=\"xml\"\n" +
      "          attributeName=\"transform\"\n" +
      "          type=\"rotate\"\n" +
      "          from=\"0 20 20\"\n" +
      "          to=\"360 20 20\"\n" +
      "          dur=\"0.5s\"\n" +
      "          repeatCount=\"indefinite\"/>\n" +
      "      </path>\n" +
      "    </svg>\n" +
      "    <p style=\"margin: 2px 0px;\" data-ng-bind=\"message\" data-ng-cloak data-ng-show=\"message\"></p>\n" +
      "  </div>\n" +
      "</div>");
}]);
angular.module('template/ngLoader/ngLoaderTemplate2.html', []).run(['$templateCache', '$sce',
  function($templateCache, $sce) {
    $templateCache.put($sce.getTrustedResourceUrl('template/ngLoader/ngLoaderTemplate2.html'),
      "<div class=\"loader\" data-ng-show=\"working\" style=\"position: absolute;top: 0px;bottom: 0px;left: 0px;right: 0px;height: 100% !important;width: 100% !important;\">\n" +
      "  <div class=\"loader-content\" style=\"position: absolute;top: 50%;left: 50%;line-height: 1;max-width: 50%;padding: 7px;-o-border-radius: 5px;border-radius: 5px;background-color: rgba(214, 214, 214, 0.5);color: #ffffff;text-transform: uppercase;text-align: center;word-break: break-word;z-index: 1;\">\n" +
      "    <svg version=\"1.1\" id=\"loader_2\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\n" +
      "     width=\"30px\" height=\"30px\" viewBox=\"0 0 50 50\" style=\"enable-background:new 0 0 50 50;\" xml:space=\"preserve\">\n" +
      "      <path fill=\"#FFFFFF\" d=\"M25.251,6.461c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615V6.461z\">\n" +
      "        <animateTransform attributeType=\"xml\"\n" +
      "          attributeName=\"transform\"\n" +
      "          type=\"rotate\"\n" +
      "          from=\"0 25 25\"\n" +
      "          to=\"360 25 25\"\n" +
      "          dur=\"0.6s\"\n" +
      "          repeatCount=\"indefinite\"/>\n" +
      "      </path>\n" +
      "    </svg>\n" +
      "    <p style=\"margin: 2px 0px;\" data-ng-bind=\"message\" data-ng-cloak data-ng-show=\"message\"></p>\n" +
      "  </div>\n" +
      "</div>");
}]);
angular.module('template/ngLoader/ngLoaderTemplate3.html', []).run(['$templateCache', '$sce',
  function($templateCache, $sce) {
    $templateCache.put($sce.getTrustedResourceUrl('template/ngLoader/ngLoaderTemplate3.html'),
      "<div class=\"loader\" data-ng-show=\"working\" style=\"position: absolute;top: 0px;bottom: 0px;left: 0px;right: 0px;height: 100% !important;width: 100% !important;\">\n" +
      "  <div class=\"loader-content\" style=\"position: absolute;top: 50%;left: 50%;line-height: 1;max-width: 50%;padding: 7px;-o-border-radius: 5px;border-radius: 5px;background-color: rgba(214, 214, 214, 0.5);color: #ffffff;text-transform: uppercase;text-align: center;word-break: break-word;z-index: 1;\">\n" +
      "    <svg version=\"1.1\" id=\"loader_3\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\n" +
      "     width=\"30px\" height=\"30px\" viewBox=\"0 0 50 50\" style=\"enable-background:new 0 0 50 50;\" xml:space=\"preserve\">\n" +
      "      <path fill=\"#FFFFFF\" d=\"M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0,14.615,6.543,14.615,14.615H43.935z\">\n" +
      "        <animateTransform attributeType=\"xml\"\n" +
      "          attributeName=\"transform\"\n" +
      "          type=\"rotate\"\n" +
      "          from=\"0 25 25\"\n" +
      "          to=\"360 25 25\"\n" +
      "          dur=\"0.6s\"\n" +
      "          repeatCount=\"indefinite\"/>\n" +
      "      </path>\n" +
      "    </svg>\n" +
      "    <p style=\"margin: 2px 0px;\" data-ng-bind=\"message\" data-ng-cloak data-ng-show=\"message\"></p>\n" +
      "  </div>\n" +
      "</div>");
}]);
angular.module('template/ngLoader/ngLoaderTemplate4.html', []).run(['$templateCache', '$sce',
  function($templateCache, $sce) {
    $templateCache.put($sce.getTrustedResourceUrl('template/ngLoader/ngLoaderTemplate4.html'),
      "<div class=\"loader\" data-ng-show=\"working\" style=\"position: absolute;top: 0px;bottom: 0px;left: 0px;right: 0px;height: 100% !important;width: 100% !important;\">\n" +
      "  <div class=\"loader-content\" style=\"position: absolute;top: 50%;left: 50%;line-height: 1;max-width: 50%;padding: 7px;-o-border-radius: 5px;border-radius: 5px;background-color: rgba(214, 214, 214, 0.5);color: #ffffff;text-transform: uppercase;text-align: center;word-break: break-word;z-index: 1;\">\n" +
      "    <svg version=\"1.1\" id=\"loader_4\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\n" +
      "     width=\"16px\" height=\"16px\" viewBox=\"0 0 24 24\" style=\"enable-background:new 0 0 50 50;\" xml:space=\"preserve\">\n" +
      "      <rect x=\"0\" y=\"0\" width=\"4\" height=\"7\" fill=\"#FFFFFF\">\n" +
      "        <animateTransform  attributeType=\"xml\"\n" +
      "          attributeName=\"transform\" type=\"scale\"\n" +
      "          values=\"1,1; 1,3; 1,1\"\n" +
      "          begin=\"0s\" dur=\"0.6s\" repeatCount=\"indefinite\" />       \n" +
      "      </rect>\n" +
      "      <rect x=\"10\" y=\"0\" width=\"4\" height=\"7\" fill=\"#FFFFFF\">\n" +
      "        <animateTransform  attributeType=\"xml\"\n" +
      "          attributeName=\"transform\" type=\"scale\"\n" +
      "          values=\"1,1; 1,3; 1,1\"\n" +
      "          begin=\"0.2s\" dur=\"0.6s\" repeatCount=\"indefinite\" />       \n" +
      "      </rect>\n" +
      "      <rect x=\"20\" y=\"0\" width=\"4\" height=\"7\" fill=\"#FFFFFF\">\n" +
      "        <animateTransform  attributeType=\"xml\"\n" +
      "          attributeName=\"transform\" type=\"scale\"\n" +
      "          values=\"1,1; 1,3; 1,1\"\n" +
      "          begin=\"0.4s\" dur=\"0.6s\" repeatCount=\"indefinite\" />       \n" +
      "      </rect>\n" +
      "    </svg>\n" +
      "    <p style=\"margin: 2px 0px;\" data-ng-bind=\"message\" data-ng-cloak data-ng-show=\"message\"></p>\n" +
      "  </div>\n" +
      "</div>");
}]);
angular.module('template/ngLoader/ngLoaderTemplate5.html', []).run(['$templateCache', '$sce',
  function($templateCache, $sce) {
    $templateCache.put($sce.getTrustedResourceUrl('template/ngLoader/ngLoaderTemplate5.html'),
      "<div class=\"loader\" data-ng-show=\"working\" style=\"position: absolute;top: 0px;bottom: 0px;left: 0px;right: 0px;height: 100% !important;width: 100% !important;\">\n" +
      "  <div class=\"loader-content\" style=\"position: absolute;top: 50%;left: 50%;line-height: 1;max-width: 50%;padding: 7px;-o-border-radius: 5px;border-radius: 5px;background-color: rgba(214, 214, 214, 0.5);color: #ffffff;text-transform: uppercase;text-align: center;word-break: break-word;z-index: 1;\">\n" +
      "    <svg version=\"1.1\" id=\"loader_5\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\n" +
      "     width=\"16px\" height=\"20px\" viewBox=\"0 0 24 30\" style=\"enable-background:new 0 0 50 50;\" xml:space=\"preserve\">\n" +
      "      <rect x=\"0\" y=\"0\" width=\"4\" height=\"10\" fill=\"#FFFFFF\">\n" +
      "        <animateTransform attributeType=\"xml\"\n" +
      "          attributeName=\"transform\" type=\"translate\"\n" +
      "          values=\"0 0; 0 20; 0 0\"\n" +
      "          begin=\"0\" dur=\"0.6s\" repeatCount=\"indefinite\" />\n" +
      "      </rect>\n" +
      "      <rect x=\"10\" y=\"0\" width=\"4\" height=\"10\" fill=\"#FFFFFF\">\n" +
      "        <animateTransform attributeType=\"xml\"\n" +
      "          attributeName=\"transform\" type=\"translate\"\n" +
      "          values=\"0 0; 0 20; 0 0\"\n" +
      "          begin=\"0.2s\" dur=\"0.6s\" repeatCount=\"indefinite\" />\n" +
      "      </rect>\n" +
      "      <rect x=\"20\" y=\"0\" width=\"4\" height=\"10\" fill=\"#FFFFFF\">\n" +
      "        <animateTransform attributeType=\"xml\"\n" +
      "          attributeName=\"transform\" type=\"translate\"\n" +
      "          values=\"0 0; 0 20; 0 0\"\n" +
      "          begin=\"0.4s\" dur=\"0.6s\" repeatCount=\"indefinite\" />\n" +
      "      </rect>\n" +
      "    </svg>\n" +
      "    <p style=\"margin: 2px 0px;\" data-ng-bind=\"message\" data-ng-cloak data-ng-show=\"message\"></p>\n" +
      "  </div>\n" +
      "</div>");
}]);
angular.module('template/ngLoader/ngLoaderTemplate6.html', []).run(['$templateCache', '$sce',
  function($templateCache, $sce) {
    $templateCache.put($sce.getTrustedResourceUrl('template/ngLoader/ngLoaderTemplate6.html'),
      "<div class=\"loader\" data-ng-show=\"working\" style=\"position: absolute;top: 0px;bottom: 0px;left: 0px;right: 0px;height: 100% !important;width: 100% !important;\">\n" +
      "  <div class=\"loader-content\" style=\"position: absolute;top: 50%;left: 50%;line-height: 1;max-width: 50%;padding: 7px;-o-border-radius: 5px;border-radius: 5px;background-color: rgba(214, 214, 214, 0.5);color: #ffffff;text-transform: uppercase;text-align: center;word-break: break-word;z-index: 1;\">\n" +
      "    <svg version=\"1.1\" id=\"loader_6\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\n" +
      "     width=\"16px\" height=\"20px\" viewBox=\"0 0 24 30\" style=\"enable-background:new 0 0 50 50;\" xml:space=\"preserve\">\n" +
      "      <rect x=\"0\" y=\"13\" width=\"4\" height=\"5\" fill=\"#FFFFFF\">\n" +
      "        <animate attributeName=\"height\" attributeType=\"XML\"\n" +
      "          values=\"5;21;5\" \n" +
      "          begin=\"0s\" dur=\"0.6s\" repeatCount=\"indefinite\" />\n" +
      "        <animate attributeName=\"y\" attributeType=\"XML\"\n" +
      "          values=\"13; 5; 13\"\n" +
      "          begin=\"0s\" dur=\"0.6s\" repeatCount=\"indefinite\" />\n" +
      "      </rect>\n" +
      "      <rect x=\"10\" y=\"13\" width=\"4\" height=\"5\" fill=\"#FFFFFF\">\n" +
      "        <animate attributeName=\"height\" attributeType=\"XML\"\n" +
      "          values=\"5;21;5\" \n" +
      "          begin=\"0.15s\" dur=\"0.6s\" repeatCount=\"indefinite\" />\n" +
      "        <animate attributeName=\"y\" attributeType=\"XML\"\n" +
      "          values=\"13; 5; 13\"\n" +
      "          begin=\"0.15s\" dur=\"0.6s\" repeatCount=\"indefinite\" />\n" +
      "      </rect>\n" +
      "      <rect x=\"20\" y=\"13\" width=\"4\" height=\"5\" fill=\"#FFFFFF\">\n" +
      "        <animate attributeName=\"height\" attributeType=\"XML\"\n" +
      "          values=\"5;21;5\" \n" +
      "          begin=\"0.3s\" dur=\"0.6s\" repeatCount=\"indefinite\" />\n" +
      "        <animate attributeName=\"y\" attributeType=\"XML\"\n" +
      "          values=\"13; 5; 13\"\n" +
      "          begin=\"0.3s\" dur=\"0.6s\" repeatCount=\"indefinite\" />\n" +
      "      </rect>\n" +
      "    </svg>\n" +
      "    <p style=\"margin: 2px 0px;\" data-ng-bind=\"message\" data-ng-cloak data-ng-show=\"message\"></p>\n" +
      "  </div>\n" +
      "</div>");
}]);
angular.module('template/ngLoader/ngLoaderTemplate7.html', []).run(['$templateCache', '$sce',
  function($templateCache, $sce) {
    $templateCache.put($sce.getTrustedResourceUrl('template/ngLoader/ngLoaderTemplate7.html'),
      "<div class=\"loader\" data-ng-show=\"working\" style=\"position: absolute;top: 0px;bottom: 0px;left: 0px;right: 0px;height: 100% !important;width: 100% !important;\">\n" +
      "  <div class=\"loader-content\" style=\"position: absolute;top: 50%;left: 50%;line-height: 1;max-width: 50%;padding: 7px;-o-border-radius: 5px;border-radius: 5px;background-color: rgba(214, 214, 214, 0.5);color: #ffffff;text-transform: uppercase;text-align: center;word-break: break-word;z-index: 1;\">\n" +
      "    <svg version=\"1.1\" id=\"loader_7\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\n" +
      "     width=\"15px\" height=\"18px\" viewBox=\"0 0 25 30\" style=\"enable-background:new 0 0 50 50;\" xml:space=\"preserve\">\n" +
      "      <rect x=\"0\" y=\"8\" width=\"4\" height=\"20\" fill=\"#FFFFFF\">\n" +
      "        <animate attributeName=\"opacity\" attributeType=\"XML\"\n" +
      "          values=\"1; .2; 1\" \n" +
      "          begin=\"0s\" dur=\"0.6s\" repeatCount=\"indefinite\" />\n" +
      "      </rect>\n" +
      "      <rect x=\"7\" y=\"8\" width=\"4\" height=\"20\" fill=\"#FFFFFF\">\n" +
      "        <animate attributeName=\"opacity\" attributeType=\"XML\"\n" +
      "          values=\"1; .2; 1\" \n" +
      "          begin=\"0.2s\" dur=\"0.6s\" repeatCount=\"indefinite\" />\n" +
      "      </rect>\n" +
      "      <rect x=\"14\" y=\"8\" width=\"4\" height=\"20\" fill=\"#FFFFFF\">\n" +
      "        <animate attributeName=\"opacity\" attributeType=\"XML\"\n" +
      "          values=\"1; .2; 1\" \n" +
      "          begin=\"0.4s\" dur=\"0.6s\" repeatCount=\"indefinite\" />\n" +
      "      </rect>\n" +
      "      <rect x=\"21\" y=\"8\" width=\"4\" height=\"20\" fill=\"#FFFFFF\">\n" +
      "        <animate attributeName=\"opacity\" attributeType=\"XML\"\n" +
      "          values=\"1; .2; 1\" \n" +
      "          begin=\"0.4s\" dur=\"0.6s\" repeatCount=\"indefinite\" />\n" +
      "      </rect>\n" +
      "    </svg>\n" +
      "    <p style=\"margin: 2px 0px;\" data-ng-bind=\"message\" data-ng-cloak data-ng-show=\"message\"></p>\n" +
      "  </div>\n" +
      "</div>");
}]);
angular.module('template/ngLoader/ngLoaderTemplate8.html', []).run(['$templateCache', '$sce',
  function($templateCache, $sce) {
    $templateCache.put($sce.getTrustedResourceUrl('template/ngLoader/ngLoaderTemplate8.html'),
      "<div class=\"loader\" data-ng-show=\"working\" style=\"position: absolute;top: 0px;bottom: 0px;left: 0px;right: 0px;height: 100% !important;width: 100% !important;\">\n" +
      "  <div class=\"loader-content\" style=\"position: absolute;top: 50%;left: 50%;line-height: 1;max-width: 50%;padding: 7px;-o-border-radius: 5px;border-radius: 5px;background-color: rgba(214, 214, 214, 0.5);color: #ffffff;text-transform: uppercase;text-align: center;word-break: break-word;z-index: 1;\">\n" +
      "    <svg version=\"1.1\" id=\"loader_8\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\n" +
      "     width=\"16px\" height=\"20px\" viewBox=\"0 0 24 30\" style=\"enable-background:new 0 0 50 50;\" xml:space=\"preserve\">\n" +
      "      <rect x=\"0\" y=\"10\" width=\"4\" height=\"10\" fill=\"#FFFFFF\" opacity=\"0.2\">\n" +
      "        <animate attributeName=\"opacity\" attributeType=\"XML\" values=\"0.2; 1; .2\" begin=\"0s\" dur=\"0.6s\" repeatCount=\"indefinite\" />\n" +
      "        <animate attributeName=\"height\" attributeType=\"XML\" values=\"10; 20; 10\" begin=\"0s\" dur=\"0.6s\" repeatCount=\"indefinite\" />\n" +
      "        <animate attributeName=\"y\" attributeType=\"XML\" values=\"10; 5; 10\" begin=\"0s\" dur=\"0.6s\" repeatCount=\"indefinite\" />\n" +
      "      </rect>\n" +
      "      <rect x=\"10\" y=\"10\" width=\"4\" height=\"10\" fill=\"#FFFFFF\"  opacity=\"0.2\">\n" +
      "        <animate attributeName=\"opacity\" attributeType=\"XML\" values=\"0.2; 1; .2\" begin=\"0.15s\" dur=\"0.6s\" repeatCount=\"indefinite\" />\n" +
      "        <animate attributeName=\"height\" attributeType=\"XML\" values=\"10; 20; 10\" begin=\"0.15s\" dur=\"0.6s\" repeatCount=\"indefinite\" />\n" +
      "        <animate attributeName=\"y\" attributeType=\"XML\" values=\"10; 5; 10\" begin=\"0.15s\" dur=\"0.6s\" repeatCount=\"indefinite\" />\n" +
      "      </rect>\n" +
      "      <rect x=\"20\" y=\"10\" width=\"4\" height=\"10\" fill=\"#FFFFFF\"  opacity=\"0.2\">\n" +
      "        <animate attributeName=\"opacity\" attributeType=\"XML\" values=\"0.2; 1; .2\" begin=\"0.3s\" dur=\"0.6s\" repeatCount=\"indefinite\" />\n" +
      "        <animate attributeName=\"height\" attributeType=\"XML\" values=\"10; 20; 10\" begin=\"0.3s\" dur=\"0.6s\" repeatCount=\"indefinite\" />\n" +
      "        <animate attributeName=\"y\" attributeType=\"XML\" values=\"10; 5; 10\" begin=\"0.3s\" dur=\"0.6s\" repeatCount=\"indefinite\" />\n" +
      "      </rect>\n" +
      "    </svg>\n" +
      "    <p style=\"margin: 2px 0px;\" data-ng-bind=\"message\" data-ng-cloak data-ng-show=\"message\"></p>\n" +
      "  </div>\n" +
      "</div>");
}]);
angular.module('template/ngLoader/ngLoaderTemplate9.html', []).run(['$templateCache', '$sce',
    function($templateCache, $sce) {
        $templateCache.put($sce.getTrustedResourceUrl('template/ngLoader/ngLoaderTemplate9.html'),
            "<div class=\"loader\" data-ng-show=\"working\" style=\"position: absolute;top: 0px;bottom: 0px;left: 0px;right: 0px;height: 100% !important;width: 100% !important;\">\n"+
            "<div class=\"loader-content\" style=\"position: absolute;top: 50%;left: 50%;line-height: 1;max-width: 50%;padding: 7px;-o-border-radius: 5px;border-radius: 5px;background-color: ;color: #ffffff;text-transform: uppercase;text-align: center;word-break: break-word;z-index: 1;\">\n"+

            " <svg width=\"44\" height=\"44\" viewBox=\"0 0 44 44\" xmlns=\"http://www.w3.org/2000/svg\" stroke=\"#fff\">\n" +
            "     <g fill=\"none\" fill-rule=\"evenodd\" stroke-width=\"2\">\n" +
            "         <circle cx=\"22\" cy=\"22\" r=\"1\">\n" +
            "             <animate attributeName=\"r\"\n" +
            "                 begin=\"0s\" dur=\"1.8s\"\n" +
            "                 values=\"1; 20\"\n" +
            "                 calcMode=\"spline\"\n" +
            "                 keyTimes=\"0; 1\"\n" +
            "                 keySplines=\"0.165, 0.84, 0.44, 1\"\n" +
            "                 repeatCount=\"indefinite\" />\n" +
            "             <animate attributeName=\"stroke-opacity\"\n" +
            "                 begin=\"0s\" dur=\"1.8s\"\n" +
            "                 values=\"1; 0\"\n" +
            "                 calcMode=\"spline\"\n" +
            "                 keyTimes=\"0; 1\"\n" +
            "                 keySplines=\"0.3, 0.61, 0.355, 1\"\n" +
            "                 repeatCount=\"indefinite\" />\n" +
            "         </circle>\n" +
            "         <circle cx=\"22\" cy=\"22\" r=\"1\">\n" +
            "             <animate attributeName=\"r\"\n" +
            "                 begin=\"-0.9s\" dur=\"1.8s\"\n" +
            "                 values=\"1; 20\"\n" +
            "                 calcMode=\"spline\"\n" +
            "                 keyTimes=\"0; 1\"\n" +
            "                 keySplines=\"0.165, 0.84, 0.44, 1\"\n" +
            "                 repeatCount=\"indefinite\" />\n" +
            "             <animate attributeName=\"stroke-opacity\"\n" +
            "                 begin=\"-0.9s\" dur=\"1.8s\"\n" +
            "                 values=\"1; 0\"\n" +
            "                 calcMode=\"spline\"\n" +
            "                 keyTimes=\"0; 1\"\n" +
            "                 keySplines=\"0.3, 0.61, 0.355, 1\"\n" +
            "                 repeatCount=\"indefinite\" />\n" +
            "         </circle>\n" +
            "     </g>\n" +
            " </svg>\n" +

            "<p style=\"margin: 2px 0px;\" data-ng-bind=\"message\" data-ng-cloak data-ng-show=\"message\"></p>\n"+
            "</div>\n"+
            "</div>"
        );
    }]);
angular.module('template/ngLoader/ngLoaderTemplate10.html', []).run(['$templateCache', '$sce',
    function($templateCache, $sce) {
        $templateCache.put($sce.getTrustedResourceUrl('template/ngLoader/ngLoaderTemplate10.html'),
            "<div class=\"loader\" data-ng-show=\"working\" style=\"position: absolute;top: 0px;bottom: 0px;left: 0px;right: 0px;height: 100% !important;width: 100% !important;\">\n"+
            "<div class=\"loader-content\" style=\"position: absolute;top: 50%;left: 50%;line-height: 1;max-width: 50%;padding: 7px;-o-border-radius: 5px;border-radius: 5px;background-color: transparent;color: #ffffff;text-transform: uppercase;text-align: center;word-break: break-word;z-index: 1;\">\n"+

            " <svg version=\"1.1\" style=\"height: 20vmin;padding: 3vmin 20vmin;vertical-align: top;\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" viewBox=\"0 0 120 120\" xml:space=\"preserve\">\n" +
            "  	<path fill=\"#D6D6D6\" d=\"M10,40c0,0,0-0.4,0-1.1c0-0.3,0-0.8,0-1.3c0-0.3,0-0.5,0-0.8c0-0.3,0.1-0.6,0.1-0.9c0.1-0.6,0.1-1.4,0.2-2.1\n" +
            "  		c0.2-0.8,0.3-1.6,0.5-2.5c0.2-0.9,0.6-1.8,0.8-2.8c0.3-1,0.8-1.9,1.2-3c0.5-1,1.1-2,1.7-3.1c0.7-1,1.4-2.1,2.2-3.1\n" +
            "  		c1.6-2.1,3.7-3.9,6-5.6c2.3-1.7,5-3,7.9-4.1c0.7-0.2,1.5-0.4,2.2-0.7c0.7-0.3,1.5-0.3,2.3-0.5c0.8-0.2,1.5-0.3,2.3-0.4l1.2-0.1\n" +
            "  		l0.6-0.1l0.3,0l0.1,0l0.1,0l0,0c0.1,0-0.1,0,0.1,0c1.5,0,2.9-0.1,4.5,0.2c0.8,0.1,1.6,0.1,2.4,0.3c0.8,0.2,1.5,0.3,2.3,0.5\n" +
            "  		c3,0.8,5.9,2,8.5,3.6c2.6,1.6,4.9,3.4,6.8,5.4c1,1,1.8,2.1,2.7,3.1c0.8,1.1,1.5,2.1,2.1,3.2c0.6,1.1,1.2,2.1,1.6,3.1\n" +
            "  		c0.4,1,0.9,2,1.2,3c0.3,1,0.6,1.9,0.8,2.7c0.2,0.9,0.3,1.6,0.5,2.4c0.1,0.4,0.1,0.7,0.2,1c0,0.3,0.1,0.6,0.1,0.9\n" +
            "  		c0.1,0.6,0.1,1,0.1,1.4C74,39.6,74,40,74,40c0.2,2.2-1.5,4.1-3.7,4.3s-4.1-1.5-4.3-3.7c0-0.1,0-0.2,0-0.3l0-0.4c0,0,0-0.3,0-0.9\n" +
            "  		c0-0.3,0-0.7,0-1.1c0-0.2,0-0.5,0-0.7c0-0.2-0.1-0.5-0.1-0.8c-0.1-0.6-0.1-1.2-0.2-1.9c-0.1-0.7-0.3-1.4-0.4-2.2\n" +
            "  		c-0.2-0.8-0.5-1.6-0.7-2.4c-0.3-0.8-0.7-1.7-1.1-2.6c-0.5-0.9-0.9-1.8-1.5-2.7c-0.6-0.9-1.2-1.8-1.9-2.7c-1.4-1.8-3.2-3.4-5.2-4.9\n" +
            "  		c-2-1.5-4.4-2.7-6.9-3.6c-0.6-0.2-1.3-0.4-1.9-0.6c-0.7-0.2-1.3-0.3-1.9-0.4c-1.2-0.3-2.8-0.4-4.2-0.5l-2,0c-0.7,0-1.4,0.1-2.1,0.1\n" +
            "  		c-0.7,0.1-1.4,0.1-2,0.3c-0.7,0.1-1.3,0.3-2,0.4c-2.6,0.7-5.2,1.7-7.5,3.1c-2.2,1.4-4.3,2.9-6,4.7c-0.9,0.8-1.6,1.8-2.4,2.7\n" +
            "  		c-0.7,0.9-1.3,1.9-1.9,2.8c-0.5,1-1,1.9-1.4,2.8c-0.4,0.9-0.8,1.8-1,2.6c-0.3,0.9-0.5,1.6-0.7,2.4c-0.2,0.7-0.3,1.4-0.4,2.1\n" +
            "  		c-0.1,0.3-0.1,0.6-0.2,0.9c0,0.3-0.1,0.6-0.1,0.8c0,0.5-0.1,0.9-0.1,1.3C10,39.6,10,40,10,40z\" transform=\"rotate(352.25 40.0001 40.0001)\">\n" +
            "  		<animateTransform attributeType=\"xml\" attributeName=\"transform\" type=\"rotate\" from=\"0 40 40\" to=\"360 40 40\" dur=\"0.8s\" repeatCount=\"indefinite\"></animateTransform>\n" +
            "  	</path>\n" +
            "  	<path fill=\"#D6D6D6\" d=\"M62,40.1c0,0,0,0.2-0.1,0.7c0,0.2,0,0.5-0.1,0.8c0,0.2,0,0.3,0,0.5c0,0.2-0.1,0.4-0.1,0.7\n" +
            "  		c-0.1,0.5-0.2,1-0.3,1.6c-0.2,0.5-0.3,1.1-0.5,1.8c-0.2,0.6-0.5,1.3-0.7,1.9c-0.3,0.7-0.7,1.3-1,2.1c-0.4,0.7-0.9,1.4-1.4,2.1\n" +
            "  		c-0.5,0.7-1.1,1.4-1.7,2c-1.2,1.3-2.7,2.5-4.4,3.6c-1.7,1-3.6,1.8-5.5,2.4c-2,0.5-4,0.7-6.2,0.7c-1.9-0.1-4.1-0.4-6-1.1\n" +
            "  		c-1.9-0.7-3.7-1.5-5.2-2.6c-1.5-1.1-2.9-2.3-4-3.7c-0.6-0.6-1-1.4-1.5-2c-0.4-0.7-0.8-1.4-1.2-2c-0.3-0.7-0.6-1.3-0.8-2\n" +
            "  		c-0.2-0.6-0.4-1.2-0.6-1.8c-0.1-0.6-0.3-1.1-0.4-1.6c-0.1-0.5-0.1-1-0.2-1.4c-0.1-0.9-0.1-1.5-0.1-2c0-0.5,0-0.7,0-0.7\n" +
            "  		s0,0.2,0.1,0.7c0.1,0.5,0,1.1,0.2,2c0.1,0.4,0.2,0.9,0.3,1.4c0.1,0.5,0.3,1,0.5,1.6c0.2,0.6,0.4,1.1,0.7,1.8\n" +
            "  		c0.3,0.6,0.6,1.2,0.9,1.9c0.4,0.6,0.8,1.3,1.2,1.9c0.5,0.6,1,1.3,1.6,1.8c1.1,1.2,2.5,2.3,4,3.2c1.5,0.9,3.2,1.6,5,2.1\n" +
            "  		c1.8,0.5,3.6,0.6,5.6,0.6c1.8-0.1,3.7-0.4,5.4-1c1.7-0.6,3.3-1.4,4.7-2.4c1.4-1,2.6-2.1,3.6-3.3c0.5-0.6,0.9-1.2,1.3-1.8\n" +
            "  		c0.4-0.6,0.7-1.2,1-1.8c0.3-0.6,0.6-1.2,0.8-1.8c0.2-0.6,0.4-1.1,0.5-1.7c0.1-0.5,0.2-1,0.3-1.5c0.1-0.4,0.1-0.8,0.1-1.2\n" +
            "  		c0-0.2,0-0.4,0.1-0.5c0-0.2,0-0.4,0-0.5c0-0.3,0-0.6,0-0.8c0-0.5,0-0.7,0-0.7c0-1.1,0.9-2,2-2s2,0.9,2,2C62,40,62,40.1,62,40.1z\" transform=\"rotate(-229.667 40 40)\">\n" +
            "  		<animateTransform attributeType=\"xml\" attributeName=\"transform\" type=\"rotate\" from=\"0 40 40\" to=\"-360 40 40\" dur=\"0.6s\" repeatCount=\"indefinite\"></animateTransform>\n" +
            "  	</path>\n" +
            "  </svg>\n" +

            "<p style=\"margin: 2px 0px;\" data-ng-bind=\"message\" data-ng-cloak data-ng-show=\"message\"></p>\n"+
            "</div>\n"+
            "</div>"
        );
    }]);
angular.module('template/ngLoader/ngLoaderTemplate11.html', []).run(['$templateCache', '$sce',
    function($templateCache, $sce) {
        $templateCache.put($sce.getTrustedResourceUrl('template/ngLoader/ngLoaderTemplate11.html'),
            "<div class=\"loader\" data-ng-show=\"working\" style=\"position: absolute;top: 0px;bottom: 0px;left: 0px;right: 0px;height: 100% !important;width: 100% !important;\">\n"+
            "<div class=\"loader-content\" style=\"position: absolute;top: 50%;left: 50%;line-height: 1;max-width: 50%;padding: 7px;-o-border-radius: 5px;border-radius: 5px;background-color: transparent;color: #ffffff;text-transform: uppercase;text-align: center;word-break: break-word;z-index: 1;\">\n"+

            " <div class=\"spinner\">\n" +
            "   <div class=\"bounce1\"></div>\n" +
            "   <div class=\"bounce2\"></div>\n" +
            "   <div class=\"bounce3\"></div>\n" +
            " </div>\n" +

            "<p style=\"margin: 2px 0px;\" data-ng-bind=\"message\" data-ng-cloak data-ng-show=\"message\"></p>\n"+
            "</div>\n"+
            "</div>"
        );
    }]);
angular.module('ngLoader', ['ngLoaderTemplates'])
.directive('loader', ['$timeout', '$sce',
  function($timeout, $sce) {
    return {
      scope: {
        working: '=',
        message: '@',
        disableBackground: '@'
      },
      restrict: 'AE',
      replace: true,
      templateUrl: function(tElem, tAttrs) {
        if (tAttrs.template !== undefined) {
          if (isNaN(parseInt(tAttrs.template))) {
            console.error('Directive Error! Attribute \'template\' must be a number. Found \'' + tAttrs.template + '\'');
          }
          else if (parseInt(tAttrs.template) < 1 || parseInt(tAttrs.template) > 20) {
            console.error('Directive Error! Attribute \'template\' must be a number between 1 and 20. Found \'' + tAttrs.template + '\'');
          }
          else {
            return $sce.getTrustedResourceUrl('template/ngLoader/ngLoaderTemplate' + tAttrs.template + '.html');
          }
        }
        return $sce.getTrustedResourceUrl('template/ngLoader/ngLoaderTemplate1.html');
      },
      link: function(scope, elem, attrs) {
        if (attrs.disableBackground == 'true') {
          elem.css({
            'background': 'rgba(240,240,240,0.25)',
            'z-index': '9999'
          });
        }
        else if (attrs.disableBackground === undefined) {}
        else {
          console.error('Directive Error! Attribute \'disable-background\' must be \'true\' for \'false\'. Found \'' + attrs.disableBackground + '\'');
        }
        var content = elem.find('div')[0];
        //$timeout(function() {
          //content.style.marginTop = -(content.offsetHeight / 2)+'px';
          content.style.marginLeft = -(content.offsetWidth / 2)+'px';
        //});
      }
    };
}]);