<?php

namespace backend\controllers;

use common\component\helpers\FileHelper;
use backend\models\UserLoginForm;
use common\models\ZwUser;

use yii\rest\ActiveController;

use Yii;

class HomeController extends ActiveController
{
    public $modelClass = '';

    /*
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
    }
    */



    public function actionAuthenticate(){

        $request = Yii::$app->request;
        $csrfToken = $request->post('csrfToken');

        if(isset($_COOKIE['csrfToken'])){
            if($_COOKIE['csrfToken'] == $csrfToken){
                $user = ZwUser::currUser();

                if($user){
                    return [
                        'member_id' => $user->user_id,
                        'username' => $user->username,
                        'serverCookie' => $_COOKIE['ss-identity'],
                        'OK' => 1
                    ];
                }
            }
        }
        return [ 'OK' => 0];
    }

    public function actionResetpassword(){

        $result = [
            'OK' => 0
        ];
        $request = Yii::$app->request;
        $email = $request->post('email');

        $member = ZwUser::findOne([
            'active' => 1,
            'email' => $email,
        ]);

        if($member != null){
            $newPassword = Yii::$app->security->generateRandomString();
            $member->setPassword($newPassword); //return the hashed password
            $done = \Yii::$app->mailer->compose('forgotPasswordTemplate', ['member' => $member, 'newPassword' => $newPassword])
                ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' robot'])
                ->setTo($member->email)
                ->setSubject('Forgot password from ' . \Yii::$app->name)
                ->send();

            $member->save(false);

            if($done){
                $result = [
                    'OK' => 1,
                    'msg' => Yii::t('backend', 'reset.password.done')
                ];
            }else{

                $result['msg'] = Yii::t('backend.errors', 'reset.password.email.not.sent');
            }

        }else{

            $result['msg'] = Yii::t('backend.errors', 'reset.password.email.not.found');
        }

        return $result;
    }

    public function actionLogin(){
        $result = [
            'OK' => 0
        ];

        $request = Yii::$app->request;

        //var_dump($_COOKIE['ss-identity']);exit;
        //var_dump(Member::getLoginId());exit;

        $userLoginForm = new UserLoginForm();

        //echo $userLoginForm->makePasswordHash('2wenAdmin');exit;

        $userLoginForm->username = $request->post('username');
        $userLoginForm->password = $request->post('password');
        $userLoginForm->rememberMe = filter_var($request->post('rememberMe'), FILTER_VALIDATE_BOOLEAN);
        $getUser = $userLoginForm->getUser();
        
        if($getUser && $getUser->validatePassword($userLoginForm->password, $getUser->password)){
            //$session = Yii::$app->session;

            $authToken = UserLoginForm::createLoginToken($getUser->user_id);
            setcookie('ss-identity', $authToken, time()+24*3600*30*365, '/'); // 1 year


            $csrfToken = bin2hex(random_bytes(32)); // PHP 7.0.0
            setcookie('csrfToken', $csrfToken, time()+24*3600*30*365, '/'); // 1 year


            $result = [
                'OK' => 1,
                'id_token' => $authToken,
                'member_id' => $getUser->user_id,
                'csrfToken' => $csrfToken
            ];
        }else{
            $result['errors'] = Yii::t('backend.errors', 'incorrect.login');
        }

        return $result;

    }

    public function actionLogout(){
        if (isset($_COOKIE['ss-identity'])) {
            unset($_COOKIE['ss-identity']);
            setcookie('ss-identity', '', time() - 3600, '/'); // empty value and old timestamp
            setcookie('csrfToken', '', time() - 3600, '/'); // empty value and old timestamp
        }

        return ['OK' => 1];
    }

    public function actionGetuser(){
        $request = Yii::$app->request;
        $id_token = $request->post('id_token');
        $member_id = $request->post('member_id');
        if($id_token != $_COOKIE['ss-identity']){
            return [ 'OK' => 0, 'errors' => 'The authentication does not match'];
        }

        $member = Member::findOne($member_id);
        if($member == null){
            return [ 'OK' => 0, 'errors' => 'Member information is not found'];
        }

        return [
            'OK' => 1,
            'name' => $member->getFullName(),
            'avatarUrl' => FileHelper::getMemberAvatar($member->member_id, 63, 63, 'img_user_account_picture.png'),
            'member_type' => $member->member_type
        ];
    }

}
