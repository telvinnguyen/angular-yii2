<?php

namespace backend\controllers;

use common\models\ZwTopic;
use misc\AjaxPagination\AjaxPagination;
use Yii;
use yii\rest\ActiveController;

class TopicController extends ActiveController
{
    public $modelClass = 'common\models\ZwTopic';

    public function actionDeletetopic()
    {
        $request = Yii::$app->request;
        $id = $request->post('topic_id');

        $entry = ZwTopic::findOne($id);
        if ($entry) {
            $entry->delete();
        }

        return ['OK' => 1];
    }

    public function actionGetpagedlist()
    {
        $request = Yii::$app->request;
        $pageIndex = $request->get('pagenum', '');
        $startOffet = $request->get('recordstartindex', '');
        $pageSize = $request->get('pagesize', '');

        $sorter = $request->get('sortdatafield', '');
        $sort_order = $request->get('sortorder', '');
        $keyword = $request->get('name_startsWith', '');


        $buildQuery = ZwTopic::find();
        $tableName = "zw_topic";

        /***  WHERE ***====*/

        $filterscount = $request->get('filterscount', 0);


        if ($filterscount > 0) {
            //$where = " WHERE (";
            $where = "(";
            $tmpdatafield = "";
            $tmpfilteroperator = "";
            for ($i = 0; $i < $filterscount; $i++) {
                // get the filter's value.
                $filtervalue = $_GET["filtervalue" . $i];
                // get the filter's condition.
                $filtercondition = $_GET["filtercondition" . $i];
                // get the filter's column.
                $filterdatafield = $_GET["filterdatafield" . $i];
                // get the filter's operator.
                $filteroperator = $_GET["filteroperator" . $i];

                if ($tmpdatafield == "") {
                    $tmpdatafield = $filterdatafield;
                } else if ($tmpdatafield <> $filterdatafield) {
                    $where .= ")AND(";
                } else if ($tmpdatafield == $filterdatafield) {
                    if ($tmpfilteroperator == 0) {
                        $where .= " AND ";
                    } else $where .= " OR ";
                }

                // build the "WHERE" clause depending on the filter's condition, value and datafield.
                switch ($filtercondition) {
                    case "CONTAINS":
                        $where .= " " . $filterdatafield . " LIKE '%" . $filtervalue . "%'";
                        break;
                    case "DOES_NOT_CONTAIN":
                        $where .= " " . $filterdatafield . " NOT LIKE '%" . $filtervalue . "%'";
                        break;
                    case "EQUAL":
                        $where .= " " . $filterdatafield . " = '" . $filtervalue . "'";
                        break;
                    case "NOT_EQUAL":
                        $where .= " " . $filterdatafield . " <> '" . $filtervalue . "'";
                        break;
                    case "GREATER_THAN":
                        $where .= " " . $filterdatafield . " > '" . $filtervalue . "'";
                        break;
                    case "LESS_THAN":
                        $where .= " " . $filterdatafield . " < '" . $filtervalue . "'";
                        break;
                    case "GREATER_THAN_OR_EQUAL":
                        $where .= " " . $filterdatafield . " >= '" . $filtervalue . "'";
                        break;
                    case "LESS_THAN_OR_EQUAL":
                        $where .= " " . $filterdatafield . " <= '" . $filtervalue . "'";
                        break;
                    case "STARTS_WITH":
                        $where .= " " . $filterdatafield . " LIKE '" . $filtervalue . "%'";
                        break;
                    case "ENDS_WITH":
                        $where .= " " . $filterdatafield . " LIKE '%" . $filtervalue . "'";
                        break;
                }

                if ($i == $filterscount - 1) {
                    $where .= ")";
                }

                $tmpfilteroperator = $filteroperator;
                $tmpdatafield = $filterdatafield;
            }

            $buildQuery->where($where);
        }


        /*====***  WHERE ***/

        if ($keyword != '')
            $buildQuery->andWhere(['LIKE', "{$tableName}.name", $keyword]);

        $countBuildQuery = clone $buildQuery;
        $pages = new AjaxPagination([
            'totalCount' => $countBuildQuery->count(),
            'pageParam' => 'pagenum',
            'pageSize' => $pageSize
        ]);
        $totalRecords = $countBuildQuery->count();
        $pageCount = ceil($totalRecords / $pageSize);


        $buildQuery->offset($startOffet);
        $buildQuery->limit($pages->limit);


        $orderBy = '';

        $orderBy .= "{$tableName}.$sorter $sort_order";
        if ($sorter != '') {
            $buildQuery->orderByString($orderBy);
        }
        $list = $buildQuery->all();


        return [
            'OK' => 1,
            'pagedList' => $list,
            'pageCount' => $pageCount,
            'totalRecords' => $totalRecords
        ];
    }
    
    public function actionForm(){
        $request = Yii::$app->request;
        $form = $request->post('form');
        
        
    }
}
