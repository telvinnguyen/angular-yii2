<?php

namespace backend\controllers;


use common\component\helpers\CommonHelper;
use common\component\helpers\GeneratorHelper;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Yii;
use yii\console\Controller;

class GeneratorController extends Controller
{

    /**
     * generate ngLoader template from svg string
     * @return array
     */
    public function actionGenngloadertemplate()
    {

        $request = Yii::$app->request;
        $submit = $request->post('submit');
        $svg = $request->post('svg');

        $model = new \stdClass();
        $model = (object)[
            'svg' => '',
            'result' => ''
        ];
        if ($submit) {

            foreach (preg_split("/((\r?\n)|(\r\n?))/", $svg) as $line) {
                // do stuff with $line
                $line = str_replace('"', '\"', $line);
                $line = '" ' . $line . '\n" +';

                $model->svg .= $line . PHP_EOL;
            }

            $model->bg_color = $request->post('bg_color');
            $model->result = $this->renderPartial('ng-loader-template', ['model' => $model]);

        }
        else {
            $model->bg_color = 'rgba(214, 214, 214, 0.5)';
        }


        return $this->render('generator', ['model' => $model]);
    }

    public function actionScanandconvertoldversion()
    {

        $scanDirs = [
            Yii::$app->params['oldControllerDir'],
            Yii::$app->params['oldVendorDir']
        ];
        $saveOutputTo = Yii::$app->params['newOutputDir'];

        // generate the files which has the name end with this string, leave empty if you want to generate all
        $genOnlyFileHasSuffixNames = []; //example: ['Commons', ...]

        foreach ($scanDirs as $scanDir) {
            //find file recursive folder
            foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($scanDir)) as $filePath) {
                $fileName = pathinfo($filePath, PATHINFO_FILENAME);
                var_dump($fileName);

                if (is_file($filePath)) {
                    if (empty($genOnlyFileHasSuffixNames))
                        GeneratorHelper::updateFile($filePath, $saveOutputTo);
                    else {
                        foreach ($genOnlyFileHasSuffixNames as $nameSuffix) {
                            if (strpos($filePath, $nameSuffix) !== false)
                                GeneratorHelper::updateFile($filePath, $saveOutputTo);
                        }
                    }

                }
            }
        }

    }

    /**
     * gen layout angularjs.php --> angularjs_minify.php
     */
    public function actionGenminangularlayout()
    {

        $scanFile = Yii::$app->params['viewLayoutDir'] . DIRECTORY_SEPARATOR . 'angularjs.php';
        $saveTo = Yii::$app->params['viewLayoutDir'] . DIRECTORY_SEPARATOR . 'angularjs_minify.php';

        $content = file_get_contents($scanFile);

        $re = "/src\\s*=\\s*\"(.+)(\\/(.+)\\.js)\"/";
        $replace = "src=\"<?= \Yii::\$app->request->BaseUrl ?>/angularjs/build/live/$3.min.js\"";
        $newContent = preg_replace($re, $replace, $content);

        //overwrite the new content
        file_put_contents($saveTo, $newContent);
    }

    /**
     * it will generate three file example: abcController.js, abc.html, abcService.js, abc.css
     */
    public function actionGenangularcombofiles()
    {
        $request = Yii::$app->request;
        $name = $request->post('name', '');
        $include_path = "";
        $route_state_entry = "";
        $routeStateDeps = [];
        $main_entry =  "";

        if ($name != "") {


            $dirPath = sprintf(Yii::$app->params['controllerDirFormat'] . DIRECTORY_SEPARATOR, $name);
            $listCamelCaseSuffixes = [
                'Controller.js', 'Service.js'
            ];

            $listSuffixes = [
                '.css', '.html'
            ];

            $camelCaseName = CommonHelper::dashesToCamelCase($name);

            foreach ($listCamelCaseSuffixes as $suffix) {
                //create directories based on the given path if they do not exist
                if (!file_exists($dirPath)) {
                    mkdir($dirPath, 0777, true);
                }

                $filePath = $dirPath . $camelCaseName . $suffix;
                file_put_contents($filePath, '');
            }

            foreach ($listSuffixes as $suffix) {
                //create directories based on the given path if they do not exist
                if (!file_exists($dirPath)) {
                    mkdir($dirPath, 0777, true);
                }

                $filePath = $dirPath . $name . $suffix;
                file_put_contents($filePath, '');
            }

            $include_path = "<script src=\"<?= \Yii::\$app->request->BaseUrl ?>/angularjs/app/controllers/$name/{$camelCaseName}Controller.js\"></script>";

            $route_state_entry = $this->renderPartial('_route-states-entry', ['name' => $name, 'camelCaseName' => $camelCaseName, 'routeStateDeps' => $routeStateDeps]);

            $main_entry = "<div ui-view=\"$name\" ng-show=\"\$state.includes('main.$name')\"></div>";
        }

        return $this->render('angular-combo-files', [
            'include_path' => $include_path,
            'route_state_entry' => $route_state_entry,
            'main_entry' => $main_entry
        ]);
    }

    public function actionFormatstaticdataarray()
    {
        $scanDir = Yii::$app->params['dirInput'];

        foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($scanDir)) as $filePath) {
            //$fileName = pathinfo($filePath, PATHINFO_FILENAME);

            if (is_file($filePath)) {
                $content = file_get_contents($filePath);

                $re = "/array\\(\"text\" => \"([\\w-\\s]+)\", \"([\\w-\\s]+)\" => \"([\\w-\\s]+)\"\\)/";
                $replace = "\"$3\" => \"$1\"";
                $newContent = preg_replace($re, $replace, $content);

                //overwrite the new content
                file_put_contents($filePath, $newContent);

            }
        }


    }

    public function actionQuickbuildsign()
    {
        /*
        $skinStructure = [
            ["(0,0)(350,350)", "(350,0)(700,350)", "(700,0)(1050,350)"],
            ["(0,350)(350,700)", "(350,350)(700,700)", "(700,350)(1050,700)"],
            ["(0,700)(350,1050)", "(350,700)(700,1050)", "(700,700)(1050,1050)"]

        ];
        //Yii::info('addSampleSkin...');
        $skin_id = GeneratorHelper::addSampleSkin($skinStructure);
        */
        GeneratorHelper::addSamplePublishedSign(Member::I_SIGN_TYPE_ID);
        //GeneratorHelper::addSampleItemVariant();
        var_dump('done');
    }

    /**
     * Gen multi-steps page
     */
    public function actionGenmultisteppage()
    {

        $request = Yii::$app->request;

        if (isset($_POST['steps'])) {
            $steps = array_filter($_POST['steps'], function($step){ return trim($step['name']) != ''; });
            $pageName = $request->post('pageName');

            // controller directory
            $dirPath = sprintf(Yii::$app->params['controllerDirFormat'] . DIRECTORY_SEPARATOR, $pageName);
            if (!file_exists($dirPath)) {
                mkdir($dirPath, 0777, true);
            }
            //controller name
            $ctrlName = CommonHelper::dashesToCamelCase($pageName);

            $step1_name = "";
            if(count($steps) > 0) $step1_name = $steps[0]['name'];
            $ctrlContent = $this->renderPartial('multi-steps-page-tpl-ctrl', [
                'ctrlName' => $ctrlName,
                'step1_name' => $step1_name
            ]);

            //controller js path
            $filePath = CommonHelper::sanitizePath($dirPath . DIRECTORY_SEPARATOR . $ctrlName . 'Controller.js');
            //save controller js file

            file_put_contents($filePath, $ctrlContent);

            $ctrlContent = $this->renderPartial('multi-steps-page-tpl-ctrl-html', [
                'ctrlName' => $ctrlName,
                'steps' => $steps
            ]);
             //controller html path
            $filePath = CommonHelper::sanitizePath($dirPath . DIRECTORY_SEPARATOR . $pageName . '.html');
            //save controller html file
            file_put_contents($filePath, $ctrlContent);

            //generate the css file of the controller
            $filePath = CommonHelper::sanitizePath($dirPath . DIRECTORY_SEPARATOR . $pageName . '.css');
            //save controller html file
            file_put_contents($filePath, '');

            $stepCount = count($steps);
            $routeStateDeps = [];

            $gotoStepName = '';
            foreach ($steps as $index=>$step) {
                $step = (object)$step;

                $stepButtons = [];

                if(trim($step->name) != ''){
                    $l = explode(',', $step->buttons);
                    if(!empty($l)) {
                        $_d = [];
                        foreach($l as $btnName){
                            if(trim($btnName) != ''){
                                $_d[CommonHelper::dashesToCamelCase($btnName)] = $btnName;
                            }

                        }
                        if(!empty($_d)) {
                            $stepButtons = $_d;
                        }
                    }

                    if($index == $stepCount-1 && $index > 0){
                        $gotoStepName = $steps[$index-1]['name'];
                    }elseif($index < $stepCount-1){
                        $gotoStepName = $steps[$index+1]['name'];
                    }

                    $dirFieldsJsTemplate = 'multi-steps-page-tpl-dir-fields';
                    $dirFieldsHtmlTemplate = 'multi-steps-page-tpl-dir-fields-html';

                    if($step->formType == 'icon'){
                        $dirFieldsJsTemplate = 'multi-steps-page-tpl-dir-icon';
                        $dirFieldsHtmlTemplate = 'multi-steps-page-tpl-dir-icon-html';
                    }


                    $directiveName = CommonHelper::dashesToCamelCase($step->name);
                    $filePath = CommonHelper::sanitizePath($dirPath . DIRECTORY_SEPARATOR . $step->name . '.js');
                    $directiveContent = $this->renderPartial($dirFieldsJsTemplate, [
                        'pageName' => $pageName,
                        'directiveName' => $directiveName,
                        'step' => $step,
                        'stepButtons' => $stepButtons,
                        'gotoStepName' => $gotoStepName
                    ]);

                    //save directive js file for each step
                    file_put_contents($filePath, $directiveContent);


                    $filePath = CommonHelper::sanitizePath($dirPath . DIRECTORY_SEPARATOR . $step->name . '.html');
                    $directiveContent = $this->renderPartial($dirFieldsHtmlTemplate, [
                        'directiveName' => $directiveName,
                        'step' => $step,
                        'index' => $index+1,
                        'stepButtons' => $stepButtons
                    ]);

                    //save directive js file for each step
                    file_put_contents($filePath, $directiveContent);

                    $routeStateDeps[] = "_yii_app.controllerPath + '/$pageName/{$step->name}.js',";
                }
            }



            $route_state_entry = htmlspecialchars($this->renderPartial('_route-states-entry', ['name' => $pageName, 'camelCaseName' => $ctrlName, 'routeStateDeps' => $routeStateDeps]));
            $main_entry = "<div ui-view=\"$pageName\" ng-show=\"\$state.includes('main.$pageName')\"></div>";

            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'OK' => 1,
                'route_state_entry' => $route_state_entry,
                'main_entry' => $main_entry,
                'steps' => $_POST['steps']
            ];
        }
        return $this->render('multi-steps-page.php');
    }


}
