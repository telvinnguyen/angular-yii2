<?php
namespace backend\controllers;

use backend\assets\BEAngularAsset;
use Yii;
use yii\web\Controller;
use common\models\LoginForm;

use yii\filters\AccessControl;
use yii\filters\VerbFilter;

/**
 * Site controller
 */
class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            /*
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            */
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        //return $this->render('index');

        //AngularJs page
        //Yii::$app->language = 'vi-VD';

        //register angular bundle layout
        BEAngularAsset::register($this->view);
        Yii::$app->view->params['ngApp'] = "zwenGlobal";
        Yii::$app->view->params['enablePreBootstrap'] = true;

        $this->view->title = \Yii::t('backend', 'ZwenGlobal');
        if (Yii::$app->params['minifyAssets']) {
            $this->layout = "angularjs_minify";
        } else {

            $this->layout = "angularjs";
        }


        return $this->render('page');
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
