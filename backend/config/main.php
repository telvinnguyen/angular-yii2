<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

use \yii\web\Request;
$baseUrl = str_replace('/backend/web', '', (new Request)->getBaseUrl());

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

//        'request' => [
//            'baseUrl' => $baseUrl,
//            'cookieValidationKey' => '0ArUHfVS-bwYwlBq6rkSVWmXrWQq1SgL',
//            'enableCsrfValidation'=>false
//        ],

        'assetManager' => [
            'linkAssets' => true
        ],

        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            //'baseUrl' => $baseUrl,
            'enablePrettyUrl' => true,
            'showScriptName' => false,

            'rules' => [
                '<controller>/<action>/<angular1:.+>' => '<controller>/<action>',
                '<controller>/<action>/<angular1:.+>/' => '<controller>/<action>',
                '<controller>/<action>/<angular1:.+>/<angular2:.+>' => '<controller>/<action>',
                '<controller>/<action>/<angular1:.+>/<angular2:.+>/' => '<controller>/<action>',
                '<controller>/<action>/<angular1:.+>/<angular2:.+>/<angular3:.+>/' => '<controller>/<action>',
            ]
        ]
    ],
    'params' => $params,
];
