<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;
use common\component\helpers\CommonHelper;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>

<html  lang="<?= Yii::$app->language ?>" <?php if(!Yii::$app->view->params['enablePreBootstrap']){ ?> ng-app="<?= Yii::$app->view->params['ngApp'] ?>" <?php } ?>>
<head>
    <!--<base href="/signsmart_angularjs/index.html" />-->
    <script>
        //document.write('<base href="' + document.location + '" />');
        document.write('<base href="<?= Url::toRoute('site/page', true) ?>/" />');
    </script>

    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- pinned logo windows 8 -->
    <meta name="application-name" content="ZwenGlobal"/>
    <meta name="msapplication-TileColor" content="#000000"/>
    <meta name="msapplication-TileImage" content="<?= Url::base(true) ?>/angularjs/app/css/ss/images/favicon.png"/>
    <link rel="shortcut icon" href="<?= Url::base(true) ?>/angularjs/app/css/ss/images/favicon.png" type="image/x-png" />

    <?= Html::csrfMetaTags() ?>

    <!-- Global settings: required -->
    <?php echo $this->render('global-settings', ['app' => \Yii::$app]) ?>

    <title><?= Html::encode($this->title) ?></title>
    <link rel="stylesheet" href="<?= Url::base(true) ?>/angularjs/app/css/ss/css/object-action-decorator.css" type="text/css" />
    <!-- Registered JS (POS_HEAD) will be loaded here -->
    <?php $this->head() ?>

    <!-- *** Initialize AngularJS app *** -->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/app-states.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/routes-states.min.js"></script>

    <!-- *** Services *** -->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/q-allsettled-decorator.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/ssHelper.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/signal-handler.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/modal-utils.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/jsonOrder.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/trust-url.min.js"></script>

    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/errorService.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/ratchetService.min.js"></script>

    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/contentService.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/registrationService.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/upgradememberService.min.js"></script>

    <!-- *** Directives *** -->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/trigger-element.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/window-exit.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/content-resolver.min.js"></script>

    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/chosen.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/nav-item.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/draggable.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/img-preload.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/load-dym-template.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/jcarousel.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/hint-text.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/ng-thumb.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/checklist-model.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/color-progress-bar.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/ng-imgAreaSelect.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/qtip-directive.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/kiosk-layout-preview.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/acc-switch-content-link.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/color-picker-directive.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/partial-css.min.js"></script>

    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/resize-iframe-content.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/fixed-iframe.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/imagemap.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/ng-click-select.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/quick-pick-item.min.js"></script>

    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/non-user-top-nav.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/auth-user-top-nav.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/toolbar-list-row.min.js"></script>

    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/directoryService.min.js"></script>

    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/content-object-toolbar.min.js"></script>

    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/content-object-variant.min.js"></script>

    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/simple-variant.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/simple-variant-row.min.js"></script>


    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/content-object-actions.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/member-accounts.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/homeboard.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/storage-info.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/item-variant-selection.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/object-action.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/object-action-decorator.min.js"></script>

    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/has-dot.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/pie-menu.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/handicap.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/credit-card-form.min.js"></script>


    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/current-time.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/fancy-box.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/choose-item-variant.min.js"></script>

    <!--    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/remote-layout.min.js"></script>-->
    <!--    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/remote-preview.min.js"></script>-->

    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/content-sign-variant.min.js"></script>



    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/calc-storage.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/upload-more-image.min.js"></script>

    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/publish-sign-run.min.js"></script>

    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/global-modal-actions.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/ratchet-listen-global-msg.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/ratchet-listen-sign-rdr-msg.min.js"></script>



    <!-- *** Controllers *** -->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/masterPageController.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/globalModalController.min.js"></script>

    <!-- Some Bootstrap Helper Libraries -->
    <link rel="stylesheet" href="<?= Url::base(true) ?>/angularjs/app/css/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="<?= Url::base(true) ?>/angularjs/app/css/override-bootstrap.css" type="text/css" />

    <!-- *** Third party *** -->
    <link rel="stylesheet" href="<?= Url::base(true) ?>/angularjs/app/css/ss/css/font-awesome-4.4.0/css/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="<?= Url::base(true) ?>/angularjs/app/css/color-progress-bar.css" type="text/css" />

    <!-- CSS of detail page would be loaded here later -->
</head>
<body window-exit ng-class="bodyClass">

<?php $this->beginBody() ?>
<?= $content ?>


<?php if(Yii::$app->view->params['enablePreBootstrap']){
    echo $this->render('//layouts/prebootstrap-standalone');
    ?>

    <script src="<?= \Yii::$app->request->BaseUrl ?>/angularjs/build/live/prebootstrap.min.js"></script>
<?php } ?>

<?php $this->endBody() ?>




</body>
</html>
<?php $this->endPage() ?>
