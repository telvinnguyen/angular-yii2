<?php use yii\helpers\Url; ?>
<script>
var _yii_app  = {
    hostUrl: '<?php echo Url::base(true) ?>',
    baseUrl: '<?php echo Url::base(true) ?>', // /frontend/web
    fixassetsUrl: '<?php echo Url::base(true) ?>/fixassets',
    layoutPath: '<?php echo Url::base(true) ?>/angularjs/app/css/ss',
    appPath: '<?php echo Url::base(true) ?>/angularjs/app',
    angularPath: '<?php echo Url::base(true) ?>/angularjs',
    systemresourcesPath: '<?php echo Url::base(true) ?>/uploads/systemresources',
    templatePath: '<?php echo Url::base(true) ?>/angularjs/app/partials',
    controllerPath: '<?php echo Url::base(true) ?>/angularjs/app/controllers',
    directivePath: '<?php echo Url::base(true) ?>/angularjs/app/directives',
    servicePath: '<?php echo Url::base(true) ?>/angularjs/app/services',
    decoratorPath: '<?php echo Url::base(true) ?>/angularjs/app/decorators',
    livePath: '<?php echo Url::base(true) ?>/angularjs/live',
    absTemplatePath: '<?php echo Url::base(true) ?>/angularjs/app/partials',
    serverName: '<?php echo $_SERVER["SERVER_NAME"] ?>',

    signSocketServer: '<?php echo Yii::$app->params['signSocketServer'] ?>',
    globalSocketServer: '<?php echo Yii::$app->params['globalSocketServer'] ?>',
    askSocketServerOvertimes: <?php echo Yii::$app->params['askSocketServerOvertimes'] ?>,

    debugMode: false
};

var _globalNames = {
    globalSocketServer: 'GLOBAL_SOCKET_SERVER',
    signSocketServer: 'SIGN_SOCKET_SERVER'
}

</script>