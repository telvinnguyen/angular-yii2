app.directive('<?php echo $directiveName ?>', function ($timeout, $ocLazyLoad, dialogs, ssHelper) {
    return {
        restrict: 'E',
        templateUrl: _yii_app.controllerPath + '/<?php echo $pageName?>/<?php echo $step->name?>.html',
        bindToController: true,
        scope: {step: '='},
        controller: function ($scope, $stateParams) {
            //$ocLazyLoad.load(_yii_app.controllerPath + '/<?php echo $pageName?>/<?php echo $step->name?>.css');
            $scope.bodyLoader = false;
            $scope.getData = function(){

            }
            $scope.getData();

            $scope.switch = function(){
                $scope.step.name = '<?php echo $gotoStepName ?>';
            }

            <?php foreach($stepButtons as $eventName => $buttonName): ?>$scope.<?php echo $eventName?> = function(){

            }
            <?php endforeach;?>

        },
        link: function (scope, element, attrs) {}
    };
});