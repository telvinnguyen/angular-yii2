<div id="container">

	<div id="header">
		<auth-user-top-nav></auth-user-top-nav>
	</div>
	<!-- /header -->

	<div id="main" holder="loading">

		<div id="content">
			<div class="content_inner" style="position: relative">
                <?php foreach($steps as $step):?><<?php echo $step['name'] ?> step="step" remote="remote" ng-show="step.name == '<?php echo $step['name'] ?>'" class="fx-fade-right plain_info" style="position: absolute"></<?php echo $step['name'] ?>>
            <?php endforeach;?>

            </div>
		</div>
		<!-- content -->

		<homeboard></homeboard>

		<div class="clr"></div>
	</div>
	<!-- /main -->
	<div class="clr"></div>
</div>