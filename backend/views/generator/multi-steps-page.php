<?php use yii\helpers\Url; ?>

<?php echo $this->render('..\layouts\global-settings', ['app' => \Yii::$app]) ?>

<script src="<?= Url::to('@web/angularjs/lib/angular-1.3.14/angular.min.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/lib/vendor/ui-codemirror/src/ui-codemirror.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/lib/ui-bootstrap-tpls-0.11.2.min.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/lib/vendor/angular-ui-router/release/angular-ui-router.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/lib/angular-1.3.14/angular-route.min.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/lib/vendor/a0-angular-storage/dist/angular-storage.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/lib/ui-router-extras/ct-ui-router-extras.min.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/app/services/ssHelper.js') ?>"></script>

<link rel="stylesheet" href="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/lib/codemirror.css') ?>">
<link rel="stylesheet" href="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/addon/fold/foldgutter.css') ?>">
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/lib/codemirror.js') ?>"></script>


<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/addon/fold/foldcode.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/addon/fold/foldgutter.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/addon/fold/brace-fold.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/addon/fold/xml-fold.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/addon/fold/markdown-fold.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/addon/fold/comment-fold.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/mode/javascript/javascript.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/mode/xml/xml.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/mode/markdown/markdown.js') ?>"></script>

<style>
    .row { margin-top:20px; }
</style>

<div ng-controller="MyController">

    <form action="<?php yii\helpers\Url::toRoute('generator/genmultisteppage') ?>" method="post" class="form-inline">
        <label>Page Name (State Name):</label> <input type="text" style="width: 300px; height: 25px;" ng-model="pageName" placeholder="Example:main.single-page"/>
        <br/>

        <div class="row">
            <label for="stepInput">Number of steps:</label>

            <div class="form-group">
                <input type="text" class="form-control" id="stepInput" ng-model="stepsNumber" placeholder="Number of step">
            </div>
            <button type="button" ng-click="genDefaultSteps()" class="btn btn-primary">Set</button>
        </div>

        <div class="navbar"></div>


        <div class="row" ng-repeat="(key,step) in steps">
            <label for="stepName">Step Name (Directive Name):</label>

            <div class="form-group">
                <input type="text" class="form-control" id="stepName" ng-model="step.name" placeholder="Example: page-step-one">
            </div>
            <label for="buttonNames">Names of the buttons:</label>

            <div class="form-group">
                <input type="text" ng-model="step.buttons" class="form-control" id="buttonNames" placeholder="Example: Save,Upload Image,Cancel">
            </div>
            <label for="formType">Form type:</label>

            <div class="form-group">
                <select id="formType" ng-model="step.formType" class="form-control input-sm">
                    <option value="icon">form +icon</option>
                    <option value="2col-fields">basic form +fields 2 column</option>
                </select>
            </div>
            <button type="button" class="btn btn-primary" ng-click="addMoreStep()">+</button>
        </div>

    </form>
    <div class="navbar"></div>
    <div class="form-group">
        <button ng-click="submit()" class="btn btn-success">Submit</button>
    </div>




<br>
Put below line into /frontend/web/angularjs/app/controllers/main/main.html
<textarea id="main_entry" ng-model="main_entry" ui-codemirror="{ onLoad : cm1_onLoad }" ui-codemirror-opts="main_entry_opts" ui-refresh='main_entry'></textarea>

<br>
Put below line into /frontend/web/angularjs/app/apps/routes-states.js
<textarea id="code" ng-model="route_state_entry"  ui-codemirror="{ onLoad : cm2_onLoad }" ui-codemirror-opts="route_state_entry_opts" ui-refresh='route_state_entry_opts'></textarea>

</div>


<script>
    var genApp = angular.module('generatorApp', ['ssFactories', 'ui.codemirror']);

    genApp.controller('MyController', ['$scope', '$timeout', 'ssHelper', function ($scope, $timeout, ssHelper) {
        $scope.steps = [
            {name: '', 'buttons': '', formType: '2col-fields'}
        ]

        $scope.stepsNumber = 1;

        $scope.genDefaultSteps = function () {
            for (var i = 0; i < $scope.stepsNumber-1; i++) {
                $scope.addMoreStep();
            }
        };

        $scope.addMoreStep = function () {
            $scope.steps.push(
                {name: '', 'buttons': '', formType: '2col-fields'}
            );
        };

        $scope.cm1_onLoad = function(_editor){
            console.log('loaded1')
            _editor.setSize(1200, 50)
        }
        $scope.cm2_onLoad = function(_editor){
            console.log('loaded2')
            _editor.setSize(1200, 500)
        }
        
        $scope.submit = function () {
            ssHelper.post('/generator/GenMultiSteppage', {pageName: $scope.pageName, steps: $scope.steps}).then(function(response){


                $scope.main_entry = response.data.main_entry

                $scope.main_entry_opts = {
                    mode: "text/html",
                    lineNumbers: true,
                    lineWrapping: true,
                    extraKeys: {"Ctrl-Q": function(cm){ cm.foldCode(cm.getCursor()); }},
                    foldGutter: true,
                    gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"]
                }


                $scope.route_state_entry = response.data.route_state_entry;
                $scope.route_state_entry_opts = {
                    mode: "javascript",
                    lineNumbers: true,
                    lineWrapping: true,
                    extraKeys: {"Ctrl-Q": function(cm){ cm.foldCode(cm.getCursor()); }},
                    foldGutter: true,
                    gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"]
                }



            });
        };
    }]);

    angular.element(document).ready(function () {
        angular.bootstrap(document, ['generatorApp']);
    });
</script>
