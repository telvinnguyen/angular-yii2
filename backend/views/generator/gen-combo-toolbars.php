<?php use yii\helpers\Url; ?>


<?php echo yii\base\View::render('//layouts/global-settings', ['app' => \Yii::$app]) ?>

<script src="<?= Url::to('@web/angularjs/lib/angular-1.3.14/angular.min.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/lib/angular-1.3.14/angular-loader.min.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/lib/angular-1.3.14/angular-resource.min.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/lib/angular-1.3.14/angular-sanitize.min.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/lib/vendor/ui-codemirror/src/ui-codemirror.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/lib/ui-bootstrap-tpls-0.11.2.min.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/lib/vendor/angular-ui-router/release/angular-ui-router.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/lib/angular-1.3.14/angular-route.min.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/lib/vendor/a0-angular-storage/dist/angular-storage.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/lib/ui-router-extras/ct-ui-router-extras.min.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/lib/vendor/angular-ui-utils/ui-utils.min.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/lib/vendor/dialog/dialogs.min.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/app/services/ssHelper.js') ?>"></script>

<link rel="stylesheet" href="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/lib/codemirror.css') ?>">
<link rel="stylesheet" href="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/addon/fold/foldgutter.css') ?>">
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/lib/codemirror.js') ?>"></script>


<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/addon/fold/foldcode.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/addon/fold/foldgutter.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/addon/fold/brace-fold.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/addon/fold/xml-fold.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/addon/fold/markdown-fold.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/addon/fold/comment-fold.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/mode/javascript/javascript.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/mode/xml/xml.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/mode/markdown/markdown.js') ?>"></script>

<style>
    .row {
        margin-top: 20px;
    }
</style>

<div ng-controller="MyController">

    <form action="<?php yii\helpers\Url::toRoute('generator/genmultisteppage') ?>" method="post" class="form-inline">

        <div class="navbar"></div>


        <div class="row">
            <label>Generate from icon path:</label>

            <div class="form-group">
                <input type="text" ng-model="formInfo.iconName" class="form-control" style="width: 672px">
            </div>

            <label>Generate icon for collection/group/item by red/green/blue</label>:</label>
            <input type="checkbox" ng-model="formInfo.is_gen_icon" />

        </div>

        <!-- Collection -->
        <div class="row">
            <div class="form-group">
                <input type="text" ng-model="formInfo.collection.oid" class="form-control" placeholder="ID" style="width:50px;">
            </div>
            <label>Collection Name:</label>

            <div class="form-group">
                <input type="text" ng-model="formInfo.collection.name" class="form-control">
            </div>
            <label for="buttonNames">Extension:</label>

            <div class="form-group">
                <input type="text" ng-model="formInfo.collection.ext" class="form-control">
            </div>
            <label for="formType">Support Ext:</label>

            <div class="form-group">
                <input type="text" ng-model="formInfo.collection.supportExt" class="form-control">
            </div>
        </div>
        <!-- Collection -->

        <!-- Group -->
        <div class="row">
            <div class="form-group">
                <input type="text" ng-model="formInfo.group.oid" class="form-control" placeholder="ID" style="width:50px;">
            </div>
            <label>Group Name:</label>

            <div class="form-group">
                <input type="text" ng-model="formInfo.group.name" class="form-control">
            </div>
            <label for="buttonNames">Extension:</label>

            <div class="form-group">
                <input type="text" ng-model="formInfo.group.ext" class="form-control">
            </div>
            <label for="formType">Support Ext:</label>

            <div class="form-group">
                <input type="text" ng-model="formInfo.group.supportExt" class="form-control">
            </div>
        </div>
        <!-- Group -->

        <!-- Item -->
        <div class="row">
            <div class="form-group">
                <input type="text" ng-model="formInfo.item.oid" class="form-control" placeholder="ID" style="width:50px;">
            </div>
            <label>Item Name:</label>

            <div class="form-group">
                <input type="text" ng-model="formInfo.item.name" class="form-control">
            </div>
            <label for="buttonNames">Extension:</label>

            <div class="form-group">
                <input type="text" ng-model="formInfo.item.ext" class="form-control">
            </div>
            <label for="formType">Support Ext:</label>

            <div class="form-group">
                <input type="text" ng-model="formInfo.item.supportExt" class="form-control">
            </div>
        </div>
        <!-- Item -->


    </form>
    <div class="navbar"></div>
    <div class="form-group">
        <button ng-click="submit()" class="btn btn-success">Submit</button>
    </div>


    <br>
    Put below line into /frontend/models/ObjectToolbar.php
    <textarea id="main_entry" ng-model="main_entry" ui-codemirror="{ onLoad : cm1_onLoad }" ui-codemirror-opts="main_entry_opts" ui-refresh='main_entry'></textarea>

    <br>
    Put below line into /frontend/web/angularjs/app/apps/routes-states.js
    <textarea id="code" ng-model="route_state_entry" ui-codemirror="{ onLoad : cm2_onLoad }" ui-codemirror-opts="route_state_entry_opts" ui-refresh='route_state_entry_opts'></textarea>

</div>


<script>
    var genApp = angular.module('generatorApp', ['ui.bootstrap', 'ssFactories', 'ui.codemirror', 'ui.utils', 'dialogs.main']);

    genApp.controller('MyController', ['$scope', '$timeout', 'ssHelper', function ($scope, $timeout, ssHelper) {
        $scope.formInfo = {
            iconName: 'aimp_mirror.jpg',
            is_gen_icon: true,

            collection: { name: '', ext: '', supportExt: ''},
            group: { name: '', ext: '', supportExt: ''},
            item: { name: '', ext: '', supportExt: ''}
        }

        $scope.cm1_onLoad = function (_editor) {
            console.log('loaded1')
            _editor.setSize(1200, 50)
        }
        $scope.cm2_onLoad = function (_editor) {
            console.log('loaded2')
            _editor.setSize(1200, 500)
        }

        $scope.submit = function () {
            ssHelper.post('/generator/Gencombotoolbars', {
                formInfo: $scope.formInfo
            }).then(function (response) {
                $scope.main_entry = response.data.main_entry
                $scope.main_entry_opts = {
                    mode: "application/x-httpd-php",
                    lineNumbers: true,
                    lineWrapping: true,
                    extraKeys: {
                        "Ctrl-Q": function (cm) {
                            cm.foldCode(cm.getCursor());
                        }
                    },
                    foldGutter: true,
                    gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"]
                }

                $scope.route_state_entry = response.data.route_state_entry;
                $scope.route_state_entry_opts = {
                    mode: "javascript",
                    lineNumbers: true,
                    lineWrapping: true,
                    extraKeys: {
                        "Ctrl-Q": function (cm) {
                            cm.foldCode(cm.getCursor());
                        }
                    },
                    foldGutter: true,
                    gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"]
                }
            });
        };
    }]);

    angular.element(document).ready(function () {
        angular.bootstrap(document, ['generatorApp']);
    });
</script>
