app.directive('<?php echo $directiveName ?>', function ($timeout, $ocLazyLoad, dialogs, ssHelper) {
    return {
        restrict: 'E',
        templateUrl: _yii_app.controllerPath + '/<?php echo $pageName?>/<?php echo $step->name?>.html',
        bindToController: true,
        scope: {step: '=', remote: '='},
        controller: function ($scope, $stateParams) {
            //$ocLazyLoad.load(_yii_app.controllerPath + '/<?php echo $pageName?>/<?php echo $step->name?>.css');
            $scope.bodyLoader = false;
            $scope.getData = function(){
                ssHelper.post('/controller/action', {
                    mtype: $scope.mtype, oid: $scope.oid, ovid: $scope.ovid
                }).then(function (response) {
                    if (response.data.OK == 1) {
                        $scope.pageTitle = response.data.pageTitle
                        $scope.formInfo = _.extendOwn($scope.formInfo, response.data.model)
                        $scope.formInfo.ovid = response.data.ovid;
                        $scope.formInfo.type = response.data.type;

                        var icon_id = parseInt($scope.formInfo.icon_image);
                        if (angular.isNumber(icon_id)) {
                            $scope.formInfo.selected_ovid = [icon_id];
                        }
                    }
                    $scope.updateStatusId = 1;
                });
            }
            $scope.getData();

            $scope.switch = function(){
                $scope.step.name = '<?php echo $gotoStepName ?>';
            }

            <?php foreach($stepButtons as $eventName => $buttonName): ?>$scope.<?php echo $eventName?> = function(){

            }
            <?php endforeach;?>

        },
        link: function (scope, element, attrs) {}
    };
});