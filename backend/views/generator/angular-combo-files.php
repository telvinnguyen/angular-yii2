<?php use yii\helpers\Url; ?>
<link rel="stylesheet" href="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/lib/codemirror.css') ?>">
<link rel="stylesheet" href="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/addon/fold/foldgutter.css') ?>">
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/lib/codemirror.js') ?>"></script>

<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/addon/fold/foldcode.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/addon/fold/foldgutter.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/addon/fold/brace-fold.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/addon/fold/xml-fold.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/addon/fold/markdown-fold.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/addon/fold/comment-fold.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/mode/javascript/javascript.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/mode/xml/xml.js') ?>"></script>
<script src="<?= Url::to('@web/angularjs/app/css/ss/js/codemirror5.2/mode/markdown/markdown.js') ?>"></script>

<script id="script">
    /*
     * Demonstration of code folding
     */
    window.onload = function() {
        var area_html = document.getElementById("myUrl");
        var area_script = document.getElementById("code");
        var area_html_main_entry = document.getElementById("main_entry");

        //editor.foldCode(CodeMirror.Pos(13, 0));
        var editor_html = CodeMirror.fromTextArea(area_html, {
            mode: "text/html",
            lineNumbers: true,
            lineWrapping: true,
            extraKeys: {"Ctrl-Q": function(cm){ cm.foldCode(cm.getCursor()); }},
            foldGutter: true,
            gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"]
        });
        editor_html.setSize(1200, 50);

        var editor_html2 = CodeMirror.fromTextArea(area_html_main_entry, {
            mode: "text/html",
            lineNumbers: true,
            lineWrapping: true,
            extraKeys: {"Ctrl-Q": function(cm){ cm.foldCode(cm.getCursor()); }},
            foldGutter: true,
            gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"]
        });

        editor_html2.setSize(1200, 50);

        var editor = CodeMirror.fromTextArea(area_script, {
            mode: "javascript",
            lineNumbers: true,
            lineWrapping: true,
            extraKeys: {"Ctrl-Q": function(cm){ cm.foldCode(cm.getCursor()); }},
            foldGutter: true,
            gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"]
        });
        editor.setSize(1200, 500);

    };
</script>

<form action="<?php yii\helpers\Url::toRoute('generator/genangularcombofiles') ?>" method="post">
    <label>Name:</label> <input type="text" style="width: 300px; height: 25px;" name="name" />
    <input type="submit" value="submit">
    <br/>
</form>


<textarea style="display: none" id="myUrl"><?php echo htmlspecialchars($include_path) ?></textarea>

<br>
Put below line into /frontend/web/angularjs/app/controllers/main/main.html
<textarea id="main_entry"><?php echo htmlspecialchars($main_entry) ?></textarea>

<br>
Put below line into /frontend/web/angularjs/app/apps/routes-states.js
<textarea id="code"><?php echo htmlspecialchars($route_state_entry) ?></textarea>

