<?php
namespace backend\models;

use common\models\ZwUser;
use Namshi\JOSE\JWS;
use phpjwt\Authentication\JWT;
use yii\base\Model;

class UserLoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;
    const JWT_PASSWORD = 'ss-secret-P@ss600';
    const cryptCycles = 12;

    private $_user = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    /**
     * @return bool|null|static
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = ZwUser::findByUsername($this->username);
        }

        return $this->_user;
    }

    public static function makePasswordHash($input)
    {
        $crypt_options = array(
            'cost' => self::cryptCycles
        );
        return password_hash($input, PASSWORD_BCRYPT, $crypt_options);
    }

    public static function createLoginToken($member_id)
    {

        //set TTL (time to live)

        $ttl = new \DateTime('now', new \DateTimeZone('UTC'));
        $ttl->modify("+1 year");

        // refer to see more about hash algorithm https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-14#section-3.1
        $jws = new JWS('HS512'); //pass hash algorithm sha-512

        $jws->setPayload(array(
            'member_id' => $member_id, //$user->getid(),
            'exp' => $ttl->format('U'),
            'jwt' => JWT::encode(array('member_id' => $member_id), UserLoginForm::JWT_PASSWORD, 'HS512')
        ));

        $privateKey = null;
        $jws->sign($privateKey);
        $token = $jws->getTokenString();
        //var_dump($token);

        return $token;

    }


}
