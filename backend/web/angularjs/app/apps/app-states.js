var app = angular.module(
    'zwenGlobal', ['ngRoute', 'ui.bootstrap', 'ngWebSocket', 'ssFactories', 'ngAnimate', 'validation', 'validation.rule',
        'ui.router', 'angular-jwt', 'angular-storage', 'uiRouterStyles', 'ngCookies', 'ngLoader', 'ui.utils', 'oc.lazyLoad',
        'dialogs.main', 'localytics.directives', 'ngResource', 'angular-iscroll', 'LocalStorageModule',
        'angularFileUpload', 'xtForm', 'ct.ui.router.extras', 'door3.css', 'notyModule',
        'ngPostMessage', 'ngClickSelect', 'ngClipboard', 'ngIdle',
    ]);

app.config(function ($compileProvider) {
    app.compileProvider = $compileProvider;
});

app.config(function ($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        debug: true,
        events: false,
        modules: [
            {
                name: 'ckeditor',
                files: [
                    _yii_app.baseUrl + '/angularjs/lib/vendor/ckeditor/ckeditor.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/angular-ckeditor/angular-ckeditor.js'
                ]
            },
            {
                name: 'jwplayer',
                files: [
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jwplayer/jwplayer.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jwplayer/jwplayer.html5.js',
                    _yii_app.directivePath + '/common/ng-jwplayer/ng-jwplayer.js'

                ],
                serie: true
            },
            {
                name: 'marquee',
                files: [
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jquery_marquee/jquery.marquee.js',
                    _yii_app.baseUrl + '/angularjs/app/controllers/view-scrolling-msg/marquee-msg.js',
                ]
            },
            {
                name: 'tooltipster',
                files: [
                    _yii_app.baseUrl + '/angularjs/lib/vendor/tooltipster/jquery.tooltipster.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/tooltipster/tooltipster.css'
                ]
            },
            {
                name: 'pgwslideshow',
                files: [
                    _yii_app.baseUrl + '/angularjs/lib/vendor/pgwSlideshow/pgwslideshow.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/pgwSlideshow/pgwslideshow.css',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/pgwSlideshow/pgwslideshow_light.css'
                ]
            },
            {
                name: 'pgwslideshow-directive',
                files: [
                    _yii_app.baseUrl + '/angularjs/app/directives/common/pgw-slide-show/pgw-slide-show.js'
                ]
            },
            {
                name: 'bugreport-addobject-directive',
                files: [
                    _yii_app.baseUrl + '/angularjs/app/directives/partials/bugreport-addobject/bugreport-addobject.js'
                ]

            },
            {
                name: 'jqwidget-calendar',
                files: [
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/jqxcore.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/jqxcalendar.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/jqx.base.css'
                ]
            },
            {
                name: 'audioMessage',
                files: [
                    _yii_app.baseUrl + '/angularjs/lib/vendor/voice_notes/audiodisplay.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/voice_notes/recorder.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/voice_notes/recorderWorker.js',
                    //_yii_app.baseUrl + '/angularjs/lib/vendor/voice_notes/main.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/voice_notes/record.css'
                ]
            },
            {
                name: 'playAudio',
                files: [
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jplayer/js/jquery.jplayer.min.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jplayer/skins/bluemonday/jplayer.bluemonday.css'
                ]
            },
            {
                name: 'ngEnter',
                files: [
                    _yii_app.baseUrl + '/angularjs/app/directives/common/ng-enter.js'
                ]
            },
            {
                name: 'onlyDecimal', /* allow only the decimal number with maxium two number after the dot */
                files: [
                    _yii_app.baseUrl + '/angularjs/app/directives/common/only-decimal.js'
                ]
            },
            {
                name: 'ngFx', /* allow only the decimal number with maxium two number after the dot */
                files: [
                    _yii_app.baseUrl + '/angularjs/lib/vendor/gsap/src/minified/TweenMax.min.js', //1st
                    _yii_app.baseUrl + '/angularjs/lib/vendor/ngFx/ngFx.min.js', //second
                ],
                serie: true // make the above order of loading dependencies file is forced to be followed.
            },
            {
                name: 'kioskDesign', /* allow only the decimal number with maxium two number after the dot */
                files: [
                    _yii_app.baseUrl + '/angularjs/app/directives/partials/frame-matrix.js',
                    _yii_app.baseUrl + '/angularjs/app/controllers/preview-kiosk/preview-kiosk-non-skin.js',
                    _yii_app.baseUrl + '/angularjs/app/controllers/preview-kiosk/preview-kiosk-with-skin.js',
                    _yii_app.baseUrl + '/angularjs/app/directives/partials/skin-arrow-scroll-content/skin-horizontal-arrow.js',
                    _yii_app.baseUrl + '/angularjs/app/directives/partials/skin-arrow-scroll-content/skin-vertical-arrow.js',
                    _yii_app.baseUrl + '/angularjs/app/controllers/preview-kiosk/preview-kiosk-item.js',
                    _yii_app.baseUrl + '/angularjs/app/directives/common/global-action/global-action.js'
                ]
            },
            {
                name: 'publishSign',
                files: [
                    _yii_app.baseUrl + '/angularjs/app/controllers/view-publish-sign/publish-sign-enter-pin.js',
                    _yii_app.baseUrl + '/angularjs/app/controllers/view-publish-sign/publish-sign-waiting.js'
                ],
                serie: true
            },
            {
                name: 'syncVideoComponent', /* allow only the decimal number with maxium two number after the dot */
                files: [
                    _yii_app.baseUrl + '/angularjs/app/services/sync-video-service.js'
                ]
            },
            {
                name: 'plupload.directive',
                files: [
                    _yii_app.baseUrl + '/angularjs/app/directives/common/plupload/plupload.full.min.js',
                    _yii_app.baseUrl + '/angularjs/app/directives/common/plupload/plupload-angular-directive.js',
                ]
            },
            {
                name: 'jquery-ui-bundle',
                files: [
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jquery-ui-1.11.4/jquery-ui.min.css',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jquery-ui-1.11.4/jquery-ui.min.js',
                    _yii_app.baseUrl + '/angularjs/app/directives/common/make-reziable.js',
                    _yii_app.baseUrl + '/angularjs/app/directives/common/make-draggable.js',
                ],
                serie: true
            },
            {
                name: 'youtube-embed',
                files: [
                    _yii_app.baseUrl + '/angularjs/lib/vendor/angular-youtube-embed/dist/iframe_api.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/angular-youtube-embed/src/angular-youtube-embed.js',
                ],
                serie: true
            },
            {
                name: 'html2canvas',
                files: [
                    _yii_app.baseUrl + '/angularjs/lib/vendor/html2canvas/html2canvas.min.js',
                    _yii_app.baseUrl + '/angularjs/app/directives/common/html2canvas-directive.js'
                ],
                serie: true
            },{
                name: 'image-map-editor',
                files: [
                    _yii_app.baseUrl + '/angularjs/app/css/ss/js/imgmap/js/imgmap.js',
                    _yii_app.baseUrl + '/angularjs/app/controllers/image-map/image-map-editor-decorator.js',
                    _yii_app.baseUrl + '/angularjs/app/controllers/image-map/image-map-editor.js'
                ],
                serie: true
            },
            {
                name: 'directory-screensize',
                files: [
                        
	                    _yii_app.layoutPath + '/images/v1080full/scripts/jquery.keyboard.js',
	                    _yii_app.layoutPath + '/images/v1080full/scripts/jquery.mousewheel.js',
	                    _yii_app.layoutPath + '/images/v1080full/scripts/jquery.thumbnailScroller.js',
	                    _yii_app.layoutPath + '/images/v1080full/scripts/jquery-ui.min.js',
	                    
	                    _yii_app.baseUrl + '/angularjs/app/directives/common/jthumbnail-scroller.js',
                        _yii_app.controllerPath + '/preview-directory/preview-directory-size.js'
	                    
	           
                ],
                serie: true
            },
            {
            	name: 'feature',
            	files: [
						_yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/styles/jqx.base.css',
	                    _yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/jqxcore.js',
	                    _yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/jqxcalendar.js',
	                    _yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/jqxdatetimeinput.js',
	                    _yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/jqxangular.js',
            	        
            	        ],
            	        serie: true
            },
            {
                name: 'jqwidgets',
                files: [
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/styles/jqx.base.css',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/jqxcore.js',

                    _yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/jqxcore.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/jqxdata.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/jqxtabs.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/jqxbuttons.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/jqxscrollbar.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/jqxmenu.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/jqxcheckbox.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/jqxlistbox.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/jqxdropdownlist.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/jqxlistbox.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/jqxradiobutton.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/jqxtooltip.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/jqxdatetimeinput.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/jqxcalendar.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/globalization/globalize.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/jqxinput.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/jqxnumberinput.js',

                    _yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/jqxgrid.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/jqxgrid.sort.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/jqxgrid.pager.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/jqxgrid.filter.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/jqxgrid.columnsresize.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/jqxgrid.selection.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/jqxpanel.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/jqxgrid.edit.js',
                    //_yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/scripts/generatedata.js',
                    //_yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/scripts/gettheme.js',

                    _yii_app.baseUrl + '/angularjs/lib/vendor/jqwidgets/jqxangular.js',
                ],
                serie: true
            },
            {
                name: 'notyModule',
                files: [
                    //_yii_app.baseUrl + '/angularjs/lib/vendor/angular-noty/buttons.css',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/angular-noty/animate.css',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/angular-noty/jquery.noty.packaged.min.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/angular-noty/angular-noty.js',
                ],
                serie: true
            },
            {
                name: 'webRTC-experiment-RTCMultiConnection',
                files:[
                    _yii_app.baseUrl + '/angularjs/lib/vendor/webRTC-experiment/firebase.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/webRTC-experiment/RTCMultiConnection.min.js'
                ],
                serie: true
            },
            {
                name: '$jsPlumb',
                files:[
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jsPlumbToolkit/jsPlumb-2.0.4.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jsPlumbToolkit/jsPlumbToolkit-1.0.11.js',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jsPlumbToolkit/jsPlumbToolkit-angular-1.0.11.js',

                    _yii_app.baseUrl + '/angularjs/lib/vendor/jsPlumbToolkit/app.css',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jsPlumbToolkit/gollum-template.css',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jsPlumbToolkit/jsPlumbToolkit-defaults.css',
                    _yii_app.baseUrl + '/angularjs/lib/vendor/jsPlumbToolkit/jsPlumbToolkit-demo.css'
                ],
                serie: true
            },
            {
                name: 'rzModule',
                files:[
                    _yii_app.baseUrl + '/angularjs/app/directives/common/rzslider/rzslider.min.js',
                    _yii_app.baseUrl + '/angularjs/app/directives/common/rzslider/rzslider.min.css'
                ],
                serie: true
            },
            {
                "name": "BE-dashboard",
                "files": [

                    _yii_app.layoutPath + '/sample/js/bootstrap.min.js',
                    _yii_app.layoutPath + '/sample/js/plugins/fastclick/fastclick.js',
                    _yii_app.layoutPath + '/sample/js/plugins/sparkline/jquery.sparkline.min.js',
                    _yii_app.layoutPath + '/sample/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
                    _yii_app.layoutPath + '/sample/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
                    _yii_app.layoutPath + '/sample/js/plugins/slimScroll/jquery.slimscroll.min.js',
                    _yii_app.layoutPath + '/sample/js/plugins/chartjs/Chart.min.js',


                    _yii_app.directivePath + '/partials/dashboard/dashboard-main-header.js',
                    _yii_app.directivePath + '/partials/dashboard/dashboard-main-sidebar.js',

                   // _yii_app.layoutPath + '/sample/js/app.min.js',


                    /* put css after script file are loaded to avoid the css files cannot be appended when switching page */
                    _yii_app.angularPath + '/app/css/bootstrap.min.css',
                    _yii_app.angularPath + '/app/css/backend.min.css',
                    _yii_app.angularPath + '/app/css/skins/_all-skins.min.css',
                ],
                serie: true
            }

        ]
    });
});


app.config(function (localStorageServiceProvider) {
    localStorageServiceProvider
        .setStorageType('sessionStorage')
        .setNotify(true, true)
});

app.config(function ($stickyStateProvider) {

    if (_yii_app.debugMode) {
        $stickyStateProvider.enableDebug(true);
    }

})
app.config(['KeepaliveProvider', 'IdleProvider', function (KeepaliveProvider, IdleProvider) {
    KeepaliveProvider.interval(2);
}]);

app.config(function ($sceProvider, $httpProvider, $sceDelegateProvider) {

    $sceProvider.enabled(false);

    $httpProvider.interceptors.push('httpRequestInterceptor');
    /*
    $sceDelegateProvider.resourceUrlWhitelist([
        // Allow same origin resource loads.
        'self'
    ]);
    */

    // The blacklist overrides the whitelist so the open redirect here is blocked.
    /*
     $sceDelegateProvider.resourceUrlBlacklist([
     'http://myapp.example.com/clickThru**'
     ]);
     */
});

app.config(["$locationProvider", function ($locationProvider) {
    $locationProvider.html5Mode(true);
}]);

