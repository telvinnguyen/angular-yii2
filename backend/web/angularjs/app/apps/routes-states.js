app.config(function ($controllerProvider, $compileProvider, $filterProvider, $locationProvider, $provide) {
    app.register = {
        controller: $controllerProvider.register,
        directive: $compileProvider.directive,
        filter: $filterProvider.register,
        factory: $provide.factory,
        service: $provide.service
    };
});

app.config(['$stateProvider', '$routeProvider', '$httpProvider',
    function ($stateProvider, $routeProvider, $httpProvider) {

        $stateProvider
        //===========================================1:BEGIN: login ========================================================
            .state('login', {
                url: '/login',
                templateUrl: _yii_app.controllerPath + '/login/login.html',
                controller: 'loginController',
                data: {
                    css: [
                        _yii_app.angularPath + '/app/css/bootstrap.min.css',
                        _yii_app.angularPath + '/app/controllers/login/login.css',
                        _yii_app.layoutPath + '/sample/js/plugins/iCheck/square/blue.css'

                    ]
                },

                resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load files for an existing module

                        $ocLazyLoad.load('ngEnter');
                        return $ocLazyLoad.load({
                                name: 'zwenGlobal',
                                files: [
                                    _yii_app.controllerPath + '/login/loginController.js',
                                    //_yii_app.livePath + '/loginController.min.js'
                                ]
                            }
                        );
                    }]
                }
            })
            //===========================================1:END: login ==========================================================

            //===========================================2:BEGIN: forgotPassword ========================================================
            .state('forgot-password', {
                url: '/forgot-password',

                controller: 'forgotPasswordController',
                templateUrl: _yii_app.controllerPath + '/forgot-password/forgot-password.html',
                data: {
                    css: [
                        _yii_app.angularPath + '/app/css/bootstrap.min.css',
                        _yii_app.angularPath + '/app/controllers/forgot-password/forgot-password.css',
                        _yii_app.layoutPath + '/sample/js/plugins/iCheck/square/blue.css'
                    ],
                },
                resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load({
                            name: 'zwenGlobal',
                            files: [
                                _yii_app.controllerPath + '/forgot-password/forgotPasswordController.js'
                            ]
                        });
                    }]
                }

            })
            //===========================================2:END: forgotPassword ========================================================
                
            //===========================================3:BEGIN: main ========================================================
            .state('main', {
                url: '/main',
                templateUrl: _yii_app.controllerPath + '/main/main.html'
            })
            //===========================================3:END: main ========================================================

            //===========================================4:BEGIN: home ========================================================
            .state('home', {
                url: '/home',
                controller: 'homeController',
                templateUrl: _yii_app.controllerPath + '/home/home.html',
                data: {
                    css: [
                        _yii_app.angularPath + '/app/controllers/home/home.css'
                    ], requiresLogin: true
                },
                resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([{
                            name: 'zwenGlobal',
                            files: [
                                _yii_app.controllerPath + '/home/homeController.js'
                            ],
                            serie: true
                        }, 'BE-dashboard']);
                    }]
                }

            })
            //===========================================4:END: home ========================================================

            //===========================================5:BEGIN: topic ========================================================
            .state('topic', {
                url: '/topic',
                controller: 'topicController',
                templateUrl: _yii_app.controllerPath + '/topic/topic.html',
                data: {
                    css: [
                        _yii_app.angularPath + '/app/controllers/topic/topic.css',
                        _yii_app.angularPath + '/lib/vendor/jqwidgets/styles/jqx.bootstrap.min.css',
                    ], requiresLogin: true
                },
                resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([{
                            name: 'zwenGlobal',
                            files: [
                                _yii_app.controllerPath + '/topic/_topic_addform.js',
                                _yii_app.controllerPath + '/topic/_topic_grid.js',
                                _yii_app.controllerPath + '/topic/topicController.js',

                            ],
                            serie: true
                        }, 'BE-dashboard', 'jqwidgets', 'ngFx']);
                    }]
                }

            })
            //===========================================5:END: topic ========================================================

            .state("otherwise", {
                url: "*path",
                templateUrl: _yii_app.controllerPath + '/404/404.html',
                data: {
                    css: [
                        _yii_app.angularPath + '/app/css/bootstrap.min.css',
                        _yii_app.angularPath + '/app/css/backend.min.css',
                        _yii_app.angularPath + '/app/css/skins/_all-skins.min.css'
                    ]
                },
                controller: function ($scope) {
                    $scope.baseUrl = _yii_app.baseUrl;
                    $scope.layoutPath = _yii_app.layoutPath;
                    $scope.bodyClass = '';

                    $scope.msg = "Sorry, this page doesn't exist."

                }
            });
    }]);

/* last state #: 1*/


app.run(function ($rootScope, $state, $stateParams, store, dialogs, $ocLazyLoad, ssHelper) {

    $rootScope.$on('$stateChangeStart', function (e, toState, toParams, fromState, fromParams) {
        if (toState.data && toState.data.requiresLogin) {
            if (!store.get('ss-identity-jwt')) { // || jwtHelper.isTokenExpired(store.get('ss-identity-jwt'))
                e.preventDefault(); // add this to make sure the next line $state.go is going to work correctly
                $state.go('login');
            } else {

                //valdidate account id
                if (toState.data.requireSuperAdminRole && toParams.acc_id != '1') {
                    e.preventDefault();
                    dialogs.notify('', 'Access denined.', {size: 'sm'});
                    $state.go('home');
                }

                if (ssHelper.isNumber(toParams.acc_id)) {

                    ssHelper.post('/subscribe/Validateaccount', {
                        acc_id: toParams.acc_id
                    }).then(function (response) {
                        //$scope.$apply(function(){})
                        if (response.data.OK == 1) {
                            if (!response.data.allowToAccess) {
                                $state.go('home');
                            }
                        }
                    });
                }
            }
        }
        else if (toState.data.isPublished) {
            //startGlobalServer($rootScope, ratchetService, store, $timeout);
        }
    });

    $rootScope.$state = $state;
})



