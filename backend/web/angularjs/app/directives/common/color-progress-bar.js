/*** require the css color-progress-bar.css
 Usage:
 <color-process-bar percent="percentOfProcess" bar-width="400" bar-height="16"></color-process-bar>
 ***/
angular.module('zwenGlobal').directive('colorProcessBar', function ($timeout, $q) {
    return {
        restrict: 'E',
        scope: {
            barWidth: '=',
            barHeight: '=',
            percent: '='
        },
        controller: function ($scope, $element, $attrs) {
            //console.log($attrs.percent);

        },
        template: function (element, attrs) {
            return '<div class="cpb-container"> <div class="cpb-progress" ng-style="{\'width\': {{barWidth}} + \'px\'}"><div class="cpb-progress-bar" ng-style="{\'height\': {{barHeight}} + \'px\'}" ng-class="layoutPercent"></div></div></div>'
        },
        link: function (scope, element, attrs) {
            scope.$watch('percent', function (newValue, oldValue) { // watch attribute
                scope.layoutPercent = 'five';
                //console.log('newvalue: ' + newValue)

                //var rangeMap = {'five': 5, 'twentyfive': 25, 'fifty': 50, 'seventyfive': 75, 'onehundred': 100};
                var statusList = ['five' ,'twentyfive', 'fifty', 'seventyfive', 'onehundred'];

                scope.upgradeProgressLevel = function (status) {
                    var deferred = $q.defer();
                    $timeout(function () {
                        if(angular.isDefined(status)){
                            scope.layoutPercent = status;

                            //find index of item in array
                            var foundIndex = _.indexOf(statusList, status);

                            if(foundIndex < 4){
                                //increased status level index
                                foundIndex += 1;
                                deferred.resolve(statusList[foundIndex]);
                            }else{
                                //console.log('reject');
                                deferred.reject(); //This forwards the error to the next error handler;
                            }
                        }

                    }, 150)
                    return deferred.promise
                }


                if(newValue >= 100 && oldValue < 25 ){
                    scope.upgradeProgressLevel('twentyfive')
                        .then(function(data){return scope.upgradeProgressLevel(data)}) //switch to fifty
                        .then(function(data){return scope.upgradeProgressLevel(data)}) //switch to seventyfive
                        .then(function(data){return scope.upgradeProgressLevel(data)}) //switch to onehundred
                }else if(newValue >=75 && oldValue < 25){
                    scope.upgradeProgressLevel('twentyfive')
                        .then(function(data){return scope.upgradeProgressLevel(data)}) //switch to fifty
                        .then(function(data){return scope.upgradeProgressLevel(data)}) //switch to seventyfive
                }else if(newValue >=50 && oldValue < 25){
                    scope.upgradeProgressLevel('twentyfive')
                        .then(function(data){return scope.upgradeProgressLevel(data)}) //switch to fifty
                }else{
                    if(newValue >= 25 && newValue < 50) {
                        scope.layoutPercent = 'twentyfive';

                    }  else if(newValue >= 50 && newValue < 75) {
                        scope.layoutPercent = 'fifty';
                    } else if(newValue >= 75 && newValue < 100) {
                        scope.layoutPercent = 'seventyfive';
                    } else if(newValue >= 100) {
                        scope.layoutPercent = 'onehundred';
                    }
                }
            })
        }
    }
});