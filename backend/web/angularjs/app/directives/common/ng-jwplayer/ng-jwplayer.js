angular.module('zwenGlobal').directive('ngJwplayer', function ($window, $compile, $timeout, ssHelper) {

    var eventPrefix = 'jwplayer.event.';
    return {
        restrict: 'E',
        scope: {
            player: '=?',
            playerId: '@',
            startFullScreen: '=?',
            setupOptions: '=setup',
            startParams: '=?',

            'onPlayerSet': '&?'

        },
        link: function(scope, element, attrs) {
            scope.$watch('setupOptions.file', function(value){
            	
            	console.log(scope.setupOptions);
            	
                if(angular.isDefined(value) && $window.jwplayer){
                    $window.jwplayer.key = "V1AO4CpuO9AOnz/DhRio0x7qhQnr9oIFrSqdtA==";

                    var id = scope.playerId || 'random_player_' + Math.floor((Math.random() * 999999999) + 1),
                        getTemplate = function (playerId) {
                            return '<div id="' + playerId + '"></div>';
                        };

                    element.html(getTemplate(id));
                    $compile(element.contents())(scope);

                    var startParams = "start";
                    if (angular.isDefined(scope.startParams)) {
                        startParams = scope.startParams;
                    }

                    var defaultOptions = {
                        id: 'playerID',
                        autostart: true,
                        startparam: startParams,

                        width: '1030',
                        height: '577',
                        events: {
                            onError: function () {
                            },
                            onReady: function () {
                                if (scope.startFullScreen) {
                                    $('#' + id).toggleClass('jwfullscreen');
                                }

                                if (angular.isDefined(attrs.onPlayerSet) && angular.isDefined(scope.player)) {
                                    var onPlayerSet = scope.onPlayerSet();
                                    onPlayerSet(scope.player);
                                }
                            },
                            onPlay: function(event){
                                applyBroadcast(eventPrefix + 'playing', scope.player, event);
                            }
                        },
                        //'skin': '/signsmart2/themes/sign_smart/js/jwplayer/skins/five.xml',
                        //'skin': _yii_app.layoutPath + '/js/jwplayer/skins/six.xml',
                        'repeat': true,
                        'modes': [
                            {type: 'flash', src: _yii_app.layoutPath + '/js/jwplayer/jwplayer.flash.swf'},
                            {type: 'html5'}
                        ]
                    }

                    if (angular.isDefined(scope.setupOptions)) {
                        defaultOptions = _.extendOwn(defaultOptions, scope.setupOptions);
                    }

//                    if (ssHelper.isLoadedInIframe()) { //check if the current page is loaded inside of a iframe
//                        defaultOptions.autostart = false; // autoplay is disabled with the video that is loaded in iframe
//                    }

                    scope.player = jwplayer(id).setup(defaultOptions);

                }

            }, true)

            function applyBroadcast() {
                var args = Array.prototype.slice.call(arguments);
                scope.$apply(function () {
                    scope.$emit.apply(scope, args);
                });
            }
        }
    };
});