app.directive('loadDymTemplate', function () {
    return {
        scope: {
            loadTemplate: '@', //Isolate Scope One Way String Binding (Parent changes affect child, child changes does not affect parent),
            params: '@' // Isolate Scope Object & Object Literal Expression Binding
        },
        restrict: 'E',
        link: function (scope, element, attrs) {
            scope.contentUrl = '';
            attrs.$observe("loadTemplate", function (value) {
                if(!_.isEmpty(value))
                    scope.contentUrl = _yii_app.absTemplatePath + '/' + value;

            });

            attrs.$observe("params", function (value) {
                if(!_.isEmpty(value))
                    scope.params = angular.fromJson(value);

            });
            //console.log(scope.$parse(attr.params)(scope))
            //t = angular.fromJson(scope.params);
        },
        template: '<div ng-include="contentUrl"></div>'
    }
});