angular.module('zwenGlobal').directive('pictureFrame', function ($timeout, $ocLazyLoad, ssHelper) {
    return {
        restrict: 'E',
        scope: {width: '@?', height: '@?', ngStyle: '@?'},
        templateUrl: _yii_app.directivePath + '/common/picture-frame/picture-frame.html',
        bindToController: true,
        controller: function ($scope, $attrs) {
            $ocLazyLoad.load(_yii_app.directivePath + '/common/picture-frame/picture-frame.css');
        },
        link: function (scope, element, attrs) {
            scope.$watch(function(){ return attrs.ngSrc}, function (src, src1) {
                scope.src = src
                var _width = scope.width || 0
                var _height = scope.height || 0
                var _ngStyle = {
                    width: _width + 'px',
                    height: _height + 'px',
                }
                if (angular.isDefined(scope.ngStyle)) {
                    scope.ngStyle = _.extendOwn(_ngStyle, scope.ngStyle);
                }
                scope.ngStyle = _ngStyle;

                scope.innerStyle = {
                    width: s.toNumber(_width) + 30 + 'px',
                    height: s.toNumber(_height) + 30 + 'px'
                };

                scope.outerStyle = {
                    width: s.toNumber(_width) + 63 + 'px',
                    height: s.toNumber(_height) + 63 + 'px'
                };

            }, true);
        }
    };
});