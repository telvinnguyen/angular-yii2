app.directive('ngThumb', ['$window', function($window) {
    var helper = {
        support: !!($window.FileReader && $window.CanvasRenderingContext2D),
        isFile: function(item) {
            return angular.isObject(item) && item instanceof $window.File;
        },
        isBlob: function(item) {
            return angular.isObject(item) && item instanceof Blob;
        },
        isImage: function(file) {
            var type =  '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|svg|svg+xml|'.indexOf(type) !== -1;
        }
    };

    return {
        restrict: 'A',
        template: '<canvas/>',
        link: function(scope, element, attributes) {
            if (!helper.support) return;
            var params;
            var canvas = element.find('canvas');

            attributes.$observe('watchKey', function(){
                console.log('watch attributes.ngThumb new filereader');
                params = scope.$eval(attributes.ngThumb);
                if (helper.isFile(params.file)==false && helper.isBlob(params.file)==false) return;
                if (!helper.isImage(params.file)) return;
                var reader = new FileReader();

                reader.onload = onLoadFile;
                reader.readAsDataURL(params.file);
            }, true);

            function onLoadFile(event) {
                var img = new Image();
                img.onload = onLoadImage;
                img.src = event.target.result;
            }
            function onLoadImage() {
                var width = params.width || this.width / this.height * params.height;
                var height = params.height || this.height / this.width * params.width;
                canvas.attr({ width: width, height: height });
                //canvas.css({ width: width, height: height });
                //canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);


                //scale
                var nWidth, nHeight;
                if ((this.width <= width && this.height <= height) || (width == 0 && height == 0)) { //If image dimension is smaller, do not resize
                    nWidth = this.width;
                    nHeight = this.height;
                } else {
                    // resize it, but keep it proportional
                    var factor;
                    if (width == 0) factor = height / this.width;
                    else if (height == 0) factor = $w / this.height;
                    else{
                        factor = _.min([(width / this.width), (height / this.height)]);
                    }

                    nWidth = this.width * factor;
                    nHeight = this.height * factor;
                }

                nWidth = Math.round(nWidth);
                nHeight = Math.round(nHeight);


                var ctx = canvas[0].getContext('2d');
                ctx.drawImage(this, 0, 0, this.width,  this.height,    // source rectangle
                    (width-nWidth)/2, (height-nHeight)/2, nWidth, nHeight)  // destination rectangle
            }
        }
    };
}]);
