setTimeout(
    function asyncBootstrap() {

        angular.bootstrap( document, [ "zwenGlobal" ] );

    },
    ( 0.0001 * 1000 ) //loading time
);
app.directive(
    "zwenGlobalLoading",
    function( $animate ) {

        // Return the directive configuration.
        return({
            link: link,
            restrict: "C"
        });

        // I bind the JavaScript events to the scope.
        function link( scope, element, attributes ) {

            $animate.leave( element.children().eq( 1 ) ).then(
                function cleanupAfterAnimation() {
                    scope = element = attributes = null;

                }
            );

        }

    }
);
