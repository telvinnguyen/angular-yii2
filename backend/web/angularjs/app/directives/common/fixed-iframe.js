/**
 * resize-iframe: options { ...}
 * expect-width: number - the fixed width that iframe should be scaled for
 * expect-height: number - the fixed height that iframe should be scaled for
 * usage: <div resize-iframe ng-src="{{url}}" expect-width="width" expect-height="height" on-message-key="previewSkin" style="width:200px;height:200px" />
 * the default width height on the style of div creates the space for the loader icon
 */
app.directive('fixedIframe', function ($timeout, ssHelper, $sce) {
    return {
        restrict: 'A',
        bindToController: true,
        scope: {
            expectWidth: '=', expectHeight: '=', 'iframeUrl': '='
        },
        template: '<div loader working="frameLoader" template="3"></div> <iframe ng-src="{{url}}" ng-hide="frameLoader"/>',
        link: function (scope, element, attrs) {
            scope.frameLoader = true;
            scope.resizable = s.toBoolean(attrs.resize || false);
            var iframe = element.find('iframe');
            scope.url = $sce.trustAsResourceUrl(scope.iframeUrl)
            //document.getElementById("e123").setAttribute('src', attrs.iframeSrc);
           //iframe.attr('ng-src', url)
            //console.log(url)
            iframe.on('load', function () {
                console.log('iframe onload')

                var current_iframe = this;

                // check is preview skin
                var url = scope.iframeUrl;
                var margin = 0;
                if(url.indexOf('preview-skin/')>=0){
                    margin = 2;
                }

                element.css('width', scope.expectWidth + margin);
                element.css('height', scope.expectHeight + margin);

                $(current_iframe).css('width', scope.expectWidth + margin);
                $(current_iframe).css('height', scope.expectHeight +margin);

                var a = $(current_iframe).contents();
                if(angular.isDefined(a)){
                    var b = a.find('body')
                    if(angular.isDefined(b) && !scope.resizable){
                        b.css({ overflow: 'hidden' });
                    }
                }
                $timeout(function(){scope.frameLoader = false;})

            })
        }
    };
})