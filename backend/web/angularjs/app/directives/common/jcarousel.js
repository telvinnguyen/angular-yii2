/**
 * getMoreFunc (note that we have to keep the method bracket ..() when passing the function into the attribute this directive
 */
app.directive('jcarousel', function ($timeout, ssHelper, $rootScope) {

    return {
        restrict: 'A',
        transclude: false,
        scope: {getMoreFunc: '&', updateBy: '=', pageIndex: '=', pageCount: '='},
        //replace: true,
        //template: '<div class="jcarousel-wrapper"><div class="jcarousel"><ul><li ng-repeat="img in images">{{img.imageKey}}</li></ul></div><a href="javascript:void(0)" class="jcarousel-control-prev">&lsaquo;</a><a href="javascript:void(0)" class="jcarousel-control-next">&rsaquo;</a></div>',
        link: function (scope, element, attrs) {
            //ssHelper.debugLog('jcarousel directive/link');
            scope.trackIndex = 0;
            var carouselOptions = {
                vertical: attrs.jcType == 'vertical'
            };
            var jcKey = (angular.isDefined(attrs.jcKey)) ? '.' + attrs.jcKey : '' ;
            var container = $(element);

            var carousel = container.find('[jcarousel-container]:first');

            var carouselInstance = carousel.jcarousel(carouselOptions);

            var carouselData = $(carouselInstance).data('jcarousel');
            //scope.$watch(attrs.updateBy, function (list) { // use attribute just when we don't have define new scope on the directive

            var pageSize = s.toNumber(attrs.jcPageSize);
            var getMoreFunction, promise;

            var reloadOptions;
            scope.$watch('updateBy', function (list, oldList) {
                //console.log(attrs.typeList + ' update by list');
                if(!angular.isDefined(list)) return false;

                pageSize = s.toNumber(attrs.jcPageSize);
                if ('typeList' in attrs && attrs.typeList == 'action') {
                    console.log('scroll 0', attrs.typeList)
                    carousel.jcarousel('scroll', 0);
                }

                if (list.length > 0 && list.length > pageSize) { // first load there is no item in list because of ajax, then length=0
                    container.find('.jcarousel-prev' + jcKey).show();
                    container.find('.jcarousel-next' + jcKey).show();

                    $timeout(function () {
                        //ssHelper.debugLog('reload jcarousel')
                    	//console.log('reloaded', attrs.typeList)
                    	
                        carousel.jcarousel('reload')
                        if ('typeList' in attrs && attrs.typeList == 'action') {

                            carousel.jcarousel('scroll',0);
                        }

                    }); // wait for the angular js done, reload the carousel because of the bug of the first position was wrong
                    //carousel.jcarousel('reload')
                    container.find('.jcarousel-prev' + jcKey)
                        .on('jcarouselcontrol:active', function () {
                            if(carouselData.vertical){
                                $(this).removeClass('jcarousel-prev-disabled-vertical');
                            }else{
                                $(this).removeClass('jcarousel-prev-disabled-horizontal');
                            }

                            $(this).addClass('jcarousel-prev-active' + jcKey);
                        })
                        .on('jcarouselcontrol:inactive', function () {
                            if(carouselData.vertical){
                                $(this).addClass('jcarousel-prev-disabled-vertical');
                            }else{
                                $(this).addClass('jcarousel-prev-disabled-horizontal');
                            }
                            $(this).removeClass('jcarousel-prev-active' + jcKey);
                        })
                        .jcarouselControl({
                            carousel: $(carousel),
                            target: '-=' + attrs.jcStep // this is number of item will be scrolled by each click
                        });

                    container.find('.jcarousel-next' + jcKey)
                        .on('jcarouselcontrol:active', function () {
                            if(carouselData.vertical){
                                $(this).removeClass('jcarousel-next-disabled-vertical');
                            }else{
                                $(this).removeClass('jcarousel-next-disabled-horizontal');
                            }
                            $(this).addClass('jcarousel-next-active' + jcKey);

                        })
                        .on('jcarouselcontrol:inactive', function () {
                            if(carouselData.vertical){
                                $(this).addClass('jcarousel-next-disabled-vertical');
                            }else{
                                $(this).addClass('jcarousel-next-disabled-horizontal');
                            }
                            $(this).removeClass('jcarousel-next-active' + jcKey);
                        })
                        .jcarouselControl({
                            carousel: $(carousel),
                            target: '+=' + attrs.jcStep // this is number of item will be scrolled by each click
                        });



                } else if (list.length > 0 && list.length <= s.toNumber(attrs.jcPageSize)) { // first load there is no item in list because of ajax, then length=0
                    container.find('.jcarousel-prev' + jcKey).hide();
                    container.find('.jcarousel-next' + jcKey).hide();
                }else if (list.length == 0) { // first load there is no item in list because of ajax, then length=0
                    container.find('.jcarousel-prev' + jcKey).hide();
                    container.find('.jcarousel-next' + jcKey).hide();
                }

                carousel.on('jcarousel:scrollend', function(event, carousel) {
                    //console.log(event, carousel)
                    //console.log('scrollend: list length, old list length:', list.length, oldList.length);
                });

                if ('typeList' in attrs && attrs.typeList == 'toolbars'){
                    scope.$root.$on('toolbars-scroll-to', function(e,data){
                        //console.log('scroll toolbars to index:', data)
                        carousel.jcarousel('scroll',data);
                    })
                }


                if(scope.pageIndex == 0){
                    $timeout(function () {
                        //carousel.jcarousel('reload')
                        scope.loadAll = 0;
                        //carousel.jcarousel('scroll', 0);

                    }, 500)
                }

            }, function (t) {
            });

            scope.loadAll = 0;

            container.find('.jcarousel-prev' + jcKey).on('click',function(){
                if($(this).hasClass('jcarousel-prev-active' + jcKey)){
                    scope.trackIndex -= 1;
                    if ('typeList' in attrs && attrs.typeList == 'toolbars') {
                        scope.$emit('toolbars-index-changed',{'curIndex': scope.trackIndex} )
                    }
                }
            })

            container.find('.jcarousel-next' + jcKey).on('click',function(){

                if($(this).hasClass('jcarousel-next-active' + jcKey)){
                    scope.trackIndex += 1;
                    if ('typeList' in attrs && attrs.typeList == 'toolbars') {
                        scope.$emit('toolbars-index-changed',{'curIndex': scope.trackIndex} )
                    }
                }

                //console.log('jcarousel-next click and loadAll = ' + scope.loadAll)
                if(scope.loadAll == 0 || scope.pageCount > 2){
                    scope.loadAll = 1;
                    //console.log('jcarousel-next-active' + jcKey)
                    if($(this).hasClass('jcarousel-next-active' + jcKey)){
                    	
                    	if(scope.pageCount > 2){

	                        getMoreFunction = scope.getMoreFunc();
	                        if(angular.isFunction(getMoreFunction)){
	                            promise = getMoreFunction(1);
	                            promise.then(function(response){

	                                //carousel.jcarousel('scroll', 2);
	                            })
	                            console.log('getMoreFunction')
	                        }
                    	}
                            
                    }
                }

            })
        }
    };
});