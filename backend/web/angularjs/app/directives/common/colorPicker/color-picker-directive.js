// Add a directive for the bootstrap color picker widget
// http://www.eyecon.ro/bootstrap-colorpicker/
app.directive('colorPicker', function($timeout) {

    return {
    	restrict: 'A',
        require: '?ngModel',
        scope: {
        	'code': '='
        },
        link: function(scope, element, attrs, controller) {
            
            //var container = $(element);
            
            
            scope.$watch('code', function(newValue){
            	
            	element.ColorPicker({
	            	onChange: function (hsb, hex, rgb) {
	            		//$('#colorSelector div').css('backgroundColor', '#' + hex);
	            		scope.code = '#' + hex;
	                	//scope.$apply();
	                	$timeout(function(){});
	                	
	            	},
	            	onSubmit: function(hsb, hex, rgb, el) {
	            		scope.code = '#' + hex;
	            		//scope.$apply();
	                	$timeout(function(){});


	            		$(el).val(hex);
	            		$(el).ColorPickerHide();
	            	},
                    onCancel: function(hsb, hex, rgb, el) {
                        scope.code = '#' + hex;
                      //scope.$apply();
	                	$timeout(function(){});
	                	
                        $(el).val(hex);
                        $(el).ColorPickerHide();
                    }
            	});
            	
            })

        }
    };
});

