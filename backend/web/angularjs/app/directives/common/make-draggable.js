/**
 * make-draggable: options { ...}
 * on-drag: function (resize-item, ui, event, object) (object contains the element which is attached the directive)
 * on-drag-stop: function (resize-item, ui, event, object)  (object contains the element which is attached the directive)
 */
angular.module('zwenGlobal').directive('makeDraggable', function ($parse) {
    return {
        restrict: 'A',
        scope: true,
        link: function postLink(scope, element, attrs) {
            var options = scope.$eval(attrs.makeDraggable);
            if(!angular.isDefined(options)){
                options = {}
            }

            element.draggable(options);
            var obj = {element: undefined}
            element.on('drag', function (evt, ui) {
                obj.element = element;
                if(angular.isDefined(attrs.onDrag)){
                	
                    $parse(attrs.onDrag)(scope.$parent, {ui: ui, evt: evt, object: obj});
                }
            });
            element.on('dragstop', function (evt, ui) {
                obj.element = element;
                if(angular.isDefined(attrs.onDragStop)){
                    $parse(attrs.onDragStop)(scope.$parent, { ui: ui, evt: evt, object: obj});
                }

            });
        }
    };
})