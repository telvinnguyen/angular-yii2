app.directive('quickPickItem', function ($timeout, ssHelper) {

    return {
        restrict: 'A',
        transclude: true,
        bindToController: true,
        templateUrl: _yii_app.directivePath + '/common/quick-pick-item/quick-pick-item.html',
        scope:{
            sender: '=', // to communicate between directive & its parent 
            selectedList: '=', 
            apiGetList: '=', //contain url and params to call API
            multiple: '@',
            onListLoaded: '&',
            onSelect: '&'
        },
        controller: function($scope, $element, $attrs, $q, errorService, ssHelper){
            //$scope.layoutPath = _yii_app.layoutPath;
            $scope.list_id = [];

            $scope.ivPageIndex = 0;
            $scope.ivPageCount = 0;
            $scope.itemVariants = [];

            $scope.sorter = 'created_date';
            $scope.sort_order = 'desc';
            $scope.keyword = '';

            $scope.selected_list = [] // contain selected ovid(s)

            $scope.$watch('selectedList', function (selected_list) {
                if(angular.isDefined(selected_list)){
                    $scope.selected_list = selected_list;
                    $scope.sender.loadItemVariants(1);
                }
            });

            // the loader of whole screen

            ssHelper.get('/content/GetContentSetting').then(function (response) {
                if (response.data.OK == 1) {
                    $scope.dim_rate = response.data.dim_rate;
                }
            });


            $scope.sender.loadItemVariants = function(_pageIndex){
                $scope.ivPageIndex = angular.isDefined(_pageIndex) ?  _pageIndex : 0;
                $scope.rowLoader = true;

                var requireParams = {
                    sorter: $scope.sorter, sort_order: $scope.sort_order,
                    keyword: $scope.keyword, pageIndex: $scope.ivPageIndex,
                    selected_list: $scope.selected_list
                }
                requireParams = _.extend(requireParams, $scope.apiGetList.params)

                var deferred = $q.defer();
                ssHelper.post($scope.apiGetList.url, requireParams).then(function (response) {
                    $scope.rowLoader = false;
                    if (response.data.OK == 1) {
                        $scope.itemVariants = response.data.list;
                        var onListLoaded = $scope.onListLoaded(); //need to mark as method once before call it
                        onListLoaded($scope.itemVariants)

                    } else {
                        errorService.throw(response.data.errors);
                        return false;
                    }
                });
                return deferred.promise;
            }

            $scope.sender.cleanSearchField = function(){
                $scope.keyword = '';
            }

            $scope.remote = {};
            $scope.remote.toggleObjectVariant = function (ov) {

                $scope.multiple = s.toBoolean($scope.multiple);
                //reload the toolbar and corresponding variant list
                var _status = ov.active;
                var list = $scope.itemVariants;
                if (!angular.isDefined(ov.name)) {
                    ov = _.findWhere(list, {ovid: ov.ovid.toString()})
                }

                if(!$scope.multiple){ //inactivate all and enable single selected ov
                    angular.forEach(list, function (entry, i) {
                        entry.active = false;
                        //entry.selected = false;
                    })

                    //active the selected only an item variant
                    ov.active = true;
                    $scope.selected_list = [ov.ovid];
                    //ov.selected = true;
                }else{ //multiple selection
                    ov.active = !_status;
                    ssHelper.toggleItemInArray($scope.selected_list, ov.ovid);
                }

                var onSelect = $scope.onSelect(); //need to mark as method once before call it
                onSelect($scope.selected_list, ov);
            }

            $scope.runSorter = function(sorter){
                $scope.sorter = sorter;
                $scope.sort_order = $scope.sort_order == 'desc' ? 'asc' : 'desc';
                $scope.sender.loadItemVariants(1);
            }

            $scope.searchByKeyword = function(){
                $scope.keyword = $scope.keyword;
                $scope.sender.loadItemVariants(1);
            }
        },
        link: function (scope, element, attrs) {

            scope.placeHolder = attrs.qpiPh;
            var listElem = $(element).find('.pick-item-list');

            var scrollerWidth = 57;

            scope.$watch('itemVariants', function (list) {
                var containerLength = list.length
                scope.visibleWidth = containerLength > 5 ?  listElem.width() - scrollerWidth*2 : listElem.width();
            });
        }
    };
});