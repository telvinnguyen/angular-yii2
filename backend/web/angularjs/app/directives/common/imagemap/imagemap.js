app.directive('imageMap', function ($http, $compile,$timeout) {
    return {
        restrict: 'EA',
        scope: {
            size: '=', //{width: 1027, height: 577},
            ngSrc: '=', //{width: 1027, height: 577},
            mapId: '@',
            wrapId: '@',
            imageMap: '=',
            qtipOptions: '=',
            listArea: '='
        },
        controller: function ($scope) {
            $scope.templateData = ''
            $scope.imgSrc = '';

        },
        link: function (scope, element, attrs) {

            var container, b, img;


            scope.$watch('listArea', function (list) {
                angular.forEach(scope.listArea, function (area, key) {
                    area.href = area.href=='' ? "#" : area.href; //mapster script wouldn't high light if anchor href = empty
                    area.attr_tip = (area.custom_tip != "" && area.target == '_popup') ? " custom_tip='tip_{area.imap_id}' " : "";
                    area.target_imap_id = 'qtip_' + area.imap_id;
                });

                $http.get(_yii_app.directivePath + '/common/imagemap/imagemap.html').then(function (response) {
                    scope.templateData = response.data;


                    container = $(element);


                    scope.imgSrc = scope.ngSrc;

                    img = $(new Image()).attr('src', '' + scope.imgSrc);
                    img.attr('id', 'fixImage');
                    img.attr('map-id', scope.mapId);
                    img.attr('usemap', '#' + scope.mapId);
                    img.attr('style', 'max-width:1027px;max-height: 577px; display: block;margin: 0 auto;');
                    img.on('load', onLoadImage);
                    //img.appendTo($(element));



                    b = $compile(scope.templateData)(scope);
                    container.html(b);

                    $(element).prepend(img)



                    //$timeout(function(){
                    //    $('#fixImage').on('load', onLoadImage);
                    //}, 3000)


                    //$(element).find('img').trigger('load');
                });
            })

            function onLoadImage() {
                console.log('image loaded')
                var imageWidth = $(this).width(),
                    imageHeight = $(this).height();

                var mapsterOptions = {
                    wrapId: scope.wrapId,
                    onConfigured: function () {
                        console.log('onConfigured')
                    },
                    stroke: true,
                    render_highlight: {
                        strokeWidth: 2,
                        fill: false
                    },
                    render_select: {
                        stroke: false,
                        fill: false
                    },
                    onClick: function (data) {
                        //this.href ~ data.e.currentTarget.href

                        if (this.href != '' && this.href !== '#' && this.target != '_popup') {
                            window.open(this.href, this.target);
                        }
                    }
                };
                if (angular.isDefined(scope.imageMap)) {
                    mapsterOptions = _.extendOwn(mapsterOptions, scope.imageMap);
                }

                /*scope.$watch('listArea', function (list) {
                 var wrapper = $(element).parent();

                 /* the following code is creating a bug which make the image is not center when switching two page via the menu button
                 if (imageWidth <= scope.size.width || imageHeight <= scope.size.height) {
                 wrapper.css('margin-top', (scope.size.height - imageHeight) / 2);
                 wrapper.css('margin-left', (scope.size.width - imageWidth) / 2);
                 }
                 */

                $(this).mapster(mapsterOptions);

                //});

                setTimeout(function() {
                    angular.forEach(scope.listArea, function (area, key) {
                        var mapTipElement = $(element).find('area#' + area.imap_id);
                        var mapTipContent = $(element).find('#qtip_' +area.imap_id);
                        console.log(mapTipElement);
                        console.log(mapTipContent);
                        var qtipOptions = {
                            content: {
                                text: area.custom_tip,
                                button: true
                            },
                            position: {
                                container: $(element).parent(),
                                at: 'top right',
                                my: 'left bottom'
                                //viewport: $('#content') // Requires Viewport plugin

                            },
                            style: {
                                classes: 'tip-overlay tip-default-font-size'
                            },
                            show: 'click',
                            hide: 'click',
                            events: {
                                hide: function(event, api) {
                                    //$(".tip-overlay").hide();// remove div qtip when hide
                                }
                            }

                        };
                        if (angular.isDefined(scope.qtipOptions)) {
                            qtipOptions = _.extendOwn(qtipOptions, scope.qtipOptions);
                        }
                        if(mapTipElement.attr('target')=="_popup" && mapTipContent.html()!='')mapTipElement.qtip(qtipOptions);


                        //scope.$watch('listArea', function (list) {
                        //    mapTipElement.qtip('destroy');
                        //    mapTipElement.mapster('rebind', mapsterOptions);
                        //})
                    });
                }, 2000);
            }
        }
    }
})