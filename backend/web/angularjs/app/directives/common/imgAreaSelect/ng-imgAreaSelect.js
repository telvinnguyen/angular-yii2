'use strict';

app.directive('ngImgAreaSelect', function () {
    return {
        restrict: 'A',
        scope: {
            selection: '=',
            ngImgAreaSelect: '=',
            options: '='
        },
        link: function postLink(scope, element, attrs) {

            if (element[0].tagName== 'IMG') {
                //do your stuff
                if(scope.selection === undefined) {
                    scope.selection = {};
                }
                if(scope.ngImgAreaSelect === undefined) {
                    scope.ngImgAreaSelect = {};
                }

                scope.ngImgAreaSelect.onSelectChange = function(img, selection) {
                    console.log("Area onSelectChange!");
                    scope.selection.x1 = selection.x1;
                    scope.selection.x2 = selection.x2;
                    scope.selection.y1 = selection.y1;
                    scope.selection.y2 = selection.y2;
                    scope.selection.width = selection.width;
                    scope.selection.height = selection.height;

                    scope.$emit('img-area-select-change', {img: img, selection: selection});

                  //scope.$apply();
                	$timeout(function(){});
                };

                scope.ngImgAreaSelect.onSelectStart = function(img, selection) {
                    scope.$emit('img-area-select-start', {img: img, selection: selection});
                };
                scope.ngImgAreaSelect.onSelectEnd = function(img, selection) {
                    scope.$emit('img-area-select-end', {img: img, selection: selection});
                };

                scope.options.instance = true; // required

                //set the element to append the area-select plugin into
                scope.options.parent = angular.element(element).parent(); // it should be the .modal-body, the parent element of the crop content
                scope.ngImgAreaSelect = angular.element(element).imgAreaSelect(scope.options);

                scope.$watch('selection', function() {
                    scope.ngImgAreaSelect.setSelection(scope.selection.x1, scope.selection.y1, scope.selection.x2, scope.selection.y2);
                    scope.ngImgAreaSelect.update();
                }, true);
            } else {
                console.log("imgAreaSelection attribute can only be used on img elements.");
            }
        }
    };
});