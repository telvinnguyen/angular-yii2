/*
* attributes
* //mode: 'addToElement or saveFile'
* //addTo: append to the element jquery selector
* //onSaved: the callback function which define the destination to post and get response
//mode: 'addToElement or saveFile'
*/
angular.module('zwenGlobal').directive('html2canvasDirective', function ($parse) {
    return {
        restrict: 'A',
        scope: true,
        link: function (scope, element, attrs) {
            console.log(attrs.captureSelector)

            element.bind('click', function(){
                html2canvas($(attrs.html2canvasDirective), {
                    onrendered: function(canvas) {
                        if(attrs.mode == 'addToElement'){
                            //document.body.appendChild(canvas);
                            $(addTo).append(canvas)
                        }else if(!angular.isDefined(attrs.mode) || attrs.mode == 'saveFile'){
                            var dataURL = canvas.toDataURL('image/png')

                            $parse(attrs.onSaved)(scope.$parent, {dataUrl: dataURL});
                        }


                    }
                });
            })
        }
    };
});