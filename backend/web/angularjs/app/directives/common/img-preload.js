app.directive('imgPreload', ['$rootScope', function($rootScope) {
    return {
        restrict: 'A',
        scope: {
            ngSrc: '@'
        },
        link: function(scope, element, attrs) {
            element.on('load', function() {
                element.addClass('in');
            }).on('error', function() {
                //
            });

            scope.$watch('ngSrc', function(newVal) {
                element.removeClass('in');
            });
        }
    };
}]);

app.directive("preloadSrc", function() {
    return {
        link: function(scope, element, attrs) {
            var img, loadImage;
            img = null;

            loadImage = function() {

                //var width = element[0].style.width;
                //var height = element[0].style.height;

                element[0].src = _yii_app.layoutPath + '/images/loader.gif';

                //if(typeof width !== 'undefined'){
                //    element[0].removeAttribute('width');
                //}
                //
                //if(typeof height !== 'undefined'){
                //    element[0].removeAttribute('height');
                //}

                img = new Image();
                img.src = attrs.preloadSrc;

                img.onload = function() {
                    element[0].src = attrs.preloadSrc;
                };
            };

            scope.$watch((function() {
                return attrs.preloadSrc;
            }), function(newVal, oldVal) {
                if (oldVal !== newVal) {
                    loadImage();
                }
            });
        }
    };
});