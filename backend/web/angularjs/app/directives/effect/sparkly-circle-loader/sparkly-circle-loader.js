angular.module('zwenGlobal').directive('sparklyCircleLoader', function ($timeout, $ocLazyLoad, ssHelper) {
    return {
        restrict: 'A',
        scope: { enable: '=sparklyCircleLoader', width: '@', height: '@'},
        template: '<canvas></canvas>',
        controller: function ($scope) {
            $ocLazyLoad.load(_yii_app.directivePath + '/effect/sparkly-circle-loader/sparkly-circle-loader.css');
        },
        link:function(scope, element){
            scope.drawing = {};

            scope.Particle = function( opt ) {
                this.radius = 7;
                this.x = opt.x;
                this.y = opt.y;
                this.angle = opt.angle;
                this.speed = opt.speed;
                this.accel = opt.accel;
                this.decay = 0.01;
                this.life = 1;
            };

            scope.Particle.prototype.step = function( i ) {
                this.speed += this.accel;
                this.x += Math.cos( this.angle ) * this.speed;
                this.y += Math.sin( this.angle ) * this.speed;
                this.angle += scope.PI / 64;
                this.accel *= 1.01;
                this.life -= this.decay;

                if( this.life <= 0 ) {
                    scope.particles.splice( i, 1 );
                }
            };

            scope.Particle.prototype.draw = function( i ) {
                scope.ctx.fillStyle = scope.ctx.strokeStyle = 'hsla(' + ( scope.tick + ( this.life * 120 ) ) + ', 100%, 60%, ' + this.life + ')';
                scope.ctx.beginPath();
                if( scope.particles[ i - 1 ] ) {
                    scope.ctx.moveTo( this.x, this.y );
                    scope.ctx.lineTo( scope.particles[ i - 1 ].x, scope.particles[ i - 1 ].y );
                }
                scope.ctx.stroke();

                scope.ctx.beginPath();
                scope.ctx.arc( this.x, this.y, Math.max( 0.001, this.life * this.radius ), 0, scope.TWO_PI );
                scope.ctx.fill();

                var size = Math.random() * 1.25;
                scope.ctx.fillRect( ~~( this.x + ( ( Math.random() - 0.5 ) * 35 ) * this.life ), ~~( this.y + ( ( Math.random() - 0.5 ) * 35 ) * this.life ), size, size );
            }

            scope.step = function() {
                scope.particles.push( new scope.Particle({
                    x: scope.width / 2 + Math.cos( scope.tick / 20 ) * scope.min / 2,
                    y: scope.height / 2 + Math.sin( scope.tick / 20 ) * scope.min / 2,
                    angle: scope.globalRotation + scope.globalAngle,
                    speed: 0,
                    accel: 0.01
                }));

                scope.particles.forEach( function( elem, index ) {
                    elem.step( index );
                });

                scope.globalRotation += scope.PI / 6;
                scope.globalAngle += scope.PI / 6;
            };

            scope.draw = function() {
                scope.ctx.clearRect( 0, 0, scope.width, scope.height );

                scope.particles.forEach( function( elem, index ) {
                    elem.draw( index );
                });
            };

            scope.init = function() {
                //scope.canvas = $(element).find('canvas').get(0)
                scope.canvas = $(element).find('canvas').get(0) //document.querySelector('canvas')
                console.log(scope.canvas)

                //scope.canvas = document.getElementById('myCanvas');
                //scope.canvas = document.createElement( 'canvas' );
                scope.ctx = scope.canvas.getContext( '2d' );
                scope.width = scope.canvas.width = scope.width;
                scope.height = scope.canvas.height = scope.height;
                scope.min = scope.width * 0.5;
                scope.particles = [];
                scope.globalAngle = 0;
                scope.globalRotation = 0;
                scope.tick = 0;
                scope.PI = Math.PI;
                scope.TWO_PI = scope.PI * 2;
                scope.ctx.globalCompositeOperation = 'lighter';
                //document.getElementById('abc').appendChild( scope.canvas );
                scope.loop();
            };

            scope.loop = function() {
                requestAnimationFrame( scope.loop );
                scope.step();
                scope.draw();
                scope.tick++;
            };


            scope.$watch('enable', function (newValue, oldValue) {
                if(s.toBoolean(scope.enable)){
                    scope.init();
                }
            });
        }
    };
});