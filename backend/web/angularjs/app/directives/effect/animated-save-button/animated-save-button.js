angular.module('zwenGlobal').directive('animatedSaveButton', function ($timeout, $ocLazyLoad, ssHelper) {
    return {
        restrict: 'E',
        scope: {
            key: '@',
            eventClick: '&'},
        templateUrl: _yii_app.directivePath + '/effect/animated-save-button/animated-save-button.html',
        controller: function ($scope) {
            $ocLazyLoad.load(_yii_app.directivePath + '/effect/animated-save-button/animated-save-button.css');
        },
        link:function(scope,element, attrs){
            var ngClick;

            scope.labelText = attrs.labelText || 'Save';
            scope.loadingText = attrs.loadingText || 'Loading';

            $(element).bind('click', function () {
                console.log(scope.key + '-save-process-done')

                $(element).attr('class', '');

                setTimeout(function () {
                    return $(element).addClass('loading-start');
                }, 0);
                setTimeout(function () {
                    return $(element).addClass('loading-progress');
                }, 500);

                var eventClick;
                if (attrs.eventClick) {
                    eventClick = scope.eventClick();
                    eventClick().then(function(){

                        setTimeout(function () {
                            return $(element).addClass('loading-end');
                        }, 500);
                        setTimeout(function () {
                            $(element).attr('class', '');

                            //done animation
                            scope.$root.$emit(scope.key + '-save-done-animation', function(){})
                        }, 1500);
                    });
                }
            });
        }
    };
});