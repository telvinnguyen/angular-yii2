angular.module('zwenGlobal').directive('hoverSelectImg', function ($timeout, $ocLazyLoad, $parse, ssHelper) {
    return {
        restrict: 'A',
        transclude: true,
        scope: {
            icon: '@hoverSelectImg',
            size: '=?',
            ngClick: '&'},
        templateUrl: _yii_app.directivePath + '/effect/hover-select-img/hover-select-img.html',
        controller: function ($scope) {
            $ocLazyLoad.load(_yii_app.directivePath + '/effect/hover-select-img/hover-select-img.css');
            //console.log($scope.ngClick)
        },
        link:function(scope,element, attrs){

            scope.size = scope.size || {width: 320, height: 448};
            scope.text = attrs.text || 'Select';
            //scope.label = attrs.label || '';

            var areaWidth = s.toNumber(scope.size.width)/3;
            var marginTop = (s.toNumber(scope.size.height)/2)-(areaWidth/2)
            var marginLeft = (s.toNumber(scope.size.width)/2)-(areaWidth/2)
            var lineHeight = areaWidth/3
            var areaTextSize = areaWidth/4


            scope.selectAreaStyle = {
                'width': areaWidth,
                'height': areaWidth,
                'line-height': areaWidth + 'px',
                'font-size': areaTextSize + 'px',
                //'margin': s.sprintf('%s %s', marginTop, marginLeft)
                'margin-top': marginTop,
                'margin-left': marginLeft,
            }

            $(element).bind('click', function () {
                var ngClick;
                if (attrs.ngClick) {
                    ngClick = scope.ngClick();
                    //ngClick();
                }

                //$parse(attrs.ngClick)(scope.$parent, {});
            });
        }
    };
});