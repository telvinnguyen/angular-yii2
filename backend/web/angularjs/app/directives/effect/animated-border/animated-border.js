angular.module('zwenGlobal').directive('animatedBorder', function ($timeout, $ocLazyLoad, ssHelper) {
    return {
        restrict: 'A',
        replace: true,
        scope: true,
        transclude: true,
        template: '<div class="animated-border" ng-transclude></div>',
        controller: function ($scope) {
            $ocLazyLoad.load(_yii_app.directivePath + '/effect/animated-border/animated-border.css');
        }
    };
});