angular.module('zwenGlobal').directive('dashboardMainSidebar', function ($timeout) {
    return {
        restrict: 'E',
        scope: {
            db: '='
        },
        transclude: true,
        templateUrl: _yii_app.directivePath + '/partials/dashboard/dashboard-main-sidebar.html',
        bindToController: true,
        controller: function ($scope, htmlHelper) {
            $scope.searchMenu = "";

            $scope.filterMenu = function(menuNames, searchModel) {
                
                
            };
        },
        link: function (scope, element, attrs) {
            console.log("sidebar directive load:", scope.db.left_sidebar.menu)
            /* attach function into the object is holding directive element,
             could use this on the controller of directive */

            /*scope.object.doSomething = function () {

            };*/
        }
    };
});

angular.module('zwenGlobal').filter('filterMenu', function(){
    return function(input_searchModel, menuNames){
        var valid = false;
        if(input_searchModel != ""){
            _.each(menuNames, function(menuName, e){
                console.log(menuName.toLowerCase().indexOf(input_searchModel.toLowerCase()))
                if(menuName.toLowerCase().indexOf(input_searchModel.toLowerCase()) > -1){
                    console.log('found')
                    valid = true;
                    return; // exit of the loop only
                }
            })
            return valid;
            console.log('not here')

            return false;
        }else{
            return true;
        }
    }
    
})