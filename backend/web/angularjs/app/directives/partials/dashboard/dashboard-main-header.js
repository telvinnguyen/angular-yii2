angular.module('zwenGlobal').directive('dashboardMainHeader', function (store, $state, $timeout, ssHelper) {
    return {
        restrict: 'E',
        scope: {
            db: '='
        },
        transclude: true,
        templateUrl: _yii_app.directivePath + '/partials/dashboard/dashboard-main-header.html',
        bindToController: true,
        controller: function ($scope) {
            $scope.logOut = function (){
                ssHelper.post('/home/logout', {});
                store.remove('ss-identity-jwt')
                store.remove('csrf-token')
                store.remove('ss-member-id')
                $state.go('login');
            }
        },
        link: function (scope, element, attrs) {
            /* attach function into the object is holding directive element,
             could use this on the controller of directive */

            /*scope.object.doSomething = function () {

            };*/
        }
    };
});