/* Extend the controller of the object-action Controller
 This holds the business code which is called by another toolbar action
 */
app.config(function ($provide) {
    $provide.decorator('objectActionDirective', function ($delegate, $controller, $rootScope, errorService, ssHelper) {
        var directive = $delegate[0];

        var controllerName = directive.controller;
        directive.controller = function ($scope, $timeout, $modal, $http, $compile, dialogs, noty, ssHelper, $ocLazyLoad,$state, $stateParams, $window, $previousState) {
            var controller = $controller(controllerName, {$scope: $scope});
            $timeout(function () {

                //$ocLazyLoad.load(_yii_app.layoutPath + '/css/object-action-decorator.css');


                /***********************************************************************************************************/
                /*******************************************BUSINESS LOGIC CODE*********************************************/
                /***********************************************************************************************************/

                $scope.showSupportedTypes = function (mtype, oid) {
                    //var deferred = $q.defer();

                    $scope.caller.mainLoader = true; // play the loader indicator
                    var getSupportedExtensionsAPI = '/toolbar/getSupportExtensions'; //deleteCollectionToolbar || deleteGroupToolbar || deleteItemToolbar
                    ssHelper.post(getSupportedExtensionsAPI, {
                        mtype: mtype, oid: oid
                    }).then(function (response) {
                        if (response.data.OK == 1) {
                            //$scope.x = _.extend($scope.x, response.data)
                            $scope.caller.mainLoader = false;
                            dialogs.notify('', '<div class="popUpContentWithPadding">' + response.data.msg + '</div>', {size: 'md'});
                            //deferred.resolve({t: 1});
                        }
                    });
                    //return deferred.promise;
                }
    

                $scope.removeContent = function (gv_id, ovid, name) {

                    var dlg = dialogs.confirm('', 'Remove Item ' + name + '. Are you sure?', {size: 'md'});

                    //var deferred = $q.defer();
                    dlg.result.then(function (btn) { // if user OK
                        $scope.caller.mainLoader = true;
                        ssHelper.post('/content/removeContent', {
                            gv_id: gv_id, iv_id: ovid
                        }).then(function (response) {
                            if (response.data.OK == 1) {
                            	 $scope.caller.loadItemVariants();
                                 $scope.caller.toggleObjectVariant({ovid: gv_id, mtype: 'Group', type: 'gv', oid: '15'});
                                 $scope.caller.mainLoader = false;
                            }
                        });
                    }, function (btn) {
                    });
                }

                $scope.openPreviewSkin = function (skin_id, w, h,action) {
                    //- 40px height bottom button action for preview public skin
                    if(!action){
                        h = h-40;
                    }
                    var previewSkinPromise = $modal.open({
                        templateUrl: _yii_app.absTemplatePath + '/modal-preview-skin.html',
                        windowTemplateUrl: _yii_app.absTemplatePath + '/modal-preview-skin-frame.html',
                        controller: 'modalPreviewSkinCtrl',
                        windowClass: 'modal-preview-skin',
                        scope: $scope,
                        backdrop: 'static',
                        resolve: {
                            params: function () {
                                return {
                                    url: _yii_app.hostUrl + '/site/page/main/preview-skin//skin_id=' + skin_id +'/action='+action,
                                    width: w, height: h + 40 // extra pixels for the action row
                                }
                            }
                        }
                    });
                }

                $scope.openPreviewKiosk = function (acc_id, kiosk_id, w, h) {
                	$scope.homeUrl = '';
                	ssHelper.post('/kiosk/checkhomeurlofkiosk', {kiosk_id: kiosk_id, acc_id: acc_id}).then(function (response) {
                		  if(response.data.homeUrl == null || response.data.homeUrl == '' ){
                			  $scope.homeUrl = _yii_app.hostUrl + s.sprintf('/site/page/main/preview-kiosk/%1$s/kiosk_id=%2$s', acc_id, kiosk_id);
                		  } else {
                			  $scope.homeUrl = response.data.homeUrl; 
                		  }
                     
	                    var previewSkinPromise = $modal.open({
	                        templateUrl: _yii_app.absTemplatePath + '/modal-preview-skin.html',
	                        windowTemplateUrl: _yii_app.absTemplatePath + '/modal-preview-skin-frame.html',
	                        controller: 'modalPreviewSkinCtrl',
	                        windowClass: 'modal-preview-skin',
	                        scope: $scope,
	                        backdrop: 'static',
	                        resolve: {
	                            params: function () {
	                                return {
	                                    url: $scope.homeUrl,
	                                    width: w, height: h //+ 40 // extra pixels for the action row
	                                }
	                            }
	                        }
	                    });
                    
                	});
                }
                
                $scope.openPreviewSign = function (acc_id, sign_iv_id, w, h) {
                	
                	$scope.homeUrl = '';
              	  	ssHelper.post('/sign/checkhomeurlofsign', {sign_iv_id: sign_iv_id, acc_id: acc_id}).then(function (response) {
	              		  if(response.data.homeUrl == null || response.data.homeUrl == '' ){
	              			  $scope.homeUrl = _yii_app.hostUrl + s.sprintf('/site/page/main/preview-sign/%1$s/sign_iv_id=%2$s', acc_id, sign_iv_id);
	              		  } else {
	              			  $scope.homeUrl = response.data.homeUrl; 
	              		  }
	                    var previewSkinPromise = $modal.open({
	                        templateUrl: _yii_app.absTemplatePath + '/modal-preview-skin.html',
	                        windowTemplateUrl: _yii_app.absTemplatePath + '/modal-preview-skin-frame.html',
	                        controller: 'modalPreviewSkinCtrl',
	                        windowClass: 'modal-preview-skin',
	                        scope: $scope,
	                        backdrop: 'static',
	                        resolve: {
	                            params: function () {
	                                return {
	                                    url: $scope.homeUrl,
	                                    width: w, height: h // extra pixels for the action row
	                                }
	                            }
	                        }
                    	});
                    });
                }

                var _waitProcessPublishSkin = function () {
                    $timeout(function () {
                        if (_process < 100) {
                            _process += 2;
                            $rootScope.$broadcast('dialogs.wait.progress', {'progress': _process});
                            _waitProcessPublishSkin();
                        } else {
                            $rootScope.$broadcast('dialogs.wait.complete');
                        }
                    }, 1000);
                };

                var _process = 40;
                $scope.publishSkin = function (skin_id) {
                    var dlg = dialogs.wait(undefined, undefined, _process, {'backdrop': 'static', size: 'md'});
                    _waitProcessPublishSkin();
                    ssHelper.post('/skin/publishSkin', {skin_cv_id: skin_id}).then(function (response) {
                        _process = 100;
                        dialogs.notify('', 'This skin has been published successfully.', {size: 'md'});//close process dialog
                        //_process = 40; //reset the fake process percentage
                    });
                }

                $scope.skinAddColumn = function (skin_id) {
                    $scope.caller.mainLoader = true;
                    ssHelper.post('/skin/SkinAddColumn', {skin_cv_id: skin_id, acc_id: 1}).then(function (response) {
                        $scope.caller.mainLoader = false;
                        $scope.caller.loadGroupVariants($scope.caller.gvPageCount);
                    });
                }

                $scope.kioskAddColumn = function (skin_id, acc_id) {
                    $scope.caller.mainLoader = true;
                    ssHelper.post('/kiosk/KioskAddColumn', {
                        kiosk_cv_id: skin_id,
                        acc_id: acc_id
                    }).then(function (response) {
                        $scope.caller.mainLoader = false;
                        $scope.caller.loadGroupVariants($scope.caller.gvPageCount);
                    });
                }

                $scope.publishItemVariant = function (ovid, acc_id) {
                	
                    var dlg = dialogs.wait(undefined, undefined, _process, {'backdrop': 'static'});
                    _waitProcessPublishSkin();
                    ssHelper.post('/content/GeneratePublishItemURL', {
                        ovid: ovid,
                        acc_id: acc_id
                    }).then(function (response) {
                        _process = 100; //done and close process dialog
                        $timeout(function () {
                            var dlg = dialogs.create(
                                _yii_app.templatePath + '/modal-copy-text.html',
                                'copyTextItemPulishCtrl',
                                {
                                    acc_id: acc_id, ov_type: 'iv', ovid: ovid,
                                    resultText: response.data.publishedUrl,
                                },
                                {size: 'md'});
                            _process = 40;
                            
                            $scope.caller.remote.loadActions();
                            
                            dlg.result.then(function (dialogData) {
                            });
                        }, 1000)

                    });
                }


                /**
                 * selection type sign
                 */

                $scope.selectSign = function (ovid, mtype) {
                    var signupPopupInstance = $modal.open({
                        templateUrl: _yii_app.absTemplatePath
                        + '/modal-sign-type-dialog.html',
                        windowTemplateUrl: _yii_app.absTemplatePath
                        + '/modal-sign-type-dialog-frame.html',
                        controller: 'signInfoCtr',
                        windowClass: 'select-sign-modal-window',
                        scope: $scope,
                        backdrop: true,
                        resolve: {
                        	mtype: function () {
                                return mtype;
                            }
                        }
                        
                    }, {size: 'md'});
                    // ssHelper.debugLog('signupPopupInstance opened ');
                    signupPopupInstance.result.then(function (discount_info) {

                    }, function (reason) {
                        // reason == 'cancel'
                    });
                }

                $scope.setRemoteSign = function (sign_iv_id, name) {
                    var dlg = dialogs.confirm('', 'Set this sign ' + name + ' as your master remtoe. Are you sure?', {size: 'md'});

                    //var deferred = $q.defer();
                    dlg.result.then(function (btn) { // if user OK

                        $scope.caller.mainLoader = true;
                        ssHelper.post('/remote/SetRemoteSign', {
                            sign_iv_id: sign_iv_id
                        }).then(function (response) {
                            if (response.data.OK == 1) {
                                dialogs.notify('', 'The sign is assigned as your Master Remote.')
                                $scope.caller.mainLoader = false;
                                ssHelper.safeRun($scope.caller.remote.loadActions, false)
                                //deferred.resolve({t: 1});
                            } else {
                                errorService.throw(response.data.errors);
                            }
                        });
                    }, function (btn) {
                    });
                }

                $scope.skinColumnCreateBox = function (column_id) {
                    $scope.caller.mainLoader = true;
                    ssHelper.post('/skin/skinColumnCreateBox', {
                        column_id: column_id
                    }).then(function (response) {
                        //$scope.$apply(function(){})
                        if (response.data.OK == 1) {

                            $scope.caller.loadItemVariants();
                        } else {
                            errorService.throw(response.data.errors);
                        }
                        $scope.caller.mainLoader = false;
                    });
                }
                
                /**
                 * public kiosk
                 */
                $scope.publicDesign = function (ovid, is_public) {
                	  if(is_public == true){
                		// when published kiosk
                		  var informRepublish = true;
                			  var dlg = dialogs.confirm('', 'Are you sure you want to replace the current published design at this time?', {size: 'md'});
                		 
                			  dlg.result.then(function (btn) {
                        	  $scope.actionPublicKiosk(ovid, informRepublish);
                        	  _waitProcessPublishSkin();
                			  });
                			 
                	  }if(is_public == false) {// when publish kiosk in fist time
                		  var informRepublish = false;
                		  $scope.actionPublicKiosk(ovid, informRepublish);	
                		 
                	  }
                }
                /**
                 * execute public kiosk
                 */
                $scope.actionPublicKiosk = function (ovid, informRepublish) {
                	 $scope.caller.mainLoader = true;
                	  ssHelper.post('/kiosk/generatepublishkioskurl', {
                          ovid            : ovid,
                          acc_id: $stateParams.acc_id,
                          informRepublish : informRepublish
                      }).then(function (response) {
                          //$scope.$apply(function(){})
                          if (response.data.OK == 1) {
                        	  $scope.caller.mainLoader = false;
                        	  _process = 100;
                        	  var dlg = dialogs.notify('','This design has been published successfully.',{size : 'md'});
          					dlg.result.then(function(name){
          						var state = $state.current.name.toString(); 
          						
          						if(state == 'main.content'){
          							var loadActions = $scope.caller.remote.recall(state, 'loadActions');
          						} else {
          							var loadActions = $scope.caller.recall(state, 'loadActions');
          						}
          						ssHelper.safeRun(loadActions);
          						
          					})
                          }
                          
                      });
                }
                
                /**
                 * set sign PIN
                 */
              	$scope.setSignPin = function (ivid) {
              		 var old_pin = '';
              		 
                    ssHelper.post('/sign/getSignPin', {
                    	ivid : ivid
        	        }).then(function (response) {
        	        	$scope.signupLoader = false;
        	            if (response.data.OK == '1') {
        	            	$scope.signupLoader = true;
        	            	var old_pin = response.data.pin;
        	            	var sign_id = response.data.sign_id;
        	            	
        	          	  var dlg = dialogs.create(
        	                      _yii_app.templatePath + '/modal-set-kiosk-pin.html',
        	                      'setKioskPinCtrl',{sign_id: sign_id, old_pin : old_pin },
        	                      {size: 'sm'});
        	            }
        	        })
            	};
            	
            	/**
            	 * duplicate variant
            	 */
            	
            	$scope.duplicateVariant = function (acc_id, ovid, mtype) {
       	          	  var dlg = dialogs.create(
       	                      _yii_app.templatePath + '/modal-duplicate-variant.html',
       	                      'duplicateVariantCtrl',{acc_id: acc_id, ovid : ovid, mtype: mtype },
       	                      {size: 'md'});
       	          	  dlg.result.then(function (callFunction) {

                          $scope.remote.updateStorage();

       	          		  if(angular.isDefined($scope.caller.remote)){
       	          			var loadItemvariants= $scope.caller.remote.recall($state.current.name.toString(), callFunction);
       	          		  } else {
       	          			var loadItemvariants= $scope.caller.recall($state.current.name.toString(), callFunction);
       	          		  }
       	          		ssHelper.safeRun(loadItemvariants);
       	          		
                    });
            	};
            	
                /**
                 * unpublish Item
                 */
                $scope.unpublishItem = function (ovid, acc_id) {
                		// when published kiosk
                			  var dlg = dialogs.confirm('', 'Are you sure you want to unpublish this item?', {size: 'md'});
                			  dlg.result.then(function (btn) {
                				  $scope.signupLoader = true;
            			         ssHelper.post('/content/Unpulishitem', {
            	                    	ovid : ovid,
            	                    	acc_id : acc_id
            	        	        }).then(function (response) {
            	        	        	$scope.signupLoader = false;
            	        	            if (response.data.OK == '1') {
            	        	            	var dlg_OK = dialogs.notify('', 'This item has been unpulished.', {size : 'md'});
            	        	              	$scope.caller.remote.loadActions();
            	        	            } else {
            	        	            	dialogs.notify('', 'You can not unpublish this item. It has not been published yet.', {size : 'md'});
            	        	            }
            	        	        })
                			  });
                			 
                }
            	

                //$scope.signRdrOpen = function (acc_id, sign_id, skin_id) {
                $scope.signRdrOpen = function (acc_id, sign_id, skin_id) {
                    if(skin_id == 0){
                        dialogs.notify('', 'This sign is not defined with a Skin. You have to set Skin for the Design of this Sign.');
                    }
                    else{
                        //$state.go('main.sign-make-redirection', {acc_id: acc_id, from_sign_id: sign_id})

                        var width = $(window).width();
                        var height = $(window).height();
                        var setupRdrRule = $modal.open({
                            templateUrl: _yii_app.absTemplatePath + '/modal-reroute.html',
                            windowTemplateUrl: _yii_app.absTemplatePath + '/modal-reroute-frame.html',
                            controller: 'modalRerouteCtrl',
                            windowClass: 'modal-preview-skin',
                            scope: $scope,
                            backdrop: 'static',
                            resolve: {
                                params: function () {
                                    return {
                                        url: _yii_app.hostUrl + s.sprintf('/site/page/main/reroute2/%s/%s', acc_id, sign_id),
                                        width: width-20, height: height,
                                        remote: $scope.remote,
                                        sign_id: sign_id
                                    }
                                }
                            }
                        });
                    }
                };

                $scope.skinBoxRdrOpen = function (acc_id, wovid, wgvid, ovid) {
                    var width = $(window).width();
                    var height = $(window).height();
                    var setupRdrRule = $modal.open({
                        templateUrl: _yii_app.absTemplatePath + '/modal-reroute.html',
                        windowTemplateUrl: _yii_app.absTemplatePath + '/modal-reroute-frame.html',
                        controller: 'modalPreviewSkinCtrl',
                        windowClass: 'modal-preview-skin',
                        scope: $scope,
                        backdrop: 'static',
                        resolve: {
                            params: function () {
                                return {
                                    url: _yii_app.hostUrl + s.sprintf('/site/page/main/skin-box-setting/%s/wovid=%s/wgvid=%s/ovid=%s', acc_id, wovid, wgvid, ovid),
                                    width: width-20, height: height
                                }
                            }
                        }
                    });
                };
                
                
                /**
                 * execute public kiosk
                 */
                $scope.downloadFile = function (acc_id, ovid) {
                	 $scope.caller.mainLoader = true;
                	  ssHelper.post('/content/downloadfile', {
                		  ovid            : ovid,
                          acc_id		  : acc_id
                      }).then(function (response) {
                    	  $scope.caller.mainLoader = false;
                          if (response.data.OK == 1) {
                        	  $window.location.href = 'http://localhost/v4/uploads/thumbnails/ZvshF13PgG_thumb_36_36.jpg';
                          }
                          
                      });
                }
                
                /**
                 * remove member from account
                 */
                $scope.removeMemberFromAccount = function (group_account_id, item_member_id, name, ovid, acc_id) {
                	  var dlg = dialogs.confirm('', 'Stop sharing current account with member ' + name +'. Are you sure?', {size: 'md'});
        			  dlg.result.then(function (btn) {
        				  	
                      	$scope.caller.mainLoader = true;
                      	ssHelper.post('/membership/removememberfromaccount', {
                      		group_account_id : group_account_id,
                      		item_member_id	 : item_member_id
                      	}).then(function (response) {
                      		$scope.caller.mainLoader = false;
                      		if (response.data.OK == 1) {
                      			 var stateName = $state.current.name;
                      			 if(stateName == 'main.member'){
                      				 var member_loadItemVariants = $scope.remote.recall(stateName, 'loadItemVariants');
                          			 var member_toggleObjectVariant = $scope.remote.recall(stateName, 'toggleObjectVariant');
                          			 ssHelper.safeRun(member_loadItemVariants).then(function(response){
                          				member_toggleObjectVariant({id: acc_id, modelName: 'AccountUser'});
                          			 })
                      			 }else{
                      				var member_loadItemVariants = $scope.caller.remote.recall(stateName, 'loadItemVariants');
                      				var member_toggleObjectVariant = $scope.caller.remote.recall(stateName, 'toggleObjectVariant');
                         			var member_loadActions = $scope.caller.remote.recall(stateName, 'loadActions');
                         			
                         			ssHelper.safeRun(member_loadActions);
                         			 ssHelper.safeRun(member_loadItemVariants).then(function(response){
                           				member_toggleObjectVariant({type: 'gv', mtype: 'Group', ovid: ovid})
               		                	
                           			 })
                      			 }
                      			
                      			
                      		
                      		}
                      		
                      	});
        			 });
                }

                $scope.createItem = function(acc_id, ovid, mtype, oid, wovid, wgvid){
                    var dlg = dialogs.create(
                        _yii_app.templatePath + '/modal-create-item.html',
                        'createItemCtrl',
                        {acc_id : acc_id, ovid : ovid, mtype : mtype, wovid: wovid, wgvid: wgvid},
                        {size: 'md'}
                    );
                    //dlg.result.then(function (callFunction) {
                    //    var loadItemvariants= $scope.caller.remote.recall($state.current.name.toString(), callFunction);
                    //    ssHelper.safeRun(loadItemvariants);
                    //
                    //});
                };

                $scope.publishSlideShow = function (acc_id, ovid, is_public) {
                    if(is_public == true){
                        // when published slideshow

                        var dlg = dialogs.confirm('', 'Are you sure you want to replace the current published the slide show at this time?', {size: 'md'});

                        dlg.result.then(function (btn) {
                            $scope.actionpublishSlideShow(ovid, acc_id);
                            _waitProcessPublishSkin();
                        });

                    }if(is_public == false) {// when publish kiosk in fist time
                        $scope.actionpublishSlideShow(ovid, acc_id);

                    }
                }

                $scope.actionpublishSlideShow = function (ovid,acc_id) {
                    var dlg = dialogs.wait(undefined, undefined, _process, {'backdrop': 'static'});
                    _waitProcessPublishSkin();
                    ssHelper.post('/slideshow/publishslideshow', {
                        ovid: ovid,
                        acc_id: acc_id
                    }).then(function (response) {
                        _process = 100; //done and close process dialog
                        if(response.data.OK ==1){
                            $timeout(function () {
                                var dlg = dialogs.create(
                                    _yii_app.templatePath + '/modal-copy-text.html',
                                    'copyTextItemPulishCtrl',
                                    {
                                        acc_id: acc_id, ov_type: 'iv', ovid: ovid,
                                        resultText: response.data.publishedUrl,
                                        notSave: true
                                    },
                                    {size: 'md'});


                                $scope.caller.remote.loadActions();

                                dlg.result.then(function (dialogData) {
                                });
                            }, 1000)
                        }else{
                            dialogs.notify('', response.data.error);
                        }


                    });
                }
                
                /**
                 *  Turn video feed
                 */
                $scope.turnVideoFeed = function (ovid, status) {
                	if(status == 1){
                		 var dlg = dialogs.confirm('', 'Are you sure to turn the Video Feed to Off?', {size: 'md'});
                         dlg.result.then(function (btn) {
                        	 $scope.executeTurnVideoFeed(ovid, status);
                         });
                	} else{
                		$scope.executeTurnVideoFeed(ovid, status);
                	}
                   
                }
            	
             	$scope.executeTurnVideoFeed = function (ovid, status) {
             		console.log(ovid, status)
             	 $scope.caller.mainLoader = true;
               	 ssHelper.post('/videofeed/turnVideoFeed', {
	                    	ovid : ovid,
	                    	status : status
	        	        }).then(function (response) {
	        	        	$scope.caller.mainLoader = false;
	        	            if (response.data.OK == '1') {
	        	            	var stateName = $state.current.name;
	        	            	var loadItemVariants = $scope.caller.remote.recall(stateName, 'loadItemVariants');
	        	            	var toggleObjectVariant = $scope.caller.remote.recall(stateName, 'toggleObjectVariant');
	        	              	ssHelper.safeRun(loadItemVariants).then(function(response){
	        	              		toggleObjectVariant({type: 'iv', mtype: 'Item', ovid: ovid})
                       			 })
	        	            }
	        	        });
               	}
             	
             	
             	$scope.extractZipFile = function (ovid, name, mtype, acc_id, wtype, wovid, wgvid) {
             		_process = 80;
             		var dlg = dialogs.wait(undefined, "Extracting...", _process, {'backdrop': 'static', size: 'sm'});
             		_waitProcessPublishSkin();
             		ssHelper.post('/file/extractZipFile', {
             			ovid   : ovid,  acc_id  : acc_id,
             			mtype  : mtype, wtype   : wtype,
             			name   : name,  wovid   : wovid,
             			wgvid  : wgvid,
             		}).then(function (response) {
             			if (response.data.OK == '1') {
             				_process = 100;
             				$timeout(function () {
             				//dialogs.notify('', 'Extracted file successfully.',{size: 'sm'});
             					var _gv_id = response.data.gv_id;
             					var stateName = $state.current.name;
         						var load = (wtype == 'Group') ? 'loadItemVariants' : 'loadGroupVariants';
             					var loadVariants = $scope.caller.remote.recall(stateName, load);
	        	            	var toggleObjectVariant = $scope.caller.remote.recall(stateName, 'toggleObjectVariant');
	        	              	ssHelper.safeRun(loadVariants).then(function(response){
	        	              		toggleObjectVariant({type: 'gv', mtype: 'Group', ovid: _gv_id})
                       			 })	
                       			_process = 100;
             				},1000);
             			}else{
             				$timeout(function () {
             					dialogs.notify('', response.data.mes, {size: 'md'});
             					_process = 100;
             				},1000);
             			}
             		});
             	}

                $scope.copyVariant = function(acc_id, mtype, ovid, name){
                    ssHelper.post('/content/copyVariant', {
                        acc_id: acc_id, mtype: mtype + 'Variant',
                        ovid: ovid, name: name
                            }).then(function (response) {
                                //$scope.$apply(function(){})
                                if (response.data.OK == 1) {
                                    $scope.remote.loadActions();
                                    noty.display({
                                        text: '<p>The object <b>'+ name + '</b> is copied.</p>',
                                        layout: 'centerRight', buttons: false, maxVisible: 1,
                                        timeout: 2000
                                    })
                                }
                            });


                }

                $scope.pasteVariant = function (acc_id, ovid, name, mtype, wtype, wovid, wgvid) {
                    var dlg = dialogs.confirm('', 'You are pasting the object <b>\'' + name + '\'</b> into this account. If the current selected toolbar does not support the extension of the pasting object, the relationship will be ignored. Are you sure to do this?', {size: 'md'});

                    dlg.result.then(function (btn) {
                        ssHelper.post('/content/pasteVariant', {
                            acc_id: acc_id, mtype: mtype + 'Variant',
                            ovid: ovid,
                            wtype: $scope.wtype, wovid: $scope.wovid,
                            wgvid: $scope.wgvid
                        }).then(function (response) {
                            //$scope.$apply(function(){})
                            if (response.data.OK == 1) {
                                $scope.remote.loadCollectionVariants();
                                $scope.remote.loadGroupVariants();
                                $scope.remote.loadItemVariants();
                                $scope.remote.loadActions();
                            }else{
                                dialogs.notify('', response.data.errors)
                            }
                        });
                    });
                };
                
                $scope.deleteAccountCode = function (mtype, ovid, name, wgvid) {
                	
                    var dlg = dialogs.confirm('', 'Delete ' + mtype + ' \'' + name + '\'. Are you sure?', {size: 'md'});
                    dlg.result.then(function (btn) {
                        ssHelper.post('/content/deleteAccountCode', {
                            mtype: mtype, ovid: ovid
                        }).then(function (response) {
                            if (response.data.OK == 1) {
                            	$scope.remote.loadItemVariants();
                                $scope.remote.toggleObjectVariant({ovid: wgvid, mtype: 'Group', type: 'gv'});
                            }
                        });
                    }, function (btn) {
                    });
                };

                _process = 40;
                $scope.checkPaymentBeforeCreatingAccount = function (acc_id, mtype, oid) {
                    $scope.caller.mainLoader = true;
                    var dlg;
                    dialogs.wait(undefined, undefined, _process, {'backdrop' : 'static', 'size': 'md'});
                    // check if user has valid subscription or ARB account
                    ssHelper.post('/membership/checkPaymentBeforeCreatingAccount', {
                    }).then(function (response) {
                        $scope.waiting_loader = false;
                        _process = 100;
                        $rootScope.$broadcast('dialogs.wait.complete');
                        if (response.data.resultCode == 1) { // free
                            $state.go('main.account', {'acc_id':acc_id,'mtype':mtype, 'oid':oid});
                        }

                        if (response.data.resultCode == 4) { // confirm user with a popup message that he has to pay with specific amount
                            dlg = dialogs.confirm('', 'You monthly subscription will be billed with the additional amount of ' + response.data.accountUnitPrice + ' dollars per month. Do you want to proceed?', {size: 'lg'});
                            dlg.result.then(function (btn) {
                                $state.go('main.account', {'acc_id':acc_id,'mtype':mtype, 'oid':oid});
                            });
                        }
                        else if (response.data.resultCode == 2) { // user currently has to update subscription => upgrademember
                            dlg = dialogs.confirm('', 'You have to be a subscribed member to create and purchase additional account. Please go to the subscription screen.', {size: 'lg'});
                            dlg.result.then(function (btn) {
                                $state.go('main.upgrademember2');
                            });
                        }
                        else if (response.data.resultCode == 3) {//create new subscription based on the existent cc info
                            dlg = dialogs.confirm('', 'You have to enter credit card before creating new account.Please go to Payment information page to create by clicking on the Yes button below.', {size: 'lg'});
                            dlg.result.then(function (btn) {

                                $state.go('main.create-payment-info');
                            });
                        }

                    });
                };
             	
                /***********************************************************************************************************/
                /*******************************************BUSINESS LOGIC CODE*********************************************/
                /***********************************************************************************************************/
            });
            return controller;
        };
        return $delegate;
    });
});

app.controller('copyTextItemPulishCtrl', function ($scope, $modalInstance, $stateParams, data, ssHelper) {
    $scope.acc_id = data.acc_id;
    $scope.ovid = data.ovid;
    $scope.ov_type = data.ov_type;
    $scope.resultText = data.resultText;
    if(data.notSave==true){
        $scope.notSave = true;
        $scope.success = true;
    }
    $scope.cancel = function () {
        $modalInstance.close('canceled');
    }; // end cancel

    $scope.saveURL = function () {
    	
        $scope.saveUrlLoader = true;
        ssHelper.post('/content/VariantSaveUrl', {
            acc_id: $scope.acc_id, wovid: $scope.wovid, wtype: $scope.wtype,
            ovid: $scope.ovid, ov_type: $scope.ov_type
        }).then(function (response) {
            //$scope.$apply(function(){})
            if (response.data.OK == 1) {
            	$scope.success = true;
                $scope.saveUrlLoader = false;
            }

            //$modalInstance.close('');
        });
    };
})


app.controller('modalPreviewSkinCtrl', function ($scope, params, $modalInstance) {
    //$scope.url = $sce.trustAsResourceUrl(params.url);
    $scope.url = params.url;
    $scope.width = params.width;
    $scope.height = params.height;

    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    }

    $scope.submit = function () {
        $modalInstance.close($scope.box);
    },
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        }
});

app.controller('modalRerouteCtrl', function ($scope, params, $modalInstance) {
    //$scope.url = $sce.trustAsResourceUrl(params.url);
    $scope.url = params.url;
    $scope.width = params.width;
    $scope.height = params.height;


    $scope.resizeOptions = {
        containment: $('body'),
        scroll: false,
        handles: 'all' //'n, e, s, w, ne, se, sw, nw'
    };
    //$scope.resizeOptions = "{containment: $('.modal-backdrop'),scroll: false, handles: 'all'}";

    $scope.close = function () {
    	params.remote.loadItemVariants().then(function(){
    		params.remote.toggleObjectVariant({mtype: 'Item', type: 'iv', ovid: params.sign_id});
         });
        $modalInstance.dismiss('cancel');
    }

    $scope.submit = function () {
        $modalInstance.close($scope.box);
    },
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        }
});


app.controller('signInfoCtr', function ($scope, $state, $modalInstance, $injector, ssHelper, dialogs, $stateParams, mtype) {

    $scope.select = {sign: "asign"};
    $scope.signPrice = {}
    
    $scope.getPriceForSigns = function(){
    	 ssHelper.post('/sign/getpriceforsigns', {
         }).then(function (response) {
             $scope.signupLoader = false;
             if (response.data.OK == 1) {
            	 $scope.signPrice = response.data.subscription
             }
         });
    }();

    // create default record in Sign table, after that redirect user to SignInfo screen
    $scope.createDefaultSignInfo = function () {

        ssHelper.post('/redirectprocess/signinfo', {
            sign: $scope.select.sign,
            acc_id : $stateParams.acc_id
        }).then(function (response) {
            $scope.signupLoader = false;
            $modalInstance.dismiss('cancel');
            if (response.data.OK == 1) {
            	
                $state.go('main.sign', {
                    acc_id: $stateParams.acc_id,
                    ivid: response.data.iv_id,
                    mtype: mtype
                });
            }

        });
    }


    $scope.step1_OK = function () {
    	 $scope.createDefaultSignInfo();
    }
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

});


angular.module('zwenGlobal').controller('setKioskPinCtrl',function($scope, $modalInstance, $stateParams, data, ssHelper, dialogs, $state){
	
	//use for set sign pin in content
	$scope.old_pin = data.old_pin;
	 $scope.cancel = function(){
		 $modalInstance.dismiss('canceled');
	 };
	//check confirm password
	   $scope.savePin = function(){
		   if(isNaN($scope.pin.password)){
			   dialogs.notify('','The PIN must be numeric',{size: 'sm'});
				return false;
		   }
		   else if(isNaN($scope.pin.re_password)){
			   dialogs.notify('','The Re-enter PIN must be numeric',{size: 'sm'});
				return false;
		   }
		   else if($scope.pin.re_password.toString().length < 4 || $scope.pin.password.toString().length < 4){
			   dialogs.notify('','The PIN must have at least 4 numbers',{size: 'sm'});
				return false;
		   }
		   else if($scope.pin.re_password != $scope.pin.password){
				dialogs.notify('','PIN does not match Re-enter PIN',{size: 'sm'});
				return false;
		   }
		   
		   $scope.signupLoader = true;
	        ssHelper.post('/sign/setpin', {
	        	sign_id : data.sign_id,
	        	pin  : $scope.pin
	        }).then(function (response) {
	        	$scope.signupLoader = false;
	            if (response.data.OK == '1') {
	            	var dlg = dialogs.notify('','This Sign pin has been set.',{size: 'sm'});
	            	// when old_pin is undefined, set sign pin signinfo
	            	
		       		   dlg.result.then(function(btn){
		       		 	$modalInstance.dismiss('canceled');
			       			 if($scope.old_pin == undefined){
								 data.pubishSign();
								 $state.go($state.current, {}, {reload: true});
			       			 }
						});
	       		   	
	            }
	        });
		 
	    };
})

//======================================================= DUPLICATE VARIANT =====================================================================

angular.module('zwenGlobal').controller('duplicateVariantCtrl',function($scope, $modalInstance, data, ssHelper, dialogs){
	
	$scope.dupItemVariant = {
			name: '',
			relationship : true,
	}
	$scope.mtype = data.mtype;
	 $scope.cancel = function(){
		 $modalInstance.dismiss('canceled');
	 };
	 
	   $scope.saveForm = function(){
		   $scope.signupLoader = true;
	        ssHelper.post('/content/duplicate'+$scope.mtype+'variant', {
	        	acc_id : data.acc_id,
	        	ovid   : data.ovid,
	        	dupItemVariant: $scope.dupItemVariant
	        }).then(function (response) {
	        	$scope.signupLoader = false;
	            if (response.data.OK == '1') {
	            	 $modalInstance.close('load'+$scope.mtype+'Variants');
	            }else{
	            	if(response.data.mes != ''){
	  	                var dlg = dialogs.notify('', response.data.mes ,{size: 'md'});
	  	        	  dlg.result.then(function (btn) {
	  	        		$modalInstance.close();
        			  });
	            	}
	            }
	        });
		 
	    };
})


//==========================================Create Item==============================================
angular.module('zwenGlobal').controller('createItemCtrl',function($scope, $modalInstance, data, ssHelper, dialogs,$stateParams,$state){
    $scope.items = [{
            name: ".document",
            value: "1"
        },{
            name: ".image",
            value: "2"
        },{
            name: ".video",
            value: "6"
        },{
            name: ".bookmark",
            value: "7"
        },{
            name: ".file",
            value: "36"
        }
    ];
    $scope.wovid = data.wovid;
    $scope.wgvid = data.wgvid;
    $scope.ovid = data.ovid;
    $scope.selected = {value:''};
    $scope.stateGo =''

    $scope.saveForm = function(){
        if($scope.selected.value==1){
            $scope.stateGo = 'main.upload-iv-doc';
        }
        if($scope.selected.value==2){
            $scope.stateGo = 'main.upload-iv-image';
        }
        if($scope.selected.value==6){
            //$scope.stateGo = 'main.upload-iv-video';
            $scope.stateGo = 'main.chunk-upload-file';
        }
        if($scope.selected.value==7){
            $scope.stateGo = 'main.bookmarks-form';
        }
        if($scope.selected.value==36){
            $scope.stateGo = 'main.upload-images'
        }

        $modalInstance.close();

        // the create item action is just displayed when a group is selected, then set ovid for wgvid
        console.log($scope.stateGo)
        console.log($scope.ovid)
        $state.go($scope.stateGo,{acc_id:$stateParams.acc_id,mtype:'Item',oid:$scope.selected.value, ovid:'', wovid:$scope.wovid,wgvid:$scope.ovid});

    }

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    }
})