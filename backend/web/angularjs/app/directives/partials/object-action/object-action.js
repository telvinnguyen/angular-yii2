app.directive('objectAction', function ($compile, $timeout) {
    return {
        restrict: 'E',
        //bindToController: true,
        //replace: true,
        scope: {
            caller: '=',
            remote: '=?',
            action: '=',
            templateFileName: '@', // this template is the file name of one of the template file  in app/partials/action-templates/
            mainLoader: '='
        },
        controller: function ($scope, $http) {
        	
            /** the priority of template is
             *  1 - template-file-name attribute on the directive object-action
             *  2 - template property of the action object
             *  3 - if the template is not found, the default value is 'default.html'
             *  All of these templates are located on app/partials/action-templates/
             */
            $scope.templateData = '';
            var fileName = '';
            if (angular.isDefined($scope.templateFileName)) {
                fileName = $scope.templateFileName
            } else {
                if (angular.isDefined($scope.action)) {
                    if (angular.isDefined($scope.action.template)) {
                        fileName = $scope.action.template
                    }
                }
            }

            if (fileName == '') {
                fileName = 'default.html'
            }

            $http.get(_yii_app.directivePath + '/partials/object-action/template/' + fileName).then(function (response) {
                $scope.templateData = response.data;

                if($scope.action.ngClick != ''){ //dynamic ng-click function
                    $scope.templateData = s.replaceAll($scope.templateData, "{{action.ngClick}}", $scope.action.ngClick);
                }
            });

            
        },
        link: function (scope, element, attrs) {
            scope.$watch('templateData', function (tpl) {
                if (angular.isDefined(tpl)) {
                    var b = $compile(tpl)(scope);
                    element.append(b);
                }
            });
        }
    };
});