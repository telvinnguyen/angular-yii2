(function( $ ) {

    $.fn.CustomScroller = function(options) {
        var a,b=[];

        var settings = $.extend( {
            'scroll_container' : this.children('.t-c'),
            'itemsSelector': 'article'
        }, options);


        var config = {
            itemMargins: 5 // Distance between the thumbnails
        };

        return this.each( function() {
            var $thumbnailScroller = $(this),
                $container = $(settings.scroll_container),
                $item = $container.find(settings.itemsSelector),
                item_length = $item.length,
                item_width = $item.outerWidth(),
                item_margin = config.itemMargins,
                total_width = (item_width + item_margin) * item_length,
                $window = $(window);

            $thumbnailScroller.css('overflow', 'hidden');
            $container.css('width', total_width);

            $window.on("resize", function () {
                var o_l = $thumbnailScroller.offset().left,
                    t_w = $thumbnailScroller.width(),
                    c_w = total_width;
                $thumbnailScroller.on("mousemove", function (e) {
                    if ($(this).width() < $container.width()) {
                        $container.css('left', -((e.pageX - o_l) * (c_w - t_w) / t_w));
                    }
                });
            }).trigger("resize");
        });

    };
})( jQuery );