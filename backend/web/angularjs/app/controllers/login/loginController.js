angular.module('zwenGlobal').controller('loginController', function ($rootScope, $scope, store, $http, $state, $stateParams, $q, $timeout, dialogs, ssHelper) {
    $rootScope.layoutPath = _yii_app.layoutPath;
    $rootScope.baseUrl = _yii_app.baseUrl;

    ssHelper.post('/home/Authenticate', {}).then(function (response) {
        //$scope.$apply(function(){})
        if (response.data.OK == 1) { // if member _id on the server not found, the OK would be 0
            var clientAuth = store.get('ss-identity-jwt')
            var serverAuth = response.data.serverCookie

            if(store.get('ss-identity-jwt') && (clientAuth == serverAuth)){
                e.preventDefault();
                $state.go('home', {}, {reload: true});
            }
            else{
                console.log('the client auth token string is not match with one of server')
            }
        }else{
            console.log('the valid cookies is not found')
        }
    });



    $scope.login_form = {
        username: '',
        password: '',
        errors: '',
        rememberMe: true
    }

    $scope.beLoginClick = function () {

        ssHelper.post('/home/login', $scope.login_form).then(function (response) {
            //$scope.$apply(function(){})
            if (response.data.OK == 1) { // if member _id on the server not found, the OK would be 0
                //store.set('ss-identity-jwt', serverAuth)
                store.set('ss-identity-jwt', response.data.id_token);
                store.set('csrf-token', response.data.csrfToken);
                
                store.set('ss-member-id', response.data.member_id);
                store.set('remember-me', $scope.login_form.rememberMe);

                $timeout(function(){
                    $state.go('home');
                })
            }else{
                $scope.login_form.errors = response.data.errors;
            }
        });
    }

    $scope.resetError = function () {
        if($scope.login_form.errors != ''){
            $scope.login_form.errors = '';
        }
    }

});