app.factory('errorService', function($http, $state, localStorageService){
    return {
        set message(value){ localStorageService.set('message', value)}, get message(){ return localStorageService.get('message') },

        reset: function(){
            var listKeys = [
                'message'
            ];

            angular.forEach(listKeys, function(value, key){
                localStorageService.remove(key)
            });
        },
        throw: function(msg){
            this.message = msg; $state.go('error');
        }
    }
})