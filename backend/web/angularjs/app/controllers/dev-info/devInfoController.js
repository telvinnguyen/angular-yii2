angular.module('zwenGlobal').controller('devInfoController', function ($scope, $state, $stateParams, $timeout, ssHelper) {
    //$rootScope.layoutPath = _yii_app.layoutPath;

    $scope.acc_id = $stateParams.acc_id;

    $scope.member = {
        id: '', upload_folder: ''
    }

    $scope.acc = {
        id: '', upload_folder: ''
    }

    $scope.ownerAccounts = [];
    $scope.step = 'kiosk-sign';


    $scope.skin = {
        id: '', name: '', published :false
    }

    $scope.kiosk = {
        id: '', name: '', skin: $scope.skin, published :false
    }

    $scope.sign = {
        id: '', name: '', kiosk: $scope.kiosk
    }

    $scope.item = {
        id: '', name: '', published :false
    }

    $scope.getData = function(){
        ssHelper.post('/dev/getdata', {
                    member_id: $scope.member_id,
                    acc_id: $scope.acc_id, kiosk_id: $scope.kiosk.id, sign_id: $scope.sign.id

                }).then(function (response) {
                    //$scope.$apply(function(){})
                    if (response.data.OK == 1) {
                        $scope.member = response.data.member;
                        $scope.acc = response.data.acc;
                        $scope.accounts = response.data.accounts;
                        $scope.kiosk = response.data.kiosk;
                        $scope.sign = response.data.sign;

                        $scope.html = ssHelper.printJson(JSON.stringify(response.data, undefined, 4));

                    } else {
                        //var errors = response.data.errors
                    }
                });
    }

    $scope.getData();

    $scope.refreshLog = function () {
        ssHelper.post('/dev/getLog', {}).then(function (response) {
                    //$scope.$apply(function(){})
                    if (response.data.OK == 1) {
                        $scope.html2 = response.data.content;
                    } else {
                        //var errors = response.data.errors
                    }
                });
    };

    $scope.cleanLog = function () {
        ssHelper.post('/dev/cleanLog', {}).then(function (response) {
                    //$scope.$apply(function(){})
                    if (response.data.OK == 1) {
                        $scope.html2 = '';
                    } else {
                        //var errors = response.data.errors
                    }
                });
    };

});