angular.module('zwenGlobal').controller('uploadImagesController', function ($rootScope, $scope, $stateParams, $modal, modalUtils, $injector, $http, store, $state, $cookieStore, ssHelper, dialogs, FileUploader) {
    $rootScope.baseUrl = _yii_app.baseUrl;
    $rootScope.layoutPath = _yii_app.layoutPath;
    $scope.acc_id = $stateParams.acc_id;
    $scope.oid = $stateParams.oid;

    $scope.wtype = $stateParams.wtype;
    $scope.wovid = $stateParams.wovid;

    $scope.wgvid = $stateParams.wgvid;
    $scope.mtype = $stateParams.mtype;
    
    $scope.formInfo = {
        name: ''
    }
    $scope.files = {
    		zipFile: [],
    		totalItem: 0,
    };
    $scope.isOnlyimage = ($scope.oid == 2) ? true : false;
    
    $scope.save = function () {
        if ($scope.files.zipFile != '' && $scope.formInfo.name != '') {
            $scope.formLoading = true;
            ssHelper.post('/file/handlemultipleuploadingthenzip', {
                wtype: $scope.wtype, wovid: $scope.wovid, wgvid: $scope.wgvid,
                total_item: $scope.file.totalItem,
                name: $scope.formInfo.name,
                oid: $scope.oid, mtype: $scope.mtype,
                files: $scope.files.zipFile,
                acc_id: $scope.acc_id
            }).then(function (response) {
                $scope.formLoading = false;
                if (response.data.OK == 1) {
            	    ssHelper.goBackToPreviousPage($scope).then(function(scope){
            	    	var page = ($scope.ovid == '') ? 0 : scope.remote.ivPageIndex;
                        scope.remote.loadItemVariants(page).then(function(){
                            scope.remote.toggleObjectVariant({mtype: 'Item', type: 'iv', ovid: response.data.ovid});
                        });
                    })
                }else if(response.data.OK == 0 && response.data.mes != '') {
                	dialogs.notify('', response.data.mes, {size: 'md'});
                }
              

            });
        } else if ($scope.files.zipFile != '' && $scope.formInfo.name == '') {
            dialogs.notify('Error', 'Name is required.', {size: 'sm'});
        } else if ($scope.files.zipFile == '' && $scope.formInfo.name != '') {
            dialogs.notify('Error', 'Please select file(s) to upload.', {size: 'sm'});
        } else {
            dialogs.notify('Error', 'Please select file(s) to upload.<br>Name is required.', {size: 'sm'});
        }
    };

    $scope.cancel = function () {
        var transitionPromise = ssHelper.backToPreviousScreen();
    };
    $scope.done = function () {
        ssHelper.doneSaveIvAndReturn('', $scope.wovid, $scope.wgvid, '', $scope.remote);
    }
});
angular.module('zwenGlobal').filter('split', function ($sce) {
   return function(input, splitChar, splitIndex) {
       return input.split(splitChar)[splitIndex];
   }
});