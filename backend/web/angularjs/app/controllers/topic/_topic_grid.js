angular.module('zwenGlobal').directive('topicGrid', function ($timeout, dialogs, ssHelper) {
    return {
        restrict: 'E',
        scope: {
            'searchModel': '='
        },
        transclude: true,
        template: '<jqx-grid id="{{clientId}}" jqx-settings="gridSettings"></jqx-grid>',
        bindToController: true,
        controller: function ($scope, $attrs) {
            $scope.layoutPath = _yii_app.layoutPath
            $scope.clientId = $attrs.gridId

            var theme = 'bootstrap';

            var editedRows = new Array();

           /*
           $scope.getListFromServer = function(){
                ssHelper.get('/topic/index').then(function (response) {
                    $scope.bindGridData(response.data)
                });
            }
            */
            $scope.bindGridData = function(){


                var source =
                {
                    datatype: "json",
                    datafields: [
                        { name: 'topic_id', type: 'string' },
                        { name: 'name', type: 'string' },
                        { name: 'active', type: 'string' }
                    ],
                    //localdata: data,


                    url: _yii_app.baseUrl + '/topic/getpagedlist', //getPagedList
                    root: 'pagedList',
                    // data: {
                    //
                    // },
                    beforeprocessing: function (data) {
                        console.log(data)
                        source.totalrecords = data.totalRecords;
                    },
                    sort: function () {
                        // update the grid and send a request to the server.
                        $("#" + $scope.clientId).jqxGrid('updatebounddata')
                    },

                    filter: function () {
                        // update the grid and send a request to the server.
                        $timeout(function(){
                            $("#" + $scope.clientId).jqxGrid('updatebounddata')
                        })
                    },

                    updaterow: function (rowid, rowdata, commit) {
                        // that function is called after each edit.

                        var rowindex = $("#" + $scope.clientId).jqxGrid('getrowboundindexbyid', rowid);
                        editedRows.push({ index: rowindex, data: rowdata });

                        // synchronize with the server - send update command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failder.
                        commit(true);
                    },
                };

                var addfilter = function () {
                    var filtergroup = new $.jqx.filter();

                    var filter_or_operator = 1;
                    var filtervalue = '';
                    var filtercondition = 'contains';
                    var filter1 = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);

                    filtervalue = '';
                    filtercondition = 'contains';
                    var filter2 = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);

                    filtergroup.addfilter(filter_or_operator, filter1);
                    filtergroup.addfilter(filter_or_operator, filter2);
                    // add the filters.
                    //$("#" + $scope.clientId).jqxGrid('addfilter', 'name', filtergroup);
                    // apply the filters.
                    $("#" + $scope.clientId).jqxGrid('applyfilters');
                }

                var tooltiprenderer = function (element) {
                    $(element).jqxTooltip({position: 'mouse', content: $(element).text() });
                }

                var dataAdapter = new $.jqx.dataAdapter(source, {
                    // formatData: function (data) {
                    //     console.log('==== Format Data ====')
                    //     data.name_startsWith = $scope.searchModel //$("#searchField").val();
                    //     return data;
                    // }
                });

                var cellclass = function (row, datafield, value, rowdata) {
                    for (var i = 0; i < editedRows.length; i++) {
                        if (editedRows[i].index == row) {
                            return "editedRow";
                        }
                    }
                }

                var cellvaluechanging = function (rowIndex, datafield, columntype, oldvalue, newvalue) {
                    console.log("cell changing value", rowIndex, datafield, columntype, newvalue)
                    return newvalue
                }

                $scope.gridSettings = {
                    width: '100%',
                    source: dataAdapter,
                    theme: theme,

                    //selectionmode: 'singlecell',
                    editable: true,
                    sortable: true,
                    filterable: true,
                    columnsresize: false,
                    enabletooltips: true,
                    pageable: true,

                    virtualmode: true,
                    rendergridrows: function () {
                        return dataAdapter.records;
                    },

                    pagesize: 10,
                    pagesizeoptions: ['10', '50', '100'],

                    columns: [
                        { text: 'Name', columntype: 'textbox', datafield: 'name', width: '85%', cellclassname: cellclass, cellvaluechanging: cellvaluechanging, rendered: tooltiprenderer },
                        { text: 'Active', columntype: 'checkbox', datafield: 'active', width: '5%', cellclassname: cellclass, cellvaluechanging: cellvaluechanging, rendered: tooltiprenderer, cellsformat:'n', cellsalign: 'right' },
                        { text: 'Action', datafield: 'Delete', width: '10%', filterable: false, sortable: false, cellbeginedit: function(){return false}, cellsrenderer: function(){
                                return '<div class="grid-button-delete" style="width: 100%"><i class="fa fa-remove" ></i> <span>Delete</span></div>'
                            }
                        },
                    ],

                    autoshowfiltericon: false,
                    ready: function () {
                        addfilter();
                        var localizationObject = {
                            filterstringcomparisonoperators: ['contains', 'does not contain'],
                            // filter numeric comparison operators.
                            filternumericcomparisonoperators: ['less than', 'greater than'],
                            // filter date comparison operators.
                            filterdatecomparisonoperators: ['less than', 'greater than'],
                            // filter bool comparison operators.
                            filterbooleancomparisonoperators: ['equal', 'not equal']
                        }

                        $("#" + $scope.clientId).jqxGrid('localizestrings', localizationObject);
                    },
                    updatefilterconditions: function (type, defaultconditions) {
                        var stringcomparisonoperators = ['CONTAINS', 'DOES_NOT_CONTAIN'];
                        var numericcomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
                        var datecomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
                        var booleancomparisonoperators = ['EQUAL', 'NOT_EQUAL'];
                        switch (type) {
                            case 'stringfilter':
                                return stringcomparisonoperators;
                            case 'numericfilter':
                                return numericcomparisonoperators;
                            case 'datefilter':
                                return datecomparisonoperators;
                            case 'booleanfilter':
                                return booleancomparisonoperators;
                        }
                    },
                    updatefilterpanel: function (filtertypedropdown1, filtertypedropdown2, filteroperatordropdown, filterinputfield1, filterinputfield2, filterbutton, clearbutton,
                                                 columnfilter, filtertype, filterconditions) {
                        var index1 = 0;
                        var index2 = 0;
                        if (columnfilter != null) {
                            var filter1 = columnfilter.getfilterat(0);
                            var filter2 = columnfilter.getfilterat(1);
                            if (filter1) {
                                index1 = filterconditions.indexOf(filter1.comparisonoperator);
                                var value1 = filter1.filtervalue;
                                filterinputfield1.val(value1);
                            }

                            if (filter2) {
                                index2 = filterconditions.indexOf(filter2.comparisonoperator);
                                var value2 = filter2.filtervalue;
                                filterinputfield2.val(value2);
                            }
                        }

                        filtertypedropdown1.jqxDropDownList({ autoDropDownHeight: true, selectedIndex: index1 });
                        filtertypedropdown2.jqxDropDownList({ autoDropDownHeight: true, selectedIndex: index2 });
                    }
                }

                $scope.createWidget = true;



                $("#" + $scope.clientId).on('cellendedit', function (event)
                {
                    // event arguments.
                    var args = event.args;
                    // column data field.
                    var dataField = event.args.datafield;
                    // row's bound index.
                    var rowBoundIndex = event.args.rowindex;
                    // cell value
                    var value = args.value;
                    // cell old value.
                    var oldvalue = args.oldvalue;
                    // row's data.
                    var rowData = args.row;

                    console.log(args, dataField, rowBoundIndex, value, rowData)
                });
            }


            $scope.bindGridData()

        },
        link: function (scope, element, attrs) {

            $timeout(function(){
                $("#" + scope.clientId).on('cellclick', function (event) {
                    var ET = event.args.datafield
                    if (ET == "Delete") {

                        var args = event.args;
                        var rowBoundIndex = event.args.rowindex;
                        var rowData = args.row;

                        console.log(rowBoundIndex, rowData.bounddata.name)

                        var entryID = rowData.bounddata.topic_id;
                        var entryName = rowData.bounddata.name;

                        var dlg = dialogs.confirm('', 'Delete topic \'' + entryName + '\'. Are you sure?', {size: 'md'});
                        //var deferred = $q.defer();
                        dlg.result.then(function (btn) {
                            // Yes
                            var deleteAPI = '/topic/deleteTopic'
                            ssHelper.post(deleteAPI, {
                                topic_id: entryID
                            }).then(function (response) {
                                if (response.data.OK == 1) {

                                    console.log("Delete OK")

                                    $timeout(function(){
                                        // scope.gridSettings.jqxGrid('updatebounddata')
                                        //scope.getListFromServer()
                                    })

                                }
                            });
                        }, function (btn) {
                        });
                    }
                })
            })
            /*scope.$watch('searchModel', function (search) {
                console.log(search)
                $("#" + scope.clientId).jqxGrid('updatebounddata');
            });*/
        }
    };
});