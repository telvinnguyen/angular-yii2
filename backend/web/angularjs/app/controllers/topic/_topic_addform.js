angular.module('zwenGlobal').directive('topicAddform', function ($timeout, dialogs, ssHelper) {
    return {
        restrict: 'E',
        scope: {
            'searchModel': '='
        },
        transclude: true,
        templateUrl: _yii_app.controllerPath + '/topic/_topic_addform.html',
        bindToController: true,
        controller: function ($scope, $attrs) {
            $scope.layoutPath = _yii_app.layoutPath

            $scope.form = {
                name: '', active: ''
            }

            $scope.addTopic = function () {
                ssHelper.post('/topic/form', {
                    form: $scope.form
                        }).then(function (response) {
                            if (response.data.OK == 1) {
                                //$scope.x = _.extend($scope.x, response.data)
                            } else {
                                dialogs.notify(response.data.errors)
                            }
                        });
            };

        },
        link: function (scope, element, attrs) {

        }
    };
});