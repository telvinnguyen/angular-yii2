angular.module('zwenGlobal').controller('forgotPasswordController', function ($rootScope, $scope, $modal, $http, $state, $stateParams, $q, $timeout, dialogs, ssHelper) {
    $rootScope.layoutPath = _yii_app.layoutPath;

    $scope.acc_id = $stateParams.acc_id;
    $rootScope.baseUrl = _yii_app.baseUrl;
    $rootScope.layoutPath = _yii_app.layoutPath;
    
    $scope.loading = false;

    $scope.email = '';
    $scope.msg = '';
    $scope.done = false;

    $scope.beForgotPassClick = function () {
        $scope.loading = true;
        ssHelper.post('/home/resetpassword', {
            email: $scope.email

        }).then(function (response) {
            $scope.loading = false;
            $scope.msg = response.data.msg;

            $modal.open({
                templateUrl: _yii_app.absTemplatePath + '/modal-msg.html',
                controller: 'resetPasswordModalCtrl',
                scope: $scope,
                backdrop: 'static'
            });

            if (response.data.OK == 1) {
                $scope.done = true;

            }
        });
    }
});



angular.module('zwenGlobal').controller('resetPasswordModalCtrl', function ($scope, $state, $modalInstance) {

    $scope.ok = function () {
        if($scope.done){
            $modalInstance.dismiss('cancel');
            //$location.path('./login').replace();
            $state.go('login');
        }else
            $modalInstance.dismiss('cancel');

    };
});