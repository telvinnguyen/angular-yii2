app.factory('modalUtils', [
    '$modalStack',
    function ($modalStack) {
        return {
            modalsExist: function () { // this seems doesn't work
                return !!$modalStack.getTop();
            },
            closeAllModals: function () {
                $modalStack.dismissAll();
            }
        };
    }
]);