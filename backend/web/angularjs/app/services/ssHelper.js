var ssFactories = angular.module('ssFactories', ['ngRoute', 'ui.router', 'angular-storage', 'ct.ui.router.extras']);
ssFactories.factory('ssHelper', function ($http, $window, $stateParams, $rootScope, $location, $q, store, $log, $previousState, $futureState, $state, dialogs) {
    //var _ = $window._; //bind for underscore

    var d = $location.protocol() + "://" + $location.host() + ":" + $location.port();
    //var b = d + '/' + 'signsmart_yii2_angularjs/';

    $rootScope.baseUrl = _yii_app.baseUrl;

    var obj = {rootUrl: d};

    obj.get = function (q) {
        return $http.get($rootScope.baseUrl + q.toLowerCase()).then(function (results) {
            //return results.data; (.data is a certain attribute from the array that is returned from response, nothing special)
            return results;
        });
    };

    obj.post = function (q, object) {
        var csrfToken = store.get('csrf-token')
        if(csrfToken != undefined && csrfToken != ""){
            object.csrfToken = csrfToken
        }
           
        var deferred = $q.defer();
        return $http({
            url: obj.buildApiUrl(q),
            method: "POST",
            data: $.param(object),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function (results) {
            return results;
        })
    };

    obj.delete = function (q) {
        var deferred = $q.defer();
        return $http.delete(obj.buildApiUrl(q)).then(function (results) {
            return results;
        })
    }

    obj.buildApiUrl = function (q) {
        return $rootScope.baseUrl + q.toLowerCase();
    }

    obj.delete = function (q) {
        return $http.delete($rootScope.baseUrl + q.toLowerCase()).then(function (results) {
            return results;
        });
    };

    obj.getQueryStrings = function (url) {
        url = typeof url !== 'undefined' ? url : window.location.href;
        var vars = [], hash;
        var hashes = url.slice(url.indexOf('?') + 1).split('&');

        var anchor = document.createElement('a');
        anchor.href = url;

        var queyrystring_id = anchor.pathname.slice(anchor.pathname.lastIndexOf('/') + 1);

        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        if (_.isNumber(queyrystring_id)) {
            vars.push('id');
            vars['id'] = queyrystring_id;
        }


        return vars;
    }

    obj.createUrl = function (router, modifyQueryStrings) {
        var currentQueryStrings = obj.getQueryStrings(document.URL);

        if (router == '') {
            router = decodeURIComponent(currentQueryStrings['prev_r']);
        }


        var _cQueryStrings = {};
        $.each(currentQueryStrings, function (index, key) {
            _cQueryStrings[key] = currentQueryStrings[key];
        });

        if (modifyQueryStrings != null) {
            $.each(modifyQueryStrings, function (key, value) {
                _cQueryStrings[key] = value;

            });
        }

        var Qstrings = "";
        var totalKeyInObj = Object.keys(_cQueryStrings).length;
        var index = 1;
        $.each(_cQueryStrings, function (key, value) {
            //if(key!='prev_r')
            Qstrings += key + "=" + value;

            if (index < totalKeyInObj)
                Qstrings += "&";
            index++;
        });

        return ($rootScope.baseUrl + router + '?' + Qstrings);
    }

    /**
     *
     * @param json object
     */
    obj.makeQueryString = function (params) {
        return '?' + $.param(params);
    }

    obj.Stringformat = function () {
        var s = arguments[0];
        for (var i = 0; i < arguments.length - 1; i++) {
            var reg = new RegExp("\\{" + i + "\\}", "gm");
            s = s.replace(reg, arguments[i + 1]);
        }

        return s;
    }

    obj.debugLog = function () {
        if (_yii_app.debugMode) {

            var args = Array.prototype.slice.call(arguments);
            //console.log.apply(console, args);

            $log.info.apply($log, args);
        }
    };

    /**
     * switch the type name of ObjectToolbar and ObjectVariant
     * @param $type
     * @returns {string}
     */
    obj.castTypeToolbarVariant = function ($type) {
        switch ($type) {
            case 'Collection':
                return 'cv';
                break;
            case 'Group':
                return 'gv';
                break;
            case 'Item':
                return 'iv';
                break;
            case 'cv':
                return 'Collection';
                break;
            case 'gv':
                return 'Group';
                break;
            case 'iv':
                return 'Item';
                break;
        }
    }

    obj.errorLog = function () {
        if (_yii_app.debugMode) {

            var args = Array.prototype.slice.call(arguments);
            //console.log.apply(console, args);

            $log.error.apply($log, args);
        }
    };

    obj.toggleItemInArray = function (array, value) {
        var index = array.indexOf(value);

        if (index === -1) {
            array.push(value);
        } else {
            array.splice(index, 1);
        }
        return array;
    }

    obj.isNumber = function (value) {
        var t = parseInt(value)
        if (isNaN(t)) return false;
        return _.isNumber(t);
    }

    obj.dataURItoBlob = function (dataURI) {
        // convert base64/URLEncoded data component to raw binary data held in a string
        var byteString;
        if (dataURI.split(',')[0].indexOf('base64') >= 0)
            byteString = atob(dataURI.split(',')[1]);
        else
            byteString = unescape(dataURI.split(',')[1]);

        // separate out the mime component
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

        // write the bytes of the string to a typed array
        var ia = new Uint8Array(byteString.length);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        var blob = new Blob([ia], {type: mimeString});
        blob.lastModifiedDate = new Date();
        blob.name = 'abc.jpeg';
        return blob;
    }

    obj.removeBackSlash = function (str) {
        return str.replace(/\\/g, "");
    }
    obj.isValidEmail = function (email) {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return re.test(email);
    }
    obj.isValidPhone = function (phone) {
    	var re = /^[0-9]{3}-[0-9]{3}-[0-9]{4}$/;
    	return re.test(phone);
    }
    /**
     *
     * @param queryStrings object contains the key value pair such as acc_id: '1'
     * @returns {promise}
     */
    obj.backToPreviousScreen = function (_isReload) {
        //get parameter
        var isReloadPage = false;
        if(angular.isDefined(_isReload)){
            isReloadPage = _isReload;
        }
        var queryStrings = arguments.length > 0 ? arguments[0] : undefined;

        var previous = $previousState.get();
        var stateName = 'main.content';
        var params = {acc_id: ''};

        if (angular.isDefined(previous) && previous != null) {
            stateName = previous.state.name;
            params = previous.params;

            if (angular.isDefined(queryStrings)) {
                params = _.extendOwn(params, queryStrings);
            }
        }

        if(isReloadPage){
            return $state.go(stateName, params, {reload: true});
        }
        else{
            return $state.go(stateName, params);
        }
    }

    obj.extractYoutubeVideoId = function (text) {
        var re = /https?:\/\/(?:[0-9A-Z-]+\.)?(?:youtu\.be\/|youtube(?:-nocookie)?\.com\S*[^\w\s-])([\w-]{11})(?=[^\w-]|$)(?![?=&+%\w.-]*(?:['"][^<>]*>|<\/a>))[?=&+%\w.-]*/ig;
        return text.replace(re, '$1');
    }


    obj.roundNumber = function (value, precision, mode) {
        mode = typeof mode !== 'undefined' ? mode : 'PHP_ROUND_HALF_DOWN';

        var m, f, isHalf, sgn; // helper variables
        precision |= 0; // making sure precision is integer
        m = Math.pow(10, precision);
        value *= m;
        sgn = (value > 0) | -(value < 0); // sign of the number
        isHalf = value % 1 === 0.5 * sgn;
        f = Math.floor(value);

        if (isHalf) {
            switch (mode) {
                case 'PHP_ROUND_HALF_DOWN':
                    value = f + (sgn < 0); // rounds .5 toward zero
                    break;
                case 'PHP_ROUND_HALF_EVEN':
                    value = f + (f % 2 * sgn); // rouds .5 towards the next even integer
                    break;
                case 'PHP_ROUND_HALF_ODD':
                    value = f + !(f % 2); // rounds .5 towards the next odd integer
                    break;
                default:
                    value = f + (sgn > 0); // rounds .5 away from zero
            }
        }

        return (isHalf ? value : Math.round(value)) / m;
    }

    obj.shortenStringWoCutWord = function (value) {
        return value.replace(/^([\s\S]{10}\S*)[\s\S]*/, "$1");
    }

    /**
     * if the number of digits of a number is less than length, add zero
     * @param number: int
     * @param length: int
     * @returns {string}
     */
    obj.formatDigits = function (formatNumber, length) {
        return ("0" + formatNumber ).slice(-length)
    }

    obj.safeRun = function (functionSignature, _notFoundRedirect) {
        var _notFoundRedirect = angular.isDefined(_notFoundRedirect) ? _notFoundRedirect : true;
        if (angular.isFunction(functionSignature)) return functionSignature();
        else {
            if (_notFoundRedirect) {
                $state.go('main.content');
            }

        }
    }

    /**
     * convert undefined or null value to string
     * @param value
     * @returns {*}
     */
    obj.safeValue = function (value) {
        if (angular.isDefined(value) && value != null && value != '')
            return value;
        return '';
    }

    obj.parseUrl = function (url) {
        var parser = document.createElement('a');
        parser.href = url; //"http://example.com:3000/pathname/?search=test#hash";

        /*
         parser.protocol; // => "http:"
         parser.host;     // => "example.com:3000"
         parser.hostname; // => "example.com"
         parser.port;     // => "3000"
         parser.pathname; // => "/pathname/"
         parser.hash;     // => "#hash"
         parser.search;   // => "?search=test"
         */
        return parser;
    }

    obj.getAllCss = function (a) {
        var sheets = document.styleSheets, o = {};
        for (var i in sheets) {
            var rules = sheets[i].rules || sheets[i].cssRules;
            for (var r in rules) {
                if (a.is(rules[r].selectorText)) {
                    o = $.extend(o, css2json(rules[r].style), css2json(a.attr('style')));
                }
            }
        }
        return o;
    }

    function css2json(css) {
        var s = {};
        if (!css) return s;
        if (css instanceof CSSStyleDeclaration) {
            for (var i in css) {
                if ((css[i]).toLowerCase) {
                    s[(css[i]).toLowerCase()] = (css[css[i]]);
                }
            }
        } else if (typeof css == "string") {
            css = css.split("; ");
            for (var i in css) {
                var l = css[i].split(": ");
                s[l[0].toLowerCase()] = (l[1]);
            }
        }
        return s;
    }

    obj.findValueInObject = function (obj, key, val) {
        var objects = [];
        for (var i in obj) {
            if (!obj.hasOwnProperty(i)) continue;
            if (typeof obj[i] == 'object') {
                objects = objects.concat(this.findValueInObject(obj[i], key, val));
            } else if (i == key && obj[key] == val) {
                objects.push(obj);
            }
        }
        return objects;
    }

    obj.removeUndefinedProperty = function (options) {
        for (var i in options) {
            if (options[i] === null || options[i] === undefined) {
                // test[i] === undefined is probably not very useful here
                delete options[i];
            }
        }
        return options;
    }

    obj.makeId = function (length) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < length; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }

    obj.isEmptyArray = function (array) {
        if (array.length == 0) return true;
        if (array.length > 0 && ( isNaN(array[0]) || angular.isDefined(array[0]) == false || array[0] == null || array[0] == '' )) return true;
    }

    obj.printJson = function (json) {
        if (typeof json != 'string') {
            json = JSON.stringify(json, undefined, 2);
        }
        json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
            var cls = 'number';
            if (/^"/.test(match)) {
                if (/:$/.test(match)) {
                    cls = 'key';
                } else {
                    cls = 'string';
                }
            } else if (/true|false/.test(match)) {
                cls = 'boolean';
            } else if (/null/.test(match)) {
                cls = 'null';
            }
            return '<span class="' + cls + '">' + match + '</span>';
        });
    }

    obj.isEmptyObject = function (obj) {
        return JSON.stringify(obj) === '{}';
    }

    obj.isEmptyOrWhiteSpace = function(str){
        return !(/\S/.test(str))  ;
    }

    obj.isLoadedInIframe = function () {
        try {
            return $window.self !== $window.top;
        }
        catch (e) {
            return true;
        }
    }

    obj.iFrameLoaded = function (id, src) {
        var deferred = $.Deferred(),
            iframe = $("").attr({
                "id": id,
                "src": src
            });

        iframe.load(deferred.resolve);

        deferred.done(function () {
            console.log("iframe loaded: " + id);
        });

        return deferred.promise();
    }

    obj.convertToShow = function (bytes, precision) {
        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (bytes <= 0) return '0.00 B';
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        if (sizes[i] == undefined) return '0.00 B';
        return (bytes / Math.pow(1024, i)).toFixed(2) + ' ' + sizes[i];
    }

    obj.checkExtAndStorage = function (validFileExtensions, size, remainStorage, acc_id) {
        var enoughStorage = remainStorage >= size;
        if (!validFileExtensions)
            dialogs.notify('Upload Error', 'File type is invalid.', {size: 'md'}); //dialogs.notify('Upload Error', 'File Invalid. Supported file types are: *.jpg, *.png, *.jpeg, *.bmp, *.gif', {size: 'md'})
        else if (!enoughStorage) {
            if (acc_id == '' || !angular.isDefined(acc_id))
                dialogs.notify('Upload Error', 'Your storage is not enough to upload this file.', {size: 'md'})
            else
                dialogs.notify('Upload Error', 'This account has no available space for uploads.<br>Please select the “STORAGE” Action when in your personal account profile and allocate storage to this sub-account.', {size: 'md'})
        }
    }

    obj.jsonOrder = function (obj) {
        console.log(obj)
        if (!obj) {
            return [];
        }
        return Object.keys(obj);
    }

    obj.doneSaveIvAndReturn = function(ovid, wovid, wgvid, return_iv_id, remote, loadActions){

        var transitionPromise;
        var previousStateName = 'main.content';
        var previousState = $previousState.get()

        if(wovid != '' && return_iv_id != ''){ // create item mode and selected this tiem
            transitionPromise = obj.backToPreviousScreen();
            transitionPromise.then(function (response) {
                if(angular.isDefined(previousState)) previousStateName = previousState.state.name;

                var loadItemVariants = remote.recall(previousStateName, 'loadItemVariants');
                var toggleObjectVariant = remote.recall(previousStateName, 'toggleObjectVariant');
                obj.safeRun(loadItemVariants).then(function(response){
                    toggleObjectVariant({ovid: return_iv_id, type: 'iv', mtype: 'Item'});
                })
            });
        }
        else if (ovid == '' && wgvid != '') { // create mode and workspace-group is selected
            transitionPromise = obj.backToPreviousScreen();
            transitionPromise.then(function (response) {
                if(angular.isDefined(previousState)) previousStateName = previousState.state.name;

                var toggleObjectVariant = remote.recall(previousStateName, 'toggleObjectVariant');
                toggleObjectVariant({ovid: wgvid, type: 'gv'});
            });
        }
        else if (ovid != '' && wgvid != '') { // update item mode in workspace and item is selected
            transitionPromise = obj.backToPreviousScreen();
            transitionPromise.then(function (response) {
                if(angular.isDefined(previousState)) previousStateName = previousState.state.name;

                var toggleObjectVariant = remote.recall(previousStateName, 'toggleObjectVariant');
                toggleObjectVariant({ovid: return_iv_id, type: 'iv'});
            });
        }
        else if (ovid == '' && wovid == '' && wgvid == '') { // create mode and workspace-group is NOT selected
            transitionPromise = $state.go('main.content', {acc_id: $stateParams.acc_id}, true);
            transitionPromise.then(function (response) {
                //var loadItemVariants = remote.recall(previousStateName, 'loadItemVariants');
                var done = remote.loadItemVariants(); // call function in the /main/content
                done.then(function (response) {
                    if(return_iv_id != ''){
                        remote.toggleObjectVariant({ovid: return_iv_id, type: 'iv'});
                    }
                });
            })
        }
        else {
            if(angular.isDefined(previousState)) previousStateName = previousState.state.name;
            console.log(previousStateName)

            transitionPromise = obj.backToPreviousScreen();
            transitionPromise.then(function (response) {
                if(angular.isDefined(loadActions)){
                    remote.loadActions();
                }

                //remote.loadItemVariants();
                var loadItemVariants = remote.recall(previousStateName, 'loadItemVariants');
                if (angular.isFunction(loadItemVariants)) loadItemVariants();
            });
        }
    }

    obj.goBackPreviousPageWithPromise = function(ovid, wovid, wgvid, return_iv_id, remote, loadActions){
        var backPromise = remote.backPromise //notice: everytime getting the promise, the promise is also removed from promise queue
        console.log(backPromise)
        if(angular.isDefined(backPromise)){
            var done = $state.go(backPromise.state.name, backPromise.state.params, true);
            done.then(function(){
                if(backPromise.callbacks.length){
                    backPromise.callbacks.reduce(function(cur, next, index) { //NOTE: the index begin from 1
                        //console.log(cur)
                        //console.log(index)
                        //if(obj.isPromise(cur)){
                        //    return cur.apply(null, backPromise.cbParams[index-1]).then(function(){
                        //        //console.log(next, backPromise.cbParams[index-1])
                        //        next.apply(null, backPromise.cbParams[index-1])
                        //    });
                        //}else{
                        //    cur.apply(null, backPromise.cbParams[index-1]);
                        //    return next;
                        //}

                        var pm = cur.apply(null, backPromise.cbParams[index-1])
                        console.log(index-1)
                        console.log(backPromise.cbParams[index-1])
                        if(obj.isPromise(pm)){
                            return pm.then(function(){
                                //console.log(next, backPromise.cbParams[index-1])
                                next.apply(null, backPromise.cbParams[index])
                            });
                        }else{
                            return next;
                        }
                    });
                }
            })
        }else{
            obj.doneSaveIvAndReturn(ovid, wovid, wgvid, return_iv_id, remote);
        }
    }

    obj.getPreviousState = function(){
        var previousState = $previousState.get()
        var stateName = 'main.content'
        var params = {acc_id: ''}
        var hasHistory = false;
        if (angular.isDefined(previousState) && previousState != null) {
            stateName = previousState.state.name;
            params = previousState.params;
            hasHistory = true;
        }

        return {
            state: {name: stateName},
            params: params,
            hasHistory: hasHistory
        }
    }

    /*
    after redirecting user to page A, return the promise that resolve the scope of page A
     */
    obj.goBackToPreviousPage = function($scope, reload){

        var deferred = $q.defer();
        var previousState = obj.getPreviousState();

        var pagerManager = $scope.remote.recall(previousState.state.name, 'pagerManager');
        var returnScope = $scope.remote.recall(previousState.state.name, 'returnScope');

        var transitionPromise = $state.go(previousState.state.name, previousState.params, {reload: reload});
        if(angular.isDefined(returnScope)){

            transitionPromise.then(function (response) {
                //pagerManager.iv.active = false;
                returnScope().then(function(scope){
                    deferred.resolve(scope); //promise
                })
            })
        }
        return deferred.promise;
    }

    obj.getPropByString = function(obj, propString) {
        if (!propString)
            return obj;

        var prop, props = propString.split('.');

        for (var i = 0, iLen = props.length - 1; i < iLen; i++) {
            prop = props[i];

            var candidate = obj[prop];
            if (candidate !== undefined) {
                obj = candidate;
            } else {
                break;
            }
        }
        return obj[props[i]];
    }


    obj.isPromise = function(promise){
        return angular.isDefined(promise) && typeof promise.then === 'function';
    }

    obj.merge2Array = function(array1, array2){
        var array3 = [];

        var arr = array1.concat(array2),
            len = arr.length;

        while (len--) {
            var itm = arr[len];
            if (array3.indexOf(itm) === -1) {
                array3.unshift(itm);
            }
        }
        return array3;
    }

    obj.extractDatetimeFromFullDateString = function(fullDateString){
        var date = new Date(fullDateString);
        return {
            d: date.getDate(), m: date.getMonth(), y: date.getFullYear()
        }
    }

    obj.convertDomtoString = function(element, isDeep){
        if(!element || !element.tagName) return '';
        var txt, ax, el= document.createElement("div");
        el.appendChild(element.cloneNode(false));
        txt= el.innerHTML;
        if(isDeep){
            ax= txt.indexOf('>')+1;
            txt= txt.substring(0, ax)+element.innerHTML+ txt.substring(ax);
        }
        el= null;
        return txt;
    }
    
    return obj;
});