angular.module('zwenGlobal').factory('httpRequestInterceptor',
    ['$rootScope', function($rootScope)
    {
        return {
            request: function($config) {
                console.log('httpRequestInterceptor')
                if( $rootScope.authToken )
                {
                    console.log('$rootScope.authToken', $rootScope.authToken)
                    $config.headers['authToken'] = $rootScope.authToken;
                }
                return $config;
            }
        };
    }]);