angular.module('zwenGlobal').factory('numberHelper', function ($http, $window, $stateParams, $rootScope, $location, $q, store, $log, $previousState, $futureState, $state, dialogs) {
    var obj = {}

    obj.float = function(){
        var result = 0.00;
        //console.log(arguments)
        _.each(arguments, function (entry, index) {
            if(!isNaN(entry) && angular.isDefined(entry)){
                result = parseFloat(entry);
            }
        })

        return result;
    }

    obj.positiveFloat = function(){
        var result = 0.00;
        //console.log(arguments)
        _.each(arguments, function (entry, index) {
            if(!isNaN(entry) && angular.isDefined(entry)){
                result = parseFloat(entry);
            }
        })

        if(result < 0) result = 0;

        return result;
    }

    obj.floatSum = function(){
        var sum = 0.00;
        //console.log(arguments)
        _.each(arguments, function (entry, index) {
            if(!isNaN(entry) && angular.isDefined(entry)){
                sum += parseFloat(entry);
            }
        })

        return sum;
    }

    obj.floatMultiply = function(){
        var sum = 0.00;
        if(arguments.length > 0) sum = 1;
        _.each(arguments, function (entry, index) {
            if(!isNaN(entry) && angular.isDefined(entry)){
                sum *= obj.float(entry);
            }
        })

        return sum;
    }
    
    return obj;
});