angular.module('zwenGlobal').filter('jsonOrder', function () {
    return function (obj) {
        if (!obj) {
            return [];
        }
        return Object.keys(obj);
    };
})