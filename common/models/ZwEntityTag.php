<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "zw_entity_tag".
 *
 * @property integer $entity_id
 * @property string $tag_name
 *
 * @property ZwEntity $entity
 * @property ZwTag $tagName
 */
class ZwEntityTag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zw_entity_tag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entity_id', 'tag_name'], 'required'],
            [['entity_id'], 'integer'],
            [['tag_name'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'entity_id' => 'Entity ID',
            'tag_name' => 'Tag Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntity()
    {
        return $this->hasOne(ZwEntity::className(), ['entity_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTagName()
    {
        return $this->hasOne(ZwTag::className(), ['tag_name' => 'tag_name']);
    }
}
