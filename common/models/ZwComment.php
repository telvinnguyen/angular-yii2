<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "zw_comment".
 *
 * @property integer $comment_id
 * @property integer $entity_id
 * @property integer $user_id
 * @property string $content
 *
 * @property ZwEntity $entity
 * @property ZwUser $user
 */
class ZwComment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zw_comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comment_id', 'entity_id', 'user_id'], 'required'],
            [['comment_id', 'entity_id', 'user_id'], 'integer'],
            [['content'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'comment_id' => 'Comment ID',
            'entity_id' => 'Entity ID',
            'user_id' => 'User ID',
            'content' => 'Content',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntity()
    {
        return $this->hasOne(ZwEntity::className(), ['entity_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(ZwUser::className(), ['user_id' => 'user_id']);
    }
}
