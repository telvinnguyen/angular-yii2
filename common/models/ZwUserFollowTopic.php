<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "zw_user_follow_topic".
 *
 * @property integer $user_id
 * @property integer $topic_id
 * @property string $join_date
 *
 * @property ZwUser $user
 * @property ZwTopic $topic
 */
class ZwUserFollowTopic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zw_user_follow_topic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'topic_id'], 'required'],
            [['user_id', 'topic_id'], 'integer'],
            [['join_date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'topic_id' => 'Topic ID',
            'join_date' => 'Join Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(ZwUser::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopic()
    {
        return $this->hasOne(ZwTopic::className(), ['topic_id' => 'topic_id']);
    }
}
