<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "zw_notification_change".
 *
 * @property integer $notification_change
 * @property integer $notification_object_id
 * @property string $action
 * @property string $actor
 *
 * @property ZwNotificationObject $notificationObject
 */
class ZwNotificationChange extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zw_notification_change';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notification_change', 'notification_object_id'], 'required'],
            [['notification_change', 'notification_object_id'], 'integer'],
            [['action'], 'string'],
            [['actor'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'notification_change' => 'Notification Change',
            'notification_object_id' => 'Notification Object ID',
            'action' => 'Action',
            'actor' => 'Actor',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificationObject()
    {
        return $this->hasOne(ZwNotificationObject::className(), ['notification_object_id' => 'notification_object_id']);
    }
}
