<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "zw_notification".
 *
 * @property integer $notification_id
 * @property integer $user_id
 *
 * @property ZwUser $user
 * @property ZwNotificationObject[] $zwNotificationObjects
 */
class ZwNotification extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zw_notification';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notification_id', 'user_id'], 'required'],
            [['notification_id', 'user_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'notification_id' => 'Notification ID',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(ZwUser::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZwNotificationObjects()
    {
        return $this->hasMany(ZwNotificationObject::className(), ['notification_id' => 'notification_id']);
    }
}
