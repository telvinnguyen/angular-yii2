<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "zw_user".
 *
 * @property integer $user_id
 * @property string $username
 * @property string $password
 * @property string $full_name
 * @property string $email
 * @property integer $active
 *
 * @property ZwComment[] $zwComments
 * @property ZwIpaddress $user
 * @property ZwIpaddress[] $zwIpaddresses
 * @property ZwNotification[] $zwNotifications
 * @property ZwUserFollowTopic[] $zwUserFollowTopics
 * @property ZwTopic[] $topics
 * @property ZwUserHasRole[] $zwUserHasRoles
 * @property ZwUserRole[] $roles
 * @property ZwUserLikeEntity[] $zwUserLikeEntities
 * @property ZwEntity[] $entities
 */
class ZwIpaddress extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zw_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['active'], 'integer'],
            [['username'], 'string', 'max' => 200],
            [['password', 'full_name', 'email'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'username' => 'Username',
            'password' => 'Password',
            'full_name' => 'Full Name',
            'email' => 'Email',
            'active' => 'Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZwComments()
    {
        return $this->hasMany(ZwComment::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(ZwIpaddress::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZwIpaddresses()
    {
        return $this->hasMany(ZwIpaddress::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZwNotifications()
    {
        return $this->hasMany(ZwNotification::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZwUserFollowTopics()
    {
        return $this->hasMany(ZwUserFollowTopic::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopics()
    {
        return $this->hasMany(ZwTopic::className(), ['topic_id' => 'topic_id'])->viaTable('zw_user_follow_topic', ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZwUserHasRoles()
    {
        return $this->hasMany(ZwUserHasRole::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoles()
    {
        return $this->hasMany(ZwUserRole::className(), ['role_id' => 'role_id'])->viaTable('zw_user_has_role', ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZwUserLikeEntities()
    {
        return $this->hasMany(ZwUserLikeEntity::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntities()
    {
        return $this->hasMany(ZwEntity::className(), ['entity_id' => 'entity_id'])->viaTable('zw_user_like_entity', ['user_id' => 'user_id']);
    }
}
