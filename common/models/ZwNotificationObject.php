<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "zw_notification_object".
 *
 * @property integer $notification_object_id
 * @property integer $notification_id
 * @property string $object
 *
 * @property ZwNotificationChange[] $zwNotificationChanges
 * @property ZwNotification $notification
 */
class ZwNotificationObject extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zw_notification_object';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notification_object_id', 'notification_id'], 'required'],
            [['notification_object_id', 'notification_id'], 'integer'],
            [['object'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'notification_object_id' => 'Notification Object ID',
            'notification_id' => 'Notification ID',
            'object' => 'Object',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZwNotificationChanges()
    {
        return $this->hasMany(ZwNotificationChange::className(), ['notification_object_id' => 'notification_object_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotification()
    {
        return $this->hasOne(ZwNotification::className(), ['notification_id' => 'notification_id']);
    }
}
