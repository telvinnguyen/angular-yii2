<?php

namespace common\models;

use common\component\helpers\CommonHelper;
use common\component\helpers\MembershipHelper;
use DateTime;
use frontend\models\ObjectToolbar;
use Namshi\JOSE\JWS;
use phpjwt\Authentication\JWT;
use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "tbl_member".
 *
 * @property string $member_id
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $email
 * @property string $facetime_id
 * @property string $login_name
 * @property string $password
 * @property string $address
 * @property string $city
 * @property string $state
 * @property string $zip
 * @property string $phone_number
 * @property string $registered_date
 * @property string $last_loggedin_date
 * @property string $address2
 * @property string $keywords
 * @property string $avatar
 * @property integer $member_type
 * @property integer $disable
 * @property string $upload_folder
 * @property string $full_name
 * @property string $description
 * @property string $beta
 * @property integer $is_admin
 * @property integer $is_superadmin
 * @property string $disabled_date
 * @property integer $priv_administer
 * @property integer $priv_new_forms
 * @property integer $priv_new_themes
 * @property string $cookie_hash
 * @property boolean $content_access
 * @property string $comment
 *
 * @property AccountUserMember[] $accountUserMembers
 * @property AccountUser[] $accounts
 * @property AssignTicket[] $assignTickets
 * @property ItemVariant[] $ticketIvs
 * @property BugReport[] $bugReports
 * @property CcInfo[] $ccInfos
 * @property Comment[] $comments
 * @property CvSharedMember[] $cvSharedMembers
 * @property CollectionVariant[] $cvs
 * @property GvSharedMember[] $gvSharedMembers
 * @property GroupVariant[] $gvs
 * @property InviteAddmember[] $inviteAddmembers
 * @property IvSharedMember[] $ivSharedMembers
 * @property ItemVariant[] $ivs
 * @property MemberAddition[] $memberAdditions
 * @property MemberAllocterm $memberAllocterm
 * @property SettingMember[] $settingMembers
 * @property Setting[] $settings
 */
class Member extends ActiveRecord// implements IdentityInterface
{
    const PROMOTION_TEST_CODE=55272;
    const I_SIGN_TYPE_ID = 1;
    const D_SIGN_TYPE_ID = 2;
    const SENDER = 'ZwenGlobal Admin';
    const JWT_PASSWORD = 'ss-secret-P@ss600';
    /**
     * @inheritdoc
     */
    const SITE_NAME = 'http://zwenglobal.com/signsmartv2/index.php/site/login';
    //    const EMAIL_SENDER = 'No-Reply@zwenglobal.com';
    const ADDITION_PHONE_ENTRY = 2;
    const ADDITION_EMAIL_ENTRY = 1;
    const REAL_MEMBER = 0;
    const BETA_MEMBER = 1;
    const MEMBER_TYPE_FREE = 1;
    const MEMBER_TYPE_GOLD = 2;
    const MEMBER_TYPE_PLATINUM = 3;
    const MEMBER_TYPE_VIP = 4;
    const MEMBER_TYPE_DISCOUNT = 9999;
    public static function tableName()
    {
        return 'tbl_member';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['registered_date', 'last_loggedin_date', 'disabled_date'], 'safe'],
            [['member_type', 'disable', 'beta', 'is_admin', 'is_superadmin', 'priv_administer', 'priv_new_forms', 'priv_new_themes'], 'integer'],
            [['description','comment'], 'string'],
            [['is_admin', 'is_superadmin'], 'required'],
            [['content_access'], 'boolean'],
            [['first_name', 'middle_name', 'last_name', 'email', 'facetime_id', 'login_name', 'password', 'address', 'city', 'phone_number', 'address2', 'keywords', 'avatar', 'upload_folder', 'cookie_hash'], 'string', 'max' => 255],
            [['state'], 'string', 'max' => 10],
            [['zip'], 'string', 'max' => 50],
            [['full_name'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'member_id' => Yii::t('app', 'Member ID'),
            'first_name' => Yii::t('app', 'First Name'),
            'middle_name' => Yii::t('app', 'Middle Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'email' => Yii::t('app', 'Email'),
            'facetime_id' => Yii::t('app', 'Facetime ID'),
            'login_name' => Yii::t('app', 'Login Name'),
            'password' => Yii::t('app', 'Password'),
            'address' => Yii::t('app', 'Address'),
            'city' => Yii::t('app', 'City'),
            'state' => Yii::t('app', 'State'),
            'zip' => Yii::t('app', 'Zip'),
            'phone_number' => Yii::t('app', 'Phone Number'),
            'registered_date' => Yii::t('app', 'Registered Date'),
            'last_loggedin_date' => Yii::t('app', 'Last Loggedin Date'),
            'address2' => Yii::t('app', 'Address2'),
            'keywords' => Yii::t('app', 'Keywords'),
            'avatar' => Yii::t('app', 'Avatar'),
            'member_type' => Yii::t('app', 'Member Type'),
            'disable' => Yii::t('app', 'Disable'),
            'upload_folder' => Yii::t('app', 'Upload Folder'),
            'full_name' => Yii::t('app', 'Full Name'),
            'description' => Yii::t('app', 'Description'),
            'beta' => Yii::t('app', 'Beta'),
            'is_admin' => Yii::t('app', 'Is Admin'),
            'is_superadmin' => Yii::t('app', 'Is Superadmin'),
            'disabled_date' => Yii::t('app', 'Disabled Date'),
            'priv_administer' => Yii::t('app', 'Priv Administer'),
            'priv_new_forms' => Yii::t('app', 'Priv New Forms'),
            'priv_new_themes' => Yii::t('app', 'Priv New Themes'),
            'cookie_hash' => Yii::t('app', 'Cookie Hash'),
            'content_access' => Yii::t('app', 'Content Access'),
        ];
    }

    public function getFullName()
    {
        return sprintf('%s %s', $this->first_name, $this->last_name);
    }


    /**
     * @return string
     */
    public static function createLoginToken($member_id)
    {

        //set TTL (time to live)

        $ttl = new \DateTime('now', new \DateTimeZone('UTC'));
        $ttl->modify("+1 year");

        // refer to see more about hash algorithm https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-14#section-3.1
        $jws = new JWS('HS512'); //pass hash algorithm sha-512

        $jws->setPayload(array(
            'member_id' => $member_id, //$user->getid(),
            'exp' => $ttl->format('U'),
            'jwt' => JWT::encode(array('member_id' => $member_id), Member::JWT_PASSWORD, 'HS512')
        ));

        $private_key_path = \Yii::getAlias('@common/config') . DIRECTORY_SEPARATOR . 'private1.txt';

//        var_dump(openssl_error_string());
        $privateKey = null;
//        if (CommonHelper::isLocalHostRequest()) {
//            //code for openSSL on window OS (localhost)
//            $privateKey = openssl_pkey_get_private(file_get_contents($private_key_path), Member::JWT_PASSWORD);
//        }

        $jws->sign($privateKey);
        $token = $jws->getTokenString();
        //var_dump($token);

        return $token;

    }

    /**
     * @param $cookie_name
     * @return array|bool
     */
    public static function validateLoginToken($cookie_name)
    {
        $result = ['OK' => 0];

        $now = new \DateTime('now', new \DateTimeZone('UTC'));
        $public_key_path = \Yii::getAlias('@common/config') . DIRECTORY_SEPARATOR . 'public1.txt';


        if (isset($_COOKIE[$cookie_name])) {
            $jws = JWS::load($_COOKIE[$cookie_name]);

            $public_key = null;

            if (false) {
            //if (CommonHelper::isLocalHostRequest()) {
                //code for openSSL on window OS (localhost)
                $public_key = openssl_pkey_get_public(file_get_contents($public_key_path));
                // verify that the token is valid and had the same values
                // you emitted before while setting it as a cookie
                //var_dump($jws->isValid($public_key, 'HS512'));exit;
                if ($jws->isValid($public_key, 'HS512')) {
                    $payload = $jws->getPayload();
                    if(is_array($payload)){
                        if (\DateTime::createFromFormat('U', $payload['exp']) > $now) {
                            $result = ['OK' => 1, 'member_id' => $payload['member_id']];
                        }
                    }
                }

            } else {
                //code for centOS (server)

                /* We've had trouble with openssl_pkey_get_private() and openssl_pkey_get_public() functions occasionally
                not recognizing well-formatted public and private keys, for reasons that are difficult to track down.
                To circumvent the need for these functions, we've used the another way to process the shakehand protocal
                */


                $payload = $jws->getPayload();
                if(is_array($payload)){
                    $decoded = JWT::decode($payload['jwt'], Member::JWT_PASSWORD, array('HS512'));


                    if (\DateTime::createFromFormat('U', $payload['exp']) > $now) {
                        $result = ['OK' => 1, 'member_id' => $decoded->member_id];
                    }
                }

            }

            //var_dump(openssl_error_string());


        }

        return $result;
    }

    /**
     * @return null
     */
    public static function getLoginId()
    {
        $v = self::validateLoginToken('ss-identity');
        if ($v['OK'] == 1) {
            return $v['member_id'];
        }
        return null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'disable' => 0]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by login name
     *
     * @param string $login_name
     * @return static|null
     */
    public static function findByUsername($login_name)
    {
        return static::findOne(['login_name' => $login_name, 'disable' => 0]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        /*
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
        */
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int)end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        //return Yii::$app->security->validatePassword($password, $this->password);
        return md5($password) == $this->password;
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = md5($password);
        //$this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccountUserMembers()
    {
        return $this->hasMany(AccountUserMember::className(), ['member_id' => 'member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccounts()
    {
        return $this->hasMany(AccountUser::className(), ['account_id' => 'account_id'])->viaTable('tbl_account_user_member', ['member_id' => 'member_id']);
    }

    public function getOwnAccounts()
    {
        return $this->hasMany(AccountUser::className(), ['account_id' => 'account_id'])
            ->viaTable('tbl_account_user_member', ['member_id' => 'member_id'], function ($query) {
                $query->andWhere(['role' => 1, 'shared' => 0])->orderBy(['sort' => SORT_DESC]);
            });
    }

    public function getOwnRestrictedAccounts()
    {
        return $this->hasMany(AccountUser::className(), ['account_id' => 'account_id'])
            ->viaTable('tbl_account_user_member', ['member_id' => 'member_id'], function ($query) {
                $query->andWhere(['role' => 1, 'shared' => 0])->orderBy(['sort' => SORT_DESC]);
            })
            ->andWhere(['<>', 'company', MembershipHelper::DEFAULT_ACCOUNT_NAME])
            ->andWhere(['is_public' => ObjectToolbar::ACCOUNT_RESTRICTED]);
    }

    public function getOwnAccountsExceptUnAssign()
    {

        return $this->hasMany(AccountUser::className(), ['account_id' => 'account_id'])
            ->viaTable('tbl_account_user_member', ['member_id' => 'member_id'], function ($query) {
                $query->andWhere(['role' => 1, 'shared' => 0])->orderBy(['sort' => SORT_DESC]);
            })->where(['<>', 'company', CommonHelper::DEFAULT_OBJECT_NAME]);
    }

    public function getSharedAccounts()
    {
        return $this->hasMany(AccountUser::className(), ['account_id' => 'account_id'])
            ->viaTable('tbl_account_user_member', ['member_id' => 'member_id'], function ($query) {
                $query->andWhere(['shared' => 1])->orderBy(['sort' => SORT_DESC]);
            });
    }

    public function getSharedAccountsExceptUnAssign()
    {
        return $this->hasMany(AccountUser::className(), ['account_id' => 'account_id'])
            ->viaTable('tbl_account_user_member', ['member_id' => 'member_id'], function ($query) {
                $query->andWhere(['shared' => 1])->orderBy(['sort' => SORT_DESC]);
            })->where(['<>', 'company', CommonHelper::DEFAULT_OBJECT_NAME]);
    }

    public function getMemberStorages()
    {
        return $this->hasMany(MemberStorage::className(), ['member_id' => 'member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssignTickets()
    {
        return $this->hasMany(AssignTicket::className(), ['member_id' => 'member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTicketIvs()
    {
        return $this->hasMany(ItemVariant::className(), ['iv_id' => 'ticket_iv_id'])->viaTable('tbl_assign_ticket', ['member_id' => 'member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBugReports()
    {
        return $this->hasMany(BugReport::className(), ['member_id' => 'member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCcInfos()
    {
        return $this->hasMany(CcInfo::className(), ['member_id' => 'member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['member_id' => 'member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCvSharedMembers()
    {
        return $this->hasMany(CvSharedMember::className(), ['shared_to_member_id' => 'member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCvs()
    {
        return $this->hasMany(CollectionVariant::className(), ['cv_id' => 'cv_id'])->viaTable('tbl_cv_shared_member', ['shared_to_member_id' => 'member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGvSharedMembers()
    {
        return $this->hasMany(GvSharedMember::className(), ['shared_to_member_id' => 'member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGvs()
    {
        return $this->hasMany(GroupVariant::className(), ['gv_id' => 'gv_id'])->viaTable('tbl_gv_shared_member', ['shared_to_member_id' => 'member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInviteAddmembers()
    {
        return $this->hasMany(InviteAddmember::className(), ['member_id' => 'member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIvSharedMembers()
    {
        return $this->hasMany(IvSharedMember::className(), ['shared_to_member_id' => 'member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIvs()
    {
        return $this->hasMany(ItemVariant::className(), ['iv_id' => 'iv_id'])->viaTable('tbl_iv_shared_member', ['shared_to_member_id' => 'member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMemberAdditions()
    {
        return $this->hasMany(MemberAddition::className(), ['member_id' => 'member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMemberAllocterm()
    {
        return $this->hasOne(MemberAllocterm::className(), ['member_id' => 'member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSettingMembers()
    {
        return $this->hasMany(SettingMember::className(), ['member_id' => 'member_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSettings()
    {
        return $this->hasMany(Setting::className(), ['id' => 'setting_id'])->viaTable('tbl_setting_member', ['member_id' => 'member_id']);
    }

    public function getItemMember()
    {
        return $this->hasOne(ItemMember::className(), ['member_id' => 'member_id']);
    }

    public function getShareVariants()
    {
        return $this->hasMany(ShareVariant::className(), ['shared_to_member_id' => 'member_id']);
    }
    public function getSubscription()
    {
        return $this->hasOne(Subscription::className(), ['id' => 'member_type']);
    }

}
