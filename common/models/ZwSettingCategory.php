<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "zw_setting_category".
 *
 * @property integer $setting_cat_id
 * @property string $name
 *
 * @property ZwSetting[] $zwSettings
 */
class ZwSettingCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zw_setting_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'setting_cat_id' => 'Setting Cat ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZwSettings()
    {
        return $this->hasMany(ZwSetting::className(), ['setting_cat_id' => 'setting_cat_id']);
    }
}
