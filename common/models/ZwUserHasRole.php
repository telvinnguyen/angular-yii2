<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "zw_user_has_role".
 *
 * @property integer $user_id
 * @property integer $role_id
 *
 * @property ZwUser $user
 * @property ZwUserRole $role
 */
class ZwUserHasRole extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zw_user_has_role';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'role_id'], 'required'],
            [['user_id', 'role_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'role_id' => 'Role ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(ZwUser::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(ZwUserRole::className(), ['role_id' => 'role_id']);
    }
}
