<?php

namespace common\models;

use backend\models\UserLoginForm;
use Namshi\JOSE\JWS;
use phpjwt\Authentication\JWT;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "zw_user".
 *
 * @property integer $user_id
 * @property string $username
 * @property string $password
 * @property string $full_name
 * @property string $email
 * @property integer $active
 *
 * @property ZwComment[] $zwComments
 * @property ZwIpaddress[] $zwIpaddresses
 * @property ZwNotification[] $zwNotifications
 * @property ZwUserFollowTopic[] $zwUserFollowTopics
 * @property ZwTopic[] $topics
 * @property ZwUserHasRole[] $zwUserHasRoles
 * @property ZwUserRole[] $roles
 * @property ZwUserLikeEntity[] $zwUserLikeEntities
 * @property ZwEntity[] $entities
 */
class ZwUser extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zw_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['active'], 'integer'],
            [['username'], 'string', 'max' => 200],
            [['password', 'full_name', 'email'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'username' => 'Username',
            'password' => 'Password',
            'full_name' => 'Full Name',
            'email' => 'Email',
            'active' => 'Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZwComments()
    {
        return $this->hasMany(ZwComment::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZwIpaddresses()
    {
        return $this->hasMany(ZwIpaddress::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZwNotifications()
    {
        return $this->hasMany(ZwNotification::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZwUserFollowTopics()
    {
        return $this->hasMany(ZwUserFollowTopic::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopics()
    {
        return $this->hasMany(ZwTopic::className(), ['topic_id' => 'topic_id'])->viaTable('zw_user_follow_topic', ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZwUserHasRoles()
    {
        return $this->hasMany(ZwUserHasRole::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoles()
    {
        return $this->hasMany(ZwUserRole::className(), ['role_id' => 'role_id'])->viaTable('zw_user_has_role', ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZwUserLikeEntities()
    {
        return $this->hasMany(ZwUserLikeEntity::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntities()
    {
        return $this->hasMany(ZwEntity::className(), ['entity_id' => 'entity_id'])->viaTable('zw_user_like_entity', ['user_id' => 'user_id']);
    }

    public function validatePassword($password)
    {
        //return Yii::$app->security->validatePassword($password, $this->password);var_dump($this->hasErrors());

        return password_verify($password, $this->password);
    }


    public static function findByUsername($login_name)
    {
        return static::findOne(['username' => $login_name, 'active' => 1]);
    }

    public static function validateLoginToken($cookie_name)
    {
        $result = ['OK' => 0];

        $now = new \DateTime('now', new \DateTimeZone('UTC'));
        $public_key_path = \Yii::getAlias('@common/config') . DIRECTORY_SEPARATOR . 'public1.txt';


        if (isset($_COOKIE[$cookie_name])) {
            $jws = JWS::load($_COOKIE[$cookie_name]);

            $public_key = null;

            if (false) {
                //if (CommonHelper::isLocalHostRequest()) {
                //code for openSSL on window OS (localhost)
                $public_key = openssl_pkey_get_public(file_get_contents($public_key_path));
                // verify that the token is valid and had the same values
                // you emitted before while setting it as a cookie
                //var_dump($jws->isValid($public_key, 'HS512'));exit;
                if ($jws->isValid($public_key, 'HS512')) {
                    $payload = $jws->getPayload();
                    if(is_array($payload)){
                        if (\DateTime::createFromFormat('U', $payload['exp']) > $now) {
                            $result = ['OK' => 1, 'member_id' => $payload['member_id']];
                        }
                    }
                }

            } else {
                //code for centOS (server)

                /* We've had trouble with openssl_pkey_get_private() and openssl_pkey_get_public() functions occasionally
                not recognizing well-formatted public and private keys, for reasons that are difficult to track down.
                To circumvent the need for these functions, we've used the another way to process the shakehand protocal
                */


                $payload = $jws->getPayload();
                if(is_array($payload)){
                    $decoded = JWT::decode($payload['jwt'], UserLoginForm::JWT_PASSWORD, array('HS512'));


                    if (\DateTime::createFromFormat('U', $payload['exp']) > $now) {
                        $result = ['OK' => 1, 'member_id' => $decoded->member_id];
                    }
                }

            }

        }

        return $result;
    }

    public static function getLoginId()
    {
        $v = self::validateLoginToken('ss-identity');
        if ($v['OK'] == 1) {
            return $v['member_id'];
        }
        return null;
    }

    public static function currUser()
    {
        $user_id = ZwUser::getLoginId();
        return ZwUser::findOne($user_id);
    }


}
