<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "zw_user_role".
 *
 * @property integer $role_id
 * @property string $name
 *
 * @property ZwUserHasRole[] $zwUserHasRoles
 * @property ZwUser[] $users
 */
class ZwUserRole extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zw_user_role';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role_id'], 'required'],
            [['role_id'], 'integer'],
            [['name'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'role_id' => 'Role ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZwUserHasRoles()
    {
        return $this->hasMany(ZwUserHasRole::className(), ['role_id' => 'role_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(ZwUser::className(), ['user_id' => 'user_id'])->viaTable('zw_user_has_role', ['role_id' => 'role_id']);
    }
}
