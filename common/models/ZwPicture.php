<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "zw_picture".
 *
 * @property integer $picture_id
 * @property integer $zw_entity_entity_id
 * @property string $file_name
 * @property string $created_date
 * @property string $size
 * @property string $ext
 *
 * @property ZwEntity $zwEntityEntity
 */
class ZwPicture extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zw_picture';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['picture_id', 'zw_entity_entity_id'], 'required'],
            [['picture_id', 'zw_entity_entity_id'], 'integer'],
            [['file_name'], 'string', 'max' => 200],
            [['created_date', 'size'], 'string', 'max' => 45],
            [['ext'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'picture_id' => 'Picture ID',
            'zw_entity_entity_id' => 'Zw Entity Entity ID',
            'file_name' => 'File Name',
            'created_date' => 'Created Date',
            'size' => 'Size',
            'ext' => 'Ext',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZwEntityEntity()
    {
        return $this->hasOne(ZwEntity::className(), ['entity_id' => 'zw_entity_entity_id']);
    }
}
