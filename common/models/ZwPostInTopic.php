<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "zw_post_in_topic".
 *
 * @property integer $topic_id
 * @property integer $post_id
 *
 * @property ZwTopic $topic
 * @property ZwPost $post
 */
class ZwPostInTopic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zw_post_in_topic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['topic_id', 'post_id'], 'required'],
            [['topic_id', 'post_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'topic_id' => 'Topic ID',
            'post_id' => 'Post ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopic()
    {
        return $this->hasOne(ZwTopic::className(), ['topic_id' => 'topic_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(ZwPost::className(), ['post_id' => 'post_id']);
    }
}
