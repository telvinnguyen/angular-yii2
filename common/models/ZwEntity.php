<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "zw_entity".
 *
 * @property integer $entity_id
 *
 * @property ZwComment[] $zwComments
 * @property ZwEntityTag[] $zwEntityTags
 * @property ZwTag[] $tagNames
 * @property ZwPicture[] $zwPictures
 * @property ZwPost[] $zwPosts
 * @property ZwUserLikeEntity[] $zwUserLikeEntities
 * @property ZwUser[] $users
 */
class ZwEntity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zw_entity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entity_id'], 'required'],
            [['entity_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'entity_id' => 'Entity ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZwComments()
    {
        return $this->hasMany(ZwComment::className(), ['entity_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZwEntityTags()
    {
        return $this->hasMany(ZwEntityTag::className(), ['entity_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTagNames()
    {
        return $this->hasMany(ZwTag::className(), ['tag_name' => 'tag_name'])->viaTable('zw_entity_tag', ['entity_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZwPictures()
    {
        return $this->hasMany(ZwPicture::className(), ['zw_entity_entity_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZwPosts()
    {
        return $this->hasMany(ZwPost::className(), ['entity_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZwUserLikeEntities()
    {
        return $this->hasMany(ZwUserLikeEntity::className(), ['entity_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(ZwUser::className(), ['user_id' => 'user_id'])->viaTable('zw_user_like_entity', ['entity_id' => 'entity_id']);
    }
}
