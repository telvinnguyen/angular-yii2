<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "zw_setting".
 *
 * @property string $setting_key
 * @property string $value
 * @property integer $setting_cat_id
 *
 * @property ZwSettingCategory $settingCat
 */
class ZwSetting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zw_setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['setting_key', 'setting_cat_id'], 'required'],
            [['setting_cat_id'], 'integer'],
            [['setting_key'], 'string', 'max' => 200],
            [['value'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'setting_key' => 'Setting Key',
            'value' => 'Value',
            'setting_cat_id' => 'Setting Cat ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSettingCat()
    {
        return $this->hasOne(ZwSettingCategory::className(), ['setting_cat_id' => 'setting_cat_id']);
    }
}
