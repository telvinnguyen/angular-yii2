<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "zw_tag".
 *
 * @property string $tag_name
 * @property string $description
 *
 * @property ZwEntityTag[] $zwEntityTags
 * @property ZwEntity[] $entities
 */
class ZwTag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zw_tag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tag_name'], 'required'],
            [['description'], 'string'],
            [['tag_name'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tag_name' => 'Tag Name',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZwEntityTags()
    {
        return $this->hasMany(ZwEntityTag::className(), ['tag_name' => 'tag_name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntities()
    {
        return $this->hasMany(ZwEntity::className(), ['entity_id' => 'entity_id'])->viaTable('zw_entity_tag', ['tag_name' => 'tag_name']);
    }
}
