<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "zw_topic".
 *
 * @property integer $topic_id
 * @property string $name
 * @property integer $active
 *
 * @property ZwPostInTopic[] $zwPostInTopics
 * @property ZwPost[] $posts
 * @property ZwUserFollowTopic[] $zwUserFollowTopics
 * @property ZwUser[] $users
 */
class ZwTopic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zw_topic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['active'], 'integer'],
            [['name'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'topic_id' => 'Topic ID',
            'name' => 'Name',
            'active' => 'Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZwPostInTopics()
    {
        return $this->hasMany(ZwPostInTopic::className(), ['topic_id' => 'topic_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(ZwPost::className(), ['post_id' => 'post_id'])->viaTable('zw_post_in_topic', ['topic_id' => 'topic_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZwUserFollowTopics()
    {
        return $this->hasMany(ZwUserFollowTopic::className(), ['topic_id' => 'topic_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(ZwUser::className(), ['user_id' => 'user_id'])->viaTable('zw_user_follow_topic', ['topic_id' => 'topic_id']);
    }
}
