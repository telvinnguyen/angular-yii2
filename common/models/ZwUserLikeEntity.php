<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "zw_user_like_entity".
 *
 * @property integer $entity_id
 * @property integer $user_id
 *
 * @property ZwEntity $entity
 * @property ZwUser $user
 */
class ZwUserLikeEntity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zw_user_like_entity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entity_id', 'user_id'], 'required'],
            [['entity_id', 'user_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'entity_id' => 'Entity ID',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntity()
    {
        return $this->hasOne(ZwEntity::className(), ['entity_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(ZwUser::className(), ['user_id' => 'user_id']);
    }
}
