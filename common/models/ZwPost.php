<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "zw_post".
 *
 * @property integer $post_id
 * @property integer $entity_id
 * @property string $name
 * @property string $created_date
 * @property string $content
 *
 * @property ZwEntity $entity
 * @property ZwPostInTopic[] $zwPostInTopics
 * @property ZwTopic[] $topics
 */
class ZwPost extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zw_post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entity_id'], 'required'],
            [['entity_id'], 'integer'],
            [['created_date'], 'safe'],
            [['content'], 'string'],
            [['name'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'post_id' => 'Post ID',
            'entity_id' => 'Entity ID',
            'name' => 'Name',
            'created_date' => 'Created Date',
            'content' => 'Content',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntity()
    {
        return $this->hasOne(ZwEntity::className(), ['entity_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZwPostInTopics()
    {
        return $this->hasMany(ZwPostInTopic::className(), ['post_id' => 'post_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopics()
    {
        return $this->hasMany(ZwTopic::className(), ['topic_id' => 'topic_id'])->viaTable('zw_post_in_topic', ['post_id' => 'post_id']);
    }
}
