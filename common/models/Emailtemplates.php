<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_emailtemplates".
 *
 * @property string $id
 * @property string $subject
 * @property string $email_content
 */
class Emailtemplates extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_emailtemplates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email_content'], 'string'],
            [['subject'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'subject' => Yii::t('app', 'Subject'),
            'email_content' => Yii::t('app', 'Email Content'),
        ];
    }
}
