<?php

Yii::setAlias('@uploads', realpath(dirname(__FILE__).'/../..') .  '/frontend/web/uploads');

return [
    'adminEmail' => 'admin@example.com',
    //'supportEmail' => 'no-reply@zwenglobal.com',
    'supportEmail' => 'telvin@vidaltek.com',
    'user.passwordResetTokenExpire' => 3600,
    'enableHOS' => false,
    'developerEmail'=>'telvin@vidaltek.com',


    'oldControllerDir' => 'E:\wamp\www\signsmart2\protected\controllers',
    'oldVendorDir' => 'E:\wamp\www\signsmart2\protected\vendors',
    'newOutputDir' => 'E:\wamp\www\signsmart2\ss_generator' . DIRECTORY_SEPARATOR,


    'dirInput' => 'E:\\sample\\',
    'dirOutput' => 'E:\\sample\\',

    'globalSocketServer' => 'localhost:8082/global',
    'signSocketServer' => 'localhost:8082/sign',

];
