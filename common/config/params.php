<?php

Yii::setAlias('@uploads', realpath(dirname(__FILE__).'/../..') .  '/frontend/web/uploads');

return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'no-reply@zwenglobal.com',
    'user.passwordResetTokenExpire' => 3600,
    'enableHOS' => false,
    'developerEmail'=>'telvin@vidaltek.com',

    'globalSocketServer' => str_replace("www.", "", $_SERVER['SERVER_NAME']) .'/global',
    'signSocketServer' => str_replace("www.", "", $_SERVER['SERVER_NAME']) .'/sign',
];
