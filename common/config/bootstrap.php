<?php
use yii\helpers\Url;

Yii::setAlias('common', dirname(__DIR__));
Yii::setAlias('frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('console', dirname(dirname(__DIR__)) . '/console');

Yii::$classMap['common\component\helpers\CommonHelper'] = '@common/components/helpers/CommonHelper.php';
Yii::$classMap['common\component\helpers\AuthHelper'] = '@common/components/helpers/AuthHelper.php';
Yii::$classMap['common\component\helpers\LogHelper'] = '@common/components/helpers/LogHelper.php';
Yii::$classMap['common\component\helpers\FileHelper'] = '@common/components/helpers/FileHelper.php';
Yii::$classMap['common\component\helpers\MembershipHelper'] = '@common/components/helpers/MembershipHelper.php';
Yii::$classMap['common\component\helpers\GeneratorHelper'] = '@common/components/helpers/GeneratorHelper.php';
Yii::$classMap['common\component\helpers\SiteLogHelper'] = '@common/components/helpers/SiteLogHelper.php';




