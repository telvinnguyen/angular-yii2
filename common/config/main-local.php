<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'bootstrap' => ['gii'],
    'modules' => [
        'gii' => 'yii\gii\Module',
        'crud' => [
            'class' => 'c006\crud\Module',
        ],
        // ...
    ],
    'language' => 'en-GB', // vi-VD
    'components' => [
        'view' => [
            'title' => 'ZwenGlobal'
            // ...
        ],
        'restClient' => [
            'class' => 'vendor\RestCurl\RESTClient'
        ],
        'curl' => [
            'class' => 'vendor\RestCurl\CURL'
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=zw_global',
            'username' => 'root',
            'password' => '1',
            'tablePrefix' => 'tbl',
            'charset' => 'utf8',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info'],
                    'categories' => ['application'],
                    //'categories' => ['yii\db\*'],
                    //'categories' => ['application','yii\db\*'],
                    //'categories' => ['yii\web\HttpException:*'],
                    'logFile' => '@backend/runtime/logs/local.log',
                ]
//                [
//                    'class' => 'yii\log\FileTarget',
//                    'levels' => ['info'],
//                    'categories' => ['yii\db\*'],
//                    'logFile' => '@frontend/runtime/logs/local.log',
//                    'maxFileSize' => 1024 * 2,
//                    'maxLogFiles' => 50,
//                ],
            ],
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,

//            'transport' => [
//                'class' => 'Swift_SmtpTransport',
//                'host' => 'terri.machighway.com',  // e.g. smtp.mandrillapp.com or smtp.gmail.com
//                'username' => 'No-Reply@ZwenGlobal.com',
//                'password' => 'CbgS(=fG[zR%',
//                'port' => '465', // Port 25 is a very common port too
//                'encryption' => 'ssl', // It is often used, check your provider or mail server specs
//            ],

            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',  // e.g. smtp.mandrillapp.com or smtp.gmail.com
                'username' => 'telvinnguyen@gmail.com',
                'password' => 'uulnloozhdisnvrv', //
                'port' => '587', // Port 25 is a very common port too
                'encryption' => 'tls', // It is often used, check your provider or mail server specs
            ],
        ],

        'i18n' => [
            'translations' => [
                'frontend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en-US',
                    'basePath' => '@common/messages'
                ],
                'backend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en-US',
                    'basePath' => '@common/messages'
                ],
            ],
        ]

    ],
];
