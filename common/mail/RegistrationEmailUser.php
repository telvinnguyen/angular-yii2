<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

?>
<p>Dear <?= Html::encode($objmember->getFullName()) ?>,</p>
<p>
    Welcome and thank you for registering at ZwenGlobal!<br/>
    Your account has now been created and you can log in by using your following username and password:<br/>

    Username: <?= Html::encode($objmember->login_name) ?><br/>
    Password: <?= Html::encode($password) ?> <br/>

    Please go to this URL to login: <a href="<?= Html::encode($sitename) ?>"><?= Html::encode($sitename) ?></a>
</p>
<br/>
Thanks,<br/>
<?= Html::encode($Sender) ?>
