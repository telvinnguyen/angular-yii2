<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

?>

<div> Dear <?= Html::encode($member->getFullName()) ?>,</div>
<p> We have received your password reset request. Below is updated login information with a new password:</p>
<p>
    Username: <?= Html::encode($member->login_name) ?>
    <br/>
    Password: <?= Html::encode($newPassword) ?>
</p>
<div>
    To change this password, do the following:<br/>
    <div style="margin-left:20px">
        • Log into your account ( Using the credentials above ).<br/>
        • Select the "Account Information" icon.<br/>
        • Choose your Membership Badge icon in the bottom row and select the "Edit" Action.<br/>
        • From the "Update Member" page, select the "Change Password" Button and input your own new password.<br/>
    </div>

</div>
<div>

    <p> Regards,</p>
    ZwenGlobal
</div>
