<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 4/29/16
 * Time: 8:31 AM
 */

namespace common\component\helpers;


use common\models\ZwSetting;

class SettingHelper
{
    public static function getSetting($key)
    {
        $st = ZwSetting::findOne($key);
        if($st)
            return $st->value;
        return "";
    }
}