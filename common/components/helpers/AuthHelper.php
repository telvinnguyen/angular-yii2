<?php

namespace common\component\helpers;

use common\models\User;
use phpjwt\Authentication\JWT;

/**
 * Class CommonHelper
 * @package common\component\helpers
 */
class AuthHelper
{

	public static function config(){
		return [
			'jwt' => array(
				// Key for signing the JWT's, suggest generate it with base64_encode(openssl_random_pseudo_bytes(64))
				'key'       =>  'weppZDVWw37T+/FxmiFxeH/AcT1aZ8xv3xWH2kcL3n5jEtNRahJinmAHtMRCfHB1quLdXZlPFt5JAVvwod144g==',
				'algorithm' => 'HS512' // Algorithm used to sign the token, see https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-40#section-3
			),
			'serverName' => 'angular-yii2',
		];
	}
	public static function generateHash($password) {
		if (defined("CRYPT_BLOWFISH") && CRYPT_BLOWFISH) {
			$salt = '$2y$11$' . substr(md5(uniqid(rand(), true)), 0, 22);
			return crypt($password, $salt);
		}
	}

	public static function verify($password, $hashedPassword) {
		return crypt($password, $hashedPassword) == $hashedPassword;
	}


	public static function makeToken($data, $issuedAt=null, $notBefore=null, $expire=null){
		$CONFIG = AuthHelper::config();

		$tokenId    = base64_encode(mcrypt_create_iv(32));
		$issuedAt   = $issuedAt ? $issuedAt : time();
		$notBefore   = $notBefore ? $notBefore : $notBefore - 10;  // Subtracting by 60 seconds
		$expire   = $expire ? $expire : $expire + 60; //Adding by 10 seconds

		$serverName = $CONFIG['serverName'];

		/*
         * Create the token as an array
         */
		$data = [
			'iat'  => $issuedAt,         // Issued at: time when the token was generated
			'jti'  => $tokenId,          // Json Token Id: an unique identifier for the token
			'iss'  => $serverName,       // Issuer
			'nbf'  => $notBefore,        // Not before
			'exp'  => $expire,           // Expire
			'data' => $data
		];

		/*
                     * Extract the key, which is coming from the config file.
                     *
                     * Best suggestion is the key to be a binary string and
                     * store it in encoded in a config file.
                     *
                     * Can be generated with base64_encode(openssl_random_pseudo_bytes(64));
                     *
                     * keep it secure! You'll need the exact key to verify the
                     * token later.
                     */
		$secretKey = base64_decode($CONFIG['jwt']['key']);
		//$secretKey = $CONFIG['jwt']['key'];

		/*
         * Extract the algorithm from the config file too
         */
		$algorithm = $CONFIG['jwt']['algorithm'];

		/*
         * Encode the array to a JWT string.
         * Second parameter is the key to encode the token.
         *
         * The output string can be validated at http://jwt.io/
         */
		$jwt = JWT::encode(
			$data,      //Data to be encoded in the JWT
			$secretKey, // The signing key
			$algorithm  // Algorithm used to sign the token, see https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-40#section-3
		);
		return $jwt;
	}
	
	
	
}