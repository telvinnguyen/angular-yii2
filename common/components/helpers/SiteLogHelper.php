<?php

namespace common\component\helpers;

namespace common\component\helpers;
use common\models\Member;
use common\models\AccountUser;
use Yii;

/**
 * Class CommonHelper
 * @package common\component\helpers
 */
class SiteLogHelper
{
    public static function add($action, $actor=''){
        if($actor == null){
            $currentUser = PermissionHelper::getUserAccessing();
            if($currentUser['id'] == 'member_id'){
                $member = Member::findOne($currentUser['val']);
                if($member) $actor = "Member: $member->login_name";

            }
            elseif($currentUser['id'] == 'account_id'){
                $acc = AccountUser::findOne($currentUser['val']);
                if($acc) {
                    $actor = "Acc: $acc->company";
                    $member = $acc->memberOwner;
                    if($member) $actor .= " Member: $member->login_name";
                }
            }
        }


        $siteLog = new SiteLog();
        $siteLog->created_date = gmdate('Y-m-d H:i:s');
        $siteLog->actor = $actor;
        $siteLog->action = $action;
        $siteLog->actor_ip_address = CommonHelper::getClientIPAddress();
        $siteLog->type = 1; // 1: normal log. 2: error
        $siteLog->save(false);
    }

    public static function addSublog($log_id, $action, $affeced_files, $type){
        $siteLog = new SiteLog();
        $siteLog->log_id = $log_id;
        $siteLog->created_date = gmdate('Y-m-d H:i:s');
        $siteLog->affected_files = $affeced_files;
        $siteLog->action = $action;
        $siteLog->actor_ip_address = CommonHelper::getClientIPAddress();
        $siteLog->type = $type; // 1: normal log. 2: error
        $siteLog->save(false);
    }
}