<?php

namespace common\component\helpers;

use common\models\AccountUser;
use common\models\Member;
use getID3;
use getid3_lib;
use Imagick;
use SplFileInfo;
use Yii;
use yii\helpers\BaseFileHelper;
use yii\helpers\Url;
use yii\web\HttpException;

class FileHelper extends BaseFileHelper
{
    const CROP_MAX_SIZE_FRAME = 580;

    const UPLOAD_FOLDER = '/uploads/';
    const SAMPLE_FOLDER = '/samples/';
    const UPLOAD_FOLDER_MEMBER = '/member/';
    const UPLOAD_FOLDER_ACCOUNT = '/account/';
    const THUMBNAILS_FOLDER = '/thumbnails/';
    const SYSTEM_RESOURCES_FOLDER = 'systemresources/';
    const UPLOAD_FILE_FOLDER_PUBLIC = '/public/';
    const REMOTE_FOLDER = '/remote/';
    const UPLOAD_FILE_FOLDER_SERVER_ACCOUNT = '/account/';
    const UPLOAD_FILE_FOLDER_SERVER_PUBLIC = '/public/';
    const UPLOAD_FILE_FOLDER_NAME = '/uploads/';
    const UPLOAD_FILE_FOLDER_AVATAR = '/avatar/';
    const UPLOAD_FILE_FOLDER_ACCOUNT_THUMB = '/thumb/';
    const APPS_FOLDER = 'apps/';

    const CACHE_FILE_FOLDER_NAME = '/manifest/';
    const PUBLISHED_KIOSKS_FOLDER = '/pubkiosks/';
    const PUBLISHED_SIGNS_FOLDER = '/pubsigns/';
    const BACKUP_PUBLISHED_KIOSKS_FOLDER = '/backup_pubkiosks/';
    const PUBLISHED_ITEMS_FOLDER = '/pubitems/';
    const PUBLISHED_GROUPS_FOLDER = '/pubgroups/';
    const DOWNLOAD_FILE_FOLDER_NAME = '/downloads/';
    const UPLOAD_FILE_FOLDER_TEMPLATE_APPCACHE = '/template/appcache/';
    const UPLOAD_FILE_FOLDER_TEMPLATE = '/template/';
    const TEMP_ZIP_FOLDER = '/tempzip/';

    const ENABLE_CAPTURE_SCREENSHOT = false;
    const EXCEL_2010 = "excel_2010";
    const FILE_AUDIO_PREFIX = 'audio';
    const FILE_ZIP_PREFIX = 'zip';

    static $VIDEO_TYPES = array('avi', 'mp4', 'mpeg', 'flv', 'mov', 'm4v');
    static $IMAGE_TYPES = array('jpg', 'jpeg', 'png', 'gif', 'bmp', 'svg');

    public static function get_root_folder()
    {
        return Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR;
    }

    public static function getUploadFolderRealPath()
    {
        return Yii::getAlias("@uploads") . DIRECTORY_SEPARATOR;
    }

    public static function getUploadPath($getFullPath = false)
    {
        if (CommonHelper::isLocalHostRequest())
            return Url::home() . FileHelper::UPLOAD_FOLDER . DIRECTORY_SEPARATOR;
        else
            return Url::base(true) . FileHelper::UPLOAD_FOLDER . DIRECTORY_SEPARATOR;
    }

    /*
    **
    * Copy a file, or recursively copy a folder and its contents
    * @param       string   $source    Source path
    * @param       string   $dest      Destination path
    * @param       string   $permissions New folder creation permissions
    * @return      bool     Returns true on success, false on failure
    */
    public static function xcopy($source, $dest, $permissions = 0755)
    {
        // Check for symlinks
        if (is_link($source)) {
            return symlink(readlink($source), $dest);
        }

        // Simple copy for a file
        if (is_file($source)) {
            return copy($source, $dest);
        }

        // Make destination directory
        if (!is_dir($dest)) {
            mkdir($dest, $permissions);
        }

        // Loop through the folder
        $dir = dir($source);
        while (false !== $entry = $dir->read()) {
            // Skip pointers
            if ($entry == '.' || $entry == '..') {
                continue;
            }

            // Deep copy directories
            self::xcopy("$source/$entry", "$dest/$entry");
        }

        // Clean up
        $dir->close();
        return true;
    }

    /**
     * @param $fromDir - should have the slash in the end of the path
     * @param $toDir - should have the slash in the end of the path
     * @param $fileName
     * @param bool|false $useNewFile
     * @return string
     */
    public static function copyPasteFile($fromDir, $toDir, $fileName, $useNewFile = true)
    {
        $ext = pathinfo($fileName, PATHINFO_EXTENSION);
        $newFileName = CommonHelper::getRandomStringByUnixTimetemp(10) . ".$ext";
        if(!$useNewFile) $newFileName = $fileName;
        if (!empty($fileName)) {
            $copyFilePath = $fromDir . $fileName;
            if (file_exists($copyFilePath)) {
                $pastingFilePath = $toDir . $newFileName;
                copy($copyFilePath, $pastingFilePath);
                chmod($pastingFilePath, 0777);
            }
        }
        return $newFileName;
    }
    
    public static function GetNewThumbImg($img, $pathFolderImg, $w, $h, $container_name = '', $defaultImage = '', $useHOS = false)
    {
        if (!$useHOS) {
            return FileHelper::getThumbnail($img, $pathFolderImg, $w, $h, true);
        } else {


            $savePath = FileHelper::getUploadFolderRealPath() . FileHelper::THUMBNAILS_FOLDER . $img;
            $savePath = str_replace('//', '/', $savePath);


            //check file exist first to start connect with HP Object Storage
            if (!is_file($savePath) && $img != '') {
                //pull content from HOS and save in file
                $savePath = HpCloudHelper::pullObjectContentToFile($container_name, $pathFolderImg . $img, $savePath);
            }

            $info = new SplFileInfo($savePath);
            $ext = $info->getExtension();
            //$baseUrl = $getFullUrl ? Url::base(true) : Url::base();
            $baseUrl = Url::base(true);
            if ($savePath != '' && is_file($savePath)) {
                $thumbnailUrl = FileHelper::getThumbnail($img, FileHelper::THUMBNAILS_FOLDER, $w, $h, true);
                //after getting thumbnail, remove origin file
                if (file_exists($savePath) && is_file($savePath)) {
                    if ($ext != "svg") {

                        /*to avoid long time load, the origin image should be keep on thumbnails for next page load,
                        SS will check if original image exist, it would use it to gen thumbnail
                        it means that we will not remove original image on first fetch
                        NOTES: we could not use thumbnail it self because its name is different with original image name
                        */

                        //unlink($savePath);
                    }
                }
            } else {
                if ($defaultImage != "")
                    $thumbnailUrl = $baseUrl . FileHelper::UPLOAD_FOLDER . $defaultImage;
                else
                    return $defaultImage;
            }
            return $thumbnailUrl;
        }
    }

    public static function getThumbnailForPDF($img, $pathFolderImg, $w, $h)
    {
        if ($img != '') {
            $info = pathinfo($img);
            // user wants to shrink to fit
            if ($pathFolderImg == FileHelper::UPLOAD_FOLDER) {
                $realImage = $img;
            } else {
                $realImage = $img;
            }

            $realImage = str_replace('//', '/', $realImage);
            $path = Url::base();

            if (file_exists($realImage) && is_file($realImage) && $info['extension'] != "svg") {
                $file_name = $info['filename'] . '_thumb_' . $w . '_' . $h . '.' . $info['extension'];
                // user wants to shrink to fit
                if ($pathFolderImg == FileHelper::UPLOAD_FOLDER) {
                    $pathThumbImg = FileHelper::getUploadFolderRealPath() . $file_name;
                } else {
                    $pathThumbImg = FileHelper::getUploadFolderRealPath() . FileHelper::THUMBNAILS_FOLDER . $file_name;
                }
                $pathThumbImg = str_replace('//', '/', $pathThumbImg);

//                if (file_exists($pathThumbImg) && is_file($pathThumbImg)) {
//
//                    @chmod($pathThumbImg, 0755);
//
//                    unlink($pathThumbImg);
//                }

                if (!file_exists($pathThumbImg)) {
                    //if (1 == 1) {
                    $pathToThumb = FileHelper::getUploadFolderRealPath() . FileHelper::THUMBNAILS_FOLDER;
                    $pathToThumb = str_replace('//', '/', $pathToThumb);
                    if (!file_exists($pathToThumb)) {
                        mkdir($pathToThumb, 0755);
                    }
                    @chmod($pathToThumb, 0755);

                    //Check if GD extension is loaded
                    if (!extension_loaded('gd') && !extension_loaded('gd2')) {
                        throw new HttpException(sprintf("%s %s %s", __FILE__, __LINE__, __FUNCTION__), 'GD is not loaded', 405);
                    }
                    //Get Image size info
                    $imgInfo = getimagesize($realImage);
                    switch ($imgInfo[2]) {
                        case 1:
                            $im = imagecreatefromgif($realImage);
                            break;
                        case 2:
                            $im = imagecreatefromjpeg($realImage);
                            break;
                        case 3:
                            $im = imagecreatefrompng($realImage);
                            break;
                        default:
                            throw new HttpException(sprintf("%s %s %s", __FILE__, __LINE__, __FUNCTION__), 'Could not get image size of ' . $realImage, 405);
                            break;
                    }

                    //Get Image size info

                    $imgInfo = getimagesize($realImage);
                    switch ($imgInfo[2]) {
                        case 1:
                            $im = imagecreatefromgif($realImage);
                            break;
                        case 2:
                            $im = imagecreatefromjpeg($realImage);
                            break;
                        case 3:
                            $im = imagecreatefrompng($realImage);
                            break;
                        default:
                            throw new HttpException(sprintf("%s %s %s", __FILE__, __LINE__, __FUNCTION__), 'Could not get image size of ' . $realImage, 405);
                            break;
                    }

                    if (($imgInfo[0] <= $w && $imgInfo[1] <= $h) || ($w == 0 && $h == 0)) { //If image dimension is smaller, do not resize
                        $nHeight = $imgInfo[1];
                        $nWidth = $imgInfo[0];
                    } else {
                        // resize it, but keep it proportional
                        if ($w == 0) $factor = $h / $imgInfo[0];
                        elseif ($h == 0) $factor = $w / $imgInfo[1];
                        else $factor = min($w / $imgInfo[0], $h / $imgInfo[1]);
                        $nWidth = $imgInfo[0] * $factor;
                        $nHeight = $imgInfo[1] * $factor;
                    }

                    $nWidth = round($nWidth);
                    $nHeight = round($nHeight);
                    $newImg = imagecreatetruecolor($nWidth, $nHeight);

                    /* Check if this image is PNG or GIF, then set if Transparent*/
                    if (($imgInfo[2] == 1) OR ($imgInfo[2] == 3)) {
                        imagealphablending($newImg, false);
                        imagesavealpha($newImg, true);
                        $transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 127);
                        imagefilledrectangle($newImg, 0, 0, $nWidth, $nHeight, $transparent);
                    }
                    imagecopyresampled($newImg, $im, 0, 0, 0, 0, $nWidth, $nHeight, $imgInfo[0], $imgInfo[1]);

                    //Generate the file, and rename it to $newfilename
                    switch ($imgInfo[2]) {
                        case 1:
                            imagegif($newImg, $pathThumbImg);
                            break;
                        case 2:
                            imagejpeg($newImg, $pathThumbImg);
                            break;
                        case 3:
                            imagepng($newImg, $pathThumbImg);
                            break;
                        default:
                            throw new HttpException(sprintf("%s %s %s", __FILE__, __LINE__, __FUNCTION__), 'Could not get image size of ' . $realImage, 405);
                            break;
                    }

                    //move it to user's upload folder
                    //user wants to shrink to fit
                    if ($pathFolderImg == FileHelper::UPLOAD_FOLDER) {
                        $pathThumbImg = $path . FileHelper::UPLOAD_FOLDER . $file_name;
                    } else {
                        $pathThumbImg = $path . FileHelper::UPLOAD_FOLDER . FileHelper::THUMBNAILS_FOLDER . $file_name;
                    }
                    $pathThumbImg = str_replace('//', '/', $pathThumbImg);
                    return $pathThumbImg;
                }

                $pathThumbImg = $path . FileHelper::UPLOAD_FOLDER . FileHelper::THUMBNAILS_FOLDER . $file_name;
                $pathThumbImg = str_replace('//', '/', $pathThumbImg);
                return $pathThumbImg;
            }
            if ($info['extension'] == "svg" && file_exists($realImage) && is_file($realImage)) {
                if ($info['extension'] == "svg") {

                    $pathThumbImg = $path . FileHelper::UPLOAD_FOLDER . $pathFolderImg . $img;
                    $pathThumbImg = str_replace('//', '/', $pathThumbImg);
                    return $pathThumbImg;
                } else {
                    $pathThumbImg = $path . FileHelper::UPLOAD_FOLDER . FileHelper::THUMBNAILS_FOLDER . $img;
                    $pathThumbImg = str_replace('//', '/', $pathThumbImg);
                    return $pathThumbImg;
                }
            } else {
                return Url::base() . FileHelper::UPLOAD_FOLDER . 'no-image.png';
            }
        } else {
            return Url::base() . FileHelper::UPLOAD_FOLDER . 'no-image.png';
        }
    }

    public static function getThumbnail($img, $pathFolderImg, $w, $h, $emptyIfFileNotFound = false)
    {
        if ($img != '') {
            //$info = pathinfo($img);

            $fileName = pathinfo($img, PATHINFO_FILENAME);
            $fileExt = pathinfo($img, PATHINFO_EXTENSION);
            if ($fileExt != null) {


                // user wants to shrink to fit
                if ($pathFolderImg == FileHelper::UPLOAD_FOLDER) {
                    $realImage = FileHelper::getUploadFolderRealPath() . $img;
                } else {
                    $realImage = FileHelper::getUploadFolderRealPath() . $pathFolderImg . $img;

                }

                $realImage = str_replace('//', '/', $realImage);
                $realImage = CommonHelper::sanitizePath($realImage);

                //$path = $getFullUrl ? Url::base(true) : Url::base();
                $path = Url::base(true);

                /***  if file does exist, check thumbnail ***/
                if (file_exists($realImage) && is_file($realImage) && $fileExt != "svg" && $fileExt != "gif") {

                    $file_name = $fileName . '_thumb_' . $w . '_' . $h . '.' . $fileExt;
                    $thumbUrl = FileHelper::getUploadPath(true) . 'thumbnails/' . $file_name;

                    // user wants to shrink to fit
                    if ($pathFolderImg == FileHelper::UPLOAD_FOLDER) {
                        $pathThumbImg = FileHelper::getUploadFolderRealPath() . $file_name;
                        $thumbUrl = FileHelper::getUploadFolderRealPath() . $file_name;
                    } else {
                        $pathThumbImg = FileHelper::getUploadFolderRealPath() . FileHelper::THUMBNAILS_FOLDER . $file_name;
                    }

                    $pathThumbImg = CommonHelper::sanitizePath($pathThumbImg);


                    /***  if the thumbnail of image does not exist, create one ***/
                    if (!file_exists($pathThumbImg) && $fileExt != "svg" && $fileExt != "gif") {
                        /*
                        $pathToThumb = FileHelper::getUploadFolderRealPath() . FileHelper::THUMBNAILS_FOLDER;
                        $pathToThumb = str_replace('//', '/', $pathToThumb);

                        @chmod($pathToThumb, 0755);
                        */

                        self::createBackgroundForThumbImage($realImage, $w, $h, $pathThumbImg, false);

                    }

                    /*
                    if ($pathFolderImg == FileHelper::UPLOAD_FOLDER) {
                        $pathThumbImg = $path . str_replace('//', '/', FileHelper::UPLOAD_FOLDER . $file_name);
                    }
                    else {
                        $pathThumbImg = $path . str_replace('//', '/', FileHelper::UPLOAD_FOLDER . FileHelper::THUMBNAILS_FOLDER . $file_name);
                    }
                    */
                    return $thumbUrl;


                }

                if (($fileExt == "svg" || $fileExt == "gif") && file_exists($realImage) && is_file($realImage)) {
                    if ($fileExt == "svg" || $fileExt == "gif") {

                        $pathThumbImg = $path . str_replace('//', '/', FileHelper::UPLOAD_FOLDER . $pathFolderImg . $img);
                        return $pathThumbImg;
                    } else {
                        $pathThumbImg = $path . str_replace('//', '/', FileHelper::UPLOAD_FOLDER . FileHelper::THUMBNAILS_FOLDER . $img);
                        return $pathThumbImg;
                    }
                } else {
                    return !$emptyIfFileNotFound ? Url::base() . FileHelper::UPLOAD_FOLDER . 'no-image.png' : "";
                }
            }
        } else {
            return !$emptyIfFileNotFound ? Url::base() . FileHelper::UPLOAD_FOLDER . 'no-image.png' : "";
        }
    }

    
    public static function getThumbnailImageFromVideo($account_id, $model)
    {
        $member_id = Member::getLoginId();
        $uploaded_file = $model->upload_file;
        $pathToFile = FileHelper::getFileRealPathUponUserViewing($uploaded_file);
        if (file_exists($pathToFile) && is_file($pathToFile)) {
            $fileName = pathinfo($pathToFile, PATHINFO_FILENAME) . '-video-captured.png';

            $containerDir = pathinfo($pathToFile, PATHINFO_DIRNAME);
            $captureImagePath = $containerDir . DIRECTORY_SEPARATOR . $fileName;
            $pathToFile = str_replace('//', '/', $pathToFile);
            $captureImagePath = str_replace('//', '/', $captureImagePath);
            //$captureImagePath = str_replace('\\', '/', $captureImagePath);

            $pathToFile = preg_replace('/\\\\/', '/', $pathToFile);
            $captureImagePath = preg_replace('/\\\\/', '/', $captureImagePath);

            $ffmpeg = 'ffmpeg';
            if (!CommonHelper::isLocalHostRequest()) {
                $ffmpeg = '/usr/local/bin/ffmpeg'; // depend where plugin is installed, if ffmpreg is on usr/bin, we don't set this prefix path
            }
            $command = $ffmpeg . ' -i ' . $pathToFile . ' -r 1 -ss 00:00:01 -r 1 ' . $captureImagePath . '';
            Yii::info('getThumbnailImageFromVideo cmd:' . $command);
            //Yii::info($command);
            shell_exec($command);


            /*
             Generated command on the server will look like this

            /usr/local/bin/ffmpeg -i /home/sigsmart/public_html/dev/frontend/web/uploads/Op65MkmjZSfU4hYLB1z5/member/charlie_x4WCr.mp4 -r 1 -ss 00:00:01 -r 1 /home/sigsmart/public_html/dev/frontend/web/uploads/Op65MkmjZSfU4hYLB1z5/member/charlie_x4WCr-video-captured.png
             * */

            //generate item image
            $imageType = Item::find()->where(['item_extension' => ObjectToolbar::IMAGE_EXT])->one();
            $currentUser = PermissionHelper::getUserAccessing();
            $newImageItem = new ItemVariant();
            $newImageItem->$currentUser['id'] = $currentUser['val'];
            $newImageItem->item_id = $imageType->item_id;
            $newImageItem->name = 'Generated image from video: ' . $model->name;
            $newImageItem->upload_file = $fileName;
            $newImageItem->setCreatedDate();
            $newImageItem->save();
            FileHelper::storeCapturedImageFromVideo($account_id, $newImageItem->iv_id, $fileName);
            return $newImageItem->iv_id;
        } else {
            return "";
            ////throw new CHttpException('Commons 4097', 'Could not capture first image from video.');
        }
    }

    public static function removeVideoThumb($video_file_name, $id, $type)
    {
        $fileWoExt = pathinfo($video_file_name, PATHINFO_FILENAME);
        $thumbName = $fileWoExt . '-video-captured.png';
        FileHelper::removeFileFromUser($thumbName, $id, $type);
    }

    public static function storeCapturedImageFromVideo($account_id, $iv_id, $filename)
    {
        if ($account_id > 0) {
            $uploadedFileSize = StorageHelper::getFileSizeInAccountFolder($account_id, $filename);
            $storage = AccountStorage::find()->where(['assoc_iv_id' => $iv_id, 'type' => StorageHelper::USAGE_IV_PREVIEW_ICON])->one();
            if ($storage == null) {
                $storage = new AccountStorage();
            }
            $storage->account_id = $account_id;
            $storage->type = StorageHelper::USAGE_IV_PREVIEW_ICON;
            $storage->assoc_iv_id = $iv_id;
            $storage->file_size = $uploadedFileSize;
            $storage->save(false);
        } else {
            $userid = Member::getLoginId();
            $uploadedFileSize = StorageHelper::getFileSizeInMemberFolder($userid, $filename);
            $storage = MemberStorage::find()->where(['assoc_iv_id' => $iv_id, 'type' => StorageHelper::USAGE_IV_PREVIEW_ICON])->one();
            if ($storage == null) {
                $storage = new MemberStorage();
            }
            $storage->member_id = $userid;
            $storage->type = StorageHelper::USAGE_IV_PREVIEW_ICON;
            $storage->assoc_iv_id = $iv_id;
            $storage->file_size = $uploadedFileSize;
            $storage->save(false);
        }

    }


    public static function createBackgroundForThumbImage($realImage, $w, $h, $outputPath, $putInFrame = false)
    {
        //Check if GD extension is loaded
        if (!extension_loaded('gd') && !extension_loaded('gd2')) {
            throw new HttpException(sprintf("%s %s %s", __FILE__, __LINE__, __FUNCTION__), 'GD is not loaded', 405);
        }

        //Get Image size info
        $imgInfo = getimagesize($realImage);

        $im = self::loadImageResource($imgInfo[2], $realImage);

        if (($imgInfo[0] <= $w && $imgInfo[1] <= $h) || ($w == 0 && $h == 0)) { //If image dimension is smaller, do not resize
            $nHeight = $imgInfo[1];
            $nWidth = $imgInfo[0];
        } else {
            // resize it, but keep it proportional
            if ($w == 0) $factor = $h / $imgInfo[0];
            elseif ($h == 0) $factor = $w / $imgInfo[1];
            else $factor = min($w / $imgInfo[0], $h / $imgInfo[1]);
            $nWidth = $imgInfo[0] * $factor;
            $nHeight = $imgInfo[1] * $factor;
        }

        $nWidth = round($nWidth);
        $nHeight = round($nHeight);
        $marge_right = 0;
        $marge_bottom = 0;
        $scaledWidth = $nWidth;
        $scaledHeight = $nHeight;
        if ($putInFrame) {
            $marge_right = ($w - $nWidth) / 2;
            $marge_bottom = ($h - $nHeight) / 2;
            $nWidth = $w;
            $nHeight = $h;
        }

        $newImg = imagecreatetruecolor($nWidth, $nHeight);
        imagealphablending($newImg, false);
        imagesavealpha($newImg, true);
        $transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 127);
        imagefilledrectangle($newImg, 0, 0, $nWidth, $nHeight, $transparent);
        imagecopyresampled($newImg, $im, $marge_right, $marge_bottom, 0, 0, $scaledWidth, $scaledHeight, $imgInfo[0], $imgInfo[1]);

        self::createImageByResource($imgInfo[2], $newImg, $outputPath);
    }

    public static function loadImageResource($type, $realImage)
    {
        switch ($type) {
            case 1:
                $im = imagecreatefromgif($realImage);
                break;
            case 2:
                $im = imagecreatefromjpeg($realImage);
                break;
            case 3:
                $im = imagecreatefrompng($realImage);
                break;
            case 6:
                $im = self::ImageCreateFromBMP($realImage);
                break;
            default:
                Yii::info(sprintf("%s %s %s", __FILE__, __LINE__, __FUNCTION__) . " error loadImageResource: $type $realImage");
                // delete if psd extension
                $extension = pathinfo($realImage, PATHINFO_EXTENSION);
//                if ($extension == 'psd')
//                    FileHelper::deleteThisFile($realImage);
//                else
                //throw new HttpException(sprintf("%s %s %s", __FILE__, __LINE__, __FUNCTION__), 'Could not get image size of ' . $realImage, 405);
                break;
        }
        return $im;
    }

    public static function ImageCreateFromBMP($filename)
    {
        //Ouverture du fichier en mode binaire
        if (!$f1 = fopen($filename, "rb")) return FALSE;

        //1 : Chargement des ent�tes FICHIER
        $FILE = unpack("vfile_type/Vfile_size/Vreserved/Vbitmap_offset", fread($f1, 14));
        if ($FILE['file_type'] != 19778) return FALSE;

        //2 : Chargement des ent�tes BMP
        $BMP = unpack('Vheader_size/Vwidth/Vheight/vplanes/vbits_per_pixel' .
            '/Vcompression/Vsize_bitmap/Vhoriz_resolution' .
            '/Vvert_resolution/Vcolors_used/Vcolors_important', fread($f1, 40));
        $BMP['colors'] = pow(2, $BMP['bits_per_pixel']);
        if ($BMP['size_bitmap'] == 0) $BMP['size_bitmap'] = $FILE['file_size'] - $FILE['bitmap_offset'];
        $BMP['bytes_per_pixel'] = $BMP['bits_per_pixel'] / 8;
        $BMP['bytes_per_pixel2'] = ceil($BMP['bytes_per_pixel']);
        $BMP['decal'] = ($BMP['width'] * $BMP['bytes_per_pixel'] / 4);
        $BMP['decal'] -= floor($BMP['width'] * $BMP['bytes_per_pixel'] / 4);
        $BMP['decal'] = 4 - (4 * $BMP['decal']);
        if ($BMP['decal'] == 4) $BMP['decal'] = 0;

        //3 : Chargement des couleurs de la palette
        $PALETTE = array();
        if ($BMP['colors'] < 16777216) {
            $PALETTE = unpack('V' . $BMP['colors'], fread($f1, $BMP['colors'] * 4));
        }

        //4 : Cr�ation de l'image
        $IMG = fread($f1, $BMP['size_bitmap']);
        $VIDE = chr(0);

        $res = imagecreatetruecolor($BMP['width'], $BMP['height']);
        $P = 0;
        $Y = $BMP['height'] - 1;
        while ($Y >= 0) {
            $X = 0;
            while ($X < $BMP['width']) {
                if ($BMP['bits_per_pixel'] == 24)
                    $COLOR = unpack("V", substr($IMG, $P, 3) . $VIDE);
                elseif ($BMP['bits_per_pixel'] == 16) {
                    $COLOR = unpack("n", substr($IMG, $P, 2));
                    $COLOR[1] = $PALETTE[$COLOR[1] + 1];
                } elseif ($BMP['bits_per_pixel'] == 8) {
                    $COLOR = unpack("n", $VIDE . substr($IMG, $P, 1));
                    $COLOR[1] = $PALETTE[$COLOR[1] + 1];
                } elseif ($BMP['bits_per_pixel'] == 4) {
                    $COLOR = unpack("n", $VIDE . substr($IMG, floor($P), 1));
                    if (($P * 2) % 2 == 0) $COLOR[1] = ($COLOR[1] >> 4);
                    else $COLOR[1] = ($COLOR[1] & 0x0F);
                    $COLOR[1] = $PALETTE[$COLOR[1] + 1];
                } elseif ($BMP['bits_per_pixel'] == 1) {
                    $COLOR = unpack("n", $VIDE . substr($IMG, floor($P), 1));
                    if (($P * 8) % 8 == 0) $COLOR[1] = $COLOR[1] >> 7;
                    elseif (($P * 8) % 8 == 1) $COLOR[1] = ($COLOR[1] & 0x40) >> 6;
                    elseif (($P * 8) % 8 == 2) $COLOR[1] = ($COLOR[1] & 0x20) >> 5;
                    elseif (($P * 8) % 8 == 3) $COLOR[1] = ($COLOR[1] & 0x10) >> 4;
                    elseif (($P * 8) % 8 == 4) $COLOR[1] = ($COLOR[1] & 0x8) >> 3;
                    elseif (($P * 8) % 8 == 5) $COLOR[1] = ($COLOR[1] & 0x4) >> 2;
                    elseif (($P * 8) % 8 == 6) $COLOR[1] = ($COLOR[1] & 0x2) >> 1;
                    elseif (($P * 8) % 8 == 7) $COLOR[1] = ($COLOR[1] & 0x1);
                    $COLOR[1] = $PALETTE[$COLOR[1] + 1];
                } else
                    return FALSE;
                imagesetpixel($res, $X, $Y, $COLOR[1]);
                $X++;
                $P += $BMP['bytes_per_pixel'];
            }
            $Y--;
            $P += $BMP['decal'];
        }

        //Fermeture du fichier
        fclose($f1);

        return $res;
    }

    public static function createImageByResource($type, $my_img, $realImage)
    {
        switch ($type) {
            case 1:
                imagegif($my_img, $realImage);
                break;
            case 2:
                imagejpeg($my_img, $realImage);
                break;
            case 3:
                imagepng($my_img, $realImage);
                break;
            case 6:
                imagepng($my_img, $realImage);
                break;
            default:
                //throw new HttpException(sprintf("%s %s %s", __FILE__, __LINE__, __FUNCTION__), 'Could not get image size of ' . $realImage, 405);
                break;
        }

    }

    public static function createTextFile($path, $file_name = "")
    {
        @chmod($path, 0777);
        if (empty($file_name))
            $file_name = CommonHelper::uniquename('textfile', 'txt');
        $filePath = $path . $file_name;
        $createFolder = fopen($filePath, 'w') or die('Cannot open file:  ' . $filePath);
        fwrite($createFolder, "");
        fclose($createFolder);
    }

    /**
     * @param $img - image name :string
     * @param $w
     * @param $h
     * @param string $defaultName
     * @return mixed|string
     */
    public static function GetThumbImgUponViewingUser($img, $w, $h, $defaultName = '')
    {
        $currentUser = PermissionHelper::getUserAccessing();
        //the below function cause the crop image thumb on regularcontent would be removed
        //FileHelper::removeBookmarkThumbImageFromUser($img, $currentUser['val'], $currentUser["id"]== "member_id"? 1:2);
        if ($currentUser["id"] == "member_id") {
            return FileHelper::GetMemberThumbImg($img, $w, $h, $currentUser["val"], $defaultName);
        } elseif ($currentUser["id"] == "account_id") {
            return FileHelper::GetAccountThumbImg($img, $w, $h, $currentUser["val"], $defaultName);
        }
        return "";
    }

    public static function getThumbUrlOfSvg($svgFileName, $width, $height)
    {
        $filePath = FileHelper::getFileRealPathUponUserViewing($svgFileName);

        $path_parts = pathinfo($filePath);

        if ($path_parts['extension'] == 'svg') {
            $svgThumbFileName = $path_parts['filename'] . "_{$width}_{$height}.png";

            //$svgThumbFilePath = FileHelper::getFileRealPathUponUserViewing($svgThumbFileName);
            $svgThumbFilePath = $path_parts['dirname'] . DIRECTORY_SEPARATOR . $svgThumbFileName;

            if (!file_exists($svgThumbFilePath)) {
                FileHelper::convertSvgToPng($filePath, $svgThumbFilePath, $width, $height);
            }
            $svgFileName = $svgThumbFileName;
        }

        return FileHelper::GetThumbImgUponViewingUser($svgFileName, $width, $height);
    }

    public static function svgScaleHack($svg, $minWidth, $minHeight)
    {
        $reW = '/(.*<svg[^>]* width=")([\d.]+px)(.*)/si';
        $reH = '/(.*<svg[^>]* height=")([\d.]+px)(.*)/si';
        preg_match($reW, $svg, $mw);
        preg_match($reH, $svg, $mh);
        $width = floatval($mw[2]);
        $height = floatval($mh[2]);

        if (!$width || !$height) return false;

        // scale to make width and height big enough
        $scale = 1;
        if ($width < $minWidth)
            $scale = $minWidth / $width;
        if ($height < $minHeight)
            $scale = max($scale, ($minHeight / $height));

        $width *= $scale * 2;
        $height *= $scale * 2;

        $svg = preg_replace($reW, "\${1}{$width}px\${3}", $svg);
        $svg = preg_replace($reH, "\${1}{$height}px\${3}", $svg);

        return $svg;
    }

    public static function convertSvgToPng($urlSVG, $urlPNG, $width = 100, $height = 100, $type = 'png')
    {
        //error_reporting(E_ALL);
        //phpinfo();
        //exit;
        $im = new Imagick();
        $svg = file_get_contents($urlSVG);
        $_svgdata = FileHelper::svgScaleHack($svg, $width, $height);

        if ($_svgdata != "")
            $svg = $_svgdata;
        $im->readImageBlob($svg);
        if ($type == 'png') {
            $im->setImageFormat("png32");
            $im->resizeImage($width, $height, imagick::FILTER_LANCZOS, 1); /*Optional, if you need to resize*/
        } else {
            $im->setImageFormat("jpeg");
            $im->adaptiveResizeImage(720, 445); /*Optional, if you need to resize*/
        }
        $im->writeImage($urlPNG); /*(or .jpg)*/
        $im->clear();
        $im->destroy();
    }


    /**
     * generate a member upload folder with a random name with permission 777
     * return the name of member upload folder
     * @return member upload folder name that is inside of /uploads/
     */
    public static function get_folder_upload()
    {
        $randomName = CommonHelper::getRandomStringByUnixTimetemp();

        //this directory contains both /member and /accounts
        $path_member_global_folder = FileHelper::getUploadFolderRealPath() . $randomName;

        if (!is_dir($path_member_global_folder)) {
            FileHelper::createDirectory($path_member_global_folder, 0777);
        }

        $path_folder_member = $path_member_global_folder . '/member/';
        $path_folder_member_thumb = $path_folder_member . '/thumb/';
        $path_folder_account = $path_member_global_folder . '/account/';
        $path_folder_member_avatar = $path_folder_member . '/avatar/';

        if (!is_dir($path_folder_member)) {
            FileHelper::createDirectory($path_folder_member, 0777);
        }

        if (!is_dir($path_folder_member_thumb)) {
            FileHelper::createDirectory($path_folder_member_thumb, 0777);
        }

        if (!is_dir($path_folder_account)) {
            FileHelper::createDirectory($path_folder_account, 0777);
        }

        if (!is_dir($path_folder_member_avatar)) {
            FileHelper::createDirectory($path_folder_member_avatar, 0777);
        }
        return $randomName;
    }

    public static function getFileName($path)
    {
        $path = str_replace('\\', '/', $path);
        $info = new SplFileInfo($path);
        return $info->getFilename();
    }

    public static function getExtension($path)
    {
        $info = new SplFileInfo($path);
        return $info->getExtension();
    }

    public static function isImage($filename)
    {
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        $imageExts = FileHelper::$IMAGE_TYPES;
        foreach ($imageExts as $imgExt) {
            if (strpos($imgExt, $ext) !== false)
                return true;
        }
        return false;
    }

    public static function removeFileFromUser($file_name, $id, $type)
    {
        $fileLocation = "";
        if ($type == 1 || $type == 'member_id') $fileLocation = FileHelper::getFileRealPathInMemberFolder($id, $file_name);
        elseif ($type == 2 || $type == 'account_id') $fileLocation = FileHelper::getFileRealPathInAccountFolder($id, $file_name);
        elseif ($type == 0) $fileLocation = FileHelper::getUploadFolderRealPath() . FileHelper::SYSTEM_RESOURCES_FOLDER . $file_name;


        if (file_exists($fileLocation) && is_file($fileLocation)) {

            if (FileHelper::isImage($file_name)) {
                /*** if the file is an image, get all related thumbnail of image and remove them ***/
                $imageInfo = pathinfo($fileLocation);
                $infoPath = FileHelper::getUploadFolderRealPath() . 'thumbnails/';
                $infoPath = str_replace('//', '/', $infoPath);


                foreach (glob($infoPath . $imageInfo['filename'] . '*.' . $imageInfo['extension']) as $filePath) {
                    $filePath = str_replace('//', '/', $filePath);

                    if (file_exists($filePath) && is_file($filePath)) {
                        unlink($filePath);
                    }
                }
            }

            unlink($fileLocation);
        }
    }

    /**
     * @param $dir
     * @param bool $keepDir remove all file but keeping top dir
     */
    public static function recursiveRemove($dir, $keepDir = true)
    {
        $structure = glob(rtrim($dir, "/") . '/*');
        if (is_array($structure)) {
            foreach ($structure as $file) {
                if (is_dir($file))
                    self::recursiveRemove($file);
                elseif (is_file($file)) unlink($file);
            }
        }
        if (!$keepDir) rmdir($dir);
    }


    public static function get_type_file_upload($filename)
    {
        $type_file = 0;
        $ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));

        switch ($ext) {
            case 'jpg':
            case 'jpeg':
            case 'png':
            case 'tif':
            case 'tiff':
            case 'gif':
            case 'bmp':
            case 'thm':
            case 'ico':
            case 'svg':
                $type_file = ObjectToolbar::IMAGE_EXT; // Image  Resources::IMAGE_FILE_TYPE
                break;
            case 'mpeg':
            case 'mpg':
            case 'mp4':
            case 'm4v':
            case 'mov':
            case 'avi':
            case 'wmv':
            case 'flc':
            case 'flac':
            case 'flv':
                $type_file = ObjectToolbar::VIDEO_EXT; // Video  Resources::VIDEO_FILE_TYPE
                break;
            case 'webapp':
            case 'iosapp':
            case 'osxapp':
            case 'aosapp':
            case 'winapp':
                $type_file = ObjectToolbar::ITEM_APP_EXT;
                break;
            case 'mp3':
            case 'wav':
            case 'm4a':
                $type_file = ObjectToolbar::MUSIC_EXT; // Audio
                break;
            case 'txt':
            case 'doc':
            case 'pdf':
            case 'csf':
            case 'psd':
            case 'eps':
            case 'docx':
            case 'xls':
            case 'xlsx':
            case 'ppt':
            case 'pptx':
            case 'rtf':
            case 'csv':
                $type_file = ObjectToolbar::DOCUMENT_EXT; // doc
                break;
            case 'zip':
                $type_file = 0; // zip type
                break;
            default:
                $type_file = 0; // Others

        }
        return $type_file;
    }

    /**
     * @param $id : number
     * @param $type : 1 or 'member_id', 2 or 'account_id'
     * @return string
     */
    public static function getUserFolderRealPath($id, $type)
    {
        return CommonHelper::sanitizePath(FileHelper::getUploadFolderRealPath() . FileHelper::get_folder_upload_file($id, $type));
    }

    public static function getVideoInfo($filePath)
    {
        //set permission before get info of the video
        $filePath = CommonHelper::sanitizePath($filePath);
        $videoInfo = null;
        if (file_exists($filePath)) {

            chmod($filePath, 0777);
            $importPath = Yii::getAlias('@vendor/getid3/');
            include_once($importPath . 'getid3.php');

            $getID3 = new getID3;
            $videoInfo = $getID3->analyze($filePath);
            getid3_lib::CopyTagsToComments($videoInfo);
        }
        return $videoInfo;
    }

    public static function convertVideoToMp4($inputPath, $outputPath)
    {
        if (file_exists($inputPath)) {
            $input = $inputPath;
            $output = $outputPath;
            //$input = escapeshellarg($inputPath); // this line make the getId3 failed because it automatically added the single quote into the path
            //$output = escapeshellarg($outputPath);

            /** Get video size **/

            $videoInfo = FileHelper::getVideoInfo($inputPath);

            $dimension = sprintf('%sx%s', $videoInfo['video']['resolution_x'], $videoInfo['video']['resolution_y']);
            /** Get video size **/

            /*
            $command = "mencoder $input -o $output -af volume=10 -aspect 16:9 -of avi -noodml -ovc x264 -x264encopts bitrate=300:level.idc=41:bframes=3:frameref=2: nopsnr: nossim: pass=1: threads=auto -oac mp3lame ";
            $command = "ffmpeg -y -i $input -acodec libfaac -ab 96k -vcodec libx264 -vpre slower -vpre main -level 21 -refs 2 -b 345k -bt 345k -threads 0 -s 640x360 $output";
            //most recently command
            $command = "ffmpeg -i $input -acodec copy -vcodec copy -scodec copy $output";
            */
            $ffmpeg = 'ffmpeg';
            if (!CommonHelper::isLocalHostRequest()) {
                $ffmpeg = '/usr/local/bin/ffmpeg'; // depend where plugin is installed, if ffmpreg is on usr/bin, we don't set this prefix path
            }

            $command = "$ffmpeg -i $input $output";

            if (!CommonHelper::isLocalHostRequest()) {
                //$command = "ffmpeg -i $input -ar 44100 -b 300k -s $dimension -vcodec mpeg4 $output";
                $ffmpeg = '/usr/local/bin/ffmpeg';
                $command = "$ffmpeg -i $input -vcodec h264 -acodec aac -strict -2 $output";
            }
			Yii::info($command);
            exec($command . " 2>&1", $out, $ret);
            if ($ret) {
//                echo "There was a problem!\n";
//                print_r($out);
            } else {
                //echo "Everything went better than expected!\n";
                /* capture output
                print_r($out);
                print_r($ret);
                */
            }

            chmod($output, 0777);
        }
    }

    public static function getIvUploadFileName($iv_id)
    {
        $image = "";
        if (is_numeric($iv_id)) {
            $itemIconImage = ItemVariant::findOne($iv_id);
            if ($itemIconImage != null)
                $image = $itemIconImage->upload_file;
        }
        return $image;
    }

    /**
     * @param $tempFolder
     */
    public static function removeAllFilesInFolder($tempFolder)
    {
        if (file_exists($tempFolder)) {
            self::recursiveRemove($tempFolder);
        }
    }

    public static function getImageDataUri($imageRealPath)
    {
        // Read image path, convert to base64 encoding
        $imageData = base64_encode(file_get_contents($imageRealPath));

        // Format the image SRC:  data:{mime};base64,{data};
        return 'data: ' . mime_content_type($imageRealPath) . ';base64,' . $imageData;
    }

    /**
     * @param $image_real_path
     * @return array: 'filename', 'filepath', 'width', 'height', 'width_scaled', 'height_scaled', 'scale_ratio'
     */
    public static function getImageScaledInfoFromRealPath($image_real_path)
    {

        if (!file_exists($image_real_path))
            return array();
        $size = getimagesize($image_real_path);
        $width = $size[0];
        $height = $size[1];

        //scale if image dimension is so big
        $width_scaled = $width;
        $height_scaled = $height;
        $scale_ratio = 1;
        $standardSize = intval(FileHelper::CROP_MAX_SIZE_FRAME);

        if ($width >= $height) {
            if ($width > $standardSize) {
                $scale_ratio = round($width / $standardSize, 10);
                $width_scaled = $standardSize;
                $height_scaled = round($height / $scale_ratio, 10);
            }
        } else {
            if ($height > $standardSize) {
                $scale_ratio = round($height / $standardSize, 10);
                $height_scaled = $standardSize;
                $width_scaled = round($width / $scale_ratio, 10);
            }
        }
        return array(
            'filename' => basename($image_real_path),
            'filepath' => $image_real_path,
            'width' => $width,
            'height' => $height,
            'width_scaled' => $width_scaled,
            'height_scaled' => $height_scaled,
            'scale_ratio' => $scale_ratio
        );
    }

    public static function cropImage($fileRealPath)
    {
        /*
            $image_in_user_folder = str_replace('http://' . (Yii::$app->request->getServerName()), '', $image_in_user_folder);
            $image_in_user_folder = str_replace(Yii::$app->request->baseUrl, '', $image_in_user_folder);
            $image_in_user_folder = FileHelper::get_root_folder() . $image_in_user_folder;

            $x1 = $_POST["x1"];
            $y1 = $_POST["y1"];
            $w = $_POST["w"];
            $h = $_POST["h"];

            $x1 = floatval($x1) * floatval($scale_ratio);
            $y1 = floatval($y1) * floatval($scale_ratio);
            $w = floatval($w) * floatval($scale_ratio);
            $h = floatval($h) * floatval($scale_ratio);

            //Scale the image to the thumb_width set above
            //$scale = $thumb_width/$w;
            $scale = 1;

            $ext = CommonHelper::GetExt($image_in_user_folder);
        */
        //$image_in_user_folder = str_replace('.' . $ext, '_thumb.' . $ext, $image_in_user_folder);

        $cropped = FileHelper::resizeThumbnailImage($image_in_user_folder, $image_in_user_folder, $w, $h, $x1, $y1, $scale);

    }

    /**
     * These parameters should be calculated with scaled before
     * @param $imageRealPath
     * @param $width
     * @param $height
     * @param $x1
     * @param $y1
     * @return mixed
     * @throws HttpException
     */
    public static function resizeImage($imageRealPath, $width, $height, $x1, $y1)
    {
        $result = ['errors' => ''];

        $isPNGorGIF = false;
        $imgInfo = getimagesize($imageRealPath);

        $source = null;

        //prepare image for the customization
        switch ($imgInfo[2]) {
            case 1:
                $source = imagecreatefromgif($imageRealPath);
                $isPNGorGIF = true;
                break;
            case 2:
                $source = imagecreatefromjpeg($imageRealPath);
                break;
            case 3:
                $isPNGorGIF = true;
                $source = imagecreatefrompng($imageRealPath);
                break;
            default:
                $result['errors'] = 'Could not resize image, the image type is not found';
                return $result;
                break;
        }
        $newImage = imagecreatetruecolor($width, $height);

        //customize
        if ($isPNGorGIF) {
            imagealphablending($newImage, false);
            imagesavealpha($newImage, true);

            //fill box with white background to fix the issue with PNG and GIF image that automatically add black background behind
            $transparent = imagecolorallocatealpha($newImage, 255, 255, 255, 127);
            imagefilledrectangle($newImage, 0, 0, $width, $height, $transparent);
        }

        if ($source == null) {
            $result['errors'] = 'Could not resize image, the image type is not found';
            return $result;
        }


        imagecopyresampled($newImage, $source, 0, 0, $x1, $y1, $width, $height, $width, $height);

        //save image from memory to file (overwrite the same input path)
        switch ($imgInfo[2]) {
            case 1:
                imagegif($newImage, $imageRealPath);
                break;
            case 2:
                imagejpeg($newImage, $imageRealPath);
                break;
            case 3:
                imagepng($newImage, $imageRealPath);
                break;
            default:
                //throw new CHttpException('Commons', 'Failed to resize image!');
                break;
        }
        chmod($imageRealPath, 0777);
        return $result;
    }

    /**
     * @param $thumb_image_name - - the physical folder path of image
     * @param $image
     * @param $width
     * @param $height
     * @param $start_width
     * @param $start_height
     * @param $scale
     * @return mixed
     */
    public static function resizeThumbnailImage($thumb_image_name, $image, $width, $height, $start_width, $start_height, $scale)
    {

        $newImageWidth = ceil($width * $scale);
        $newImageHeight = ceil($height * $scale);

        $isPNGorGIF = false;
        $imgInfo = getimagesize($image);
        switch ($imgInfo[2]) {
            case 1:
                $source = imagecreatefromgif($image);
                $isPNGorGIF = true;
                break;
            case 2:
                $source = imagecreatefromjpeg($image);
                break;
            case 3:
                $isPNGorGIF = true;
                $source = imagecreatefrompng($image);
                break;
            default:
                //throw new CHttpException('Commons 653', 'Unsupported filetype! ' . $image);
                break;
        }
        $newImage = imagecreatetruecolor($newImageWidth, $newImageHeight);

        if ($isPNGorGIF) {
            imagealphablending($newImage, false);
            imagesavealpha($newImage, true);
            $transparent = imagecolorallocatealpha($newImage, 255, 255, 255, 127);
            imagefilledrectangle($newImage, 0, 0, $newImageWidth, $newImageHeight, $transparent);
        }
        imagecopyresampled($newImage, $source, 0, 0, $start_width, $start_height, $newImageWidth, $newImageHeight, $width, $height);

        switch ($imgInfo[2]) {
            case 1:
                imagegif($newImage, $thumb_image_name);
                break;
            case 2:
                imagejpeg($newImage, $thumb_image_name);
                break;
            case 3:
                imagepng($newImage, $thumb_image_name);
                break;
            default:
                //throw new CHttpException('Commons', 'Failed to resize image!');
                break;
        }
        chmod($thumb_image_name, 0777);

        return $thumb_image_name;
    }

    public static function getWidthImage($image, $realPathInImage = false)
    {
        $width = 0;
        if (!$realPathInImage) {
            $image = str_replace(Url::base(true), '', $image);
            $image = str_replace(Url::base(), '', $image);
            $image = FileHelper::get_root_folder() . '/' . $image;
            $image = str_replace('//', '/', $image);
        }

        $image = str_replace('//', '/', $image);
        if (!empty($image) && file_exists($image) && is_file($image)) {
            $size = getimagesize($image);
            $width = $size[0];
        }
        return $width;
    }

    public static function getImageHeightByWidth($image_real_path, $width){
        $size = getimagesize($image_real_path);
        $actualWidth = $size[0];
        $actualHeight = $size[1];

        $ratio = round($actualHeight / $actualWidth, 10);
        $height = round($ratio * $width, 10);
        return $height;
    }


    public static function getHeightImage($image, $realPathInImage = false)
    {
        $height = 0;
        if (!$realPathInImage) {

            $image = str_replace(Url::base(true), '', $image);
            $image = str_replace(Url::base(), '', $image);
            $image = FileHelper::get_root_folder() . '/' . $image;
        }

        if (!empty($image) && file_exists($image) && is_file($image)) {
            $size = getimagesize($image);
            $height = $size[1];
        }
        return $height;
    }

    /**
     * @param $path
     * @param string $file_name
     */
    public static function createFile($path, $file_name = "")
    {
        @chmod($path, 0777);
        if (empty($file_name))
            $file_name = CommonHelper::uniquename('textfile', 'txt');
        $filePath = $path . $file_name;

        $createFolder = fopen($filePath, 'w') or die('Cannot open file:  ' . $filePath);
        fwrite($createFolder, "");
        fclose($createFolder);
    }

    /** Copy a file to the remote folder by file name. Depeding on the provided member_id or account_id
     * @param $remote_id
     * @param $file_name
     * @param int $from_member_id
     * @param int $from_acc_id
     */
    public static function copyFileToRemoteFolder($remote_id, $file_name, $from_member_id = 0, $from_acc_id = 0)
    {
        $remoteFolder = FileHelper::getRemoteFolder($remote_id);
        FileHelper::copyFileToFolder($file_name, $remoteFolder, $from_member_id, $from_acc_id);
    }

    public static function copyFileToFolder($file_name, $destFolderPath, $from_member_id = 0, $from_acc_id = 0)
    {
        $filePath = '';
        if ($from_member_id != 0) {
            $filePath = FileHelper::getFileRealPathInMemberFolder($from_member_id, $file_name);
        } elseif ($from_acc_id != 0) {
            $filePath = FileHelper::getFileRealPathInAccountFolder($from_acc_id, $file_name);
        }

        if ($filePath != '' && file_exists($filePath)) {
            copy($filePath, $destFolderPath . $file_name);
        }
    }

    public static function fileName($path)
    {
        return pathinfo($path, PATHINFO_FILENAME);
    }

    public static function layoutPath($scheme = false)
    {
        return Url::base($scheme) . '/frontend/web/angularjs/app/css/ss';
    }

    public static function downloadFile($iv_id)
    {

        $iv = ItemVariant::findOne($iv_id);

        if ($iv) {
            $file = $iv->upload_file;
            $ext = pathinfo($file)['extension'];
            $file_name = $iv->name . '.' . $ext;
            $filePath = self::getFilePathOfIv($iv_id);
        }

        if (empty($file_name)) {
            $file_name = basename($filePath);
        }
        $file_name = preg_replace('/,/', '.', $file_name);
        if (!empty($filePath) && file_exists($filePath)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . $file_name);
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filePath));
            readfile($filePath);
            exit;
        }else{
        	echo "<script type='text/javascript'>alert('File not Found - Cannot download file .');</script>";
        	exit;
        }
    }

    /**
     * @param $filePath
     * @param array $rgb
     * @param $newFileName (without extension)
     * @return mixed
     */
    public static function changeImgColor($filePath, $rgb = [0xFF, 0, 0xFF], $newFileName)
    {
        $extension = pathinfo($filePath, PATHINFO_EXTENSION);
        $fileDir = pathinfo($filePath, PATHINFO_DIRNAME);
        $filePath = CommonHelper::sanitizePath($filePath);


        $output = CommonHelper::sanitizePath($fileDir . DIRECTORY_SEPARATOR . $newFileName . ".$extension");
        FileHelper::replaceImgColor($filePath, $rgb, $output);
        return $output;
    }

    public static function replaceImgColor($file, $rgb, $targetName)
    {
        $extension = pathinfo($file, PATHINFO_EXTENSION);
        $im = null;
        if ($extension == 'jpg' || $extension == 'jpeg') $im = imagecreatefromjpeg($file);
        if ($extension == 'png') $im = imagecreatefrompng($file);
        if ($extension == 'gif') $im = imagecreatefromgif($file);

        imagefilter($im, IMG_FILTER_GRAYSCALE);
        imagefilter($im, IMG_FILTER_COLORIZE, $rgb[0], $rgb[1], $rgb[2]);
        imagealphablending($im, true);
        imagesavealpha($im, false);

        if ($extension == 'jpg' || $extension == 'jpeg') imagejpeg($im, $targetName);
        if ($extension == 'png') imagepng($im, $targetName);
        if ($extension == 'gif') imagegif($im, $targetName);

        imagedestroy($im);

    }

    /**
     * @param string $filePath
     * @param string $member_id
     * @param string $account_id
     * @param boolean $genNewName
     * @return mixed|string
     */
    public static function moveFileToUserFolder($filePath, $member_id = '', $account_id = '', $genNewName = true)
    {
        
        return "";
    }


    public static function createZipFile($folder_to_zip = null, $files_to_zip = null, $output_file)
    {
        // must enable zip extension # pecl install zip
        $zip = new \ZipArchive();
        $zip->open($output_file, \ZipArchive::CREATE);
        if (!empty($folder_to_zip) && file_exists($folder_to_zip)) {
            $files = scandir($folder_to_zip);
        } elseif (!empty($files_to_zip)) {
            $files = $files_to_zip;
            $folder_to_zip = "";
        }
      
        foreach ($files as $file) {
            if (is_array($file)) {
                $file = $file['result'];
            }
            if (file_exists($file))
                $zip->addFromString(basename($file), file_get_contents($folder_to_zip . $file));
        }

        $zip->close();
    }

    public static function getScreenShotByUrlIfItRunning($webUrl, $save_location, $file_name, $width, $height)
    {
        FileHelper::processScreenShotByUrl($webUrl, $save_location, $file_name, $width, $height);
    }

    public static function processScreenShotByUrl($webUrl, $save_location, $file_name, $width, $height)
    {
        $saveFilePath = $save_location . DIRECTORY_SEPARATOR . $file_name;
        $saveFilePath = CommonHelper::sanitizePath($saveFilePath);
        $saveFilePath = str_replace('//', '/', $saveFilePath);

        if (!CommonHelper::isLocalHostRequest()) {

            if (file_exists($saveFilePath) && is_file($saveFilePath)) {
                //remove old file
                unlink($saveFilePath);
            }

            $setting = SettingAccount::find()->innerJoinWith([
                    'setting' => function ($query) {
                        $query->andWhere(['name' => KioskHelper::SKIN_CAPTURE_SCREENSHOT_ENGINE_DELAY]);

                    }]
            )->andWhere(['account_id' => PermissionHelper::SS_ADMIN_ID])->one();


            //$command = "/usr/bin/wkhtmltoimage-amd64";
            //hardcode
            $ex = sprintf('xvfb-run --server-args="-screen 0, 1024x680x24" /usr/local/bin/wkhtmltoimage --use-xserver -f png --quality 83 --load-error-handling ignore --enable-javascript --no-stop-slow-scripts --javascript-delay ' . $setting->value . ' --width ' . $width . ' --height ' . $height . ' %s %s 2>&1 &', $webUrl, $save_location . $file_name);

        } else {
            $command = Yii::$app->params['wkhtmltoimageExecuserEngine'];
            $ex = "$command --width $width --height $height $webUrl " . $saveFilePath;
        }
        // NOTE: Don't forget to `escapeshellarg()` any user input!

        exec($ex);
        chmod($saveFilePath, 0777);
    }

    public static function replaceStringIntoTextFilesInDir($dirPath, $search, $replace)
    {
        $files = scandir($dirPath);
        foreach ($files as $filename) {
            if ($filename != '' && $filename != '.' && $filename != '..') {
                $filePath = $dirPath . $filename;
                var_dump($search, $replace);
                $tmp = file_get_contents($filePath);
                $tmp = str_replace($search, $replace, $tmp);
                var_dump($tmp);
                file_put_contents($filePath, $tmp);
            }
        }

    }

    public static function getUploaderFolderOfMemberOrAccount()
    {
        $currentUser = PermissionHelper::getUserAccessing();
        $folderUpload = FileHelper::get_folder_upload_file($currentUser['val'], $currentUser['id']);
        $folderUpload = FileHelper::getUploadFolderRealPath() . $folderUpload;
        $folderUpload = str_replace('//', '/', $folderUpload);
        return $folderUpload;
    }
    
    public static function get_zip_originalsize($filename) {
    	$size = 0;
    	$resource = zip_open($filename);
    	while ($dir_resource = zip_read($resource)) {
    		$size += zip_entry_filesize($dir_resource);
    	}
    	zip_close($resource);
    
    	return $size;
    }
    
    
}