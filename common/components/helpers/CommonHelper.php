<?php

namespace common\component\helpers;
use common\models\Domain;
use common\models\Member;
use common\models\AccountUser;
use Crypt;
use DOMDocument;
use Yii;
use yii\helpers\Url;

/**
 * Class CommonHelper
 * @package common\component\helpers
 */
class CommonHelper
{

    const DEFAULT_OBJECT_DIM = "obscure";
	const IS_CONFIRMED_STATUS = 1;
	const CONFIRM_TYPE_REQUEST_INVITE_MEMBER = 1;
	const IS_NOT_CONFIRMED_STATUS = 0;
	const CONFIRM_ACCEPT = 1;
	const CONFIRM_TYPE_APPROVE_BY_ADMIN = 2;
	const ACCOUNT_VIEWABLE = 0;
	const ACCOUNT_SHARE = 1;
    const ROLE_ADMIN = 1;

	const CONFIRM_REFUSE = 0;
	const CONFIRM_EXPIRED = 2;
	const DEFAULT_OBJECT_NAME = "UNASSIGNED";
    const ADDITION_EMAIL_ENTRY = 1;
    const ADDITION_PHONE_ENTRY = 2;
	
    static $US_STATE = array(
        array("text" => "Alabama", "value" => "AL"),
        array("text" => "Alaska", "value" => "AK"),
        array("text" => "Arizona", "value" => "AZ"),
        array("text" => "Arkansas", "value" => "AR"),
        array("text" => "California", "value" => "CA"),
        array("text" => "Colorado", "value" => "CO"),
        array("text" => "Connecticut", "value" => "CT"),
        array("text" => "Delaware", "value" => "DE"),
        array("text" => "Florida", "value" => "FL"),
        array("text" => "Georgia", "value" => "GA"),
        array("text" => "Hawaii", "value" => "HI"),
        array("text" => "Idaho", "value" => "ID"),
        array("text" => "Illinois", "value" => "IL"),
        array("text" => "Indiana", "value" => "IN"),
        array("text" => "Iowa", "value" => "IA"),
        array("text" => "Kansas", "value" => "KS"),
        array("text" => "Kentucky", "value" => "KY"),
        array("text" => "Louisiana", "value" => "LA"),
        array("text" => "Maine", "value" => "ME"),
        array("text" => "Maryland", "value" => "MD"),
        array("text" => "Massachusetts", "value" => "MA"),
        array("text" => "Michigan", "value" => "MI"),
        array("text" => "Minnesota", "value" => "MN"),
        array("text" => "Mississippi", "value" => "MS"),
        array("text" => "Missouri", "value" => "MO"),
        array("text" => "Montana", "value" => "MT"),
        array("text" => "Nebraska", "value" => "NE"),
        array("text" => "Nevada", "value" => "NV"),
        array("text" => "New Hampshire", "value" => "NH"),
        array("text" => "New Jersey", "value" => "NJ"),
        array("text" => "New Mexico", "value" => "NM"),
        array("text" => "New York", "value" => "NY"),
        array("text" => "North Carolina", "value" => "NC"),
        array("text" => "North Dakota", "value" => "ND"),
        array("text" => "Ohio", "value" => "OH"),
        array("text" => "Oklahoma", "value" => "OK"),
        array("text" => "Oregon", "value" => "OR"),
        array("text" => "Pennsylvania", "value" => "PA"),
        array("text" => "Puerto Rico", "value" => "PR"),
        array("text" => "Rhode Island", "value" => "RI"),
        array("text" => "South Carolina", "value" => "SC"),
        array("text" => "South Dakota", "value" => "SD"),
        array("text" => "Tennessee", "value" => "TN"),
        array("text" => "Texas", "value" => "TX"),
        array("text" => "Utah", "value" => "UT"),
        array("text" => "Vermont", "value" => "VT"),
        array("text" => "Virginia", "value" => "VA"),
        array("text" => "Washington", "value" => "WA"),
        array("text" => "Washington D.C.", "value" => "DC"),
        array("text" => "West Virginia", "value" => "WV"),
        array("text" => "Wisconsin", "value" => "WI"),
        array("text" => "Wyoming", "value" => "WY"),
        array("text" => "Other", "value" => "OTHER"));

    static $US_STATE_AUTO = array(
        "AL" => "Alabama",
        "AK" => "Alaska",
        "AZ" => "Arizona",
        "AR" => "Arkansas",
        "CA" => "California",
        "CO" => "Colorado",
        "CT" => "Connecticut",
        "DE" => "Delaware",
        "FL" => "Florida",
        "GA" => "Georgia",
        "HI" => "Hawaii",
        "ID" => "Idaho",
        "IL" => "Illinois",
        "IN" => "Indiana",
        "ID" => "Iowa",
        "KS" => "Kansas",
        "KY" => "Kentucky",
        "LA" => "Louisiana",
        "ME" => "Maine",
        "MD" => "Maryland",
        "MA" => "Massachusetts",
        "MI" => "Michigan",
        "MN" => "Minnesota",
        "MS" => "Mississippi",
        "MO" => "Missouri",
        "MT" => "Montana",
        "NE" => "Nebraska",
        "NV" => "Nevada",
        "NH" => "New Hampshire",
        "NJ" => "New Jersey",
        "NM" => "New Mexico",
        "NY" => "New York",
        "NC" => "North Carolina",
        "ND" => "North Dakota",
        "OH" => "Ohio",
        "OK" => "Oklahoma",
        "OR" => "Oregon",
        "PA" => "Pennsylvania",
        "PR" => "Puerto Rico",
        "RI" => "Rhode Island",
        "SC" => "South Carolina",
        "SD" => "South Dakota",
        "TN" => "Tennessee",
        "TX" => "Texas",
        "UT" => "Utah",
        "VT" => "Vermont",
        "VA" => "Virginia",
        "WA" => "Washington",
        "DC" => "District of Columbia",
        "WV" => "West Virginia",
        "WI" => "Wisconsin",
        "WY" => "Wyoming",
        "OTHER" => "Other");
    
    const KIOSK_CACHE_FOLDER_FORMAT = 'kioskcache_%s/';
    const KIOSK_CACHE_DATA_FORMAT = 'kioskcache_%s.appcache';
    const SIGN_CACHE_DATA_FORMAT = 'sign_%s.appcache';
    const VARIANT_CACHE_DATA_FORMAT = 'variant_%s.appcache';
    const FILE_FOLDER_TEMPLATE_APPCACHE = 'template/appcache/';
    
    const KIOSK_CACHE_FORMAT = 'kioskcache.appcache';
    

    /**
     * @return string
     */

    public static function applyChangeAfterDonePayment($selectedMemberType, $member, $orderForm, $memberAllocCap)
    {
        $memberBundle = MemberBundle::find()->where(['member_id' => $member->member_id])->one();

        /*** if
         * apply discount code or change member level => remove current member bundle
         * =====================================================***/

        if ($orderForm->useDiscountCode == 'true' || $orderForm->member_level != (int)$member->member_type) {

            if ($memberBundle != null)
                $memberBundle->delete();
        }

        /*** if
         * apply discount code is applied, apply the benefit from the new bundle
         * =====================================================***/


        if (isset($orderForm)) {
            if ($orderForm->buy_more_storage < 0) $orderForm->buy_more_storage = 0;
            if ($orderForm->buy_more_acc < 0) $orderForm->buy_more_acc = 0;
        }

        if ($orderForm->useDiscountCode == 'true') {

            //$memberBundle = Bundles::findOne($orderForm->bundle_id);
            $discount_iv_id = $orderForm->iv_id;
            $bundle = Bundles::getGroupBundleByDiscountIvCondition(['iv_id' => $discount_iv_id]);
            Yii::info('applyChangeAferDonePayment - $bundle');
            Yii::info($bundle->attributes);
            if($bundle){
                MembershipHelper::SaveMemberBundle($member->member_id, $bundle, $orderForm);
            }

        } elseif ($orderForm->member_level != $member->member_type) {


        }

        /*** Update member alloc cap ***/
        if($orderForm->storage_total){
            $memberAllocCap->diskspace_cap = $orderForm->storage_total * pow(1024, 3);
        }
        if($orderForm->acc_total){
            //$memberAllocCap->account_cap = $orderForm->acc_total;
            $memberAllocCap->account_cap = $member->getOwnAccounts()->count();
        }
        $memberAllocCap->save(false);

        if ($memberBundle != null) {

            if ($memberAllocCap->account_cap + $orderForm->buy_more_acc == $memberBundle->accounts
                && ($memberAllocCap->diskspace_cap / pow(1024, 3)) + $orderForm->buy_more_storage == $memberBundle->storage
            ) {

                CommonHelper::limitAccountsInSubscription();
            }

        }
        $member->member_type = $selectedMemberType;
        $member->save(false);

        return $memberBundle;
    }

    public static function limitAccountsInSubscription()
    {
        $member_id = Member::getLoginId();
        $member = Member::findOne($member_id);

        $allAccounts = $member->ownAccounts;

        $memberAllocterm = $member->memberAllocterm;

        $kioskType = Collection::find()->where(['collection_extension' => ObjectToolbar::KIOSK_APP_COLLECTION_EXT])->one();
        $allKiosk = CollectionVariant::find()->where(['member_id' => $member_id, 'collection_id' => $kioskType->collection_id])->all();

        foreach ($allAccounts as $account) {

            $allAccountKiosk = CollectionVariant::find()->where(['account_id' => $account->account_id, 'collection_id' => $kioskType->collection_id])->all();
            $allKiosk = array_merge($allKiosk, $allAccountKiosk);
        }

        // if the total of account, kiosk in user greater than current total of account, kiosk delete them
        $account_to_delete = count($allAccounts) - $memberAllocterm->account_cap;

        //$kiosk_to_delete = count($allKiosk) - $memberAllocterm->kiosk_cap;
        if ($account_to_delete > 0) {
        	for ($i = 0; $i < $account_to_delete; $i++) {
        		foreach ($allAccounts as $key => $account) {
        			if ($account->company == MembershipHelper::DEFAULT_ACCOUNT_NAME) {
                        MembershipHelper::removeAccount($account->account_id);
        				unset($allAccounts[$key]);
        				break;
        			}
        		}
        	}
        }
        
    }

    public static function getNumberDecimalFromString($raw)
    {
        return preg_replace("/[^0-9.]/", '', $raw);
    }

    public static function convertFileSize($sizeInBytes)
    {
        //$units = array('B', 'KB', 'MB', 'GB', 'TB', 'PB' , 'EB', 'ZB', 'YB');
        $fileMgr = new StorageHelper();
        $obj = $fileMgr->bytes_to_string($sizeInBytes, 2);
        return sprintf('%s %s', number_format(CommonHelper::floorp($obj['num'], 2), 2), $obj['str']);
    }

    public static function convertMbToGb($size)
    {
        if (strpos($size, 'MB') !== FALSE) {
            $size = str_replace('MB', '', $size);
            $size = number_format(floorp($size / 1024, 2), 2);
        }
        return $size;
    }

    public static function gen_unique_key()
    {
        return md5(microtime() . rand()); // there is no special character
    }

    public static function isNullOrEmptyString($var)
    {
        return (!isset($var) || trim($var) === '');
    }

    public static function appendQueryStringIntoURL($url, $new_query)
    {
        $separator = (parse_url($url, PHP_URL_QUERY) == NULL) ? '?' : '&';
        $url .= $separator . $new_query;
        return $url;
    }

    public static function getSafeString($input, $default = '')
    {
        return ($input == null || $input == '') ? $default : $input;
    }

    public static function getCurrentOwnerOfViewingContent()
    {
        $memberOwner = null;
        $currentUser = PermissionHelper::getUserAccessing();
        if ($currentUser['id'] == 'member_id') {
            $member_id = $currentUser['val'];
            $memberOwner = Member::findOne($member_id);
        } else {
            $acc = AccountUser::findOne($currentUser['val']);
            if ($acc != null)
                $memberOwner = $acc->memberOwner;
        }

        return $memberOwner;
    }

    public static function getSafeInt($input, $default = 0)
    {
        return is_numeric($input) ? filter_var($input, FILTER_VALIDATE_INT) : $default;
    }

    public static function getSafeFloat($input, $default = 0)
    {
        return is_numeric($input) ? floatval($input) : $default;
    }

    public static function getSafeBoolean($input)
    {
        /*
        filter_var('true', FILTER_VALIDATE_BOOLEAN); // true
        filter_var(1, FILTER_VALIDATE_BOOLEAN); // true
        filter_var('1', FILTER_VALIDATE_BOOLEAN); // true
        filter_var('on', FILTER_VALIDATE_BOOLEAN); // true
        filter_var('yes', FILTER_VALIDATE_BOOLEAN); // true

        filter_var('false', FILTER_VALIDATE_BOOLEAN); // false
        filter_var(0, FILTER_VALIDATE_BOOLEAN); // false
        filter_var('0', FILTER_VALIDATE_BOOLEAN); // false
        filter_var('off', FILTER_VALIDATE_BOOLEAN); // false
        filter_var('no', FILTER_VALIDATE_BOOLEAN); // false
        filter_var('asdfasdf', FILTER_VALIDATE_BOOLEAN); // false
        filter_var('', FILTER_VALIDATE_BOOLEAN); // false
        filter_var(null, FILTER_VALIDATE_BOOLEAN); // false
        */
        return filter_var($input, FILTER_VALIDATE_BOOLEAN);
    }
    public static function get($var, $default = null)
    {
        return isset($var) ? $var : $default;
    }

    public static function simplifyString($raw)
    {
        return preg_replace('/[^A-Za-z0-9_\-]/', '_', $raw);
    }

    /**
     * @return array $_POST
     */
    public static function getJsonPostData()
    {
        $a = new ItemVariant();
        return json_decode(file_get_contents('php://input'), true);
    }

    /**
     * @return array
     */
    public static function cryptCreateKey()
    {
        set_include_path(Yii::getAlias("@vendor/phpseclib"));
        include_once('Crypt/RSA.php');

        $password = 'ss-secret-P@ss600';

        $rsa = new \Crypt_RSA();

        $rsa->setPassword($password);
        $rsa->setHash('sha512');
//        $rsa->setPublicKeyFormat(CRYPT_RSA_PUBLIC_FORMAT_RAW);
//        $genKeys = $rsa->createKey();

        $privatekey = $rsa->getPrivateKey(); // could do CRYPT_RSA_PRIVATE_FORMAT_PKCS1 too
        $publickey = $rsa->getPublicKey();

        return [
            'privatekey' => $privatekey,
            'publickey' => $publickey
        ];
    }

    /**
     * @return array
     */
    public static function createKeyByOpenSSL()
    {
        $password = 'ss-secret-P@ss600';
        $config = array(
            //'config' => 'C:\OpenSSL-Win64\bin\openssl.cfg',
            "digest_alg" => "sha512",
            "private_key_bits" => 4096,
            "private_key_type" => OPENSSL_KEYTYPE_RSA,
        );

        $res = openssl_pkey_new($config);
        //$res=openssl_pkey_new();
        //Yii::info(openssl_error_string());
        // Get private key
        openssl_pkey_export($res, $privkey, $password);

        // Get public key
        $pubkey = openssl_pkey_get_details($res);
        $pubkey = $pubkey["key"];

        return [
            'privatekey' => $privkey,
            'pubkey' => $pubkey
        ];
    }

    public static function convertAnythingToArray($o)
    {
        return json_decode(json_encode($o), true);
    }

    public static function generateKeys()
    {
        $dn = array("countryName" => 'US', "stateOrProvinceName" => 'TX', "localityName" => 'Katy', "organizationName" => 'Vidaltek', "organizationalUnitName" => 'Vidaltek', "commonName" => 'vidaltek company', "emailAddress" => 'info@vidaltek.com');
        $privkeypass = 'ss-secret-P@ss600';
        $numberofdays = 965;

        $config = array(
            //'config' => 'C:\OpenSSL-Win64\bin\openssl.cfg',
            "digest_alg" => "sha512",
            "private_key_bits" => 4096,
            "private_key_type" => OPENSSL_KEYTYPE_RSA,
        );

        $privkey = openssl_pkey_new($config);
        $csr = openssl_csr_new($dn, $privkey);
        $sscert = openssl_csr_sign($csr, null, $privkey, $numberofdays);

        openssl_x509_export($sscert, $publickey);
        openssl_pkey_export($privkey, $privatekey, $privkeypass);
        openssl_csr_export($csr, $csrStr);

        file_put_contents('E:/private.pem', $privatekey); // Will hold the exported PriKey
        file_put_contents('E:/public.pem', $publickey); // Will hold the exported PubKey
        file_put_contents('E:/cert.crt', $csrStr);     // Will hold the exported Certificate
    }

    /**
     * @param $list
     * @return array
     */
    public static function listActiveRecordToArray($list)
    {
        $result = [];
        foreach ($list as $ar) {
            $result[] = $ar->toArray();
        }
        return $result;
    }

    /**
     * @param $str
     * @return int
     */
    public static function countUppercase($str)
    {
        //preg_match_all("/\b[A-Z][A-Za-z0-9]+\b/",$str,$matches);
        preg_match_all('/[A-Z]/', $str, $matches);
        return count($matches[0]);
    }

    /**
     * @param $input
     * @param $length
     * @param string $continueText
     * @return string
     */
    public static function getLimitText($input, $length, $continueText = "...")
    {
        $resultString = $input;
        if ($input != "") {
            if (strlen($input) > $length) {
                $resultString = sprintf("%s%s", substr($input, 0, $length - strlen($continueText) - 1), $continueText);
            }
        }

        return $resultString;
    }

    /**
     * @param $input
     * @param $length
     * @param $allowUpperCaseCharCount
     * @param int $lengthWhenReachUpperCaseLimit
     * @param string $continueText
     * @return string
     */
    public static function getCustomLimitText($input, $length, $allowUpperCaseCharCount, $lengthWhenReachUpperCaseLimit = 0, $continueText = "...")
    {
        $resultString = $input;
        if ($input != "") {

            if (CommonHelper::countUppercase($input) > $allowUpperCaseCharCount) {
                if ($lengthWhenReachUpperCaseLimit == 0)
                    $lengthWhenReachUpperCaseLimit = $allowUpperCaseCharCount;
            }

            if ($length > $lengthWhenReachUpperCaseLimit) {
                return CommonHelper::getLimitText($input, $lengthWhenReachUpperCaseLimit, $continueText);
            } else {
                return CommonHelper::getLimitText($input, $length, $continueText);
            }
        }

        return $resultString;
    }

    /**
     * @param $val
     * @param $precision
     * @return float
     */
    public static function floorp($val, $precision)
    {
        $mult = pow(10, $precision);
        return floor($val * $mult) / $mult;
    }

    public static function exportPublicKeyFromPrivateKey($private_key_path, $password)
    {
        //$private_key_path = \Yii::getAlias('@common/config') . DIRECTORY_SEPARATOR . 'private.pem';
        $_privateKey = openssl_pkey_get_private(file_get_contents($private_key_path), $password);
        $keyDetails = openssl_pkey_get_details($_privateKey);
        return $keyDetails['key'];
    }

    public static function isLocalHostRequest()
    {
        return (strpos($_SERVER['HTTP_HOST'], 'localhost') !== false);
    }

    /**
     * @param array $sensitive_data_arr
     * @param string $secret
     * @return array
     */
    public static function encrypt_symmetric($sensitive_data_arr = array(), $secret = "")
    {

        $importPath = Yii::getAlias('@vendor/cryptPhp/');
        include_once($importPath . 'Crypt.php');

        $encryptedStr = "";
        if (!empty($sensitive_data_arr)) {

            $crypt = new Crypt;
            $crypt->setKey($secret);
            $crypt->setComplexTypes(TRUE);
            $crypt->setData($sensitive_data_arr);
            $encryptedStr = $crypt->encrypt();
        }
        return array($encryptedStr, $secret);
    }

    /**
     * @param $encryptedStr
     * @param $key
     * @return array
     */
    public static function decrypt_symmetric($encryptedStr, $key)
    {
        $importPath = Yii::getAlias('@vendor/cryptPhp/');
        include_once($importPath . 'Crypt.php');

        $crypt = new Crypt;

        $crypt->setData($encryptedStr);
        $crypt->setKey($key);
        $crypt->setComplexTypes(TRUE);
        $decrypted = $crypt->decrypt();
        return $decrypted; // array
    }

    public static function sanitizePath($path)
    {
        $path = str_replace('\\', '/', $path);
        return preg_replace('~(?<!https:|http:)[/\\\\]+~', "/", trim($path));
    }

    /**
     * add scheme http or https
     * @param $url
     * @return string
     */
    public static function addScheme($url){
        $url = ltrim($url, '/');
        $url = CommonHelper::sanitizePath($url);
        $hostInfo = Yii::$app->getUrlManager()->getHostInfo();
        $siteScheme = parse_url($hostInfo, PHP_URL_SCHEME); //http or https

        //check scheme of url
        $urlScheme = parse_url($url, PHP_URL_SCHEME);
        if(!$urlScheme){ // if url does not contain any scheme, add it
            $url = $siteScheme . '://' . $url;
        }

        return $url;

    }

    public static function jwtEncode()
    {

    }

    public static function array_column($input = null, $columnKey = null, $indexKey = null)
    {
        // Using func_get_args() in order to check for proper number of
        // parameters and trigger errors exactly as the built-in array_column()
        // does in PHP 5.5.
        $argc = func_num_args();
        $params = func_get_args();

        if ($argc < 2) {
            trigger_error("array_column() expects at least 2 parameters, {$argc} given", E_USER_WARNING);
            return null;
        }

        if (!is_array($params[0])) {
            trigger_error(
                'array_column() expects parameter 1 to be array, ' . gettype($params[0]) . ' given',
                E_USER_WARNING
            );
            return null;
        }

        if (!is_int($params[1])
            && !is_float($params[1])
            && !is_string($params[1])
            && $params[1] !== null
            && !(is_object($params[1]) && method_exists($params[1], '__toString'))
        ) {
            trigger_error('array_column(): The column key should be either a string or an integer', E_USER_WARNING);
            return false;
        }

        if (isset($params[2])
            && !is_int($params[2])
            && !is_float($params[2])
            && !is_string($params[2])
            && !(is_object($params[2]) && method_exists($params[2], '__toString'))
        ) {
            trigger_error('array_column(): The index key should be either a string or an integer', E_USER_WARNING);
            return false;
        }

        $paramsInput = $params[0];
        $paramsColumnKey = ($params[1] !== null) ? (string)$params[1] : null;

        $paramsIndexKey = null;
        if (isset($params[2])) {
            if (is_float($params[2]) || is_int($params[2])) {
                $paramsIndexKey = (int)$params[2];
            } else {
                $paramsIndexKey = (string)$params[2];
            }
        }

        $resultArray = array();

        foreach ($paramsInput as $row) {
            $key = $value = null;
            $keySet = $valueSet = false;

            if ($paramsIndexKey !== null && array_key_exists($paramsIndexKey, $row)) {
                $keySet = true;
                $key = (string)$row[$paramsIndexKey];
            }

            if ($paramsColumnKey === null) {
                $valueSet = true;
                $value = $row;
            } elseif (is_array($row) && array_key_exists($paramsColumnKey, $row)) {
                $valueSet = true;
                $value = $row[$paramsColumnKey];
            }

            if ($valueSet) {
                if ($keySet) {
                    $resultArray[$key] = $value;
                } else {
                    $resultArray[] = $value;
                }
            }

        }

        return $resultArray;
    }

    /**
     * @param $input
     * @param $page - page index should start from 1, not zero
     * @param $show_per_page
     * @return array
     */
    public static function arrayPaging($input, $page, $show_per_page)
    {
        $start = ($page - 1) * $show_per_page;
        $end = $start + $show_per_page;
        $count = count($input);

        // Conditionally return results
        if ($start < 0 || $count <= $start) {
            // Page is out of range
            return array();
        } else if ($count <= $end)
            // Partially-filled page
            return array_slice($input, $start);
        else
            // Full page
            return array_slice($input, $start, $end - $start);
    }

    public static function getRandomStringByUnixTimetemp($length = 20, $level = 2)
    {
        list ($usec, $sec) = explode(' ', microtime());
        srand(( float )$sec + (( float )$usec * 100000));

        $validchars [1] = "0123456789abcdfghjkmnpqrstvwxyz";
        $validchars [2] = "123456789abcdfghjkmnpqrstvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $validchars [3] = "0123456789_!@#$%&*()-=+/abcdfghjkmnpqrstvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_!@#$%&*()-=+/";

        $folder_name = "";
        $counter = 0;

        while ($counter < $length) {
            $actChar = substr($validchars [$level], rand(0, strlen($validchars [$level]) - 1), 1);

            // All character must be different
            if (!strstr($folder_name, $actChar)) {
                $folder_name .= $actChar;
                $counter++;
            }
        }
        return $folder_name;
    }

    public static function getFirstObjFromArray($array)
    {
        $obj = null;
        if (is_array($array) && count($array) > 0)
            $obj = $array[0];
        return $obj;
    }

    public static function serializeArrayObject($array, $filePath = "")
    {
        if ($filePath != '') {
            chmod($filePath, 0777);

            file_put_contents($filePath, serialize($array));
            chmod($filePath, 0777);
        }
    }

    //read array object
    public static function deserializeArrayObject($filePath = "")
    {
        if ($filePath != '') {
            chmod($filePath, 0777);
            if (file_exists($filePath))
                return unserialize(file_get_contents($filePath));
            else
                return false;
        }
        return false;
    }


    public static function uniquename($prefix, $extension = "")
    {
        $return = '';
        for ($i = 0; $i < 7; $i++) {
            $return .= chr(rand(97, 122));
        }

        if (empty($extension))
            $return = "$prefix-$return-" . time();
        else
            $return = "$prefix-$return-" . time() . ".$extension";
        return $return;
    }

    public static function format_url_website($url, $forceProtocol = "")
    {

        $url = CommonHelper::formatUrlByHost($url, $forceProtocol);
        if (!CommonHelper::validateWebUrl($url)) {
            $url = "";
        }

        return $url;
    }

    public static function validateWebUrl($url)
    {
        return (filter_var($url, FILTER_VALIDATE_URL) !== false);
    }

    public static function getUrlForIframe($url)
    {
        $url = CommonHelper::format_url_website($url);
        $mysite = 'http://' . $_SERVER['SERVER_NAME'];
        if (strpos($url, '.swf') !== false || strpos($url, '.flv') !== false) {
            /* dev4 temporarily ignore this: handle the flash file
            $url = Yii::$app->createAbsoluteUrl('admincontent/ExternalFlashVideo', array(
                'url' => urlencode($url),
                'layout' => 'none'
            ));
            */
            $url = "";
        }
        elseif (substr($url, 0, strlen($mysite)) != $mysite && !CommonHelper::canUrlPutInNormalIframe($url)) {
            $url = Url::base(true) . '/piframe/frames/index2.php?url=' . urlencode($url);
        }
        return $url;
    }

    public static function canUrlPutInNormalIframe($url)
    {
        $pos = true;
        if (empty($url)) {
            return false;
        }
        //lookup the domain that is same with the given url before we may do the post
        $pageUrlArr = parse_url($url);

        $host = isset($pageUrlArr['host']) ? $pageUrlArr['host'] : "";
        $host = str_replace('www.', '', $host); //remove www

        $originDomain = Domain::find()->where(['domain' => $host])->one();

        if ($originDomain == null) {
            $getHeader = get_headers($url, 1);

            //track it then we don't have to check the header of same domain again
            $originDomain = new Domain;
            $originDomain->full_url = $url;
            $originDomain->domain = $host;
            $originDomain->is_origin = 0;

            if (isset($getHeader)) {
                if (array_key_exists('x-frame-options', array_change_key_case($getHeader, CASE_LOWER))) {
                    $pos = false;
                    $originDomain->is_origin = 1;

                }
            }
            $originDomain->save(false);
        } else {
            $pos = $originDomain->is_origin == 0;
        }

        return $pos;
    }

    /**
     * @param $processUrl
     * @param string $forceProtocol
     * @return string
     */
    public static function formatUrlByHost($processUrl, $forceProtocol = "")
    {

        $scheme = "";
        $host = "";
        $path = "";
        $port = "";
        $www = "";

        $pageUrlArr = parse_url(CommonHelper::getCurrentURL());
        $processUrlArr = parse_url(strtolower($processUrl));
        
        //edit by payton
        if (strpos($pageUrlArr['host'], 'www.') !== false || strpos($processUrlArr['host'], 'www.') !== false) {
            $www = 'www.';
        }
        $pageUrlArr_host = str_replace('www.', '', $pageUrlArr['host']);
        $processUrlArr_host = str_replace('www.', '', $processUrlArr['host']);

        if (strpos($pageUrlArr['host'], 'localhost') !== false) {
            // set port for localhost only, live does not need it
            $port = isset($pageUrlArr['port']) ? $pageUrlArr['port'] : $port;
            //$www = "";
        } else {
            $port = "";
        }

        if ($pageUrlArr_host == $processUrlArr_host) {
            // set port for localhost only, live does not need it
            $scheme = isset($pageUrlArr['scheme']) ? $pageUrlArr['scheme'] : $scheme;
            $host = $pageUrlArr_host;
        } else {
            $scheme = isset($processUrlArr['scheme']) ? $processUrlArr['scheme'] : $scheme;
            $host = $processUrlArr_host;
            $port = $processUrlArr['port'];
        }

        $host = str_replace('localhost.', 'localhost', $host);

        if ($forceProtocol != "") {
            $scheme = $forceProtocol;
        }
		
        if (strpos($processUrlArr['host'], $_SERVER['SERVER_NAME']) === FALSE) {
            if ($processUrlArr['scheme'] == "") {
                $scheme = $pageUrlArr['scheme'];
            }
        }


        //$path = isset($processUrlArr['path']) ? $processUrlArr['path'] : $path;
        $_caseURLArr = parse_url($processUrl);
        
        $path = isset($_caseURLArr['path']) ? $_caseURLArr['path'] : $path;
        if ($scheme != "")
            $scheme .= "://";

        if ($port != "")
            $port = ":$port";

        $jointQueryStrings = "";
        if (isset($processUrlArr['query'])) {
            $jointQueryStrings = "?";
        }
        $url = sprintf('%s%s%s%s%s%s', $scheme, $www . $host, $port, $path, $jointQueryStrings, @$processUrlArr['query']);
        return $url;
    }

    public static function getCurrentURL()
    {
        $currentURL = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
        $currentURL .= $_SERVER["SERVER_NAME"];

        if ($_SERVER["SERVER_PORT"] != "80" && $_SERVER["SERVER_PORT"] != "443") {
            $currentURL .= ":" . $_SERVER["SERVER_PORT"];
        }

        $currentURL .= $_SERVER["REQUEST_URI"];
        return $currentURL;
    }

    public static function genRandomValidDirectoryName($length = 20, $level = 2)
    {
        list ($usec, $sec) = explode(' ', microtime());
        srand(( float )$sec + (( float )$usec * 100000));

        $validchars [1] = "0123456789abcdfghjkmnpqrstvwxyz";
        $validchars [2] = "123456789abcdfghjkmnpqrstvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $validchars [3] = "0123456789_!@#$%&*()-=+/abcdfghjkmnpqrstvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_!@#$%&*()-=+/";

        $folder_name = "";
        $counter = 0;

        while ($counter < $length) {
            $actChar = substr($validchars [$level], rand(0, strlen($validchars [$level]) - 1), 1);

            // All character must be different
            if (!strstr($folder_name, $actChar)) {
                $folder_name .= $actChar;
                $counter++;
            }
        }
        return $folder_name;
    }

    public static function dashesToCamelCase($string, $capitalizeFirstCharacter = false)
    {

        $str = str_replace(' ', '', ucwords(str_replace('-', ' ', $string)));

        if (!$capitalizeFirstCharacter) {
            $str[0] = strtolower($str[0]);
        }

        return $str;
    }

    public static function GetParamsForm($name, $defaultValue = "")
    {
        return self::getParam($name, $defaultValue);
    }

    public static function getParam($name, $defaultValue = null)
    {
        return isset($_GET[$name]) ? self::getDefaultWhenEmpty($_GET[$name], $defaultValue) : (isset($_POST[$name]) ? Commons::getDefaultWhenEmpty($_POST[$name], $defaultValue) : $defaultValue);
    }

    public static function getDefaultWhenEmpty($checkValue, $defaultValue)
    {
        if ($checkValue == "")
            return $defaultValue;
        else
            return $checkValue;
    }

    /**
     * remove invalid xml syntax from $value
     * @param $value
     * @return string
     */
    public static function stripInvalidXml($value)
    {
        $ret = "";
        $current = "";
        if (empty($value)) {
            return $ret;
        }

        $length = strlen($value);
        for ($i = 0; $i < $length; $i++) {
            $current = ord($value{$i});
            if (($current == 0x9) ||
                ($current == 0xA) ||
                ($current == 0xD) ||
                (($current >= 0x20) && ($current <= 0xD7FF)) ||
                (($current >= 0xE000) && ($current <= 0xFFFD)) ||
                (($current >= 0x10000) && ($current <= 0x10FFFF))
            ) {
                $ret .= chr($current);
            } else {
                $ret .= " ";
            }
        }
        return $ret;
    }

    public static function fixMalformedXml($xml)
    {
        $dom = new DOMDocument();

        libxml_use_internal_errors(true);
        //$dom->strictErrorChecking = false;
        $dom->loadHTML($xml, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        //$dom->load($xmlFile);
        $dom->recover = TRUE;
        //libxml_use_internal_errors(true);
        # remove <!DOCTYPE
//        $dom->removeChild($dom->doctype);
//        # remove <html><body></body></html>
//        $dom->replaceChild($dom->firstChild->firstChild->firstChild, $dom->firstChild);
        $fixed = $dom->saveHTML();
        $fixed = str_replace(['<html>', '<body>', '</html>', '</body>'], '', $fixed);
        return $fixed;

    }

    public static function removeNonCharactersFromString($string)
    {
        return preg_replace("/[^A-Za-z0-9 ]/", '', $string);
    }

    public static function extractYoutubeVideoId($text)
    {
        $text = preg_replace('~
        # Match non-linked youtube URL in the wild. (Rev:20130823)
        https?://         # Required scheme. Either http or https.
        (?:[0-9A-Z-]+\.)? # Optional subdomain.
        (?:               # Group host alternatives.
          youtu\.be/      # Either youtu.be,
        | youtube         # or youtube.com or
          (?:-nocookie)?  # youtube-nocookie.com
          \.com           # followed by
          \S*             # Allow anything up to VIDEO_ID,
          [^\w\s-]       # but char before ID is non-ID char.
        )                 # End host alternatives.
        ([\w-]{11})      # $1: VIDEO_ID is exactly 11 chars.
        (?=[^\w-]|$)     # Assert next char is non-ID or EOS.
        (?!               # Assert URL is not pre-linked.
          [?=&+%\w.-]*    # Allow URL (query) remainder.
          (?:             # Group pre-linked alternatives.
            [\'"][^<>]*>  # Either inside a start tag,
          | </a>          # or inside <a> element text contents.
          )               # End recognized pre-linked alts.
        )                 # End negative lookahead assertion.
        [?=&+%\w.-]*        # Consume any URL (query) remainder.
        ~ix',
            '$1',
            $text);
        return $text;
    }

    public static function getLinesFromStringFile($txt_file_real_path)
    {
        $file_content = file_get_contents($txt_file_real_path);
        return explode("\n", $file_content);
    }

    public static function prepareYoutubeLink($url, $autoplay = true)
    {

        if (preg_match('/youtube\.com\/watch\?v=([^\&\?\/]+)/', $url, $id)) {
            $video_id = $id[1];
        } else if (preg_match('/youtube\.com\/embed\/([^\&\?\/]+)/', $url, $id)) {
            $video_id = $id[1];
        } else if (preg_match('/youtube\.com\/v\/([^\&\?\/]+)/', $url, $id)) {
            $video_id = $id[1];
        } else if (preg_match('/youtu\.be\/([^\&\?\/]+)/', $url, $id)) {
            $video_id = $id[1];
        } else {
//            // not an youtube video
//            $result = KioskCommons::getUrlForIframe($url);
//            $youtube= false;
        }
//        return ($youtube)?  "http://www.youtube.com/embed/".$video_id."?".$mode : $result;
        return $video_id;
    }

    /**
     * @param $stateName :string ~ example: main.upload-images, this will be converted into main/upload-images
     * @param $stateParams :string ~ example: /1/ovid=123/ov_type=cv
     * @param bool $getFullPath
     * @return string
     */
    public static function getAngularAppLink($stateName, $stateParams, $getFullPath = true)
    {
        $stateName = str_replace('.', '/', $stateName);
        $link = Url::base($getFullPath) . "/site/page/$stateName";

        if (!empty($stateParams)) {
            $link .= $stateParams;
        }
        return $link;
    }
    
    public static function getAngularAppLinkManifest($stateName, $stateParams, $getFullPath = true)
    {
        $stateName = str_replace('.', '/', $stateName);
        $link = Url::base($getFullPath) . "/site/manifest/$stateName";

        if (!empty($stateParams)) {
            $link .= $stateParams;
        }
        return $link;
    }

    public static function getAccountSettings($account_id)
    {
        $setting = SettingAccount::find()->innerJoinWith([
                'setting' => function ($query) {
                    $query->andWhere(['name' => self::DEFAULT_OBJECT_DIM]);

                }]
        )->andWhere(['account_id' => $account_id])->one();

        return $setting;
    }

    public static function getCurrentMemberSettings($member_id)
    {
        $setting = SettingMember::find()->innerJoinWith([
                'setting' => function ($query) {
                    $query->andWhere(['name' => self::DEFAULT_OBJECT_DIM]);
                }]
        )->andWhere(['member_id' => $member_id])->one();

        return $setting;


    }


 public static function addMemberToAccount($target_account_id, $member_id_list, $role = self::ROLE_ADMIN, $uc_id = 0)
    {
        $shared_member_names = array();

        $existedAccount = AccountUser::findOne($target_account_id);
        if ($existedAccount != null) {
            foreach ($member_id_list as $member_id) {
                $existedMember = Member::findOne($member_id);
                if ($existedMember != null) {

                    $item_member = $existedMember->itemMember;
                    $group_acccount = $existedAccount->groupAccount;
                    if ($group_acccount != null && $item_member != null) {
                        $aum = AccountUserMember::find()->where(['account_id' => $target_account_id, 'member_id' => $member_id])->one();
                        if ($aum == null) {
                            $shareToMember = new AccountUserMember();
                            $shareToMember->account_id = $target_account_id;
                            $shareToMember->member_id = $member_id;
                            $shareToMember->role = $role;
                            $shareToMember->shared = self::ACCOUNT_SHARE;
                            $shareToMember->uc_id = $uc_id; 
                            if($uc_id === 0){
                            	$dfuClassSetting = Setting::find()->where(['name' => PermissionHelper::DEFAULT_USER_CLASS_ID])->one();
                            	if ($dfuClassSetting != null) {
                            		$shareToMember->uc_id = $dfuClassSetting->value_default;
                            	}
                            }

                            $shareToMember->save(false);
                        }
                        array_push($shared_member_names, $existedMember->getFullName());

                        /*
                        $groupaccount_itemmember = GaIm::find()->where(
                            [
                                'item_member_id' => $item_member->item_member_id,
                                'group_account_id' => $group_acccount->group_account_id
                            ]
                        )->one();
                        */

                        /*
                        if ($groupaccount_itemmember == null) {

                            $groupaccount_itemmember = new GaIm();
                            $groupaccount_itemmember->group_account_id = $group_acccount->group_account_id;
                            $groupaccount_itemmember->item_member_id = $item_member->item_member_id;
                            $groupaccount_itemmember->save(false);
                        }
                        */

                    }
                }
            }
        }

        return $shared_member_names;
    }

    public static function getAppSetting($settingKey){
        $setting = Setting::find()->where(['name' => $settingKey])->one();
        if ($setting != null) {
            return $setting->value_default;
        }
        return "";
    }
    

    public static function removeAccountUserMember($account_id, $member_id)
    {
    	// delete relationship account - member
    	AccountUserMember::deleteAll(array(
    			'member_id' => $member_id,
    			'account_id' => $account_id
    	));
    	// delete relationship group_account - item_member
    	$Acc = AccountUser::findOne($account_id);
    	$Mem = Member::findOne($member_id);
    	$item_member = $Mem->itemMember;
    	$group_acccount = $Acc->groupAccount;

        /*
        GaIm::deleteAll(array(
    			'item_member_id' => $item_member->item_member_id,
    			'group_account_id' => $group_acccount->group_account_id
    	));
        */
    	// end
    }
    
    public static function resetAccount($account_id)
    {
    	$account = AccountUser::findOne($account_id);
    	if ($account == null)
    		return false;
    	$accStorage = AccountStorage::find()->where(['account_id' => $account_id])->all();
    	CommonHelper::removeAllDirectoryInAccount($account_id);
    	foreach ($accStorage as $storage) {
    		$fileSize = CommonHelper::getSafeFloat($storage->file_size);
    		$file_name = "";
    		$iv = ItemVariant::findOne($storage->assoc_iv_id);
    		switch ($storage->type) {
    			case StorageHelper::USAGE_AVATAR:
    				$file_name = $account->avatar;
    				break;
    			case StorageHelper::USAGE_IV_ICON:
    
    				$file_name = $iv->icon_image;
    				break;
    			case StorageHelper::USAGE_IV_PREVIEW_ICON:
    
    				$file_name = FileHelper::getIvUploadFileName($iv->preview_kiosk_icon);
    				break;
    			case StorageHelper::USAGE_IV_OBJECT_FILE:
    
    				$file_name = $iv->upload_file;
    				break;
    		}
    
    
    		if ($fileSize > 0 && empty($file_name) == false) {
    			FileHelper::removeFileFromUser($file_name, $account->account_id, PermissionHelper::TYPE_OF_ACCOUNT);
    		}
    	}
    
    
    	$useHOS = Yii::$app->params['enableHOS'];
    	if ($useHOS) {
    		FileHelper::removeHpCloudAccountFolder($account->account_id);
    	}
    
    	//clean file tracking storage info
    
    	AccountStorage::deleteAll(array('account_id' => $account_id));
    
    	//remove collection variant, group variant, item variant by account id
    	CollectionVariant::deleteAll(array('account_id' => $account->account_id));
    	GroupVariant::deleteAll(array('account_id' => $account->account_id));
    	ItemVariant::deleteAll(array('account_id' => $account->account_id));
    	AccountStartup::deleteAll(array('account_id' => $account->account_id));
    		
    	$account->avatar = "";
    	$account->diskspace_cap = 0;
    	$account->company = self::DEFAULT_OBJECT_NAME;
    	$account->save(false);
    	
    	//rename to UNASSIGN
    	$group_account = $account->GroupAccount;
    	if($group_account != null){
    		$group_variant = GroupVariant::findOne($group_account->gv_id);
    		$group_variant->name = self::DEFAULT_OBJECT_NAME;
    		$group_variant->save(false);
    	}
    	
    	return $account;
    }
    
    public static function removeAllDirectoryInAccount($id)
    {
    	$useHOS = Yii::$app->params['enableHOS'];
    	$directory = DirectorySetup::findOne($id);
    	$directoryLayout = DirectoryListingLayout::find()->where(['id_directory' => $id])->all();
    	DirectorySetup::deleteAll(array('account_id' => $id));
    	if (isset($directory)) {
    		foreach ($directory as $dir) {
    			DirectoryListingColorFont::deleteAll(array('id_directory' => $dir->id));
    			DirectoryListingLayout::deleteAll(array('id_directory' => $dir->id));
    			DirectoryListing::deleteAll(array('id_directory' => $dir->id));
    			DirectoryPublic::deleteAll(array('directory_id' => $dir->id));
    
    			$filePhp = CommonHelper::create_permalink_title($directory->directory_title) . '_' . $directory->id . '.php';
    			$fileXml = CommonHelper::create_permalink_title($directory->directory_title) . '_' . $directory->id . '.xml';
    			FileHelper::removeFileFromUser($filePhp, $id, 2, FileHelper::UPLOAD_FILE_FOLDER_PUBLIC);
    			FileHelper::removeFileFromUser($fileXml, $id, 2, FileHelper::UPLOAD_FILE_FOLDER_PUBLIC);
    
    			if ($useHOS) {
    				FileHelper::removeHOSFileUponUserViewing($dir->building_image);
    
    			} else {
    				FileHelper::removeFileFromUser($dir->building_image, $id, 2);
    
    			}
    		}
    	}
    	if (isset($directoryLayout)) {
    		foreach ($directoryLayout as $dir) {
    
    			if ($useHOS) {
    				FileHelper::removeHOSFileUponUserViewing($dir->image_file);
    			} else {
    				FileHelper::removeFileFromUser($dir->image_file, $id, 2);
    			}
    
    		}
    	}
    }
    

    public static function create_permalink_title($str)
    {
    	$str = CommonHelper::shorten_string($str, 4);
    	$str = str_replace(' ...', '', $str);
    	$str = preg_replace('`&([a-z]{1,2})(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig);`i', '\\1', $str);
    	$str = html_entity_decode($str, ENT_NOQUOTES, 'UTF-8');
    	$str = preg_replace(array('`[^a-z0-9]`i', '`[-]+`'), '_', $str);
    	$str = strtolower(trim($str, '_'));
    	return $str;
    }
    
    public static function shorten_string($string, $wordsreturned)
    /*  Returns the first $wordsreturned out of $string.  If string
     contains fewer words than $wordsreturned, the entire string
    is returned.
    */
    {
    	$retval = $string; //  Just in case of a problem
    
    	$array = explode(" ", $string);
    	if (count($array) <= $wordsreturned) /*  Already short enough, return the whole thing
    	*/ {
    	$retval = $string;
    	} else /*  Need to chop of some words
    	*/ {
    	array_splice($array, $wordsreturned);
    	$retval = implode(" ", $array) . " ...";
    	}
    	return $retval;
    }

    public static function moveFileFromTempToCurrentUser($file_name, $item_ext = ""){

        $currentUser = PermissionHelper::getUserAccessing();
        $filesFolder = "";
        if ($currentUser['id'] == 'member_id') {
            $member_id = $currentUser['val'];
            $member = Member::findOne($member_id);
            if (!empty($member) && !empty($member->upload_folder)) {
                //$folder_upload_member = $member_info['upload_folder'];
                $filesFolder = FileHelper::getUploadFolderRealPath() . $member->upload_folder . ObjectToolbar::UPLOAD_FILE_FOLDER_MEMBER;

            }
        } elseif ($currentUser['id'] == 'account_id') {
            $acc_id = $currentUser['val'];
            $acc = AccountUser::findOne($acc_id);
            $member = Member::findOne($acc->member_id);
            if (!empty($member) && !empty($member->upload_folder)) {
                //$folder_upload_member = $member_info['upload_folder'];
                $filesFolder = FileHelper::getUploadFolderRealPath() . $member->upload_folder . FileHelper::UPLOAD_FOLDER_ACCOUNT . $acc->upload_folder;
            }
        }
		
        $from_file_path = FileHelper::getUploadFolderRealPath() . ObjectToolbar::UPLOAD_TEMP_FOLDER_NAME . $file_name;
        $from_file_path = str_replace('//', '/', $from_file_path);

        $to_file_path = $filesFolder . '/' . $file_name;
        $to_file_path = str_replace('//', '/', $to_file_path);

        @chmod($from_file_path, 0777);
        @chmod($to_file_path, 0777);
        
        $tempFileExt = pathinfo($file_name, PATHINFO_EXTENSION);

        if ($item_ext == ObjectToolbar::VIDEO_EXT && $tempFileExt != 'mp4'){// && self::isLocalHostRequest() == false
        	$extension = '.mp4';
        	$output_file_path = $filesFolder .pathinfo($file_name, PATHINFO_FILENAME).$extension;
        	CommonHelper::convertToMp4($from_file_path, $output_file_path);
        	rename($from_file_path, $output_file_path);
        }else{
        	if(is_file($from_file_path)){
        		rename($from_file_path, $to_file_path);
        	}
        }
          
    }

    //check file in system resource, if file existed, return url
    public static function checkExistFileInSystemResource($file, $defaultImageName = "", &$found = true)
    {
        $absolutePath = sprintf("%s%s/%s", FileHelper::getUploadPath(true), ObjectToolbar::SYSTEM_RESOURCES_DIR, $file);

        $realFilePath = sprintf("%s%s%s/%s", realpath(FileHelper::get_root_folder()), FileHelper::UPLOAD_FILE_FOLDER_NAME, ObjectToolbar::SYSTEM_RESOURCES_DIR, $file);

        if (!empty($file) && file_exists($realFilePath))
            return $absolutePath;

        $found = false;
        $_defaultDisplayFileName = 'no-image.png';
        if ($defaultImageName != "")
            $_defaultDisplayFileName = $defaultImageName;
        return FileHelper::getUploadPath(true) . $_defaultDisplayFileName;
    }

    public static function isObjectInArray($obj, $property, $array)
    {
    	$found = null;
    	foreach ($array as $item) {
    		if ($obj->$property == $item->$property) {
    			$found = $item;
    			break;
    		}
    	}
    	return $found;
    }

    public static function isEnableToAccessAdminView($account_id = '', $member_id = '')
    {
        $request = Yii::$app->request;
        if ($account_id == '' || $account_id == '0' || $account_id == 0) {
            $account_id = $request->post('acc_id', '');
        }

        if ($member_id == '' || $member_id == '0' || $member_id == 0) {
            $member_id = Member::getLoginId();
        }

        return CommonHelper::isAdminUser() == true || CommonHelper::isAdminOrViewingAsAdminUnderAccountAdmin($account_id, $member_id) == true;
    }
    public static function isAdminUser()
    {
        $getrole = CommonHelper::getCurrentMemberRole();
        return ($getrole == PermissionHelper::SS_ADMIN_ID || $getrole == ObjectToolbar::MEMBER_ROLE_ADMIN);
    }

    public static function getCurrentMemberRole()
    {
        $member_id = Member::getLoginId();
        $currentMember = Member::findOne($member_id);
        if ($currentMember == null) {
            throw new HttpException('3418', 'Could not find login user');
        }
        if ($currentMember->is_superadmin == 1 && $currentMember->is_admin == 1)
            return ObjectToolbar::SYSTEM_ADMIN_MEMBER_ID;
        elseif ($currentMember->is_admin == 1)
            return ObjectToolbar::MEMBER_ROLE_ADMIN;
        else
            return ObjectToolbar::MEMBER_ROLE_REGULAR;
    }
    public static function isAdminOrViewingAsAdminUnderAccountAdmin($account_id = '', $member_id = '')
    {

        $request = Yii::$app->request;

        if ($account_id == '' || $account_id == '0' || $account_id == 0) {
            $account_id = $request->post('id', '');
        }

        if ($member_id == '' || $member_id == '0' || $member_id == 0) {
            $member_id = Member::getLoginId();
        }

        $currentMember = Member::findOne($member_id);
        if ($currentMember->is_admin)
            return true;

        $currentAccount = AccountUser::findOne($account_id);
        if ($currentAccount != null) {
            //Yii::info($account_id. ' - '.$member_id);
//            $searchMatch = $currentAccount->find()->with(array(
//                'MembersUnderAsAdmin' => array(
//                    'condition' => 'AccountUserMembers.member_id=:_member_id AND AccountUserMembers.account_id=:_account_idt',
//                    'params' => array('_member_id' => $member_id, '_account_idt' => $account_id)
//                )))->where(array('is_admin' => 1));
            $searchMatch = $currentAccount->find()->innerJoinWith(array(
                'accountUserMembers' => function($query) use($member_id,$account_id){
                    $query->where(['tbl_account_user_member.member_id' => $member_id])
                        ->andWhere(['tbl_account_user_member.account_id' => $account_id])
                        //->andWhere(['is_admin'=> 1])
                    ;
                }
                ))->andWhere(['is_admin'=> 1])->all();

           // Yii::info(count($searchMatch));
            return count($searchMatch) > 0;
        }

        return false;
    }

    public static function move_file_upload_folder_account($upload_file_name, $folder_member, $folder_desc)
    {
        $folder_upload_user = '';
        if (isset($upload_file_name) && !empty($upload_file_name)) {
            $root_folder = FileHelper::get_root_folder();
            $upload_file_src = FileHelper::getUploadFolderRealPath(). 'temp/' . $upload_file_name;
            // move folder upload file
            if (file_exists($upload_file_src)) {
                $upload_file_desc = FileHelper::getUploadFolderRealPath() . $folder_member . FileHelper::UPLOAD_FOLDER_ACCOUNT . $folder_desc . '/' . $upload_file_name;
                rename($upload_file_src, $upload_file_desc);
            }
        }
        return $folder_upload_user;
    }
    public static function gen_folder_upload($length = 20, $level = 2)
    {
        list ($usec, $sec) = explode(' ', microtime());
        srand(( float )$sec + (( float )$usec * 100000));

        $validchars [1] = "0123456789abcdfghjkmnpqrstvwxyz";
        $validchars [2] = "123456789abcdfghjkmnpqrstvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $validchars [3] = "0123456789_!@#$%&*()-=+/abcdfghjkmnpqrstvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_!@#$%&*()-=+/";

        $folder_name = "";
        $counter = 0;

        while ($counter < $length) {
            $actChar = substr($validchars [$level], rand(0, strlen($validchars [$level]) - 1), 1);

            // All character must be different
            if (!strstr($folder_name, $actChar)) {
                $folder_name .= $actChar;
                $counter++;
            }
        }
        return $folder_name;
    }
    public static function isViewingUnderSharedAccountRoleAdmin()
    {
        $request = Yii::$app->request;
        $account_id = $request->post('id', '');
        $member_id = Member::getLoginId();

        $currentAccount = AccountUser::findOne($account_id);
        if ($currentAccount == null || $currentAccount->is_admin) {
            return false;
        }

        $searchMatch = $searchMatch = AccountUserMember::find()->where([
                'member_id' => $member_id, 'account_id' => $account_id, 'uc_id' => '1', 'shared' => '1']
        );

        return count($searchMatch) > 0;
    }
//    static $ADDRESS_TYPE = array(
//        array("text" => "Billing", "value" => "1"),
//        array("text" => "Shipping ", "value" => "2"),
//        array("text" => "Physical ", "value" => "3"),
//        array("text" => "Other ", "value" => "4")
//    );

    static $ADDRESS_TYPE = array(
        "1" => "Billing",
        "2" => "Shipping ",
        "3" => "Physical",
        "4" => "Other"
    );

    public static function filter_by_value($array, $index, $value)
    {
        $newarray = array();
        if (is_array($array) && count($array) > 0) {
            foreach (array_keys($array) as $key) {
                $temp[$key] = $array[$key][$index];

                if ($temp[$key] == $value) {
                    $newarray[] = $array[$key];
                    break;
                }
            }
        }
        return $newarray;
    }

    public static function getSampleImageName(){ return rand(1, 10) . '.jpg'; }
    public static function getSampleVideoName(){ //pick random 1 extension
        $possibleExts = ['mp4', 'avi', 'mkv', 'm4v', 'mpg', 'mov', 'wmv', 'flv'];
        return '1' . ".{$possibleExts[array_rand($possibleExts, 1)]}";
    }
    public static function getSampleUrl(){ //pick random 1 url
        $possibleUrls = [
            'http://apple.com', 'http://www.nationalgeographic.com', 'http://codepen.io', 'http://www.teslamotors.com',
            'https://www.amctheatres.com', 'http://www.hbo.com', 'http://www.axn-asia.com', 'https://www.discovery.co.za/portal'
        ];
        return '1' . ".{$possibleUrls[array_rand($possibleUrls, 1)]}";
    }

    /* get a range with PHP from A to ZZ
            a b c ... aa ... zx zy zz
            example: createColumnsArray(27): A B C ... AZ.
        */
    public static function createColumnsArrayByLength($length, $first_letters = '')
    {
        $columns = array();
        $letters = range('A', 'Z');

        // Iterate over 26 letters.
        foreach ($letters as $key => $letter) {
            // Paste the $first_letters before the next.
            $column = $first_letters . $letter;

            // Add the column to the final array.
            $columns[] = $column;
            // If it was the end column that was added, return the columns.
            if ($key == ($length - 1))
                return $columns;

        }

        // Add the column children.
        foreach ($columns as $column) {
            // Don't itterate if the $end_column was already set in a previous itteration.
            // Stop iterating if you've reached the maximum character length.
            if (count($columns) < $length) {
                $new_columns = self::createColumnsArrayByLength($length - count($columns), $column);
                // Merge the new columns which were created with the final columns array.
                $columns = array_merge($columns, $new_columns);
            }
        }

        return $columns;
    }

    public static function createAssignAlphabetChar($index){
        $alphabetList = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charLength = strlen($alphabetList);

        $n = (int) ($index / $charLength) - 1;
        $remainder = $index%$charLength;
        $result = '';
        for($i=0;$i<=$n;$i++){
            $result .= 'A';
        }
        $result .= $alphabetList[$remainder];
        return $result;
    }
    public static function utf8ize($d) {
        if (is_array($d)) {
            foreach ($d as $k => $v) {
                $d[$k] = utf8ize($v);
            }
        } else if (is_string ($d)) {
            return utf8_encode($d);
        }
        return $d;
    }
    /**
     * Get page title (Create/Update Something) based on the edit_mode, if edit_mode is not set,
     * it would check the $pkName - a key of query string to check whether the page is in edit mode or not
     * @param $name
     * @param null $edit_mode
     * @param string $pkName
     * @return string
     */
    public static function getPageTitle($name, $edit_mode = null, $pkName = ""){
        $request = Yii::$app->request;
        if($edit_mode == null){
            $post_id = $request->post($pkName);
            $edit_mode = !CommonHelper::isNullOrEmptyString($post_id);
        }

        $actionName = $edit_mode ? "Update" : "Create";
        return "$actionName $name";
    }

    public static function removeUnwantedStringFromString($forbidArr, $string){
        $result = $string;
        foreach($forbidArr as $forbid_word)
        {
            $result = str_replace($forbid_word, '', $string);
        }
        return $result;
    }

    /** add a new value into the array, if the value is exist, remote the duplicate
     * @param $value
     * @param $array
     * @return array
     */
    public static function addUniqueValueIntoArray($value, $array){
        $array[] = $value; return array_unique($array);
    }

    public static function array_flatten(array $array)
    {
        $flat = array(); // initialize return array
        $stack = array_values($array); // initialize stack
        while ($stack) // process stack until done
        {
            $value = array_shift($stack);
            if (is_array($value)) // a value to further process
            {
                $stack = array_merge(array_values($value), $stack);
            }
            else // a value to take
            {
                $flat[] = $value;
            }
        }
        return $flat;
    }
    
    
    public static function updateObjectUcActions($mtype, $oid, $uc_id, $list_action_id)
    {
    
    
    	$master_obj_prop_id = $mtype . '_id';
    	$modelUcActionName ="common\\models\\".strtoupper(substr($mtype, 0, 1)) . 'ActionHasUc';
    
    	//reset all action of user class & collection to false first
    	$modelUcActionName::updateAll(['active' => 0],
    			"$master_obj_prop_id =:oid AND uc_id =:uc_id",
    			['oid' => $oid, 'uc_id' => $uc_id]);
    
    	foreach ($list_action_id as $action_id) {
    		//active action of user class for the give object (collection, group or item)
    		$rts = $modelUcActionName::find()->where([
    				$master_obj_prop_id => $oid, 'uc_id' => $uc_id, 'action_id' => $action_id
    		])->one();
    		
    		if ($rts == null) { //if the action relationship did not exist, create new one
    			//check if the main relationship between the master obj and ObjectAction exist or not
    			$modelActionName = "common\\models\\".ucfirst($mtype) . 'Action';
    			$temp_rts = $modelActionName::find()->where([
    					$master_obj_prop_id => $oid, 'action_id' => $action_id
    			])->one();
    			if ($temp_rts == null) {
    				$temp_rts = new $modelActionName;
    				$temp_rts->{$master_obj_prop_id} = $oid;
    				$temp_rts->action_id = $action_id;
    				$temp_rts->save(false);
    			}
    
    			$rts = new $modelUcActionName;
    			$rts->{$master_obj_prop_id} = $oid;
    			$rts->uc_id = $uc_id;
    			$rts->action_id = $action_id;
    
    		}
    
    		$rts->active = 1;
    		$rts->save(false);
    	}
    }
    
    public static function getAttributeInArrayObject($array, $attribute){
    	$attribute_arr=[];
    	foreach($array as $value){
    		$attribute_arr[] = $value->$attribute;
    	}
    	
    	return $attribute_arr;
    }

    public static function getWebsiteUrlStatus($url, &$error)
    {

        $found_site = false;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_NOBODY, true);

        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); //ignore ssl with link starts with https

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10); //follow up to 10 redirections - avoids loops
        $data = curl_exec($ch);
        curl_close($ch);
        if ($data == false) {
            $error = "Domain could not be found";
        }
        else {
            preg_match_all("/HTTP\/1\.[1|0]\s(\d{3})/", $data, $matches);
            $code = end($matches[1]);
            if ($code == 200) {
                $error = "Page Found";
                $found_site = true;
            }
            elseif ($code == 404) {
                $error = "Page Not Found";
            }
            elseif ($code == 405) {
                $error = "Method Not Allowed";
                $found_site = true;
            }
        }

        return $found_site;
    }

    public static function getClientIPAddress() {
        // check for shared internet/ISP IP
        if (!empty($_SERVER['HTTP_CLIENT_IP']) && validate_ip($_SERVER['HTTP_CLIENT_IP'])) {
            return $_SERVER['HTTP_CLIENT_IP'];
        }

        // check for IPs passing through proxies
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            // check if multiple ips exist in var
            if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') !== false) {
                $iplist = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                foreach ($iplist as $ip) {
                    if (validate_ip($ip))
                        return $ip;
                }
            } else {
                if (validate_ip($_SERVER['HTTP_X_FORWARDED_FOR']))
                    return $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
        }
        if (!empty($_SERVER['HTTP_X_FORWARDED']) && validate_ip($_SERVER['HTTP_X_FORWARDED']))
            return $_SERVER['HTTP_X_FORWARDED'];
        if (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) && validate_ip($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
            return $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
        if (!empty($_SERVER['HTTP_FORWARDED_FOR']) && validate_ip($_SERVER['HTTP_FORWARDED_FOR']))
            return $_SERVER['HTTP_FORWARDED_FOR'];
        if (!empty($_SERVER['HTTP_FORWARDED']) && validate_ip($_SERVER['HTTP_FORWARDED']))
            return $_SERVER['HTTP_FORWARDED'];

        // return unreliable ip since all else failed
        return $_SERVER['REMOTE_ADDR'];
    }

    public static function arrayToObject(array $array, $className)
    {
        return unserialize(sprintf(
            'O:%d:"%s"%s',
            strlen($className),
            $className,
            strstr(serialize($array), ':')
        ));
    }

    public static function objectToObject($instance, $className)
    {
        return unserialize(sprintf(
            'O:%d:"%s"%s',
            strlen($className),
            $className,
            strstr(strstr(serialize($instance), '"'), ':')
        ));
    }
    
    public static function convertToMp4($input, $output)
    {
    	$input = escapeshellarg($input);
    	$output = escapeshellarg($output);
    	// echo "Converting $input to $output<br />";
    	// $command = "mencoder $input -o $output -af volume=10 -aspect 16:9 -of avi -noodml -ovc x264 -x264encopts bitrate=300:level_idc=41:bframes=3:frameref=2: nopsnr: nossim: pass=1: threads=auto -oac mp3lame ";
    	//$command = "ffmpeg -y -i $input -acodec libfaac -ab 96k -vcodec libx264 -vpre slower -vpre main -level 21 -refs 2 -b 345k -bt 345k -threads 0 -s 640x360 $output";
    	$command = "ffmpeg -i $input -acodec copy -vcodec copy -scodec copy $output";
    	Yii::info($command);
    	shell_exec($command);
    
    }

    public static function isSiteSSL()
    {
        return (isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) != 'off') ? TRUE : FALSE;
    }

    /**
     * return the site protocol without double slashes
     * @return string
     */
    public static function getSiteProtocol()
    {
        return CommonHelper::isSiteSSL() ? "https:" : "http:";
    }
    
    
    public static function getFastUrl($string)
    {
    	$regex = '/http?\:\/\/[^\" ]+/i';
    	preg_match_all($regex, $string, $matches);
    	return ($matches[0]);
    }

}