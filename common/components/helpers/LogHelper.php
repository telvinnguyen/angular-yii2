<?php

namespace common\component\helpers;
use Yii;

/**
 * Class LogHelper
 * @package common\component\helpers
 */
class LogHelper
{
    const ARB_SILENT_POST = "ARBSilentPost";

    public static function add($dump, $category){
        $filePath = '';
        if($category == LogHelper::ARB_SILENT_POST){
            $filePath = CommonHelper::sanitizePath(FileHelper::get_root_folder() . 'logs/ARB_silentPost.log');
        }
        $data = print_r($dump, true) . "\n";
        chmod($filePath, 0755);

        //$data = var_export($array);
        file_put_contents($filePath, $data, FILE_APPEND | LOCK_EX);

    }

}