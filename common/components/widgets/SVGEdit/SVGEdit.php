<?php

namespace common\component\widgets;

use Yii;
use yii\base\Widget;

/**
 * SVG-Edit Class File
 * http://code.google.com/p/svg-edit/
 * @svg-editor version 2.6
 */
class SVGEdit extends Widget
{
    /*
        Defined in the widget under 'name'=>@string
    */
    public $name = 'SVG-Edit';

    /*
        Defined in the widget under 'options'=>array()
    */
    public $options = array();

    /*
    * Best not to allow any options that will change the default paths, etc - it will break otherwise
    */
    public $allowedOptions = array(
        'lang', 'bkgd_url', 'img_save', 'dimensions', 'initFill', 'initStroke', 'initTool', 'exportWindowType', 'imgPath', 'jGraduatePath', 'langPath', 'extPath', 'extensions', 'showlayers', 'wireframe', 'gridSnapping', 'gridColor', 'baseUnit', 'snappingStep', 'showRulers', 'no_save_warning', 'canvas_expansion', 'show_outside_canvas', 'iconsize', 'bkgd_color', 'selectNew', 'save_notice_done', 'export_notice_done', 'allowedOrigins', 'canvasName', 'initOpacity', 'colorPickerCSS', 'preventAllURLConfig', 'preventURLContentLoading', 'lockExtensions', 'noDefaultExtensions', 'showGrid', 'noStorageOnLoad', 'forceStorage', 'emptyStorageOnDecline', 'paramurl', 'selectNew',
        //custom attribute
        'dbImageInfo'
    );

    public $loadFromString = null;

    public $loadFromDataURI = null;

    public $loadFromUrl = null;


    public function init()
    {
        parent::init();

    }

    public function run()
    {
        $options = array();
        if (isset($this->options)) {
            //$allowedOptions = array_flip($this->allowedOptions);

            foreach ($this->options as $optKey => $option) {
                if (!is_array($option)) {
                    if (in_array($optKey, $this->allowedOptions)) {
                        $options[$optKey] = $option;
                    }
                } else {

                    if (in_array($optKey, $this->allowedOptions)) {
                        $options[$optKey] = $option;
                    }
                }
            }
        }
        $options = json_encode($options);
        return $this->render("svg-editor", [
            'name' => $this->name,
            'options' => $options,
            'loadFromString' => $this->loadFromString,
            'loadFromDataURI' => $this->loadFromDataURI,
            'loadFromUrl' => $this->loadFromUrl
        ]);
    }

}
