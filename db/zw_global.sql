-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Apr 28, 2016 at 01:50 PM
-- Server version: 5.5.42
-- PHP Version: 7.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zw_global`
--

-- --------------------------------------------------------

--
-- Table structure for table `zw_activity`
--

CREATE TABLE `zw_activity` (
  `act_id` int(11) NOT NULL,
  `type` int(11) DEFAULT NULL,
  `description` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `zw_comment`
--

CREATE TABLE `zw_comment` (
  `comment_id` bigint(20) NOT NULL,
  `entity_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `content` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `zw_entity`
--

CREATE TABLE `zw_entity` (
  `entity_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `zw_entity_tag`
--

CREATE TABLE `zw_entity_tag` (
  `entity_id` int(11) NOT NULL,
  `tag_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `zw_ipaddress`
--

CREATE TABLE `zw_ipaddress` (
  `ip_address_id` int(11) NOT NULL,
  `ip` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `zw_notification`
--

CREATE TABLE `zw_notification` (
  `notification_id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `zw_notification_change`
--

CREATE TABLE `zw_notification_change` (
  `notification_change` int(11) NOT NULL,
  `notification_object_id` bigint(20) NOT NULL,
  `action` text COLLATE utf8_unicode_ci,
  `actor` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `zw_notification_object`
--

CREATE TABLE `zw_notification_object` (
  `notification_object_id` bigint(20) NOT NULL,
  `notification_id` bigint(20) NOT NULL,
  `object` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `zw_picture`
--

CREATE TABLE `zw_picture` (
  `picture_id` int(11) NOT NULL,
  `zw_entity_entity_id` int(11) NOT NULL,
  `file_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `size` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ext` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `zw_post`
--

CREATE TABLE `zw_post` (
  `post_id` int(11) NOT NULL,
  `entity_id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `zw_post_in_topic`
--

CREATE TABLE `zw_post_in_topic` (
  `topic_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `zw_setting`
--

CREATE TABLE `zw_setting` (
  `setting_key` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `setting_cat_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `zw_setting`
--

INSERT INTO `zw_setting` (`setting_key`, `value`, `setting_cat_id`) VALUES
('login_attemp', '3', 3),
('signup_notification_email', 'telvin@vidaltek.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `zw_setting_category`
--

CREATE TABLE `zw_setting_category` (
  `setting_cat_id` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `zw_setting_category`
--

INSERT INTO `zw_setting_category` (`setting_cat_id`, `name`) VALUES
(1, 'system'),
(2, 'misc'),
(3, 'authentication'),
(4, 'authorization');

-- --------------------------------------------------------

--
-- Table structure for table `zw_tag`
--

CREATE TABLE `zw_tag` (
  `tag_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `zw_topic`
--

CREATE TABLE `zw_topic` (
  `topic_id` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` int(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `zw_topic`
--

INSERT INTO `zw_topic` (`topic_id`, `name`, `active`) VALUES
(1, 'Fish', 1),
(2, 'Animal', 1),
(3, 'Movie', 1),
(4, 'Holywood', 1),
(5, 'Internet', 1),
(6, 'Europe', 1),
(7, 'Graphic', 1),
(8, 'Investing', 1),
(9, 'Music', 1),
(10, 'Baking', 1),
(11, 'Dance', 1),
(12, 'Investing', 1);

-- --------------------------------------------------------

--
-- Table structure for table `zw_user`
--

CREATE TABLE `zw_user` (
  `user_id` int(11) NOT NULL,
  `username` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `full_name` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` int(1) DEFAULT NULL,
  `login_failed` int(2) DEFAULT NULL,
  `openid_url` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `zw_user_follow_topic`
--

CREATE TABLE `zw_user_follow_topic` (
  `user_id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `join_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `zw_user_has_role`
--

CREATE TABLE `zw_user_has_role` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `zw_user_like_entity`
--

CREATE TABLE `zw_user_like_entity` (
  `entity_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `zw_user_role`
--

CREATE TABLE `zw_user_role` (
  `role_id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `zw_activity`
--
ALTER TABLE `zw_activity`
  ADD PRIMARY KEY (`act_id`);

--
-- Indexes for table `zw_comment`
--
ALTER TABLE `zw_comment`
  ADD PRIMARY KEY (`comment_id`,`entity_id`,`user_id`),
  ADD KEY `fk_zw_comment_zw_entity1_idx` (`entity_id`),
  ADD KEY `fk_zw_comment_zw_user1_idx` (`user_id`);

--
-- Indexes for table `zw_entity`
--
ALTER TABLE `zw_entity`
  ADD PRIMARY KEY (`entity_id`);

--
-- Indexes for table `zw_entity_tag`
--
ALTER TABLE `zw_entity_tag`
  ADD PRIMARY KEY (`entity_id`,`tag_name`),
  ADD KEY `fk_zw_entity_has_zw_tag_zw_tag1_idx` (`tag_name`),
  ADD KEY `fk_zw_entity_has_zw_tag_zw_entity1_idx` (`entity_id`);

--
-- Indexes for table `zw_ipaddress`
--
ALTER TABLE `zw_ipaddress`
  ADD PRIMARY KEY (`ip_address_id`),
  ADD KEY `fk_zw_ipaddress_zw_user_idx` (`user_id`);

--
-- Indexes for table `zw_notification`
--
ALTER TABLE `zw_notification`
  ADD PRIMARY KEY (`notification_id`),
  ADD KEY `fk_zw_notification_zw_user1_idx` (`user_id`);

--
-- Indexes for table `zw_notification_change`
--
ALTER TABLE `zw_notification_change`
  ADD PRIMARY KEY (`notification_change`,`notification_object_id`),
  ADD KEY `fk_zw_notification_change_zw_notification_object1_idx` (`notification_object_id`);

--
-- Indexes for table `zw_notification_object`
--
ALTER TABLE `zw_notification_object`
  ADD PRIMARY KEY (`notification_object_id`),
  ADD KEY `fk_zw_notification_object_zw_notification1_idx` (`notification_id`);

--
-- Indexes for table `zw_picture`
--
ALTER TABLE `zw_picture`
  ADD PRIMARY KEY (`picture_id`),
  ADD KEY `fk_zw_picture_zw_entity1_idx` (`zw_entity_entity_id`);

--
-- Indexes for table `zw_post`
--
ALTER TABLE `zw_post`
  ADD PRIMARY KEY (`post_id`),
  ADD KEY `fk_zw_post_zw_entity1_idx` (`entity_id`);

--
-- Indexes for table `zw_post_in_topic`
--
ALTER TABLE `zw_post_in_topic`
  ADD PRIMARY KEY (`topic_id`,`post_id`),
  ADD KEY `fk_zw_topic_has_zw_post_zw_post1_idx` (`post_id`),
  ADD KEY `fk_zw_topic_has_zw_post_zw_topic1_idx` (`topic_id`);

--
-- Indexes for table `zw_setting`
--
ALTER TABLE `zw_setting`
  ADD PRIMARY KEY (`setting_key`),
  ADD KEY `fk_zw_setting_zw_setting_category_idx` (`setting_cat_id`);

--
-- Indexes for table `zw_setting_category`
--
ALTER TABLE `zw_setting_category`
  ADD PRIMARY KEY (`setting_cat_id`);

--
-- Indexes for table `zw_tag`
--
ALTER TABLE `zw_tag`
  ADD PRIMARY KEY (`tag_name`);

--
-- Indexes for table `zw_topic`
--
ALTER TABLE `zw_topic`
  ADD PRIMARY KEY (`topic_id`);

--
-- Indexes for table `zw_user`
--
ALTER TABLE `zw_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `zw_user_follow_topic`
--
ALTER TABLE `zw_user_follow_topic`
  ADD PRIMARY KEY (`user_id`,`topic_id`),
  ADD KEY `fk_zw_user_has_zw_topic_zw_topic1_idx` (`topic_id`),
  ADD KEY `fk_zw_user_has_zw_topic_zw_user1_idx` (`user_id`);

--
-- Indexes for table `zw_user_has_role`
--
ALTER TABLE `zw_user_has_role`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `fk_zw_user_has_zw_user_role_zw_user_role1_idx` (`role_id`),
  ADD KEY `fk_zw_user_has_zw_user_role_zw_user1_idx` (`user_id`);

--
-- Indexes for table `zw_user_like_entity`
--
ALTER TABLE `zw_user_like_entity`
  ADD PRIMARY KEY (`entity_id`,`user_id`),
  ADD KEY `fk_zw_entity_has_zw_user_zw_user1_idx` (`user_id`),
  ADD KEY `fk_zw_entity_has_zw_user_zw_entity1_idx` (`entity_id`);

--
-- Indexes for table `zw_user_role`
--
ALTER TABLE `zw_user_role`
  ADD PRIMARY KEY (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `zw_activity`
--
ALTER TABLE `zw_activity`
  MODIFY `act_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zw_ipaddress`
--
ALTER TABLE `zw_ipaddress`
  MODIFY `ip_address_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zw_notification`
--
ALTER TABLE `zw_notification`
  MODIFY `notification_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zw_notification_object`
--
ALTER TABLE `zw_notification_object`
  MODIFY `notification_object_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zw_picture`
--
ALTER TABLE `zw_picture`
  MODIFY `picture_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zw_post`
--
ALTER TABLE `zw_post`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zw_setting_category`
--
ALTER TABLE `zw_setting_category`
  MODIFY `setting_cat_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `zw_topic`
--
ALTER TABLE `zw_topic`
  MODIFY `topic_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `zw_user`
--
ALTER TABLE `zw_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `zw_comment`
--
ALTER TABLE `zw_comment`
  ADD CONSTRAINT `fk_zw_comment_zw_entity1` FOREIGN KEY (`entity_id`) REFERENCES `zw_entity` (`entity_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_zw_comment_zw_user1` FOREIGN KEY (`user_id`) REFERENCES `zw_user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zw_entity_tag`
--
ALTER TABLE `zw_entity_tag`
  ADD CONSTRAINT `fk_zw_entity_has_zw_tag_zw_entity1` FOREIGN KEY (`entity_id`) REFERENCES `zw_entity` (`entity_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_zw_entity_has_zw_tag_zw_tag1` FOREIGN KEY (`tag_name`) REFERENCES `zw_tag` (`tag_name`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zw_ipaddress`
--
ALTER TABLE `zw_ipaddress`
  ADD CONSTRAINT `fk_zw_ipaddress_zw_user` FOREIGN KEY (`user_id`) REFERENCES `zw_user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zw_notification`
--
ALTER TABLE `zw_notification`
  ADD CONSTRAINT `fk_zw_notification_zw_user1` FOREIGN KEY (`user_id`) REFERENCES `zw_user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zw_notification_change`
--
ALTER TABLE `zw_notification_change`
  ADD CONSTRAINT `fk_zw_notification_change_zw_notification_object1` FOREIGN KEY (`notification_object_id`) REFERENCES `zw_notification_object` (`notification_object_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `zw_notification_object`
--
ALTER TABLE `zw_notification_object`
  ADD CONSTRAINT `fk_zw_notification_object_zw_notification1` FOREIGN KEY (`notification_id`) REFERENCES `zw_notification` (`notification_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `zw_picture`
--
ALTER TABLE `zw_picture`
  ADD CONSTRAINT `fk_zw_picture_zw_entity1` FOREIGN KEY (`zw_entity_entity_id`) REFERENCES `zw_entity` (`entity_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zw_post`
--
ALTER TABLE `zw_post`
  ADD CONSTRAINT `fk_zw_post_zw_entity1` FOREIGN KEY (`entity_id`) REFERENCES `zw_entity` (`entity_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zw_post_in_topic`
--
ALTER TABLE `zw_post_in_topic`
  ADD CONSTRAINT `fk_zw_topic_has_zw_post_zw_post1` FOREIGN KEY (`post_id`) REFERENCES `zw_post` (`post_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_zw_topic_has_zw_post_zw_topic1` FOREIGN KEY (`topic_id`) REFERENCES `zw_topic` (`topic_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `zw_setting`
--
ALTER TABLE `zw_setting`
  ADD CONSTRAINT `fk_zw_setting_zw_setting_category` FOREIGN KEY (`setting_cat_id`) REFERENCES `zw_setting_category` (`setting_cat_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zw_user_follow_topic`
--
ALTER TABLE `zw_user_follow_topic`
  ADD CONSTRAINT `fk_zw_user_has_zw_topic_zw_topic1` FOREIGN KEY (`topic_id`) REFERENCES `zw_topic` (`topic_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_zw_user_has_zw_topic_zw_user1` FOREIGN KEY (`user_id`) REFERENCES `zw_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `zw_user_has_role`
--
ALTER TABLE `zw_user_has_role`
  ADD CONSTRAINT `fk_zw_user_has_zw_user_role_zw_user1` FOREIGN KEY (`user_id`) REFERENCES `zw_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_zw_user_has_zw_user_role_zw_user_role1` FOREIGN KEY (`role_id`) REFERENCES `zw_user_role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `zw_user_like_entity`
--
ALTER TABLE `zw_user_like_entity`
  ADD CONSTRAINT `fk_zw_entity_has_zw_user_zw_entity1` FOREIGN KEY (`entity_id`) REFERENCES `zw_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_zw_entity_has_zw_user_zw_user1` FOREIGN KEY (`user_id`) REFERENCES `zw_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
