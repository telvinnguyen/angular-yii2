<?php

namespace console\controllers;

use common\component\helpers\RatchetGlobalServer;
use common\component\helpers\RatchetMainServer;
use common\component\helpers\RatchetSignServer;
use Ratchet\App;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Yii;
use Ratchet\WebSocket\WsServer;

//important
use yii\console\Controller;

class RatchetController extends Controller
{

    public function actionIndex(){

        Yii::info(Yii::$app->params['serverName']);
        Yii::info(123);
    }

    /**
     * start sign server

    public function actionStartsignserver()
    {
        $server = IoServer::factory(
            new HttpServer(
                new WsServer(
                    new RatchetSignServer()
                )
            ),
            8083
        );
        $server->run();

        //Yii::info('Server was started successfully. Setup logging to get more details.'.PHP_EOL);
    }
     */

    public function actionStartglobalserver()
    {
        $server = IoServer::factory(
            new HttpServer(
                new WsServer(
                    new RatchetGlobalServer()
                )
            ),
            8082
        );

        $server->run();

        //Yii::info('Server was started successfully. Setup logging to get more details.'.PHP_EOL);
    }

    public function actionStartsocketserver()
    {
        $consoleServerName = Yii::$app->params['serverName'];

        $server = new App($consoleServerName, 8082);


        if($consoleServerName == 'localhost'){
            $server->route('/global', new RatchetGlobalServer());
            $server->route('/sign', new RatchetSignServer());
        }else{
            $class01 = 'dev';
            $server->route('/global' . $class01, new RatchetGlobalServer());
            $server->route('/sign' . $class01, new RatchetSignServer());

            $class02 = 'test';
            $server->route('/global' . $class02, new RatchetGlobalServer());
            $server->route('/sign' . $class02, new RatchetSignServer());


            $class03 = 'prod';
            $server->route('/global' . $class03, new RatchetGlobalServer());
            $server->route('/sign' . $class03, new RatchetSignServer());
        }

        $server->run();

        //Yii::info('Server was started successfully. Setup logging to get more details.'.PHP_EOL);
    }
}